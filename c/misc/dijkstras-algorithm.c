#include<stdio.h>

#define MAX 100
#define TEMP 0
#define PERM 1
#define infinity 9999
#define NIL -1

void findPath(int s, int v );
void Dijkstra( int s);
int min_temp( );
void create_graph();

int n;
int adj[MAX][MAX];
int predecessor[MAX];
int pathLength[MAX];
int status[MAX];

int main()
{
    int s,v;

    create_graph();

    printf("Enter Source Vertex : ");
    scanf("%d",&s);

    Dijkstra(s);

    while(1)
    {
        printf("Enter Destination Vertex(-1 to quit): ");
        scanf("%d",&v);
        if(v == -1)
            break;
        if(v < 0 || v >= n )
            printf("This Vertex Doesn't Exist\n");
        else if(v == s)
            printf("Source and Destination Vertices are Same\n");
        else if( pathLength[v] == infinity )
            printf("There is No Path from Source to Destination Vertex\n");
        else
            findPath(s,v);
    }

    return 0;
}

void Dijkstra( int s)
{
    int i,current;


    for(i=0; i<n; i++)
    {
        predecessor[i] =  NIL;
        pathLength[i] = infinity;
        status[i] = TEMP;
    }

    pathLength[s] = 0;

    while(1)
    {

        current = min_temp( );

        if( current == NIL )
            return;

        status[current] = PERM;

        for(i=0; i<n; i++)
        {

            if ( adj[current][i] !=0 && status[i] == TEMP )
                if( pathLength[current] + adj[current][i] < pathLength[i] )
                {
                    predecessor[i] = current;
                    pathLength[i] = pathLength[current] + adj[current][i];
                }
        }
    }
}

int min_temp( )
{
    int i;
    int min = infinity;
    int k = NIL;
    for(i=0;i<n;i++)
    {
        if(status[i] == TEMP && pathLength[i] < min)
        {
            min = pathLength[i];
            k = i;
        }
    }
    return k;
}


void findPath(int s, int v )
{
    int i,u;
    int path[MAX];
    int shortdist = 0;
    int count = 0;


    while( v != s )
    {
        count++;
        path[count] = v;
        u = predecessor[v];
        shortdist += adj[u][v];
        v = u;
    }
    count++;
    path[count]=s;

    printf("Shortest Path: ");
    for(i=count; i>=1; i--)
        printf("%d  ",path[i]);
    printf("\n Shortest Distance : %d\n", shortdist);
}

void create_graph()
{
    int i,max_edge,origination,destination, weight;

    printf("Enter number of vertices : ");
    scanf("%d",&n);
    max_edge = n*(n-1);

    for(i=1;i<=max_edge;i++)
    {
        printf("Enter Edge %d( -1 -1 to quit ) : ",i);
        scanf("%d %d",&origination,&destination);

        if( (origination == -1) && (destination == -1) )
            break;

        printf("Enter Weight of this Edge : ");
        scanf("%d",&weight);

        if( origination >= n || destination >= n || origination<0 || destination<0)
        {
            printf("Edge is Invalid\n");
            i--;
        }
        else
            adj[origination][destination] = weight;
    }
}
