//------------------------------------------------------------------------------
/*Interlude: Structures and unions*/

#include <sys/types.h>
#include <stdio.h>

void structures_unions(void) {
	struct foo {
		size_t size;
		char name[64];
		int answer_to_ultimate_question;
		unsigned shoe_size;
	} my_foo;
	//Direct structure access.
	my_foo.size = sizeof(struct foo);

	struct foo *foo_ptr = &my_foo;
	printf("Address of my_foo: %p\n", &my_foo);
	printf("Value of foo_ptr (= &my_foo): %p\n", foo_ptr);
	putchar('\n'); //blank line

	//This is the way we already know about:
	(*foo_ptr).size = sizeof(struct foo);
	//But there's a better way:
	foo_ptr->size = sizeof(struct foo);
	//Same thing, but shorter.

	//More levels of indirection make it uglier:
	struct foo **foo_ptr_ptr = &foo_ptr;
	(**foo_ptr_ptr).size = sizeof(struct foo);
	(*foo_ptr_ptr)->size = sizeof(struct foo);

	//The Pascal way (sigh):
	//foo_ptr_ptr^^.size := new_size;

	printf("my_foo.size: %zu\n", my_foo.size);
	putchar('\n'); //blank line
	printf("(*foo_ptr).size: %zu\n", (*foo_ptr).size);
	printf("foo_ptr->size: %zu\n", foo_ptr->size);
	putchar('\n'); //blank line

	printf("Address of foo_ptr: %p\n", &foo_ptr);
	printf("Value of foo_ptr_ptr (= &foo_ptr): %p\n", foo_ptr_ptr);
	putchar('\n'); //blank line

	printf("(**foo_ptr_ptr).size: %zu\n", (**foo_ptr_ptr).size);
	printf("(*foo_ptr_ptr)->size: %zu\n", (*foo_ptr_ptr)->size);
}
