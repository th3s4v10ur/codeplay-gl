void pointers_and_const(void) {
	//The placement of the const keyword matters:
	const int *ptr_a; //Here, the int (*ptr_a) is const. '*ptr_a = 42' is illegal.
	int const *ptr_b; //Same as ptr_a.
	int *const ptr_c; //Here, the pointer (ptr_b) is const. '*ptr_c = 42' is legal, but '++ptr_c' is not.
	//Note the similarity of ptr_b and ptr_c. The first is equivalent to ptr_a; only the last is a different type where the pointer, not the int, is constant. Be sure which one you mean.
}
