//------------------------------------------------------------------------------
/*Starting off*/

#include <stdio.h>

void starting_off(void) {
	int foo = 3;
	int *foo_ptr = &foo; //foo_ptr is a pointer to foo; its type is int * (pointer to int)
	printf("Value of foo: %d\n", foo);
	printf("Address of foo: %p\n", &foo);
	printf("Value of foo_ptr (= &foo): %p\n", foo_ptr);
}
