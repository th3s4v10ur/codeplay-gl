//------------------------------------------------------------------------------
/*Strings (and why there is no such thing)*/

#include <sys/types.h>
#include <stdio.h>
#include <string.h>

static size_t strlen1(const char *str);
static size_t strlen2(const char *str);

void strings(void) {
	char str[] = "I am the Walrus";
	/*This array (str) is 16 bytes (16 chars) long.
	 *The 'string' is 15 characters.
	 *The last element of the array (str[15]) is 0, called 'NUL' in ASCII.
	 *Thus it is called the 'NUL terminator', because it signals the end of the string.
	 *But, aside from string literal syntax ("foo"), this is just an idiom.
	 *The idiom is also supported by C's 'string library' (the contents of string.h).
	 */
	size_t len = strlen(str); //len = 15
	printf("str: %s\n", str);
	printf("Your library's strlen: %zu\n", len);
	printf("Demonstration strlen (strlen1) using pointer arithmetic: %zu\n", strlen1(str));
	printf("Demonstration strlen (strlen2) using indexing: %zu\n", strlen2(str));
}

static size_t strlen1(const char *str) { //Note the pointer syntax here
	//Remember, there is no 'string' here. There is only an array of characters, of which the last one is 0.
	size_t len = 0U;
	while(*(str++)) ++len; //When *str is 0, the loop exits.
	return len;
}
static size_t strlen2(const char *str) {
	size_t i;
	for(i = 0U; str[i]; ++i);
	//When the loop exits, i is the length of the string.
	return i;
}
