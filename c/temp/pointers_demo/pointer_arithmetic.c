//------------------------------------------------------------------------------
/*Pointer arithmetic (or: why 1 == 4)*/

#include <stdio.h>

void pointer_arithmetic(void) {
	//Resuming from 'Arrays'...
	int array[] = { 45, 67, 89 };

	int *array_ptr = array;
	printf(" first element: %i\n", *(array_ptr++));
	printf("second element: %i\n", *(array_ptr++));
	printf(" third element: %i\n", *array_ptr);
	/*output:
	 * first element: 45
	 *second element: 67
	 * third element: 89
	 */
	//Each ++ expression adds 1 to the variable. But in fact, the pointer moves more than 1 byte, because it is an int pointer (it moves sizeof(int) bytes). The amount added (1) is multiplied by the size (sizeof(int)) of the type of the pointer (int).

	//If it was a void pointer (void *), the multiplier would be 1.
}
