/*demonstration program for the pointer talk version 1.1 by Mac-arena the Bored Zo
 *this file: main program
 */

extern void starting_off(void);
extern void declarations(void);
extern void assignment(void);
extern void dereferencing(void);
extern void arrays(void);
extern void pointer_arithmetic(void);
extern void indexing(void);
extern void structures_unions(void);
extern void multiple_indirection(void);
extern void pointers_and_const(void);
extern void function_pointers(void);
extern void strings(void);

#include <stdio.h>

int main(void) {
	starting_off();
	putchar('\n'); //blank line

	declarations();

	assignment();
	dereferencing();
	puts("---");

	arrays();
	pointer_arithmetic();
	putchar('\n'); //blank line
	indexing();
	puts("---");

	structures_unions();
	puts("---");

	pointers_and_const();
	function_pointers();

	multiple_indirection();
	puts("---");

	strings();

	return 0;
}
