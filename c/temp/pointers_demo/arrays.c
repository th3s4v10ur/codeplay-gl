//------------------------------------------------------------------------------
/*Interlude: Arrays*/

#include <stdio.h>

void arrays(void) {
	//Here's an array:
	int array[] = { 45, 67, 89 };

	//Now, we try to refer to this array:
	printf("array: %p\n", array); //Prints some hexadecimal string like 0x12307734
	//But %p means 'pointer'. We used a pointer to the array's first element (&array[0]) here. The array, you see, decayed to a pointer.

	int *array_ptr = array;
	//Equivalently:
	array_ptr = &array; //This & is implicit in the first method
	array_ptr = &array[0];

	//This equivalence doesn't hold for pointer variables, only array variables. See what happens when we try it with a pointer variable:
	int **array_ptr_ptr;
	array_ptr_ptr = array_ptr; //This will warn, because we are assigning a pointer of a different type: An int * value (array_ptr) to an int ** variable (array_ptr).
	array_ptr_ptr = array; //Same here: array decays to int *, but this variable holds int **.
	array_ptr_ptr = &array_ptr; //Valid: &array_ptr is a pointer to a variable holding a pointer to an int.
	array_ptr_ptr = &array_ptr[0]; //This will warn, because &array_ptr[0], like array_ptr itself, is an int *.
}
