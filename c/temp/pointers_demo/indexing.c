//------------------------------------------------------------------------------
/*Indexing*/

#include <stdio.h>

void indexing(void) {
	//Resuming from 'Arrays'...
	int array[] = { 45, 67, 89 };
	//Resuming from 'Pointer arithmetic'...
	int *array_ptr;

	printf("array[0]: %i (first element)\n", array[0]); //Prints 45
	//Believe it or not, the index operator has nothing to do with arrays.
	//Remembering that the type of the pointer is int, array[0] == array + (0 * sizeof(int)).

	//A better example:
	array_ptr = &array[1];
	puts("array_ptr = &array[1]");
	printf("array_ptr[1]: %i (last element)\n", array_ptr[1]); //Prints 89

	putchar('\n'); //blank line

	//Stated diagrammatically:
	puts(" 0  1  2 <- indices into array");
	puts("    0  1 <- indices into array_ptr");
	puts("-- -- --");
	puts("45 67 89 <- values in the actual array");

	putchar('\n'); //blank line
}
