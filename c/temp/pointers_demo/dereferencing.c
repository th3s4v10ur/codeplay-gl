//------------------------------------------------------------------------------
/*Dereferencing*/

#include <stdio.h>

void dereferencing(void) {
	//Resuming from 'Starting off'...
	int foo = 3;
	int *foo_ptr = &foo;

	*foo_ptr = 42; //'foo_ptr = 42' changes the pointer; this changes the value *at* the pointer (note dereference operator, prefix *)
	printf("foo (after *foo_ptr = 42): %i\n", foo); //Prints 42

	int bar = *foo_ptr; //Dereference as read; bar now == 42
}
