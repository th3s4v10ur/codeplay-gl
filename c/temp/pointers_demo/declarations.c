//------------------------------------------------------------------------------
/*Declaration syntax*/

void declarations(void) {
	//This way is evil:
	int* ptr_a, ptr_b;
	//Type of ptr_b is int - NOT int *!
	//Only ptr_a is actually a pointer.

	//Better way:
	int *ptr_c, ptr_d; //ptr_d is more clearly not a pointer
	int ptr_e, *ptr_f; //Even clearer here

	//Ultimate clarity
	int  ptr_g;
	int *ptr_h;

	//Gratuitous parentheses are allowed around the asterisk and variable name in a declaration.
	int ((not_a_pointer)), (*ptr_i), (((*ptr_j)));
}
