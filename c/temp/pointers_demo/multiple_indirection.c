//------------------------------------------------------------------------------
/*Multiple indirection*/

#include <stdio.h>

void multiple_indirection(void) {
	int    a =  3;
	int   *b = &a;
	int  **c = &b;
	int ***d = &c;

	  *d ==   c; //Dereferencing an (int ***) once gets you an (int **) (3 - 1 = 2)

	//Note: C does not support chaining comparisons (e.g., a == b == c); each relational operator only compares two operands at a time. So, for one example, (a == b == c) is actually ((a == b) == c).
	//In the examples below, this is a comparison of an integer (the result of the first comparison) to a pointer (the other term of the second comparison). This is not what I'd intended, and both GCC and Clang rightly warn about it.
	//I've maintained the chained-comparison style below for clarity, but commented the lines out to show that such expressions are to be disfavored.

	//Dereferencing an (int ***) twice, or an (int **) once, gets you an (int *) (3 - 2 = 1; 2 - 1 = 1):
	// **d ==  *c ==  b; 

	//Dereferencing an (int ***) thrice, or an (int **) twice, or an (int *) once, gets you an int (3 - 3 = 0; 2 - 2 = 0; 1 - 1 = 0):
	//***d == **c == *b == a == 3;

	//Thus, the & operator can be thought of as adding asterisks (or 'increasing pointer level'), and the * and [] operators as removing asterisks ('decreasing pointer level').

	printf("Address of a: %p\n", &a);
	printf("Value of b (= &a): %p\n", b);
	putchar('\n'); //blank line
	printf("Address of b: %p\n", &b);
	printf("Value of  c (= &b): %p\n",  c);
	printf("Value of *c (= &a): %p\n", *c);
	putchar('\n'); //blank line
	printf("Address of c: %p\n", &c);
	printf("Value of   d (= &c): %p\n",  d);
	printf("Value of  *d (= &b): %p\n",  *d);
	printf("Value of **d (= &a): %p\n", **d);
	putchar('\n'); //blank line
	printf("Value of a = *b = **c = ***d: %i\n", a);
}
