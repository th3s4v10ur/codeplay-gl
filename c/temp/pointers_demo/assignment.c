//------------------------------------------------------------------------------
/*Assignment and pointers*/

#include <stdio.h>

void assignment(void) {
	//Resuming from 'Starting off'...
	int foo = 3;
	int *foo_ptr = &foo;

	//This line elicits a warning in gcc:
	foo_ptr = 42;
	//Remember that foo_ptr is (was) a pointer to foo.
	//You might have expected that this line would set foo to 42 through the pointer, but it doesn't.
	//Instead, we changed the pointer that's in foo_ptr; foo is unchanged. What changed is that foo_ptr no longer points to foo; it points into random wilderness.
	printf("foo (after  foo_ptr = 42): %i (note that foo is unchanged)\n", foo); //Prints 3 (see?)
}
