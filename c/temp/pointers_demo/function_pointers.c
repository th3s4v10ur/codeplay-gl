#include <string.h>

static char *(*get_strcpy_ptr(void))(char *dst, const char *src);

void function_pointers(void) {
	enum { str_length = 18U }; //Remember the NUL terminator!
	char src[str_length] = "This is a string.", dst[str_length];

	//Functions decay to pointers, the same as arrays do.
	//When you call a function, you really call an address.
	strcpy(dst, src); //The function call operator in action. Notice the function pointer on the left side.

	//Function pointers are declared similarly to variables, but with parentheses in order to separate the * indicating a pointer from the zero or more * in the return type.

	//char *strcpy(char *dst, const char *src); //An ordinary function declaration, for reference
	char *(*strcpy_ptr)(char *dst, const char *src); //Pointer (indicated by inner *) to strcpy-like function

	strcpy_ptr =  strcpy; //Decay: implicit &
	strcpy_ptr = &strcpy; //This works the same
	//strcpy_ptr = &strcpy[0]; //But not this

	//Even with the parameter names removed, this is still the same type as strcpy_ptr.
	char *(*strcpy_ptr_noparams)(char *, const char *) = strcpy_ptr;

	//Type of strcpy_ptr: char *(*)(char *dst, const char *src)
	//You use this in a cast:
	void *my_strcpy = strcpy; //Imagine that you got this somewhere else (e.g. that my_strcpy was a function parameter)
	strcpy_ptr = (char *(*)(char *dst, const char *src))my_strcpy;

	//As you might expect, a pointer to a pointer to a function has two asterisks inside of the parentheses:
	char *(**strcpy_ptr_ptr)(char *, const char *) = &strcpy_ptr;

	//And we can have an array of function-pointers:
	char *(*strcpies[3])(char *, const char *) = { strcpy, strcpy, strcpy };
	char *(*strcpies2[])(char *, const char *) = { strcpy, strcpy, strcpy }; //Array size is optional, same as ever

	strcpies[0](dst, src);

	//Here's a pathological declaration, taken from the C99 standard.
	/*'[This declaration] declares a function f with no parameters returning an int,
	 * a function fip with no parameter specification returning a pointer to an int,
	 * and a pointer pfi to a function with no parameter specification returning an int.'
	 *(6.7.5.3[16])
	 */
	int f(void), *fip(), (*pfi)();
	/*Equivalent to:
	int f(void);
	int *fip();   //Function returning int pointer
	int (*pfi)(); //Pointer to function returning int
	 */

	//A function pointer can even be the return value of a function.
	//char *(*get_strcpy_ptr(void))(char *dst, const char *src);
	strcpy_ptr = get_strcpy_ptr();

	//Because function pointer syntax is so mind-bending, most developers use typedefs to abstract them:
	typedef char *(*strcpy_funcptr)(char *, const char *);

	strcpy_funcptr strcpy_ptr2 = strcpy;
	//strcpy_funcptr get_strcpy_ptr(void);		
}

static char *(*get_strcpy_ptr(void))(char *dst, const char *src) {
	return strcpy;
}
