#!/usr/bin/env bash

zone="tld1M"

mkdir -p zones
ln -s ../../${zone}_dnssec/zones/${zone}.zone zones/
../../tools/upd_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_querydb.sh ./zones/${zone}.zone ${zone} 100
../../tools/gen_trace.py -d 100
../../tools/pack_dataset.sh
