#!/usr/bin/env bash

zone="tld1M"

mkdir -p zones
BASEDIR=./zones ../../tools/sign_zone.sh ${zone} ../${zone}/zones/${zone}.zone ./zones/${zone}.zone nsec3
../../tools/upd_zonelist.sh
../../tools/gen_configs.sh
ln -s ../${zone}_nsec/querydb
../../tools/gen_trace.py -d 100
../../tools/pack_dataset.sh
