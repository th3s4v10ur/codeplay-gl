#!/usr/bin/env bash

zone_count=1000000

mkdir -p zones
sed -e "s%@DOTZONE@%%g" -e "s%@ZONE@%@%g" ../../tools/zone.tpl > ./zones/template.zone
../../tools/gen_zonelist.py ${zone_count}
../../tools/gen_configs.sh zones/template.zone
../../tools/gen_querydb.sh stub.zone stub 0
../../tools/gen_trace.py
../../tools/pack_dataset.sh
