#!/usr/bin/env bash

zone="."

mkdir -p zones
dig AXFR ${zone} @b.root-servers.net. +onesoa +noclass > ./zones/root.zone
../../tools/upd_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_querydb.sh ./zones/root.zone ${zone} 60 10000
../../tools/gen_trace.py -d 70
../../tools/pack_dataset.sh
