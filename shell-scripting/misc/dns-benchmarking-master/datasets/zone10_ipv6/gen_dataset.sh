#!/usr/bin/env bash

zone="zone10"

ln -s ../${zone}/zones
../../tools/upd_zonelist.sh
../../tools/gen_configs.sh
ln -s ../${zone}/querydb
../../tools/gen_trace.py -t ::1
../../tools/pack_dataset.sh
