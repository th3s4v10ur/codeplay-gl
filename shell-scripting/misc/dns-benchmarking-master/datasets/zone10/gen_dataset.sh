#!/usr/bin/env bash

zone="zone10"

mkdir -p zones
sed -e "s%@DOTZONE@%.${zone}.%g" -e "s%@ZONE@%${zone}.%g" ../../tools/zone.tpl > ./zones/${zone}.zone
../../tools/upd_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_querydb.sh ./zones/${zone}.zone ${zone} 0 10000
../../tools/gen_trace.py
../../tools/pack_dataset.sh
