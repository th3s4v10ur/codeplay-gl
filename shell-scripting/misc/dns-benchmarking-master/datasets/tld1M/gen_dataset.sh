#!/usr/bin/env bash

zone="tld1M"
rr_count="1000000"

../../tools/gen_zone.py ${zone} ${rr_count}
../../tools/upd_zonelist.sh
../../tools/gen_configs.sh
../../tools/gen_querydb.sh ./zones/${zone}.zone ${zone} 0
../../tools/gen_trace.py
../../tools/pack_dataset.sh
