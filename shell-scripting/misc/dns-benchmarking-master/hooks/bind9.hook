#!/usr/bin/env bash
source ${HOOKDIR}/wait_start.hook

#
# BIND9 benchmark hook
#

export REPO="https://gitlab.isc.org/isc-projects/bind9.git"
export CONFIGURE_PARAMS="--with-readline=no --enable-static --disable-shared --with-gssapi=no --with-libxml2=no"
export SRC_DIR="${BASEDIR}/bind9.src"
export BIN="${SRC_DIR}/bin/named/named"

function ns_info() {
	echo "BIND $(${BIN} -V|head -n1|awk '{print $2}')"
}

function ns_pid() {
	echo $PID
}

function ns_init() {
	if [ -n "${bind9_bin}" ]; then
		BIN=${bind9_bin}

		echo "using '$(ns_info)'"
	else
		# Check out specific branch if set
		version="master"
		[ -n "${1}" ] && version=${1}
		[ -d ${SRC_DIR} ] && rm -r ${SRC_DIR} &>>$LOG
		if ! git clone --depth 1 $REPO -b ${version} ${SRC_DIR} &>>$LOG; then
			echo "error: couldn't fetch '${version}' from '${REPO}'"
			return 1
		fi
		echo "checked out '${version}'"

		# Build sources
		pushd ${SRC_DIR} > /dev/null
		autoreconf -if &>>$LOG
		CFLAGS=$CFLAGS ./configure ${CONFIGURE_PARAMS} &>>$LOG
		make -j${CPUS} &>>$LOG
		popd > /dev/null
		if [ ! -x ${BIN} ]; then
			echo "error: couldn't make an executable"
			return 1
		fi

		echo "built '$(ns_info)'"
	fi
}

function ns_prep() {
	return 2 # Don't measure
}

function ns_start() {
	${BIN} -c ${1} -f -n ${CPUS} &>server.log &
	PID=$!

	wait_start "all zones loaded"
	return 0
}

function ns_stop() {
	wait_stop
}
