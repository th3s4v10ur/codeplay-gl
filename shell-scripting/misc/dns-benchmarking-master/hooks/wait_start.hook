#!/usr/bin/env bash

export PID=""
export DIG="$(which dig)"
export DIG_ADDR="127.0.0.1"
if [ -f ${BASEDIR}/queries-ipv6.pcap ]; then
	DIG_ADDR="::1"
fi

wait_for_zones()
{
	# Wait for 1% of last zones
	# Probabilistic, but digging 1M+ times is too slow and affects the results
	zonelist="$1"
	zonecount=$(cat ${zonelist}|wc -l)
	portion=$(( ${zonecount} / 100 + 1 ))
	while read zone;
	do
		ret=1
		while [ $ret -ne 0 ]; do
			${DIG} @${DIG_ADDR} -p ${PORT} +notcp +retry=0 +time=1 ${zone} SOA|grep -q NOERROR
			ret=$?
			if [ $ret -ne 0 ]; then
				sleep 1
			fi
		done
	done < <(tail -n ${portion} ${zonelist})
	# Wait for low CPU load to make sure we're done loading
	cpu_sec=0
	while [ ${portion} -lt ${zonecount} ] && [ ${cpu_sec} -lt 15 ]; do
		cpu_pct=$(top -p $PID -b -n 1|tail -n 1|awk '{printf "%.0f", $9}')
		if (( ${cpu_pct} < 2 )); then
			(( cpu_sec += 1 ))
		else
			cpu_sec=0
		fi
		sleep 1
	done
}

wait_start()
{
	# Wait until started
	sleep 1
	while kill -0 ${PID}; do
		if grep -E -i -q "${1}" ${BASEDIR}/server.log &>/dev/null; then
			return 0
		fi
		sleep 1
	done
}

wait_stop()
{
	echo "wait_stop for PID $PID"
	kill $PID &>/dev/null
	wait $PID &>/dev/null
	PID=""
	sleep 10
}
