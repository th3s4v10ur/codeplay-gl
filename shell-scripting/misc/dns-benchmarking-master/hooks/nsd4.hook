#!/usr/bin/env bash
source ${HOOKDIR}/wait_start.hook

#
# NSD4 benchmark hook
#

export REPO="https://github.com/NLnetLabs/nsd.git"
export CONFIGURE_PARAMS="--enable-root-server --disable-flto"
export SRC_DIR="${BASEDIR}/nsd4.src"
export BIN="${SRC_DIR}/nsd"

function ns_info() {
	CPUPROFILE=
	echo "NSD $(${BIN} -v 2>&1|head -n 1|awk '{print $3}')"
}

function ns_pid() {
	echo $PID
}

function ns_init() {
	if [ -n "${nsd4_bin}" ]; then
		BIN=${nsd4_bin}

		echo "using '$(ns_info)'"
	else
		# Check out specific branch if set
		version="master"
		[ -n "${1}" ] && version=${1}

		if ! git clone --depth=1 $REPO -b $version $SRC_DIR &>> $LOG; then
			echo "error: couldn't fetch '${version}' from '${REPO}'"
			return 1
		fi
		echo "checked out '${version}'"

		# Build sources
		pushd ${SRC_DIR} > /dev/null
		autoreconf -if &>>$LOG
		if [ -n "${nsd4_gperf}" ] ; then
			export LIBS="-ltcmalloc_and_profiler"
		fi
		CFLAGS=$CFLAGS ./configure ${CONFIGURE_PARAMS} &>>$LOG
		make &>>$LOG
		popd > /dev/null
		if [ ! -x ${BIN} ]; then
			echo "error: couldn't make an executable"
			return 1
		fi
		
		echo "built '$(ns_info)'"
	fi
}

function ns_prep() {
	return 2 # Don't measure 
}

function ns_start() {
	export CPUPROFILE=${BASEDIR}/nsd4_cpuprof
	${BIN} -c ${1} -d &>server.log &
	PID=$!

        wait_start "nsd started"
        return 0
}

function ns_stop() {
	wait_stop
        if [ -n "${nsd4_gperf}" ] ; then
                google-pprof ${BIN} ${BASEDIR}/nsd4_cpuprof > ${BASEDIR}/nsd4_cpuprof.prof 2>/dev/null
                google-pprof --pdf ${BIN} ${BASEDIR}/nsd4_cpuprof > ${BASEDIR}/nsd4_cpuprof.pdf 2>/dev/null
                if [ -d ${BASEDIR}/results ]; then
                        cp ${BASEDIR}/nsd4_cpuprof* ${BASEDIR}/results/
                fi
        fi
}
