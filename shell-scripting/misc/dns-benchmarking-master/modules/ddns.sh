#!/usr/bin/env bash

NSUPDATE_TIMEOUT=900

function module_init()
{
	:
}

function module_exec() {
        # Start nameserver
        ns_prep ${NAMECONF}
        ns_start ${NAMECONF}
        if [ $? -gt 0 ]; then
                echo "error: failed to start '${1}'"
                return 1
        fi

	# Initialize data output
	prefixlist=( "small-" "large-" )
	zonelist=( $(cd zones; ls *.zone) )
        output="${BASEDIR}/results/ddns"
	if [ ! -f ${output} ]; then
		echo -n "# " >> ${output}
		for datafile in ${updatelist[@]}; do
			echo -n "${datafile%%.update} " >> ${output}
		done
		echo >> ${output}
	fi

	shopt -s nullglob
        update="update.txt"
	results=""
	for zone in ${zonelist[@]}; do

		# Perform all updates
		zone_results=""
		zone="${zone%%.zone}"
		for data_prefix in ${prefixlist[@]}; do
			sum=0
			printf "zone: %12s, updates: %6s, times: " "$zone" "$data_prefix"
			for datafile in ${data_prefix}update.*; do
				# Preprocess the update file
				echo "server 127.0.0.1 ${PORT}" > ${update}
				echo "zone ${zone}" >> ${update}
				sed -e "s%@ZONE@%${zone}%g" ${datafile} >> ${update}
				echo "send" >> ${update}

				# Send the update
				/usr/bin/time -f "%e" nsupdate -t ${NSUPDATE_TIMEOUT} -u ${NSUPDATE_TIMEOUT} -r 0 ${update} >> ${LOG} 2>time.log
				sum=`echo "$sum $(cat time.log)" | awk '{printf "%.2f", $1 + $2}'`
				head -n 1 time.log | tr "\n" " "
				rm time.log
				sleep 2
			done
			average=`echo "$sum 10" | awk '{printf "%.2f", $1 / $2}'`
			echo "average: $average"
			zone_results="${zone_results},${average}"
		done
		results="${results}\t${zone_results:1}"
	done

        # Preprocess the update file
	#zone="large-dnssec"
        #echo "server 127.0.0.1 ${PORT}" > ${update}
        #sed -e "s%@ZONE@%${zone}%g" 1000.update >> ${update}

	#for i in 1 .. 10; do
        	# Send the update
        #        nsupdate -t ${NSUPDATE_TIMEOUT} -u ${NSUPDATE_TIMEOUT} -r 0 ${update} >> ${LOG}
        #done

	# Gather data
        echo -e "\"$(ns_info)\"${results}" >> ${output}

        # Stop nameserver
        ns_stop ${NAMECONF}
}
