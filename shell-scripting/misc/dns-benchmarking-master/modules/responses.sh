#!/usr/bin/env bash

export IP_VERSION=4
export PKT_OVERHEAD=42
export IPTABLES="iptables"
export TRACE="queries-ipv4.pcap"

# Discover network setup
function net_discover()
{
	UNIQ="guniq"
	if [ `uname` = "Linux" ]; then UNIQ="uniq"; fi

	ip4=""; ip6=""; mac=""
	addr=""; link=""
	if [ "`ssh $1 uname`" = "Linux" ]; then
		addr="$(ssh $1 ip -d -o addr|grep ": ${2}"|grep -o -E "(inet|inet6) ([^ ]+)"|grep -v Link|$UNIQ -w 5)"
		link="$(ssh $1 ip -d -o link|grep ": ${2}"|grep -o -E "(ether) ([^ ]+)"|$UNIQ -w 5)"
	else
		addr="$(ssh $1 ifconfig ${2}|grep -o -E "(inet|inet6) ([^ ]+)"|$UNIQ -w 5)"
		link="$(ssh $1 ifconfig ${2}|grep -o -E "ether ([^ ]+)|$UNIQ -w 5")"
	fi

	while read -r l; do
		l=( $(echo $l) )
		case ${l[0]} in
		inet)  ip4=( ${net_v4[@]} $(echo ${l[1]}|cut -d\/ -f1) ) ;;
		inet6) ip6=( ${net_v6[@]} $(echo ${l[1]}|cut -d\/ -f1) ) ;;
		esac
	done <<< "$addr"
	while read -r l; do
		l=( $(echo $l) )
		case ${l[0]} in
		ether) mac=( ${net_mac[@]} ${l[1]} ) ;;
		esac
	done <<< "$link"

	# Decide used address
	if [ ${IP_VERSION} -eq 4 ]; then
		echo "${mac} ${ip4}"
	else
		echo "${mac} ${ip6}"
	fi
}

# Rewrite pcap file according to current network settings
function trace_rewrite()
{
	player=( $(echo ${players[$2]}|tr ':' ' ') )
	net_dst=( $(net_discover ${target} ${iface}) )
	net_src=( $(net_discover ${player[0]} ${player[1]}) )

	tcprewrite --dlt=enet --enet-dmac=${net_dst[0]} --enet-smac=${net_src[0]} -i ${1} -o tmp.1 &>>${LOG}
	tcpprep --auto=client --cachefile=tmp.cache --pcap=tmp.1 &>>${LOG}

	if [ -z "${net_dst[1]}" ]; then
		echo "FATAL ERROR: net_dst[1] is empty. Is the dst interface UP?"
		return
	fi

	if [ -z "${net_src[1]}" ]; then
		echo "FATAL ERROR: net_src[1] is empty. Is the src interface UP?"
		return
	fi

	addr_map="${net_src[1]}:${net_dst[1]}"
	if [ ${IP_VERSION} -eq 6 ]; then
		addr_map="[${net_dst[1]}]:[${net_src[1]}]"
	fi

	tcprewrite -C --portmap=53:${PORT} --endpoints=${addr_map} --cachefile=tmp.cache -i tmp.1 -o ${1}.live.${2} &>>${LOG}
	rm tmp.1 tmp.cache

	scp ${BASEDIR}/${1}.live.${2} ${player[0]}:/tmp/ &>>${LOG}

	# Add static ARP or NDP records to server and players
	ssh "${player[0]}" sudo ip -${IP_VERSION} neigh replace "${net_dst[1]}" lladdr "${net_dst[0]}" dev "${player[1]}"
	sudo ip -${IP_VERSION} neigh replace "${net_src[1]}" lladdr "${net_src[0]}" dev "${iface}"
}

function stats() {
	# Query: bytes, pkts, dropped, Reply: bytes, pkts, dropped
        echo $(cat /proc/net/dev | grep ${iface} | awk '{print $2,$3,$5,$10,$11,$13}')
}

# Replay pcap file to nameserver and evaluate number of answered responses
function trace_replay()
{
	# Prepare tcpreplay flags
	speed="-t"
	[ "${1}" -gt 0 ] && speed="-p $(( ${1} / ${#players[@]} ))"
	netmap="--netmap --nm-delay ${netmap_delay:-5}"
	[ "${netmap_delay}" -eq 0 ] && netmap=""
	duration="${replay_duration:-30}"
	rflags="-K -q -T gtod -l 10000000 ${netmap} --duration=${duration} ${speed}"

	# Stats before
	s0=( $(stats) )

	# Replay the trace files
	jobs=()
	i=0; while [ ${i} -lt ${#players[@]} ]; do
		player=( $(echo ${players[$i]}|tr ':' ' ') )
		cmd="sudo tcpreplay -i ${player[1]} ${rflags} /tmp/${2}.live.${i}"
		(ssh "${player[0]}" "${cmd}" 2>&1 | grep "Actual:" >> tcpreplay.out) &
		jobs=( ${jobs[@]} $! )
		(( i += 1 ))
	done
	i=0; while [ ${i} -lt ${#players[@]} ]; do
		wait ${jobs[$i]}
		(( i += 1 ))
	done

	# Stats after
	s1=( $(stats) )

	query_size=$(bc <<< "${s1[0]} - ${s0[0]}")
	query_pkts=$(bc <<< "${s1[1]} - ${s0[1]}")
	query_drop=$(bc <<< "${s1[2]} - ${s0[2]}")

	reply_size=$(bc <<< "${s1[3]} - ${s0[3]}")
	reply_pkts=$(bc <<< "${s1[4]} - ${s0[4]}")
	reply_drop=$(bc <<< "${s1[5]} - ${s0[5]}")

	# Calculate results
	answered=$(bc <<< "scale=2; ${reply_pkts}*100.0/${query_pkts}")
	query_rate=$(bc <<< "scale=0; ${query_pkts}/${duration}")
	reply_rate=$(bc <<< "scale=0; ${reply_pkts}/${duration}")
	query_avglen=$(bc <<< "scale=0; ${query_size}/${query_pkts} - $PKT_OVERHEAD")
	reply_avglen=$(bc <<< "scale=0; ${reply_size}/${reply_pkts} - $PKT_OVERHEAD")

	echo ${answered} ${query_rate} ${query_avglen} ${reply_rate} ${reply_avglen}
}

function module_init()
{
	# Check for IPv6 measurement
	if [ ${ip} ] && [ ${ip} = "6" ]; then
		IP_VERSION=6
		PKT_OVERHEAD=62
		IPTABLES="ip6tables"
		TRACE="queries-ipv6.pcap"
	fi

	if [ ! -f ${TRACE} ]; then
		echo "'${TRACE}' not found, skipping"
		return 1
	fi

	# Populate address list
	addr=( $(net_discover ${target} ${iface}) )
	ADDRLIST=( 127.0.0.1 ${addr[1]} )
}

function module_exec()
{
	output="${BASEDIR}/results/${1}.data"

	# Drop filesystem and page caches
	sudo sync
	echo 3|sudo tee /proc/sys/vm/drop_caches &>/dev/null

	# Start nameserver
	ns_prep ${NAMECONF}
	ns_start ${NAMECONF}
	if [ $? -gt 0 ]; then
		echo "error: failed to start '${1}'"
		return 1
	fi

	# Perform measurement of answered queries
	do_rates=(0)
	if [ "${replay_rates}" ]; then
		if [ "${replay_step}" ] && [ ${#replay_rates[@]} -ge 2 ]; then
			do_rates=( $(${SEQ} ${replay_rates[0]} ${replay_step} ${replay_rates[1]}) )
		else
			do_rates=${replay_rates[@]}
		fi
	fi

	# Prepare pcap files for replaying
	i=0; while [ ${i} -lt ${#players[@]} ]; do
		trace_rewrite ${TRACE} ${i}
		if [ $? -ne 0 ]; then
			echo "error: failed to prepare tracefile for ${players[$i]}"
			return 1
		fi
		(( i += 1 ))
	done

	# Replay the pcap files
	for pps in ${do_rates[@]}; do
		result=( $(trace_replay ${pps} ${TRACE}) )

		[ ${pps} -eq 0 ] && pps="maximum"
		echo "${1} ${result[0]} % answered for ${result[1]} q/s, ${result[2]} B avg (${result[3]} a/s, ${result[4]} B avg)"
		echo -e "${result[1]}\t${result[0]}\t${result[2]}" >> ${output}.tmp
	done

	# Stop nameserver
	ns_stop ${NAMECONF}

	# Save results
	echo -e "# $(ns_info)" > ${output}
	cat ${output}.tmp|sort -n >> ${output}
	rm ${output}.tmp
}
