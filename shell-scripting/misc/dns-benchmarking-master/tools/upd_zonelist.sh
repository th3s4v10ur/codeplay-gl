#!/bin/bash

ZONELIST=zonelist

# Generate zone list
echo -n > ${ZONELIST}
find -L ${ZONEDIR} -name "*.zone"|while read z; do
	zone=${z##*/}
	zone=${zone%%.zone}
	if [ ${zone} == "root" ]; then
		zone="."
	else
		zone="${zone}."
	fi
	echo ${zone} >> ${ZONELIST}
done
