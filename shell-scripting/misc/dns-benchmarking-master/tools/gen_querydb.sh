#!/usr/bin/env bash

# In almost all cases, other RCODEs make significantly less than 1% of the query volume
# Therefore we're going to omit them
if [ $# -lt 3 ]; then
	echo "Usage: $0 <zonefile> <zone> <nxdomain percentage> [count]"
	exit 1
fi

zonefile=${1}
zone=${2}
[ "${zone: -1}" != "." ] && zone="${zone}."
nxdomain_pct=${3}
need_cnt=100000
if [ $# -ge 4 ]; then
	need_cnt=${4}
fi

# Check if the zone file contains IN class column
line=$(grep -m 1 "SOA" ${zonefile} | awk '{print $3}')
awk_expr="{print \$1,\$3}"
[ "${line}" == "IN" ] && awk_expr="{print \$1,\$4}"

# Extract suitable record candidates from the zone
tmpf=$(mktemp)
awk "${awk_expr}" ${zonefile}|grep -m 150000 -E " (A|NS|SOA|PTR|MX|AAAA)$"|sort -u -R >> ${tmpf}

# Create NOERROR queries
noerror_pct=$(( 100 - ${nxdomain_pct} ))
noerror_max=$(( (${need_cnt} * ${noerror_pct}) / 100 ))
noerror_cnt=0
echo -n > querydb
while [ ${noerror_cnt} -lt ${noerror_max} ]; do
	remaining_cnt=$(( ${noerror_max} - ${noerror_cnt} ))
	head ${tmpf} -n ${remaining_cnt} >> querydb
	noerror_cnt=$(cat querydb|wc -l)
	echo "... ${noerror_cnt} NOERROR queries built"
done
echo "added ${noerror_cnt} NOERROR queries (${noerror_pct}%)"

# Fill in NXDOMAIN queries
nxdomain_cnt=$(( ${need_cnt} - ${noerror_cnt} ))
i=0;
while [ ${i} -lt ${nxdomain_cnt} ]; do
	while [ ${i} -lt ${nxdomain_cnt} ] && read line; do
		echo "${line}" | sed -e "s/\(\.${zone}\ \)/_${i}nxd\1/" -e "s/\(^${zone}\ \)/${i}nxd.\1/" >> querydb
		(( i += 1 ))
	done < <(cat ${tmpf})
	echo "... ${i} NXDOMAIN queries built"
done
echo "added ${i} NXDOMAIN queries (${nxdomain_pct}%)"

# Shuffle everything
echo "shuffling"
shuf querydb > ${tmpf}
mv ${tmpf} querydb
