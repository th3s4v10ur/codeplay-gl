#!/bin/bash

if [ -z $1 ]; then
	echo "Need a number of RRs to change, 50 <= x <= 1000."
	exit 1
fi

re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then
	echo "Invalid input, use a number between 50 and 1000."
	exit 1
fi

if [ -z $2 ]; then
	echo "Need a zonefile to read the RRs from."
	exit 1
fi

if [ ! -f $2 ]; then
    echo "File not found."
    exit 1
fi

rr_count=1000

if (( $1 > 1000 )); then
	echo "Using 1000 as the maximum."
elif (( $1 < 50 )); then
	echo "Using 50 as the minimum."
	rr_count=50
else
	rr_count="$1"
fi


echo "Generating $rr_count removals and $rr_count additions."

update_n=0
for range_start in {10..9010..1000}; do
	let range_end="$range_start + $rr_count"
	let past_range_end="$range_start + $rr_count + 1"
	sed -n "${range_start},${range_end}p;${past_range_end}q" $2 > /tmp/nodes.txt
	let update_n="$update_n + 1"
	update_file="${3}update.${update_n}"
	rm $update_file

	echo "Generating $update_file."
	while IFS='' read -r line || [[ -n "$line" ]]; do
		node=`echo $line | cut --fields=1 --delimiter="." -`
		echo "update delete ${node}.@ZONE@." >> $update_file
		echo "update add ${node}.@ZONE@. 86400 IN A 127.0.0.1" >> $update_file
	done < /tmp/nodes.txt
done

