#!/bin/bash

ZONEDIR=zones
ZONEFILE=""

# Optional common zone file path
if [ $# -eq 1 ]; then
	ZONEFILE=$1
fi

shopt -s nullglob
for zt in ../../tools/configs-zone/*.zone.template
do
	output=`basename $zt`
	source $zt > ${output%%.zone.template}-zones.conf
done

cp ../../tools/configs/*.conf .
