#!/usr/bin/python

import tempfile
import sys
import shutil
import threading
import Queue
import os
import re
import subprocess

THREADS=7


def sign(infile, outfile, tmpdir):

    zonename=re.sub(r'\.zone$', '', os.path.basename(infile))

    print "sign: z=%s in=%s out=%s t=%s"%(zonename, infile, outfile, tmpdir)

    # Generate keys
    nf = open('/dev/null', 'w')
    ps = [ 'dnssec-keygen', '-r', '/dev/urandom', '-3',
        '-n', 'ZONE', '-K',  tmpdir]
    zsk = subprocess.check_output(ps + [zonename], stderr=nf).strip()
    ksk = subprocess.check_output(ps + ["-f", "KSK"] + [zonename],
                    stderr=nf).strip()
    zskf = os.path.join(tmpdir, '%s.key'%zsk)
    zskpf = os.path.join(tmpdir, '%s.private'%zsk)
    kskf = os.path.join(tmpdir, '%s.key'%ksk)
    kskpf = os.path.join(tmpdir, '%s.private'%ksk)
    print "Generated zsk=%s ksk=%s"%(zsk, ksk)

    # Append keys to the zone
    tzfn = os.path.join(tmpdir, '%s.zone'%zonename)
    with open(tzfn, 'w') as tzo:
        with open(infile, 'r') as zf:
            tzo.write(zf.read())
            tzo.write('\n')
        with open(zskf, 'r') as kf:
            tzo.write(kf.read())
            tzo.write('\n')
        with open(kskf, 'r') as kf:
            tzo.write(kf.read())
            tzo.write('\n')

    # Sign the zone
    zonename = (zonename if zonename[-1]=='.' else '%s.'%zonename)
    sz = subprocess.check_output(["dnssec-signzone", "-u", "-H", "0",
                "-3", "-", "-o", zonename, "-k",
                kskf, "-d", tmpdir,
                tzfn, zskf], stderr=nf).strip()
    nf.close()
    print "Return from signzone: %s"%sz
    shutil.move(sz, outfile)
    for f in (tzfn, zskf, zskpf, kskf, kskpf, os.path.join(tmpdir, 'dsset-%s'%zonename)):
        os.unlink(f)


def sign_thread(q, outdir, tmpdir):
    while True:
        inf = None
        try:
            inf = q.get_nowait()
        except:
            return

        of = os.path.join(outdir, os.path.basename(inf))
        sign(inf, of, tmpdir)
        q.task_done()

def print_usage(name):
    print "%s <input dir> <output dir>"%name

def main():
    if len(sys.argv) != 3:
        print_usage(sys.argv[0])
        sys.exit(1)

    indir = os.path.abspath(sys.argv[1])
    outdir = os.path.abspath(sys.argv[2])

    if not os.path.isdir(indir):
        raise Exception("Input dir does not exist: "+indir)
    if not os.path.isdir(outdir):
        raise Exception("Output dir does not exist: "+outdir)

    tmpdir=tempfile.mkdtemp()
    print "Created tmpdir: %s"%tmpdir

    q=Queue.Queue()

    i=0
    for root, dirs, files in os.walk(indir):
        for filename in files:
            f=os.path.join(root, filename)
            q.put(f)

    if THREADS < 2:
        sign_thread(q, outdir, tmpdir)
    else:
        threads=[]
        for ti in range(0,THREADS):
            t=threading.Thread(target=sign_thread,
                    args=(q, outdir, tmpdir))
            threads.append(t)
            t.start()
        for t in threads:
            t.join()

    print "Removing tmpdir: %s"%tmpdir
    shutil.rmtree(tmpdir)

if __name__ == '__main__':
    main()

