/******************************************************************************
project: "Stationery" extension for Thunderbird
filename: stationery.jsm
author: Łukasz 'Arivald' Płomiński <arivald@interia.pl>

description: Main file of Stationery.

example:
Components.utils.import('resource://stationery/content/stationery.jsm');
  
******************************************************************************/
'use strict';

Components.utils.import('resource://gre/modules/iteratorUtils.jsm');
Components.utils.import('resource://gre/modules/mailServices.js');
Components.utils.import('resource://gre/modules/Services.jsm');
Components.utils.import('resource://gre/modules/AddonManager.jsm');
Components.utils.import('resource://gre/modules/XPCOMUtils.jsm');

var EXPORTED_SYMBOLS = ['Stationery', 'stationeryExtensionGUID'];

const stationeryExtensionGUID = '{d0e38b3a-0d60-46bf-bf01-83d4ba041015}';

var Stationery = {};

/*
let s = window.performance.now();    
Stationery.alert(''+window.performance.now() - s);
*/

//for modules 
Stationery.modules = {};
  
//detect this extension version and other data
Stationery.addonVersion = false;

AddonManager.getAddonByID(stationeryExtensionGUID, function(addon) { 
  Stationery.addonVersion = addon.version;
} );

//todo: try use log4moz
//Components.utils.import("resource://gre/modules/gloda/log4moz.js"); 

//general exception handler.
Stationery.handleException = function(e, alertMessage) {
  try {
    let message;
    if (typeof e == 'string') message = e; else message = e.message;
      
    let sourceName = '';
    let sourceLine = '';
    let lineNumber = 0;
    let columnNumber = 0;
    
    if(e.QueryInterface) {
      if(e.QueryInterface(Components.interfaces.nsIException)) {
        sourceName = e.filename;
        if(e.location) sourceLine = e.location.sourceLine;
        lineNumber = e.lineNumber;
        columnNumber = e.columnNumber;  
      }
    } else {
      if(e.stack) sourceLine = e.stack;
      if(e.sourceName) sourceName = e.stack;
      if(e.fileName) sourceName = e.fileName;
      if(e.lineNumber) lineNumber = e.lineNumber;
    }
    
    let errorObject = Stationery.XPCOM('nsIScriptError');
    errorObject.init('Stationery: ' + message, sourceName, sourceLine, lineNumber, columnNumber, errorObject.errorFlag, 'Stationery');
    Services.console.logMessage(errorObject);
    
    //debug only
    let prefsBranch = Services.prefs.getBranch('extensions.stationery.debug.');
    if ((prefsBranch.getPrefType('alertOnException') == prefsBranch.PREF_BOOL) && prefsBranch.getBoolPref('alertOnException'))
      Services.prompt.alert(null, 'Stationery exception [DEBUG]', 'Exception type: ' + typeof(e) + ', ' + e.toString() + "\n\n" + message);
    
  } catch(e2){
    //unlikelly, but happen while component is loaded...
    Components.utils.reportError("WARNING! Stationery.handleException() failed!\nError messages:\n" + e2.message + "\nOriginal exception:\n" + e.message);
  }
  if (alertMessage)
    Services.prompt.alert(null, 'Stationery exception', alertMessage);
}

Stationery.alert = function(message) {
  Services.prompt.alert(null, 'Stationery alert', message);
}


/////////////////////////////////////////////////////////////////////////////////////  
//code to create component instances
let xpcomComponents = {};

Stationery.RegisterXPCOM = function(interfaceName, componentString, interfaceType) {
  xpcomComponents[interfaceName] = {comStr: componentString, iface: interfaceType };
}
  
//returns new XPCOM instance
Stationery.XPCOM = function(interfaceName) {
  let o = xpcomComponents[interfaceName];
  if (!o) throw Components.Exception('Stationery.XPCOM() >> unregistred component: "' + interfaceName + '"!');
  return Components.classes[o.comStr].createInstance(o.iface);
}

// common XPCOM components
let ci = Components.interfaces;
Stationery.RegisterXPCOM('nsIScriptError', '@mozilla.org/scripterror;1', ci.nsIScriptError);
Stationery.RegisterXPCOM('nsIFilePicker', '@mozilla.org/filepicker;1', ci.nsIFilePicker);
Stationery.RegisterXPCOM('nsILocalFile', '@mozilla.org/file/local;1', ci.nsILocalFile);
Stationery.RegisterXPCOM('nsIScriptableUnicodeConverter', '@mozilla.org/intl/scriptableunicodeconverter', ci.nsIScriptableUnicodeConverter);
Stationery.RegisterXPCOM('nsITimer', '@mozilla.org/timer;1', ci.nsITimer);

Stationery.RegisterXPCOM('nsIFileInputStream', '@mozilla.org/network/file-input-stream;1', ci.nsIFileInputStream);
Stationery.RegisterXPCOM('nsIScriptableInputStream', '@mozilla.org/scriptableinputstream;1', ci.nsIScriptableInputStream);
Stationery.RegisterXPCOM('nsIConverterInputStream', '@mozilla.org/intl/converter-input-stream;1', ci.nsIConverterInputStream);

      
//services not imported from TB modules      
XPCOMUtils.defineLazyServiceGetter(Stationery, "fontEnumerator", "@mozilla.org/gfx/fontenumerator;1", "nsIFontEnumerator");
XPCOMUtils.defineLazyServiceGetter(Stationery, "parserUtils", "@mozilla.org/parserutils;1", "nsIParserUtils");

//one shared instance is enough
XPCOMUtils.defineLazyGetter(Stationery, "Messenger", function () { 
  return Components.classes["@mozilla.org/messenger;1"].createInstance(Components.interfaces.nsIMessenger); 
});



/////////////////////////////////////////////////////////////////////////////////////
//Do any per-window initializations/deinitialization

Stationery.onLoadWindow = function(event) {
  try {
    //event.target for 'load' is XULDocument, it's defaultView is same as global JS "window" object
    let win = event.target.defaultView;
    //loop over modules, calling its windowLoaded(win) function
      for (let key in Stationery.modules)
        if ('windowLoaded' in Stationery.modules[key])
           try {
            Stationery.modules[key].windowLoaded(win);
          } catch (e) {Stationery.handleException(e); }
  } catch (e) { Stationery.handleException(e); }
}

Stationery.onUnloadWindow = function(event) {
  try {
    //event.target for 'unload' is XULDocument, it's defaultView, is same as global JS "window" object
    let win = event.target.defaultView;
    //loop over modules, calling its releaseWindow(win) function
      for (let key in Stationery.modules)
        if ('releaseWindow' in Stationery.modules[key])
           try {
            Stationery.modules[key].releaseWindow(win);
          } catch (e) {Stationery.handleException(e); }
    //release references, for garbage collector
    delete win.Stationery_;
    delete win.Stationery;
  } catch (e) { Stationery.handleException(e); }
}

let beforeFirstWindowInit = true;
Stationery.initWindow = function(win) {

  if (beforeFirstWindowInit) {
    beforeFirstWindowInit = false;
    //loop over modules, calling its beforeInitWindow() function
    for (let key in Stationery.modules) 
      if ('beforeInitWindow' in Stationery.modules[key])
        try {
          Stationery.modules[key].beforeInitWindow();
        } catch (e) { Stationery.handleException(e); }
  }

  //make single object to hold all our variables related to window.
  //use one object to avoid polluting window namespace.
  win.Stationery_ = {};
  
  //onload initializations
  win.addEventListener('load', Stationery.onLoadWindow, false);
  //ensure uninitialize on window close
  win.addEventListener('unload', Stationery.onUnloadWindow, false);
  
  //loop over modules, calling its initWindow(win) function
  for (let key in Stationery.modules) 
    if ('initWindow' in Stationery.modules[key])
      try {
        Stationery.modules[key].initWindow(win);
      } catch (e) { Stationery.handleException(e); }
}

/////////////////////////////////////////////////////////////////////////////////////
//l10n
let l10n = Services.strings.createBundle("chrome://stationery/locale/stationery.properties");

Stationery._ = function(string) { try {
  return l10n.GetStringFromName(string);
} catch(e) { Stationery.handleException(e, string); return string; } }

Stationery._f = function(string, args) { try {
  return l10n.formatStringFromName(string, args, args.length);
} catch(e) { Stationery.handleException(e); return string; } }

/////////////////////////////////////////////////////////////////////////////////////
//OS recognition, OS dependent functions
Stationery.OSisWindows = Services.appinfo.OS.match(/^(WINNT)|(WINCE)$/i);
Stationery.OSisMasOC = Services.appinfo.OS.match(/^(Darwin)$/i);
Stationery.OSisUnix = Services.appinfo.widgetToolkit.match(/^(gtk2)|(qt)$/i);

Stationery.getFilePathSeparator = function() {
  if (Stationery.OSisWindows) return '\\'; else return '/'; 
}


/////////////////////////////////////////////////////////////////////////////////////
//Unicode conversion helpers
Stationery.toUnicode = function(charset, data) {
  let uConv = Stationery.XPCOM('nsIScriptableUnicodeConverter');
  uConv.charset = charset;
  return uConv.ConvertToUnicode(data);
}

Stationery.fromUnicode = function(charset, data) {
  let uConv = Stationery.XPCOM('nsIScriptableUnicodeConverter');
  uConv.charset = charset;
  return uConv.ConvertFromUnicode(data) + uConv.Finish()
}

/////////////////////////////////////////////////////////////////////////////////////
// "class" attribute management helpers
Stationery.hasClass = function(e,c) {return e.className.match(new RegExp('(\\s|^)'+c+'(\\s|$)'));}
Stationery.addClass = function(e,c) {if(!Stationery.hasClass(e,c))e.className+=' '+c;}
Stationery.removeClass = function(e,c) {if(Stationery.hasClass(e,c))e.className=e.className.replace(new RegExp('(\\s|^)'+c+'(\\s|$)'),' ');}


/////////////////////////////////////////////////////////////////////////////////////
// timer

Stationery.makeTimer = function() {
  return {
    nsITimer: Stationery.XPCOM('nsITimer'),
    code: null,
    
    notify: function(aTimer) {
      if (typeof this.code == 'function') 
        try { 
          let code = this.code;
          if (this.nsITimer.type == this.nsITimer.TYPE_ONE_SHOT) this.code = null;
          code(); 
        } catch (e) {Stationery.handleException(e); }
    },
        
    QueryInterface: function(aIID) {
      if (aIID.equals(Components.interfaces.nsITimerCallback) || aIID.equals(Components.interfaces.nsISupports)) return this;
      throw Components.results.NS_ERROR_NO_INTERFACE;
    },
    
    startInterval: function(code, millisec) {
      this.nsITimer.cancel();
      this.code = code;
      this.nsITimer.initWithCallback(this, millisec, this.nsITimer.TYPE_REPEATING_SLACK);
    },
    
    startTimeout: function(code, millisec) {
      this.nsITimer.cancel();
      this.code = code;
      this.nsITimer.initWithCallback(this, millisec, this.nsITimer.TYPE_ONE_SHOT);
    },
    
    cancel: function(code, millisec) {
      this.nsITimer.cancel();
      this.code = null;
    },
  };
}


/////////////////////////////////////////////////////////////////////////////////////
// window  helpers

Stationery.isWindow = function(win) { 
  return (typeof win == 'object') && ('document' in win); 
}

Stationery.getWindowType = function(win) {
  if (!Stationery.isWindow((win)) || !win.document.documentElement.hasAttribute('windowtype')) return false;
  return win.document.documentElement.getAttribute('windowtype');
}

Stationery.isMessengerWindow = function(win) { return Stationery.getWindowType(win) == 'mail:3pane' }
Stationery.isComposerWindow = function(win) { return Stationery.getWindowType(win) == 'msgcompose' }
Stationery.isMessageWindow = function(win) { return Stationery.getWindowType(win) == 'mail:messageWindow' }


/////////////////////////////////////////////////////////////////////////////////////
// event helpers
Stationery.fireEvent = function(win, eventName) {
  let event = win.document.createEvent("Events");
  event.initEvent('stationery-' + eventName, true, false);
  win.dispatchEvent(event);
}

Stationery.fireAsyncEvent = function(win, eventName) {
  win.setTimeout(function() {
    let event = win.document.createEvent("Events");
    event.initEvent('stationery-' + eventName, true, false);
    win.dispatchEvent(event);
  }, 0);
}

Stationery.fireCancellableEvent = function(win, eventName) {
  //fire event to notify other code that template will be loaded  
  let event = win.document.createEvent("Events");
  event.initEvent('stationery-' + eventName, true, true);
  return win.dispatchEvent(event);
}


  
/////////////////////////////////////////////////////////////////////////////////////
//load other modules

Components.utils.import('resource://stationery/content/prefs.jsm');
Components.utils.import('resource://stationery/content/templates.jsm');
Components.utils.import('resource://stationery/content/options.jsm');
Components.utils.import('resource://stationery/content/menu.jsm');
Components.utils.import('resource://stationery/content/composer_utils.jsm');
Components.utils.import('resource://stationery/content/syntax_hilite.jsm');


