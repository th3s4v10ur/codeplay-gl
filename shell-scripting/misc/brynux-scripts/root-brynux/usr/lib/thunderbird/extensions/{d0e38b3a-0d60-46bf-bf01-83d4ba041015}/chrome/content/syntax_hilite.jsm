/******************************************************************************
project: "Stationery" extension for Thunderbird
filename: syntax_hilite.jsm
author: Łukasz 'Arivald' Płomiński <arivald@interia.pl>
description: Syntax highlighting
******************************************************************************/
'use strict';

Components.utils.import('resource://stationery/content/stationery.jsm');
Components.utils.import('resource://gre/modules/iteratorUtils.jsm');
Components.utils.import("resource://gre/modules/mailServices.js");
Components.utils.import("resource://gre/modules/Services.jsm");

var EXPORTED_SYMBOLS = [];

Stationery.definePreference('SourceEditOptions', { type: 'json', default: {
  wordWrap: true,
  lineSeparator: false,
  base: { c: 'black', bk: 'white', f: 'monospace', fs: 10},
  markup: { c: 'fuchsia', bk: 'white', b: true, i: false },
  tag: { c: 'blue', bk: 'white', b: true, i: false },
  attrib: { c: 'green', bk: 'white', b: false, i: false },
  attValue: { c: 'black', bk: 'white', b: true, i: false },
  doctype: { c: 'white', bk: 'gray', b: true, i: false },
  comment: { c: 'gray', bk: 'white', b: false, i: false },
} });

Stationery.syntax = {
  getStyleBlock: function() {
    try{
      let prefs = Stationery.getPref('SourceEditOptions');
      
      let result = [];
      result.push('<style type="text/css">');
      result.push(' body { ' + getCSS(prefs.base) + ' }');
      if (prefs.wordWrap)
        result.push(' body { white-space: pre-wrap; }');
      else
        result.push(' body { white-space: pre; }');
      
      result.push(' .m { ' + getCSS(prefs.markup) + ' }');
      result.push(' .t { ' + getCSS(prefs.tag) + ' }');
      result.push(' .n { ' + getCSS(prefs.attrib) + ' }');
      result.push(' .v { ' + getCSS(prefs.attValue) + ' }');
      result.push(' .doctype { ' + getCSS(prefs.doctype) + ' }');
      result.push(' .c { ' + getCSS(prefs.comment) + ' }');
      
      //class "line" is used to speed-up editing. apparently nsiEditor did not like a lot of <span> elements, so I use <div> elements to group them.
      result.push(' .line { min-height: 1.3em; }');
      if (prefs.lineSeparator)
        result.push(' .line { border-bottom: 1px solid #D0D0D0; }');

      result.push('</style>');
    
      return result.join('\n');
    } catch (e) { Stationery.handleException(e); return ''; }
  },

  getBody: function(sourceHtml) {
    try{
      let result = [];

      let currentCharIdx = 0;
      let startCharIdx = 0;
      let len = sourceHtml.length;
      
      let tagName = '';
      let attribName = '';
      let attribValue = '';
      let quotedAttribValue = '';
      
      let isStyleBlock = false;

      while(currentCharIdx < len) {
        //markup begin character
        if(sourceHtml[currentCharIdx] == '<') {

          //check it is doctype ... 
          if(sourceHtml.substr(currentCharIdx, 9) == '<!DOCTYPE') {
            result.push('<span class="doctype">&lt;');
            ++currentCharIdx; // "<"
            while(currentCharIdx < len)
              if(sourceHtml[currentCharIdx] != '>') {
                result.push(encodeHtmlEntities(sourceHtml[currentCharIdx]));
                ++currentCharIdx;
              } else break;
            result.push('&gt;</span>');
            ++currentCharIdx; // ">"
            continue;
          }

          //check it is comment... 
          if(sourceHtml.substr(currentCharIdx, 4) == '<!--') { 
            if(isStyleBlock) {
              result.push('<span class="c">&lt;!--</span>');
              currentCharIdx += 4; // "<!--"
              
              startCharIdx = currentCharIdx;
              while(currentCharIdx < len) {
                if(sourceHtml.substr(currentCharIdx, 3) == '-->')
                  break;
                else {
                  if((sourceHtml[currentCharIdx] == '{') 
                  || (sourceHtml[currentCharIdx] == '}') 
                  || (sourceHtml[currentCharIdx] == ':') 
                  || (sourceHtml[currentCharIdx] == ';') 
                  || (sourceHtml[currentCharIdx] == '(') 
                  || (sourceHtml[currentCharIdx] == ')') 
                  || (sourceHtml[currentCharIdx] == ',') 
                  || (sourceHtml[currentCharIdx] == '!') 
                  || (sourceHtml[currentCharIdx] == '@') 
                  || (sourceHtml[currentCharIdx] == '{')) {
                     
                    if(currentCharIdx - startCharIdx > 0)
                      result.push(encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx)));
                    startCharIdx = currentCharIdx + 1;
                    result.push('<span class="m">' + sourceHtml[currentCharIdx] + '</span>')
                  }
                 ++currentCharIdx;
                }
              }
                
              if(currentCharIdx - startCharIdx > 0)
                result.push(encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx)));

              result.push('<span class="comment">--&gt;</span>');
              currentCharIdx += 3; // "-->"
              continue;
            } else { //comment outside of style block
              result.push('<span class="c">&lt;!--');
              currentCharIdx += 4; // "<!--"
              startCharIdx = currentCharIdx;
              while(currentCharIdx < len && sourceHtml.substr(currentCharIdx, 3) != '-->') ++currentCharIdx;
              if(currentCharIdx - startCharIdx > 0)
                result.push(encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx)));
              result.push('--&gt;</span>');
              currentCharIdx += 3; // "-->"
              continue;
            }
          }
          
          //is ordinal tag, so ...
          if(sourceHtml.substr(currentCharIdx, 2) == '</') { //maybe it is ending tag?
            result.push('<span class="m">&lt;/</span>');
            currentCharIdx += 2;
          } else {
            result.push('<span class="m">&lt;</span>');
            ++currentCharIdx;
          }
          

          // now we looking for tag name, till first white space, or end of tag
          tagName = '';
          //skip WS
          startCharIdx = currentCharIdx;
          while(currentCharIdx < len)
            if((sourceHtml[currentCharIdx] == ' ') 
            || (sourceHtml[currentCharIdx] == "\n") 
            || (sourceHtml[currentCharIdx] == "\t"))
              ++currentCharIdx;
            else break;
          if(currentCharIdx - startCharIdx > 0)
            result.push(encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx)));
          
          startCharIdx = currentCharIdx;
          while(currentCharIdx < len) {
            if((sourceHtml[currentCharIdx] == ' ') 
            || (sourceHtml[currentCharIdx] == "\n") 
            || (sourceHtml[currentCharIdx] == "\t") 
            || (sourceHtml[currentCharIdx] == '/') 
            || (sourceHtml[currentCharIdx] == '=') 
            || (sourceHtml[currentCharIdx] == '"') 
            || (sourceHtml[currentCharIdx] == "'") 
            || (sourceHtml[currentCharIdx] == '>') 
            || (sourceHtml[currentCharIdx] == '<') 
            || (sourceHtml[currentCharIdx] == '&'))
              break;
            ++currentCharIdx;
          }
          if(currentCharIdx - startCharIdx > 0)
            tagName = sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx);
            
          isStyleBlock = tagName.match(/.*style.*/i);
          result.push('<span class="t">' + tagName + '</span>');

          //process attribs
          while(currentCharIdx < len) {
            if((sourceHtml[currentCharIdx] == '/') || (sourceHtml[currentCharIdx] == '>'))
              break;
            //skip WS
            startCharIdx = currentCharIdx;
            while(currentCharIdx < len)
              if((sourceHtml[currentCharIdx] == ' ') 
              || (sourceHtml[currentCharIdx] == "\n") 
              || (sourceHtml[currentCharIdx] == "\t"))
                ++currentCharIdx;
              else break;
            if(currentCharIdx - startCharIdx > 0)
              result.push(encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx)));
            
            attribName = '';
            startCharIdx = currentCharIdx;
            while(currentCharIdx < len) {
              if((sourceHtml[currentCharIdx] == ' ') 
              || (sourceHtml[currentCharIdx] == "\n") 
              || (sourceHtml[currentCharIdx] == "\t") 
              || (sourceHtml[currentCharIdx] == '=') 
              || (sourceHtml[currentCharIdx] == '"') 
              || (sourceHtml[currentCharIdx] == "'") 
              || (sourceHtml[currentCharIdx] == '/') 
              || (sourceHtml[currentCharIdx] == '>') 
              || (sourceHtml[currentCharIdx] == '<') 
              || (sourceHtml[currentCharIdx] == '&'))
                break;
              ++currentCharIdx;
            }
            if(currentCharIdx - startCharIdx > 0)
              attribName = sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx);
              
            if(attribName == '') break;
            
            result.push('<span class="n">' + attribName + '</span>');
            
            //skip WS
            startCharIdx = currentCharIdx;
            while(currentCharIdx < len)
              if((sourceHtml[currentCharIdx] == ' ') 
              || (sourceHtml[currentCharIdx] == "\n") 
              || (sourceHtml[currentCharIdx] == "\t")) 
                ++currentCharIdx;
              else break;
            if(currentCharIdx - startCharIdx > 0)
              result.push(encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx)));
              
            //here should be '=', otherwise it is empty atrib
            if(sourceHtml[currentCharIdx] == '=') {
              result.push('<span class="m">=</span>');
              ++currentCharIdx;
            
              //skip WS
              startCharIdx = currentCharIdx;
              while(currentCharIdx < len)
                if((sourceHtml[currentCharIdx] == ' ') 
                || (sourceHtml[currentCharIdx] == "\n") 
                || (sourceHtml[currentCharIdx] == "\t")) 
                  ++currentCharIdx;
                else break;
              if(currentCharIdx - startCharIdx > 0)
                result.push(encodeHtmlEntities((sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx))));
                
              //there may be qoute or not
              if((sourceHtml[currentCharIdx] == '"') || (sourceHtml[currentCharIdx] == "'")) {
                quotedAttribValue = sourceHtml[currentCharIdx];
                result.push('<span class="m">' + quotedAttribValue + '</span>');
                ++currentCharIdx;
              } else
                quotedAttribValue = '';

              //get attribValue
              attribValue = '';
              startCharIdx = currentCharIdx;
              while(currentCharIdx < len) {
                if(quotedAttribValue == '' /*no quotes*/) {
                  if((sourceHtml[currentCharIdx] == ' ') 
                  || (sourceHtml[currentCharIdx] == "\n") 
                  || (sourceHtml[currentCharIdx] == "\t")
                  || (sourceHtml[currentCharIdx] == '/') 
                  || (sourceHtml[currentCharIdx] == '>') 
                  || (sourceHtml[currentCharIdx] == '<'))
                    break
                } else {
                  if (sourceHtml[currentCharIdx] == quotedAttribValue)
                    break;
                    
                  //special case for very invalid HTMl, when somebody edits quoted attrib value
                  //returning null prevents applying syntax
                  if((sourceHtml[currentCharIdx] == '>') 
                  || (sourceHtml[currentCharIdx] == '<'))
                    return null;
                }

                ++currentCharIdx;
              }
              if(currentCharIdx - startCharIdx > 0)
                attribValue = encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx));
              result.push('<span class="v">' + attribValue + '</span>');
              attribValue = '';
              
              if(sourceHtml[currentCharIdx] == quotedAttribValue) {
                result.push('<span class="m">' + sourceHtml[currentCharIdx] + '</span>');
                ++currentCharIdx;
                quotedAttribValue = '';
              }
            }
          }
          //skip WS
          startCharIdx = currentCharIdx;
          while(currentCharIdx < len)
            if((sourceHtml[currentCharIdx] == ' ') 
            || (sourceHtml[currentCharIdx] == "\n") 
            || (sourceHtml[currentCharIdx] == "\t")) 
              ++currentCharIdx;
            else break;
          if(currentCharIdx - startCharIdx > 0)
            result.push(encodeHtmlEntities(sourceHtml.substr(startCharIdx, currentCharIdx - startCharIdx)));
                
          //here it is markup too
          if(sourceHtml.substr(currentCharIdx, 2) == '/>') { //optimized version, 2 characters in same span
            result.push('<span class="m">/&gt;</span>');
            currentCharIdx += 2;
          }
          if(sourceHtml[currentCharIdx] == '/') {
            result.push('<span class="m">/</span>');
            ++currentCharIdx;
          }
          if(sourceHtml[currentCharIdx] == '>') {
            result.push('<span class="m">&gt;</span>');
            ++currentCharIdx;
          }
          continue;
        }
        
        if(sourceHtml[currentCharIdx] == '>') {
          result.push('<span class="m">&gt;</span>');
          ++currentCharIdx;
          continue;
        }
        
        result.push(encodeHtmlEntities(sourceHtml[currentCharIdx]));
        ++currentCharIdx;
      }
      
      return '<div class="line">' + result.join('') + '</div>';
      
  /*    return (
        '<div class="line">' + result.join('').replace(/\n/g, "</div><div class=\"line\">") + '</div>'
      ).replace(/<div class="line"><\/div>/g, "<div class=\"line\"><br></div>");
  */
    } catch (e) { Stationery.handleException(e); return null; }
  },

  
}


function encodeHtmlEntities(string) {
  var tagsToReplace = { '&': '&amp;', '<': '&lt;', '>': '&gt;', '\n': '</div><div class="line">' };
  return string.replace(/[\n&<>]/g, function (tag) { return tagsToReplace[tag] || tag; });
}

function escapeFontFamily(fontFamily) {
  if (!(fontFamily == 'monospace' || fontFamily == 'serif' || fontFamily == 'sansserif'))
    fontFamily = '"' + fontFamily + '", monospace';
  return fontFamily;
}  
  
function getCSS(prefsBlock) {
  if (typeof prefsBlock !== 'object') return '';
  let css = [];
  if ('c' in prefsBlock) css.push('color:' + prefsBlock['c']);
  if ('bk' in prefsBlock) css.push('background-color:' + prefsBlock['bk']);
  if ('f' in prefsBlock) css.push('font-family:' + prefsBlock['f']);
  if ('fs' in prefsBlock) css.push('font-size:' + prefsBlock['fs'] + 'pt');
  if ('b' in prefsBlock) if (prefsBlock['b']) css.push('font-weight: bold');
  if ('i' in prefsBlock) if (prefsBlock['i']) css.push('font-style: italic');
  return css.join('; ');
}
