#!/bin/sh

show_menu(){
    NORMAL=`echo "\033[m"`
    MENU=`echo "\033[36m"` #Blue
    NUMBER=`echo "\033[33m"` #yellow
    FGRED=`echo "\033[41m"`
    RED_TEXT=`echo "\033[31m"`
    ENTER_LINE=`echo "\033[33m"`
    echo -e "${MENU}${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  1)${MENU} Make brynux i386"
    echo -e "${MENU}  ${NUMBER}  2)${MENU} Make brynux x86_64 ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  3)${MENU} Make brynux i386 and x86_64 ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  4)${MENU} Write 32bit to USB ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  5)${MENU} Write 64bit to USB ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  6)${MENU} Copy USB-DRIVE files ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  7)${MENU} Write dual 32/64 to USB ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  8)${MENU} Write 4 brynux sticks ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  9)${MENU} ------------------------------------------------------------ ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 10)${MENU}  ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 11)${MENU} Auto-partition /dev/sdd with fdisk. ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 12)${MENU} Update www.brynux.com repository. ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 13)${MENU} Rsync to SP-NAS-1 ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 14)${MENU} Rsync from SP-NAS-1 to Me ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 15)${MENU} BOOT i686 ISO ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 16)${MENU} BOOT x86_64 ISO ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 17)${MENU} BOOT USB ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 18)${MENU} ------------------------------------------------------------ ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 19)${MENU} Mount Brynux on sdd ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 20)${MENU} Unmount Brynux on sdd ${NORMAL}"
    echo -e "${MENU}  ${NUMBER} 21)${MENU} Write Kali64 to USB ${NORMAL}"
    echo -e "${MENU}  ${NUMBER}  x)${MENU} Exit ${NORMAL}"
    echo -e "${MENU}${NORMAL}"
    echo -e "${ENTER_LINE}Please enter a menu option and enter or ${RED_TEXT}enter to exit. ${NORMAL}"
    read opt
}
function option_picked() {
    COLOR='\033[01;31m' # bold red
    RESET='\033[00;00m' # normal white
    MESSAGE=${@:-"${RESET}Error: No message passed"}
    echo -e "${COLOR}${MESSAGE}${RESET}"
}

clear
show_menu
while [ opt != '' ]
    do
    if [[ $opt = "" ]]; then 
            exit;
    else
        case $opt in
1) clear;
        option_picked "Option 1 Picked";

      	echo "Starting kickstart compilation."
	setenforce 0; setarch i686 livecd-creator -v -c /home/user/Downloads/livecd/scripts/brynux.ks -f brynux-24-i686 --cache=/home/user/Downloads/livecd/zcache/live --tmpdir=/home/user/Downloads/livecd/zcache/live --releasever=24 --title="brynux 24 i686" --product="brynux 24";
        echo "i686 kickstart compilation complete."
        show_menu;
        ;;

2) clear;
            option_picked "Option 2 Picked";

      	echo "Starting kickstart compilation."

	livecd-creator -v -c /home/user/Downloads/livecd/scripts/brynux.ks -f brynux-24-x86_64 --cache=/home/user/Downloads/livecd/zcache/live --tmpdir=/home/user/Downloads/livecd/zcache/live --releasever=24 --title="brynux 24 x86_64" --product="brynux 24";
        echo "x86_84 kickstart compilation complete."
        show_menu;
            ;;
3) clear;
            option_picked "Option 3 Picked";

      	echo "Starting kickstart compilation."

	livecd-creator -v -c /home/user/Downloads/livecd/scripts/brynux.ks -f brynux-24-x86_64 --cache=/home/user/Downloads/livecd/zcache/live --tmpdir=/home/user/Downloads/livecd/zcache/live --releasever=24 --title="brynux 24 x86_64" --product="brynux 24";
	setenforce 0; setarch i686 livecd-creator -v -c /home/user/Downloads/livecd/scripts/brynux.ks -f brynux-24-i686 --cache=/home/user/Downloads/livecd/zcache/live --tmpdir=/home/user/Downloads/livecd/zcache/live --releasever=24 --title="brynux 24 i686" --product="brynux 24";

        echo "i686 and x86_64 kickstart compilation complete."

        show_menu;
            ;;

4) clear;
            option_picked "Option 3 Picked";
        echo "Write 32bit to USB";
	if [ -d /run/media/user/BRYNUX-OS ]; then
		echo "Unmounting /run/media/user/BRYNUX-OS"
		rm -fr /run/media/user/BRYNUX-OS/32-bit/* 
	        umount /run/media/user/BRYNUX-OS
	else
        	echo "Good. BRYNUX-OS is not yet mounted."
	fi

	livecd-iso-to-disk  --delete-home --unencrypted-home --home-size-mb 128 --overlay-size-mb 128 --livedir 32-bit --multi --label BRYNUX-OS brynux-24-i686.iso /dev/sdd1
        echo "Write 32bit to USB complete";
	show_menu;
            ;;

5) clear;
            option_picked "Option 4 Picked";
        echo "Write 64bit to USB";
	if [ -d /run/media/user/BRYNUX-OS ]; then
		rm -fr /run/media/user/BRYNUX-OS/64-bit/*
		echo "Unmounting /run/media/user/BRYNUX-OS" 
	        umount /run/media/user/BRYNUX-OS
	else
        	echo "Good. BRYNUX-OS is not yet mounted."
	fi

	echo "Writing 64-bit brynux"
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 512 --overlay-size-mb 1024 --multi --livedir 64-bit --label BRYNUX-OS brynux-24-x86_64.iso /dev/sdd2

	show_menu;
            ;;
6) clear;
            option_picked "Option 6 Picked";
        echo "Copy USB-DRIVE files";
	if [ -d /run/media/user/USB-DRIVE ]; then
		echo "Unmounting /run/media/user/USB-DRIVE" 
	        umount /run/media/user/USB-DRIVE
	else
        	echo "Good. USB-DRIVE is not yet mounted."
	fi

	echo "Mounting USB-DRIVE to copy files."
	mkdir /run/media/user/USB-DRIVE;
	mount /dev/sdd1 /run/media/user/USB-DRIVE;

	cp -fr /home/user/Downloads/livecd/cdrom-files/* /run/media/user/USB-DRIVE

	umount /run/media/user/USB-DRIVE;
	rm -fr /run/media/user/USB-DRIVE;
	show_menu;
            ;;
7) clear;
            option_picked "Option 5 Picked";
        echo "Writing dual 32/64 bit to USB";
        if [ -b "/dev/sdd1" ]
        then
	echo "sdd1 is mounted. I will unmount";
        umount /dev/sdd1;
        fi;
        if [ -b "/dev/sdd2" ]
       	then
        echo "sdd2 is mounted. I will unmount";
        umount /dev/sdd2;
       	fi;

	echo "---------------------------------------------------";
        echo "| Partitioning /dev/sdd                            |";
	echo "---------------------------------------------------";
	
        echo "Partitioning /dev/sdd";
	bash /home/user/Downloads/livecd/scripts/make-fdisk.sh;
        echo "Partitioning complete";

	echo "---------------------------------------------------";
	echo "| Writing 32-bit brynux                            |";
	echo "---------------------------------------------------";

	livecd-iso-to-disk  --unencrypted-home --home-size-mb 128 --overlay-size-mb 256 --multi --livedir 32-bit --resetmbr --label BRYNUX-OS brynux-24-i686.iso /dev/sdd2;
    
	echo "---------------------------------------------------";	
	echo "| Writing 64-bit brynux                            |";
	echo "---------------------------------------------------";

	livecd-iso-to-disk  --unencrypted-home --home-size-mb 1024 --overlay-size-mb 1536 --livedir 64-bit --resetmbr --label BRYNUX-OS brynux-24-x86_64.iso /dev/sdd2;

	echo "---------------------------------------------------";
        echo "| Updating UUIDs in boot menu                      |";
	echo "---------------------------------------------------";

	mkdir -p /run/media/user/BRYNUX-OS;
	mount /dev/sdd2 /run/media/user/BRYNUX-OS;
	mkdir -p /run/media/user/BRYNUX-OS/64-bit/syslinux;
	cp -fr /run/media/user/BRYNUX-OS/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/64-bit/syslinux/
	cp -fr /home/user/Downloads/livecd/root-liveusb/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/syslinux/
	bash /home/user/Downloads/livecd/scripts/copy-uuid.sh;

	echo "---------------------------------------------------";
        echo "| Copying demo media to USB-DRIVE                  |";
	echo "---------------------------------------------------";

	mkdir /run/media/user/USB-DRIVE;
	mount /dev/sdd1 /run/media/user/USB-DRIVE;

	cp -fr /home/user/Downloads/livecd/zcdrom/* /run/media/user/USB-DRIVE

	echo "---------------------------------------------------";
        echo "| Cleaning up before exit                          |";
	echo "---------------------------------------------------";

        umount /run/media/user/BRYNUX-OS;
	rm -fr /run/media/user/BRYNUX-OS;
	umount /run/media/user/USB-DRIVE;
	rm -fr  /run/media/user/USB-DRIVE;

	echo "---------------------------------------------------";
        echo "| Dual brynux compilation completed                |";
	echo "---------------------------------------------------";


	show_menu;
            ;;

8) clear;
            option_picked "Option 8 Picked";
	sh /home/user/Downloads/livecd/scripts/make-multiple-usb-sticks.sh
        show_menu;
            ;;

9) clear;
            option_picked "Option 9 Picked";
        if [ -b "/dev/sdd1" ]
        then
	echo "sdd1 is mounted. I will unmount"
        umount /dev/sdd1;
        fi
        if [ -b "/dev/sdd2" ]
       	then
        echo "sdd2 is mounted. I will unmount"
        umount /dev/sdd2;
       	fi

        echo "Writing brynux-24.dd to /dev/sdd";

        dcfldd if=/home/user/Downloads/livecd/zcache/brynux-24.dd of=/dev/sdd conv=notrunc bs=20M

        echo "Writing brynux-24.dd complete";
        show_menu;
            ;;

10) clear;
            option_picked "Option 10 Picked";
        if [ -b "/dev/sdd1" ]
        then
	echo "sdd1 is mounted. I will unmount"
        umount /dev/sdd1;
        fi
	if [ -b "/dev/sdd2" ]
        then
	echo "sdd2 is mounted. I will unmount"
        umount /dev/sdd2;
        fi

	if [ -b "/dev/sdc1" ]
        then
	echo "sdc1 is mounted. I will unmount"
        umount /dev/sdc1;
        fi
	if [ -b "/dev/sdc2" ]
        then
	echo "sdc2 is mounted. I will unmount"
        umount /dev/sdc2;
        fi

	if [ -b "/dev/sdd1" ]
        then
	echo "sdd1 is mounted. I will unmount"
        umount /dev/sdd1;
        fi
	if [ -b "/dev/sdd2" ]
        then
	echo "sdd2 is mounted. I will unmount"
        umount /dev/sdd2;
        fi

	if [ -b "/dev/sde1" ]
        then
	echo "sde1 is mounted. I will unmount"
        umount /dev/sde1;
        fi
	if [ -b "/dev/sde2" ]
        then
	echo "sde2 is mounted. I will unmount"
        umount /dev/sde2;
        fi
	if [ -b "/dev/sdf1" ]
        then
	echo "sdf1 is mounted. I will unmount"
        umount /dev/sdf1;
        fi
	if [ -b "/dev/sdf2" ]
        then
	echo "sdf2 is mounted. I will unmount"
        umount /dev/sdf2;
        fi

	echo "Writing brynux-24.dd to all USB sticks";

        #dcfldd if=/home/user/Downloads/livecd/zcache/brynux-24.dd of=/dev/sdd of=/dev/sdc of=/dev/sde of=/dev/sdf conv=notrunc bs=20M
        dcfldd if=/home/user/Downloads/livecd/zcache/brynux-24.dd of=/dev/sdd conv=notrunc bs=20M & 
	dcfldd if=/home/user/Downloads/livecd/zcache/brynux-24.dd of=/dev/sdc conv=notrunc bs=20M & 
        dcfldd if=/home/user/Downloads/livecd/zcache/brynux-24.dd of=/dev/sdd conv=notrunc bs=20M & 
        dcfldd if=/home/user/Downloads/livecd/zcache/brynux-24.dd of=/dev/sde conv=notrunc bs=20M & 
        dcfldd if=/home/user/Downloads/livecd/zcache/brynux-24.dd of=/dev/sdf conv=notrunc bs=20M;

        echo "Writing brynux-24.dd complete";

        show_menu;
            ;;


11) clear;
            option_picked "Option 11 Picked";
        echo "Partitioning /dev/sdd";

        bash /home/user/Downloads/livecd/scripts/make-fdisk.sh

        echo "Partitioning complete";
        show_menu;
            ;;

12) clear;
            option_picked "Option 12 Picked";

        echo "Updating www.brynux.com repository";

	createrepo --update /home/user/Downloads/livecd/rpmbuild;
	rsync -azv --delete /home/user/Downloads/livecd/rpmbuild/ brynux@brynux.com://home/sites/brynux/web/repo;

        show_menu;
            ;;

13) clear;
            option_picked "Option 13 Picked";

	echo "---------------------------------------------------";
        echo "| Rsyncing to SP-NAS-1                             |";
	echo "---------------------------------------------------";

	rsync -azv --delete --exclude '*.iso' --exclude 'zcache/temp-drive-for-brynux-*' --exclude 'zcache/live/'  /home/user/Downloads/livecd/ spinningplanet@10.240.1.2:/livecd

        show_menu;
            ;;
14) clear;
            option_picked "Option 14 Picked";

	echo "---------------------------------------------------";
        echo "| Rsyncing from SP-NAS-1 to Me                     |";
	echo "---------------------------------------------------";

	rsync -azv --delete --exclude '*.iso' --exclude 'zcache/temp-drive-for-brynux-*' --exclude 'zcache/live/' spinningplanet@10.240.1.2:/home/brynux/livescd/ /home/user/Downloads/livecd;
	chown -R user:user /home/user/Downloads/livecd; 
	if [ -L ../rpmbuild ]; then
		echo "Symlink exists for rpmbuild" 
	else
		ln -s /home/user/Downloads/livecd/rpmbuild/ ../../rpmbuild;
	fi


        show_menu;
            ;;
15) clear;
            option_picked "Option 15 Picked";

	echo "---------------------------------------------------";
        echo "| Booting i686 ISO                                |";
	echo "---------------------------------------------------";

	qemu-kvm -m 1536 -vga vmware -cdrom brynux-24-i686.iso

        show_menu;
            ;;
16) clear;
            option_picked "Option 16 Picked";

	echo "---------------------------------------------------";
        echo "| Booting x86_64 ISO                              |";
	echo "---------------------------------------------------";

	qemu-kvm -m 1536 -vga vmware -cdrom brynux-24-x86_64.iso

        show_menu;
            ;;
17) clear;
            option_picked "Option 17 Picked";
	echo "---------------------------------------------------";
        echo "| Booting /dev/sdd as virtual machine             |";
	echo "---------------------------------------------------";

	if [ -b "/dev/sdd1" ]
	then
	echo "sdd1 is mounted. I will unmount"
	umount /dev/sdd1;
	fi
        if [ -b "/dev/sdd2" ]
       	then
        echo "sdd2 is mounted. I will unmount"
        umount /dev/sdd2;
       	fi

	qemu-kvm -vga std -hda /dev/sdd

        show_menu;
            ;;
18) clear;
            option_picked "Option 18 Picked";

	echo "---------------------------------------------------";
        echo "| ____              |";
	echo "---------------------------------------------------";

        show_menu;
            ;;
19) clear;
            option_picked "Option 19 Picked";

	echo "---------------------------------------------------";
        echo "| Mount Brynux on sdd                              |";
	echo "---------------------------------------------------";

	mkdir /run/media/user/BRYNUX-OS; mount /dev/sdd2 /run/media/user/BRYNUX-OS
	df -h;
	cd /run/media/user/BRYNUX-OS; ls -lh;
	exit;
            ;;

20) clear;
            option_picked "Option 20 Picked";

	echo "---------------------------------------------------";
        echo "| Unmount Brynux on sdd                            |";
	echo "---------------------------------------------------";

        if [ -b "/dev/sdd1" ]
        then
	echo "sdd1 is mounted. I will unmount"
        umount /dev/sdd1;
        fi
	if [ -b "/dev/sdd2" ]
        then
	echo "sdd2 is mounted. I will unmount"
        umount /dev/sdd2;
        fi

	cd /home/usr/Downloads/livecd;
	rm -fr /run/media/user/BRYNUX-OS;
	df -h;
	exit;
            ;;
21) clear;
            option_picked "Option 4 Picked";
        echo "Write 64bit Kali64 to USB";
	        umount /dev/sdd1
	        umount /dev/sdd2

	echo "Writing Kali64"
	livecd-iso-to-disk  --reset-mbr --format kali-linux-1.1.0a-amd64.iso /dev/sdd1

	show_menu;
            ;;


x)exit;
        ;;

        \n)exit;
        ;;

*)clear;
        option_picked "Pick an option from the menu";
        show_menu;
        ;;
    esac
fi
done
