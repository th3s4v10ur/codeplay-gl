cd ~user/Downloads/livecd/cache/
echo "Making initrd0"
	if [[ -d initrd0-temp ]]; 
	then
        	echo "Success. Directory initrd0-temp exists."
	else
        	echo "Creating initrd0-temp directory."
        	mkdir initrd0-temp
	fi
echo "Copying initrd0 from USB"
cp -fr /run/media/user/BRYNUX-OS/syslinux/initrd0.img /home/user/Downloads/livecd/cache/
cd initrd0-temp
rm -fr *
cp -fr ../initrd0.img initrd0.gz
rm -fr ../initrd0.img
echo "Extracting initrd0"
gunzip initrd0.gz
cpio -id < initrd0
rm -f initrd0
echo "Adding brynux custom theme to initrd0"
sed -i s/^Theme=.*/Theme=brynux/ usr/share/plymouth/plymouthd.defaults 
sed -i s/^Theme=.*/Theme=brynux/ etc/plymouth/plymouthd.conf 
cp -fr ~user/Downloads/livecd/rpmbuild/BUILD/plymouth-theme-brynux-0.1 usr/share/plymouth/themes/brynux
find . | cpio --create --format='newc' > initrd0
gzip initrd0
echo "Updating initrd0 on USB"
rm -f /run/media/user/BRYNUX-OS/syslinux/initrd0.img
mv initrd0.gz /run/media/user/BRYNUX-OS/syslinux/initrd0.img
rm -fr *
cd ~user/Downloads/livecd/

