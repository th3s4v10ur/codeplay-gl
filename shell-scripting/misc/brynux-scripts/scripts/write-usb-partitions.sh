#!/bin/sh
sdd=$1

dd if=/dev/zero of=/dev/$sdd bs=1k count=2048

for i in /dev/$sdd;do
echo "n
p
1
2048
+10G
n
p
2
20973568
31457279
t
1
b
t
2
b
a
2
w
"|fdisk $i;mkfs.vfat -n USB-DRIVE /dev/${sdd}1; mkfs.vfat -n BRYNUX-OS /dev/${sdd}2; mkdir /run/media/user/${sdd}2; mount /dev/${sdd}2 /run/media/user/${sdd}2; cp -fr /home/user/Downloads/livecd/root-liveusb/* /run/media/user/${sdd}2; extlinux -i /run/media/user/${sdd}2/syslinux/; umount /run/media/user/${sdd}2; dd if=/usr/share/syslinux/mbr.bin of=/dev/$sdd bs=440 count=1; partprobe /dev/sdd; rm -fr /run/media/user/${sdd}2;
done 
