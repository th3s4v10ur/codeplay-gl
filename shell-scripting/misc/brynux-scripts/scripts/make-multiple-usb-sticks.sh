#!/bin/sh
        umount /dev/sdd1;
        umount /dev/sdd2;
        umount /dev/sdc1;
        umount /dev/sdc2;
        umount /dev/sdd1;
        umount /dev/sdd2;
        umount /dev/sde1;
        umount /dev/sde2;

echo "Writing sdd";
dd if=/dev/zero of=/dev/sdd bs=1k count=2048
sdd="/dev/sdd"
for i in $sdd;do
echo "n
p
1
2048
+10G
n
p
2
20973568
31457279
t
1
b
t
2
b
a
2
w
"|fdisk $i;mkfs.vfat -n USB-DRIVE /dev/sdd1; mkfs.vfat -n BRYNUX-OS /dev/sdd2; partprobe /dev/sdd; done 
        echo "Partitioning complete";
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 128 --overlay-size-mb 256 --multi --livedir 32-bit --resetmbr --label BRYNUX-OS brynux-20-i686.iso /dev/sdd2;
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 1024 --overlay-size-mb 1536 --livedir 64-bit --resetmbr --label BRYNUX-OS brynux-20-x86_64.iso /dev/sdd2;
	mkdir -p /run/media/user/BRYNUX-OS;
	mount /dev/sdd2 /run/media/user/BRYNUX-OS;
	mkdir -p /run/media/user/BRYNUX-OS/64-bit/syslinux;
	cp -fr /run/media/user/BRYNUX-OS/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/64-bit/syslinux/
	cp -fr /home/user/Downloads/livecd/root-liveusb/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/syslinux/
	bash /home/user/Downloads/livecd/scripts/copy-uuid.sh;
	mkdir /run/media/user/USB-DRIVE;
	mount /dev/sdd1 /run/media/user/USB-DRIVE;
	cp -fr /home/user/Downloads/livecd/zcdrom/* /run/media/user/USB-DRIVE
        umount /run/media/user/BRYNUX-OS;
	rm -fr /run/media/user/BRYNUX-OS;
	umount /run/media/user/USB-DRIVE;
	rm -fr  /run/media/user/USB-DRIVE;

	echo "---------------------------------------------------";
        echo "| sdd written                                     |";
	echo "---------------------------------------------------";

echo "Writing sdc";
dd if=/dev/zero of=/dev/sdc bs=1k count=2048
sdd="/dev/sdc"
for i in $sdd;do
echo "n
p
1
2048
+10G
n
p
2
20973568
31457279
t
1
b
t
2
b
a
2
w
"|fdisk $i;mkfs.vfat -n USB-DRIVE /dev/sdc1; mkfs.vfat -n BRYNUX-OS /dev/sdc2; partprobe /dev/sdc; done 
        echo "Partitioning complete";
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 128 --overlay-size-mb 256 --multi --livedir 32-bit --resetmbr --label BRYNUX-OS brynux-20-i686.iso /dev/sdc2;
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 1024 --overlay-size-mb 1536 --livedir 64-bit --resetmbr --label BRYNUX-OS brynux-20-x86_64.iso /dev/sdc2;
	mkdir -p /run/media/user/BRYNUX-OS;
	mount /dev/sdc2 /run/media/user/BRYNUX-OS;
	mkdir -p /run/media/user/BRYNUX-OS/64-bit/syslinux;
	cp -fr /run/media/user/BRYNUX-OS/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/64-bit/syslinux/
	cp -fr /home/user/Downloads/livecd/root-liveusb/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/syslinux/
	bash /home/user/Downloads/livecd/scripts/copy-uuid.sh;
	mkdir /run/media/user/USB-DRIVE;
	mount /dev/sdc1 /run/media/user/USB-DRIVE;
	cp -fr /home/user/Downloads/livecd/zcdrom/* /run/media/user/USB-DRIVE
        umount /run/media/user/BRYNUX-OS;
	rm -fr /run/media/user/BRYNUX-OS;
	umount /run/media/user/USB-DRIVE;
	rm -fr  /run/media/user/USB-DRIVE;

	echo "---------------------------------------------------";
        echo "| sdc written                                     |";
	echo "---------------------------------------------------";

echo "Writing sdd";
dd if=/dev/zero of=/dev/sdd bs=1k count=2048
sdd="/dev/sdd"
for i in $sdd;do
echo "n
p
1
2048
+10G
n
p
2
20973568
31457279
t
1
b
t
2
b
a
2
w
"|fdisk $i;mkfs.vfat -n USB-DRIVE /dev/sdd1; mkfs.vfat -n BRYNUX-OS /dev/sdd2; partprobe /dev/sdd; done 
        echo "Partitioning complete";

echo "Writing sde";
dd if=/dev/zero of=/dev/sde bs=1k count=2048
sde="/dev/sde"
for i in $sde;do
echo "n
p
1
2048
+10G
n
p
2
20973568
31457279
t
1
b
t
2
b
a
2
w
"|fdisk $i;mkfs.vfat -n USB-DRIVE /dev/sde1; mkfs.vfat -n BRYNUX-OS /dev/sde2; partprobe /dev/sde; done 
        echo "Partitioning complete";
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 128 --overlay-size-mb 256 --multi --livedir 32-bit --resetmbr --label BRYNUX-OS brynux-20-i686.iso /dev/sde2;
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 1024 --overlay-size-mb 1536 --livedir 64-bit --resetmbr --label BRYNUX-OS brynux-20-x86_64.iso /dev/sde2;
	mkdir -p /run/media/user/BRYNUX-OS;
	mount /dev/sde2 /run/media/user/BRYNUX-OS;
	mkdir -p /run/media/user/BRYNUX-OS/64-bit/syslinux;
	cp -fr /run/media/user/BRYNUX-OS/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/64-bit/syslinux/
	cp -fr /home/user/Downloads/livecd/root-liveusb/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/syslinux/
	bash /home/user/Downloads/livecd/scripts/copy-uuid.sh;
	mkdir /run/media/user/USB-DRIVE;
	mount /dev/sde1 /run/media/user/USB-DRIVE;
	cp -fr /home/user/Downloads/livecd/zcdrom/* /run/media/user/USB-DRIVE
        umount /run/media/user/BRYNUX-OS;
	rm -fr /run/media/user/BRYNUX-OS;
	umount /run/media/user/USB-DRIVE;
	rm -fr  /run/media/user/USB-DRIVE;

	echo "---------------------------------------------------";
        echo "| sde written                                     |";
	echo "---------------------------------------------------";

	livecd-iso-to-disk  --unencrypted-home --home-size-mb 128 --overlay-size-mb 256 --multi --livedir 32-bit --resetmbr --label BRYNUX-OS brynux-20-i686.iso /dev/sdd2;
	livecd-iso-to-disk  --unencrypted-home --home-size-mb 1024 --overlay-size-mb 1536 --livedir 64-bit --resetmbr --label BRYNUX-OS brynux-20-x86_64.iso /dev/sdd2;
	mkdir -p /run/media/user/BRYNUX-OS;
	mount /dev/sdd2 /run/media/user/BRYNUX-OS;
	mkdir -p /run/media/user/BRYNUX-OS/64-bit/syslinux;
	cp -fr /run/media/user/BRYNUX-OS/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/64-bit/syslinux/
	cp -fr /home/user/Downloads/livecd/root-liveusb/syslinux/syslinux.cfg /run/media/user/BRYNUX-OS/syslinux/
	bash /home/user/Downloads/livecd/scripts/copy-uuid.sh;
	mkdir /run/media/user/USB-DRIVE;
	mount /dev/sdd1 /run/media/user/USB-DRIVE;
	cp -fr /home/user/Downloads/livecd/zcdrom/* /run/media/user/USB-DRIVE
        umount /run/media/user/BRYNUX-OS;
	rm -fr /run/media/user/BRYNUX-OS;
	umount /run/media/user/USB-DRIVE;
	rm -fr  /run/media/user/USB-DRIVE;

	echo "---------------------------------------------------";
        echo "| sdd written                                     |";
	echo "---------------------------------------------------";

