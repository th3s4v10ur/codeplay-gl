# Brynux Live Desktop Fedora Remix
# Date: October 8th, 2012
# Maintained by the Brynn Neilson:
# http://www.brynux.com/
# mailto:brynn at spinningplanet dot co dot nz
#
# Install tools to creare remix: 
# List installed RPMs by size: rpm -q -a --qf "%10{SIZE}\t%{NAME}\n" | sort -k1,1n
# Turn off repos: nano /usr/share/spin-kickstarts/fedora-repo-not-rawhide.ks
##### x86_64 #####
# livecd-creator -v -c brynux.ks -f brynux-24-x86_64 --cache=/home/user/Downloads/livecd/zcache/live --releasever=24 --title="brynux-24  x86_64" --product="brynux-24"
#####  i386  #####
# setenforce 0; setarch i686 livecd-creator -v -c brynux.ks -f brynux-24-i686 --cache=/home/user/Downloads/livecd/zcache/live --releasever=24 --title="brynux-24 i686" --product="brynux-24" 
#####  Test in QEMU #####
# qemu-kvm -m 1536 -vga vmware -cdrom brynux-24-i686.iso
# qemu-kvm -m 1536 -vga vmware -cdrom brynux-24-x86_64.iso
# qemu-kvm -vga std -hda /dev/sdd
#####  Burn to USB #####
# mkfs.vfat -n USB-DRIVE /dev/sdd1; mkfs.vfat -n BRYNUX-OS /dev/sdd2
# livecd-iso-to-disk --unencrypted-home --overlay-size-mb 3600 --label BRYNUX-OS --reset-mbr brynux-24-i686.iso /dev/sdd2
# livecd-iso-to-disk --unencrypted-home --overlay-size-mb 3600 --label BRYNUX-OS --reset-mbr brynux-24-x86_64.iso /dev/sdd2
# dd if=/dev/sdd of=brynux-32bit.img bs=4M && sync
# dd if=/dev/sdc of=/dev/sdd bs=24M	
#####  Make new plymouth boot #####
# dnf install @development-tools fedora-packager
# cd ~user/rpmbuild/BUILD
# exit
# tar -cvzf ../SOURCES/plymouth-theme-brynux-0.1.tar.gz plymouth-theme-brynux-0.1
# rpmbuild -ba ../SPECS/plymouth-theme-brynux.spec
# dnf remove plymouth-theme-brynux -y
# dnf install ~user/rpmbuild/RPMS/noarch/plymouth-theme-brynux-0.1-1.fc24.noarch.rpm
# Guide on themeing here: http://brej.org/blog/?cat=16
# Guide to rebuild initrd: http://askubuntu.com/questions/10258/how-to-change-live-cd-splash-screen
#####  Make new Skype #####
# cd ~user/rpmbuild
# setarch i386 rpmbuild --define "_topdir $(pwd)" --define "SOURCES $(pwd)" -ba SPECS/skype.spec
#####  Create local repo #####
# dnf install createrepo
# createrepo --database /home/user/Downloads/livecd/rpmbuild
# createrepo --update /home/user/Downloads/livecd/rpmbuild
# rsync -azv --chmod=o-rwx -p --delete /home/user/Downloads/livecd/rpmbuild/ brynux@10.240.1.193://home/sites/brynux/web/repo

###################################
###
### Repositories and time zone
###
###################################

# Turn off "updates" in /usr/share/spin-kickstarts/fedora-live-base.ks
%include /usr/share/spin-kickstarts/fedora-live-workstation.ks
#I am deleting the old partitions with this
#clearpart --all --drives=sda
timezone NZ
lang en_NZ.UTF-8
keyboard us
#firewall
firewall --enabled --service=ipp-client,mdns,samba,samba-client,ssh
selinux --disabled
#logging --level=info
logging --level=debug
services --enabled=ksmtuned,lirc,NetworkManager,restorecond,spice-vdagentd --disabled=abrtd,abrt-ccpp,abrt-oops,abrt-vmcore,abrt-xorg,capi,iprdump,iprinit,iprupdate,iscsi,iscsid,isdn,libvirtd,multipathd,netfs,network,nfs,nfslock,pcscd,rpcbind,rpcgssd,rpcidmapd,rpcsvcgssd,sendmail,sm-client,sshd

repo --name=fedora --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch --exclude kernel*debug* --exclude kernel-PAE*
repo --name=updates --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f$releasever&arch=$basearch --exclude kernel*debug* --exclude kernel-PAE*
#repo --name=fedora --baseurl=http://ucmirror.canterbury.ac.nz/linux/fedora/linux/updates/$releasever/$basearch/ --exclude kernel*debug*  --exclude kernel-PAE*
#repo --name=updates --baseurl=http://ucmirror.canterbury.ac.nz/linux/fedora/linux/updates/$releasever/$basearch/ --exclude kernel*debug*  --exclude kernel-PAE*
repo --name=brynux --baseurl=http://www.brynux.com/repo/
repo --name=google-chrome --baseurl=http://dl.google.com/linux/chrome/rpm/stable/$basearch
#repo --name=virtualbox --baseurl=http://download.virtualbox.org/virtualbox/rpm/fedora/$releasever/$basearch
repo --name=adobe --baseurl=http://linuxdownload.adobe.com/linux/$basearch
repo --name=rpmfusion-free --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-$releasever&arch=$basearch
repo --name=rpmfusion-non-free --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-$releasever&arch=$basearch
repo --name=rpmfusion-free-updates --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-updates-released-$releasever&arch=$basearch
repo --name=rpmfusion-non-free-updates --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch
#repo --name=rpmfusion-free-updates-testing --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-updates-testing-$releasever&arch=$basearch
#repo --name=rpmfusion-non-free-updates-testing --mirrorlist=http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-updates-testing-$releasever&arch=$basearch
#repo --name=russianfedora-nonfree --mirrorlist=http://mirrors.rfremix.ru/mirrorlist?repo=nonfree-fedora-$releasever&arch=$basearch
#repo --name=russianfedora-nonfree-updates --mirrorlist=http://mirrors.rfremix.ru/mirrorlist?repo=nonfree-fedora-updates-released-$releasever&arch=$basearch
#repo --name=russianfedora-free --mirrorlist=http://mirrors.rfremix.ru/mirrorlist?repo=free-fedora-$releasever&arch=$basearch
#repo --name=russianfedora-free-updates --mirrorlist=http://mirrors.rfremix.ru/mirrorlist?repo=free-fedora-updates-released-$releasever&arch=$basearch


###################################
###
### Packages
###
###################################
#%packages --instLangs=en_NZ.UTF-8 en_US.UTF-8 --excludedocs
%packages --excludedocs
# Base install stuff
kernel-headers
@hardware-support
#-@development-tools
kernel-devel
gcc
dkms
git
yum-plugin-priorities

# Remove GNOME shell extensions
-gnome-shell-extension-gpaste
-gnome-shell-extension-native-window-placement

# Remove things we don't want
-dracut-config-rescue
-lohit-devanagari-fonts
-sil-abyssinica-fonts
-smc-meera-fonts
-ghostscript-fonts
-abattis-cantarell-fonts
-lohit-telugu-fonts
-paratype-pt-sans-fonts
-smc-fonts-common
-baobab
-NetworkManager-vpnc
-NetworkManager-openvpn
-NetworkManager-pptp
-NetworkManager-l2tp
-xorg-x11-drv-wacom
-isdn4k-utils
-memtest86+
-tboot
-system-config-firewall-base
-dnf-langpacks
-perl-HTTP-Tiny
-lohit-gujarati-fonts
-thai-scalable-waree-fonts
-lohit-bengali-fonts
-khmeros-base-fonts
-wqy-zenhei-fonts
-lohit-tamil-fonts
-mailnag
-thai-scalable-fonts-common
-cjkuni-uming-fonts
-lohit-kannada-fonts
-sil-nuosu-fonts
-lklug-fonts
-khmeros-fonts-common
-jomolhari-fonts
-dejavu-sans-mono-fonts
-lohit-assamese-fonts
-stix-fonts
-sil-padauk-fonts
-paktype-naqsh-fonts
-gnome-font-viewer
-tabish-eeyek-fonts
-cjkuni-uming-fonts
-reiserfs-utils
-yelp
-simple*
-eog
-gnome-boxes
-aisleriot
-evolution*
-opencc
-anthy
-nautilus-sendto
-folks
-transmission
-seahorse
-orca
-firefox
-shotwell
-totem
-system-config-firewall
-rhythmbox
-sound-juicer
-rhythmbox
-gnome-backgrounds
-empathy
-totem
-planner
-deja-dup
-totem-nautilus
-totem-pl-parser
-abrt-*
-sendmail
-eekboard
-mplayer*
-gnome-mplayer
-gnome-photos
-gnome-contacts
-transmission*
-libpinyin*
-gnome-user-docs
-opencc 
-selinux-policy-doc
-anthy
-gnome-color-manager
-sane*
-libhangul
-gucharmap
-vinagre
-gnome-font-viewer
-gnome-clocks
-gnome-disk-utility
-gnome-screensaver
-gnome-screenshot
-gnome-system-monitor
-gnome-dictionary
-gnome-contacts
-gnome-photos 
-vlgothic-fonts
-java-1.8.0-openjdk-headless
-gnome-documents
-gnome-maps
-gnome-weather
-man-pages
-desktop-backgrounds-basic
-skkdic
-brasero
-brasero-nautilus

# Drop some system-config things
-system-config-language
-system-config-network
-system-config-rootpassword
-system-config-services
-policycoreutils-gui
-devassistant

## Change to generic release ##
-fedora-logos
#-fedora-release
-fedora-release-notes
#-plymouth-theme*
#-plymouth-theme-charge
plymouth-theme-brynux
plymouth-plugin-script
generic-logos
#generic-release

#### BRYNUX Stuff ####
# install 700MB            
####

## system stuff ##
#hplip
#hplip-libs
#hplip-common
#libsane-hpaio
firewall-config
sane-backends
x11vnc
# Samsung printer drivers
splix
linux-firmware
# Create live cds
livecd-tools

## Shell Apps ##
nano
elinks
jwhois
gnome-packagekit
anaconda
tar
wget
# libXaw for remote desktop
libXaw 

## Non free ##
# Broadcom wireless
broadcom-wl
b43-openfwwf
b43-fwcutter
akmod-wl
# Nvidia drivers
#akmod-nvidia 
#xorg-x11-drv-nvidia-libs

# Dialog for brynux upgrade script
#dialog

## GUI Apps ##
google-chrome-stable
#chromium
thunderbird
flash-plugin
vlc
ffmpeg
opencv
mkvtoolnix
ffmpegthumbnailer
-libreoffice-core
-java-1.8.0-openjdk-headless
#vlc-extras
gedit
#cheese
#pidgin # secure chats

####
# end 700MB 
####

# Graphics
gimp
gimp-resynthesizer # magic heal plugin
gimp-lqr-plugin # intellegent scale plugin
gimp-elsamuko 	# Collection of colour and compositing scripts https://sites.google.com/site/elsamuko/gimp
gimp-data-extras # extra brushes, palettes, and gradients
inkscape
#kdenlive
gthumb
blender
gcolor2
# Internet
#linphone
filezilla
# Audio
#rhythmbox
audacity-freeworld
# Utils
file-roller-nautilus
vokoscreen
# Virtual machine managers
#virt-manager
#libvirt
#VirtualBox-4.3
openssh-askpass
# Remote desktop apps
#vinagre
#vino

# Games
#klavaro

%end
###################################
### Post Installation - NO CHROOT
###
###################################

%post --nochroot --log=/mnt/sysimage/root/ks-post.log

# Change fedora logos & anaconda logos
cp -fr /home/user/Downloads/livecd/root-brynux/usr/share/icons/brynux/* $INSTALL_ROOT/usr/share/icons/Fedora
cp -fr /home/user/Downloads/livecd/root-brynux/* $INSTALL_ROOT/
chmod -R 755 $INSTALL_ROOT/usr/share

# Copy initrd with brynux boot screen to livecd root
echo "Install Root is $INSTALL_ROOT"
echo "Live Root is $LIVE_ROOT"
#cp $INSTALL_ROOT/boot/initrd-plymouth.img $LIVE_ROOT/isolinux/




#sh /home/user/Downloads/livecd/scripts/make-live-initrd.sh




# Copy Boot icon
cp -fr /home/user/Downloads/livecd/root-brynux/.VolumeIcon.icns $LIVE_ROOT

# Backup kickstart file to image
#cp /home/user/Downloads/livecd/brynux.ks $INSTALL_ROOT/root/

# Install default profiles 
# mkdir -p $INSTALL_ROOT/etc/skel/Intranet
# to tar up the user settings on template machine type tar --exclude 'Intranet' -cvpzf /home/user-settings-mini.tar.gz
# tar -C $INSTALL_ROOT/etc/skel/ -xvf /root/livecd/user-mini.tar.gz

%end
###################################
###
### Post Installation
###
###################################

%post --log=/root/ks-post.log

# Set the brynux plymouth theme
sed -i s/^Theme=.*/Theme=brynux/ /etc/plymouth/plymouthd.conf
sed -i s/^Theme=.*/Theme=brynux/ /usr/share/plymouth/plymouthd.defaults

  # if it's encrypted, we need to unlock it
  if [ "\$(/sbin/blkid -s TYPE -o value \$homedev 2>/dev/null)" = "crypto_LUKS" ]; then
    echo
    echo "Setting up encrypted /home device"
    plymouth ask-for-password --command="cryptsetup luksOpen \$homedev EncHome"
    homedev=/dev/mapper/EncHome
  fi

# update time on reboot
cat >> /etc/crontab <<EOF
@reboot /usr/sbin/ntpdate tk1.ihug.co.nz 2>&1 > /dev/null
EOF

# Manually install RTL wireless drivers
git clone https://github.com/pvaret/rtl8192cu-fixes.git
dkms add ./rtl8192cu-fixes && dkms install 8192cu/1.10 && depmod -a
cp ./rtl8192cu-fixes/blacklist-native-rtl8192.conf /etc/modprobe.d/
cat >> /etc/udev/rules.d/70-wifi-powersave.rules <<EOF
ACTION=="add", SUBSYSTEM=="net", KERNEL=="wlan*" RUN+="/usr/bin/iw dev %k set power_save off"
EOF


# make stuff automatically start in gui
mkdir -p /etc/skel/.config/autostart/
#cp /usr/local/share/applications/google-chrome.desktop /etc/skel/.config/autostart/
#cp /usr/share/applications/mozilla-thunderbird.desktop /etc/skel/.config/autostart/
#cp /usr/share/applications/fedora-linphone.desktop /etc/skel/.config/autostart/

# Tell nano to use color coding
for i in /usr/share/nano/*nanorc ; do echo "include \"$i\"" ; done >> /etc/skel/.nanorc

# Tell gnome-terminal to use 256 colours
#cat >> /etc/skel/.bashrc  <<EOF
#export TERM="xterm-256color"
#EOF

# rebrand as brynux

	echo "---------------------------------------------------";
	echo "| Rebranding as brynux                             |";
	echo "---------------------------------------------------";

echo "brynux" > /etc/fedora-release
echo "brynux" > /etc/system-release
echo "cpe:/o:brynux:brynux:24" > /etc/system-release-cpe
echo "BRYNUX Kernel \r on an \m (\l)" > /etc/issue
echo "BRYNUX Kernel \r on an \m (\l)" > /etc/issue.net

cat >/etc/os-release << EOF
NAME=brynux
VERSION="24 (Trinity)"
ID="brynux"
VERSION_ID=24
PRETTY_NAME="brynux-24 (Trinity)"
ANSI_COLOR="0;34"
CPE_NAME="cpe:/o:brynuxproject:brynux:24"
HOME_URL="https://www.brynux.com/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=24
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=24
EOF

	echo "---------------------------------------------------";
	echo "| Rebranding complete                              |";
	echo "---------------------------------------------------";

### Create custom settings ###
	echo "---------------------------------------------------";
	echo "| Creating gschema over rides                       |";
	echo "---------------------------------------------------";
cat >> /usr/share/glib-2.0/schemas/z_brynux_os_settings.gschema.override << EOF
# disable updates
#[org.gnome.settings-daemon.plugins.updates]
#active=false
# change wallpaper
[org.gnome.desktop.background]
color-shading-type='solid'
picture-options='scaled'
picture-uri='file:///usr/share/backgrounds/brynux/default/brynux.xml'
primary-color='#ff6600'
# Change screen lock colour
[org.gnome.desktop.screensaver]
primary-color='#ff6600'
secondary-color='#ff6600'
# create dashboard doc items
[org.gnome.shell]
favorite-apps=['google-chrome.desktop', 'firefox.desktop', 'mozilla-thunderbird.desktop', 'gedit.desktop', 'nautilus.desktop']
#'gedit.desktop', 'nautilus.desktop', 'gnome-terminal.desktop', 'anaconda.desktop']
# Add date and 12hr time to top
[org.gnome.desktop.interface]
clock-show-date=true
clock-format='12h'
EOF
rm -f /usr/share/glib-2.0/schemas/org.gnome.shell.gschema.override

# Disable updates
gsettings set org.gnome.software download-updates false

# Disable screensaver locking
# KP - disable screensaver locking
cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.screensaver.gschema.override << FOE

[org.gnome.desktop.screensaver]
lock-enabled=false
FOE

# KP - hide the lock screen option
cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.lockdown.gschema.override << FOE

[org.gnome.desktop.lockdown]
disable-lock-screen=true
FOE

# KP - turn off screensaver
gconftool-2 --direct --config-source xml:readwrite:/etc/gconf/gconf.xml.mandatory --type bool --set /apps/gnome-screensaver/idle_activation_enabled false

# Rebuild schema cache with any overrides we installed
glib-compile-schemas /usr/share/glib-2.0/schemas
echo "done"

# Un-mute sound card (fixes some issues reported)
amixer set Master 85% unmute 2>/dev/null
amixer set PCM 85% unmute 2>/dev/null
pactl set-sink-mute 0 0
pactl set-sink-volume 0 50000

# Turn off screensaver
gconftool-2 --direct --config-source xml:readwrite:/etc/gconf/gconf.xml.mandatory --type bool --set /apps/gnome-screensaver/idle_activation_enabled false

# Disable dnf update service
systemctl --no-reload disable dnf-updatesd.service 2> /dev/null || :
systemctl stop dnf-updatesd.service 2> /dev/null || :

# Remove universal access icon
sed -i "s,'ally'\, ,,g" /usr/share/gnome-shell/js/ui/panel.js
sed -i "s,'ally'\:,//'ally'\:,g" /usr/share/gnome-shell/js/ui/panel.js

# Delete password for root
sudo passwd -d root

# Change boot screen to brynux
/usr/sbin/plymouth-set-default-theme brynux

# Import repo keys
/usr/bin/rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-*-primary

# Fix repos
/usr/bin/dnf-config-manager --disable rawhide
/usr/bin/dnf-config-manager --enable fedora
/usr/bin/dnf-config-manager --enable updates
sed -i s/\$releasever/24/ /etc/yum.repos.d/*.repo

# Change gcolor2 icon
cat >/usr/share/applications/gcolor2.desktop <<EOF
[Desktop Entry]
Name=GColor2 
GenericName=Color Selector
Comment=Choose Colors
Exec=gcolor2
Icon=/usr/share/icons/brynux/scalable/apps/gcolor2.svg
Terminal=false
Type=Application
Categories=Graphics;GTK;
EOF

# Change default media locations to USB stick
	echo "---------------------------------------------------";
	echo "| Creating default media locations script          |";
	echo "---------------------------------------------------";

mkdir -p /home/liveuser/.config/autostart/
#cat >>/home/liveuser/.config/autostart/brynux-change-home-folders.sh << EOF
#if [[ -d /run/media/user/USB-DRIVE ]]; then 
#	sleep 10;
#	xdg-user-dirs-update --set DOWNLOAD /run/media/liveuser/USB-DRIVE/Downloads;
#	xdg-user-dirs-update --set TEMPLATES /run/media/liveuser/USB-DRIVE/Templates;
#	xdg-user-dirs-update --set PUBLICSHARE /run/media/liveuser/USB-DRIVE/Public;
#	xdg-user-dirs-update --set DOCUMENTS /run/media/liveuser/USB-DRIVE/Documents;
#	xdg-user-dirs-update --set MUSIC /run/media/liveuser/USB-DRIVE/Music;
#	xdg-user-dirs-update --set PICTURES /run/media/liveuser/USB-DRIVE/Pictures;
#	xdg-user-dirs-update --set VIDEOS /run/media/liveuser/USB-DRIVE/Videos ;
#else
#	exit; 
#fi
#EOF
#chmod 755 /home/liveuser/.config/autostart/brynux-change-home-folders.sh

#cat >>/home/liveuser/.config/autostart/brynux-change-home-folders.desktop << EOF
#[Desktop Entry]
#Name=Change Home folders
#Comment=Changes Home folders to	point at USB-DRIVE
#Keywords=shell;prompt;command;commandline;
#Exec=sh -c "/home/liveuser/.config/autostart/brynux-change-home-folders.sh"
#Icon=utilities-terminal
#Type=Application
#Categories=GNOME;GTK;System;TerminalEmulator;
#StartupNotify=false
#EOF
#/usr/bin/chmod 755 /home/liveuser/.config/autostart/brynux-change-home-folders.desktop
chown -R liveuser:liveuser /home/liveuser
#chown root:root /home/liveuser/.config/autostart/brynux-change-home-folders.*

cat >>/etc/yum.repos.d/virtualbox.repo << EOF
[virtualbox]
name=Fedora \$releasever - \$basearch - VirtualBox
baseurl=http://download.virtualbox.org/virtualbox/rpm/fedora/\$releasever/\$basearch
enabled=1
gpgcheck=1
gpgkey=http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc
EOF

cat >>/etc/yum.repos.d/brynux.repo << EOF
[brynux]
name=Brynux  -
baseurl=http://www.brynux.com/repo/
enabled=1
gpgcheck=0
EOF

# Remove packages that got past my exclusions
# dnf -y remove kernel-debug transmission* libpinyin* gnome-user-docs opencc selinux-policy-doc anthy gnome-color-manager libhangul gucharmap ghostdcript gnome-games-help rw-fonts babobab vinagre gnome-font-viewer gnome-clocks gnome-disk-utility gnome-screensaver gnome-screenshot gnome-system-monitor gnome-dictionary gnome-mplayer gnome-contacts gnome-photos gnote cpp java-1.8.0-openjdk-headless gnome-weather  gnome-clocks gnome-boxes gnome-backgrounds
dnf -y remove libreoffice*

# Manuall install stuff
# Fix tvnz ondemand and other drm secure video sites
dnf -y install https://sourceforge.net/projects/postinstaller/files/fedora/releases/24/x86_64/libhal1-flash-0.3.2-1.20151001gitc213405.fc24.x86_64.rpm

# manually make things smaller
#cd /usr/share/locale/
#rm -fr sl zh_TW zh_CN sv sk ru pl nl nb ja it hu gl fr fi es en_GB el de da cs en_AU en_CA en@shaw en en@boldquot en@quot be bg hr id ko pt_BR ca lt rw tr vi
rm -fr /usr/share/backgrounds/spherical-cow/ /usr/share/icons/HighContrast/* /usr/share/libgweather* /usr/share/applications/kde4/nepomukbackup.desktop  /usr/share/applications/kde4/nepomukcleaner.desktop 
#/usr/share/anaconda/gnome/

%end


