# Copy UUID from 64-bit sysconfig
uuid64=`grep -m 1 -o -P '(?<=root\=live\:UUID\=).*(?=rootfstype)' /run/media/user/BRYNUX-OS/64-bit/syslinux/syslinux.cfg`;
sed -i "s,uuid64bit,$uuid64,g" /run/media/user/BRYNUX-OS/syslinux/syslinux.cfg;
# Copy UUID from 32-bit sysconfig
uuid32=`grep -m 1 -o -P '(?<=root\=live\:UUID\=).*(?=rootfstype)' /run/media/user/BRYNUX-OS/32-bit/syslinux/syslinux.cfg`;
sed -i "s,uuid32bit,$uuid32,g" /run/media/user/BRYNUX-OS/syslinux/syslinux.cfg;



