#!/bin/sh
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
echo -e '\E[1;33;40m'"   Running this program will share your screen with Spinning Planet Support.   "; tput sgr0
echo -e '\E[1;33;40m'"   Follow these steps to connect:                                              "; tput sgr0
echo -e '\E[1;33;40m'"   1. Close all program except for this one                                    "; tput sgr0
echo -e '\E[1;33;40m'"   2. Call Spinning Planet Support on +64 6 358 6007                           "; tput sgr0
echo -e '\E[1;33;40m'"   3. Press Y on your keyboard when Spinning Planet say it's OK to connect     "; tput sgr0
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
read -r -p "Are you sure? [Y/n] " response
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
then
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
echo -e '\E[1;33;40m'"   Running Spinning Planet Remote Helpdesk. Close this window when             "; tput sgr0
echo -e '\E[1;33;40m'"   you're ready to disconnect ...                                              "; tput sgr0
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0

x11vnc -quiet -safer -rfbport 0 -xkb -connect_or_exit help.spinningplanet.co.nz:5201 -display :0

else
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
echo -e '\E[1;33;40m'"   Remote support cancelled.                                                   "; tput sgr0
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
echo -e '\E[1;33;40m'"                                                                               "; tput sgr0
fi
