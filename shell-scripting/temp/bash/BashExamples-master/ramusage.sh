#!/bin/sh

while true
    #This command shows ram usage
    do free -m | awk 'NR==2{printf "RAM Kullanımı: %s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2}'
    #This command shows disk usage
    df -h | awk '$NF=="/"{printf "Disk Kullanımı: %d/%dGB (%s)\n", $3,$2,$5}'
    #This command shows cpu load
    top -bn1 | grep load | awk '{printf "İşlemci Kullanımı: %.2f\n", $(NF-2)}'
    sleep 25
done
