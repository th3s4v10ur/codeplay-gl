#!/bin/bash

set -eu
cd $(dirname $0)

for FN in ./fn/* ; do source $FN ; done

URL="https://github.com/blackle0pard"

start_script

# get response status code
info_log "check URL:$(echo $URL)"
STATUS_CODE=$(curl -LI $URL -o /dev/null -w '%{http_code}\n' -s)
info_log "HTTP status code:$STATUS_CODE"

stop_script
exit 0


