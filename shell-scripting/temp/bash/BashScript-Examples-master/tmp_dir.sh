#!/bin/bash

set -eu
cd $(dirname $0)

for FN in ./fn/* ; do source $FN ; done

start_script

TMP_DIR=$(mktemp -u)

info_log "create a temporary directory."
mkdir -m 600 $TMP_DIR
info_log "$(ls -ldh --time-style=+%Y-%m-%d\ %H:%M:%S $TMP_DIR)"

info_log "delete a temporary directory."
rm -rf $TMP_DIR

if [ -e $TMP_DIR ]; then
  warn_log "a temporary directory is found."
  warn_log "$(ls -ld $TMP_DIR)"
  stop_script
  exit 1
fi

stop_script
exit 0
