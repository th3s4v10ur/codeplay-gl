#!/bin/bash

set -eu
cd $(dirname $0)

# now
DATE_NOW=$(date "+%Y-%m-%d %H:%M:%S")
echo $DATE_NOW

# tomorrow
DATE_TMR=$(date "+%Y-%m-%d %H:%M:%S" -d "1 day")
echo $DATE_TMR

# yesterday
DATE_YSTD=$(date "+%Y-%m-%d %H:%M:%S" -d "-1 day")
echo $DATE_YSTD

# unix time
DATE_UT=$(date "+%s")
echo $DATE_UT



