#!/bin/bash

set -eu
cd $(dirname $0)

for FN in ./fn/* ; do source $FN ; done

start_script

if [ "$(whoami)" != "root" ]
then
    warn_log "You must be root to run $0"
    stop_script
    exit 1
fi

stop_script
exit 0
