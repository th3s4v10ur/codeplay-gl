#!/bin/bash

#set -eu
cd $(dirname $0)

for FN in ./fn/* ; do source $FN ; done

start_script

if [ "$1" = "" ];then
    warn_log "invalid argument."
    warn_log "Usage: sh $0 [option]"
    stop_script    
    exit 1
fi

stop_script
exit 0
