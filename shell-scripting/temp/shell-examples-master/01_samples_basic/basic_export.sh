#!/bin/sh
# -----------------------------------------------------------------
# author:   hoojo
# email:    hoojo_@126.com
# github:   https://github.com/hooj0
# create date: 2018-06-28
# copyright by hoojo @ 2018
# -----------------------------------------------------------------




# =================================================================
#		export——输出或设置变量
# =================================================================
# 输出所有的环境变量。
# 如果你想查看某个特定变量的值，用 echo $VARIABLE_NAME

# 设置变量值
# 如果要设置一个变量，可以用: export my_var=/c/windows/system
# -----------------------------------------------------------------
# 语法： export [-fnp][变量名称]=[变量设置值]
#
# 参数说明：
#
#   -f 　代表[变量名称]中为函数名称。
#   -n 　删除指定的变量。变量实际上并未删除，只是不会输出到后续指令的执行环境中。
#   -p 　列出所有的shell赋予程序的环境变量。
# -----------------------------------------------------------------


# =================================================================
# 注意到语言设置（中文或英文等）对许多命令行工具有一些微妙的影响，比如排序的顺序和性能。
# 大多数 Linux 的安装过程会将 LANG 或其他有关的变量设置为符合本地的设置。
# 要意识到当你改变语言设置时，排序的结果可能会改变。
# 明白国际化可能会使 sort 或其他命令运行效率下降许多倍。
# 来忽略掉国际化并按照字节来判断顺序。
# =================================================================
export LC_ALL=C



# =================================================================
#	export 示例1：
# =================================================================
export

# output:
#-------------------------------------------------------------------
# AWS_HOME=/Users/adnanadnan/.aws
# LANG=en_US.UTF-8
# LC_CTYPE=en_US.UTF-8
# LESS=-R

echo $AWS_HOME

# output:
#-------------------------------------------------------------------
# /Users/adnanadnan/.aws



# =================================================================
#	export 示例2：
# =================================================================

export jq=/usr/bin/jq

echo $jq

# output:
#-------------------------------------------------------------------
# /usr/bin/jq

read exits