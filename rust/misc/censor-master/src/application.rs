use crate::config;
use crate::widgets::{View, Window};
use gio::prelude::*;
use gtk::prelude::*;
use std::env;
use std::{cell::RefCell, rc::Rc};

use glib::{Receiver, Sender};
pub enum Action {
    OpenFile,
    LoadFile(String),
    ViewEmpty,
    ViewPhoto,
}

pub struct Application {
    app: gtk::Application,
    window: Window,
    sender: Sender<Action>,
    receiver: RefCell<Option<Receiver<Action>>>,
}

impl Application {
    pub fn new() -> Rc<Self> {
        let app = gtk::Application::new(Some(config::APP_ID), Default::default()).unwrap();

        glib::set_application_name(&format!("{}Banner Viewer", config::NAME_PREFIX));
        glib::set_prgname(Some("censor"));

        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RefCell::new(Some(r));

        let window = Window::new(sender.clone());

        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Censor/shortcuts.ui");
        let dialog: gtk::ShortcutsWindow = builder.get_object("shortcuts").unwrap();
        window.widget.set_help_overlay(Some(&dialog));

        let application = Rc::new(Self { app, window, sender, receiver });

        application.setup_gactions();
        application.setup_signals();
        application.setup_css();
        application
    }

    pub fn run(&self, app: Rc<Self>) {
        info!("{}Banner Viewer ({})", config::NAME_PREFIX, config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let app = app.clone();
        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.do_action(action));

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }

    fn setup_gactions(&self) {
        // Quit
        let app = self.app.clone();
        let simple_action = gio::SimpleAction::new("quit", None);
        simple_action.connect_activate(move |_, _| app.quit());
        self.app.add_action(&simple_action);
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);

        // About
        let window = self.window.widget.clone();
        let simple_action = gio::SimpleAction::new("about", None);
        simple_action.connect_activate(move |_, _| {
            let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Censor/about_dialog.ui");
            let about_dialog: gtk::AboutDialog = builder.get_object("about_dialog").unwrap();
            about_dialog.set_transient_for(Some(&window));

            about_dialog.connect_response(|dialog, _| dialog.destroy());
            about_dialog.show();
        });
        self.app.add_action(&simple_action);

        self.app.set_accels_for_action("window.open", &["<primary>o"]);
        self.app.set_accels_for_action("window.save", &["<primary>s"]);
        self.app.set_accels_for_action("window.undo", &["<primary>z"]);
        self.app.set_accels_for_action("window.redo", &["<primary><shift>z"]);
        self.app.set_accels_for_action("window.paste", &["<primary>v"]);
        self.app.set_accels_for_action("window.copy", &["<primary>c"]);
        self.app.set_accels_for_action("window.zoom('in')", &["<primary>plus"]);
        self.app.set_accels_for_action("window.zoom('out')", &["<primary>minus"]);
        self.app.set_accels_for_action("window.effect('blur')", &["<primary>b"]);
        self.app.set_accels_for_action("window.effect('filled')", &["<primary>f"]);
        self.app.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
    }

    fn setup_signals(&self) {
        let window = self.window.widget.clone();
        self.app.connect_activate(move |app| {
            window.set_application(Some(app));
            app.add_window(&window);
            window.present();
        });
    }

    fn setup_css(&self) {
        let theme = gtk::IconTheme::get_default().unwrap();
        theme.add_resource_path("/com/belmoussaoui/Censor/icons");

        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/com/belmoussaoui/Censor/style.css");
        gtk::StyleContext::add_provider_for_screen(&gdk::Screen::get_default().unwrap(), &p, 500);
    }

    fn open_file(&self) {
        let open_dialog = gtk::FileChooserNative::new(Some("Open File"), Some(&self.window.widget), gtk::FileChooserAction::Open, Some("Open"), Some("Cancel"));

        let png_filter = gtk::FileFilter::new();
        png_filter.set_name(Some("image/png"));
        png_filter.add_mime_type("image/png");
        let jpg_filter = gtk::FileFilter::new();
        jpg_filter.set_name(Some("image/jpg"));
        jpg_filter.add_mime_type("image/jpeg");
        jpg_filter.add_mime_type("image/jpg");

        open_dialog.add_filter(&png_filter);
        open_dialog.add_filter(&jpg_filter);

        if gtk::ResponseType::from(open_dialog.run()) == gtk::ResponseType::Accept {
            let file = open_dialog.get_filename().unwrap();
            self.sender.send(Action::LoadFile(file.to_str().unwrap().to_string())).unwrap();
        };
        open_dialog.destroy();
    }

    fn load_file(&self, filepath: String) {
        self.sender.send(Action::ViewEmpty).unwrap();
        self.window.set_open_file(filepath);
        self.sender.send(Action::ViewPhoto).unwrap();
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::OpenFile => self.open_file(),
            Action::LoadFile(file) => {
                self.load_file(file.clone());
                // self.window.set_loaded_file(file);
            }
            Action::ViewEmpty => {
                self.window.set_view(View::Empty);
            }
            Action::ViewPhoto => {
                self.window.set_view(View::Photo);
            }
        };

        glib::Continue(true)
    }
}
