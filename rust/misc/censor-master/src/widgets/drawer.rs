use crate::application::Action;
use gdk::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use std::cell::RefCell;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::rc::Rc;

use gdk_sys;
use glib::translate::*;

pub trait PixbufExt {
    fn create_surface(&self, scale: i32, for_window: Option<&gdk::Window>) -> Option<cairo::ImageSurface>;
}

impl PixbufExt for gdk_pixbuf::Pixbuf {
    fn create_surface(&self, scale: i32, for_window: Option<&gdk::Window>) -> Option<cairo::ImageSurface> {
        unsafe { from_glib_full(gdk_sys::gdk_cairo_surface_create_from_pixbuf(self.to_glib_none().0, scale, for_window.to_glib_none().0)) }
    }
}
#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Filter {
    Blur,
    Filled,
}

impl Filter {
    pub fn from_str(value: &str) -> Filter {
        match value {
            "filled" => Filter::Filled,
            "blur" => Filter::Blur,
            _ => panic!("Unknown value: {}", value),
        }
    }
}

pub struct Drawer {
    pub widget: gtk::ScrolledWindow,
    builder: gtk::Builder,
    area: gtk::DrawingArea,
    surface: RefCell<cairo::ImageSurface>,
    filter: RefCell<Filter>,
    zoom: RefCell<f64>,
    offset: RefCell<(f64, f64)>,
    select_position: RefCell<(f64, f64)>, // (x, y) position of the selection
    history: RefCell<Vec<gdk_pixbuf::Pixbuf>>,
    future: RefCell<Vec<gdk_pixbuf::Pixbuf>>,
    sender: Sender<Action>,
}

impl Drawer {
    pub fn new(sender: Sender<Action>) -> Rc<Self> {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Censor/drawer.ui");
        let widget: gtk::ScrolledWindow = builder.get_object("scrolled_window").unwrap();
        let area: gtk::DrawingArea = builder.get_object("drawing_area").unwrap();
        widget.show();

        let surface = cairo::ImageSurface::create(cairo::Format::Rgb24, 300, 300);

        let drawer = Rc::new(Drawer {
            widget,
            surface: RefCell::new(surface.unwrap()),
            filter: RefCell::new(Filter::Blur),
            zoom: RefCell::new(1.0),
            select_position: RefCell::new((0.0, 0.0)),
            history: RefCell::new(Vec::new()),
            future: RefCell::new(Vec::new()),
            offset: RefCell::new((0.0, 0.0)),
            builder,
            area,
            sender,
        });
        drawer.init(drawer.clone());
        drawer.setup_signals(drawer.clone());
        drawer
    }
    fn init(&self, s: Rc<Self>) {
        // Setup drop area

        let hadj: gtk::Adjustment = self.builder.get_object("hadjustment").unwrap();
        let vadj: gtk::Adjustment = self.builder.get_object("vadjustment").unwrap();
        let drawer = s.clone();
        hadj.connect_value_changed(move |hadj| {
            let allocation = drawer.area.get_allocation();
            let scaled_width = drawer.surface.borrow().clone().get_width() as f64 * drawer.zoom.borrow().clone();
            let max_offset_x = scaled_width - allocation.width as f64;
            let max_value = hadj.get_upper() - hadj.get_page_size();
            let value = hadj.get_value();

            let offset_x = -1.0 * value * max_offset_x / max_value;
            let mut offset = drawer.offset.borrow_mut();
            offset.0 = offset_x;
            drawer.area.queue_draw()
        });
        let drawer = s.clone();
        vadj.connect_value_changed(move |adj| {
            let allocation = drawer.area.get_allocation();
            let scaled_height = drawer.surface.borrow().clone().get_height() as f64 * drawer.zoom.borrow().clone();
            let max_offset_y = scaled_height - allocation.height as f64;
            let max_value = adj.get_upper() - adj.get_page_size();
            let value = adj.get_value();

            let offset_y = -1.0 * value * max_offset_y / max_value;
            let mut offset = drawer.offset.borrow_mut();
            offset.1 = offset_y;
            drawer.area.queue_draw()
        });
    }
    pub fn set_filter(&self, filter: Filter) {
        self.filter.replace(filter);
    }

    pub fn get_filter(&self) -> Filter {
        self.filter.borrow().clone()
    }

    pub fn save_to(&self, file: PathBuf) {
        match File::create(file.to_str().unwrap()) {
            Ok(mut buffer) => {
                let surface = self.surface.borrow().clone();
                surface.write_to_png(&mut buffer).expect("Failed to save file");
            }
            Err(err) => error!("Failed to save the PNG {}", err),
        }
    }

    pub fn set_zoom(&self, zoom: f64) {
        self.zoom.replace(zoom);
        self.area.queue_draw();
    }

    pub fn get_zoom(&self) -> f64 {
        self.zoom.borrow().clone()
    }

    pub fn undo(&self) {
        let mut history = self.history.borrow_mut();
        if history.len() == 1 {
            // don't clean everything
            return;
        }
        if let Some(dropped_pixbuf) = history.pop() {; // drop the latest element
            self.future.borrow_mut().push(dropped_pixbuf);
        }
        if let Some(pixbuf) = history.last() {
            let surface = pixbuf.create_surface(self.get_zoom() as i32, None).unwrap();
            self.surface.replace(surface);
            self.area.queue_draw();
        }
    }
    pub fn redo(&self) {
        let mut future = self.future.borrow_mut();
        if let Some(pixbuf) = future.pop() {
            let surface = pixbuf.create_surface(self.get_zoom() as i32, None).unwrap();
            self.history.borrow_mut().push(pixbuf); // push back to history
            self.surface.replace(surface);
            self.area.queue_draw();
        }
    }

    pub fn get_pixbuf(&self) -> gdk_pixbuf::Pixbuf {
        let latest_item = self.history.borrow();
        latest_item.last().unwrap().clone()
    }

    pub fn paste(&self, s: Rc<Self>) {
        // Await for any pixbuf content
        let display = gdk::Display::get_default().unwrap();
        let clipboard = gtk::Clipboard::get_default(&display).unwrap();
        if clipboard.wait_is_image_available() {
            if let Some(pixbuf) = clipboard.wait_for_image() {
                s.load_pixbuf(pixbuf.clone());
                s.area.queue_draw();
            }
        }
        self.sender.send(Action::ViewPhoto).unwrap();
    }

    pub fn copy(&self) {
        if self.history.borrow().len() != 0 {
            let display = gdk::Display::get_default().unwrap();
            let clipboard = gtk::Clipboard::get_default(&display).unwrap();

            let surface = self.surface.borrow().clone();
            let pixbuf = gdk::pixbuf_get_from_surface(&surface, 0, 0, surface.get_width(), surface.get_height()).unwrap();

            clipboard.set_image(&pixbuf);
        }
    }

    fn setup_signals(&self, s: Rc<Self>) {
        self.area.add_events(
            gdk::EventMask::BUTTON_PRESS_MASK | gdk::EventMask::BUTTON_RELEASE_MASK | gdk::EventMask::ENTER_NOTIFY_MASK | gdk::EventMask::POINTER_MOTION_MASK | gdk::EventMask::LEAVE_NOTIFY_MASK,
        );

        let drawer = s.clone();
        let hadj: gtk::Adjustment = self.builder.get_object("hadjustment").unwrap();
        let vadj: gtk::Adjustment = self.builder.get_object("vadjustment").unwrap();
        self.area.connect_draw(move |_, ctx| {
            let surface = drawer.surface.borrow().clone();
            let offset = drawer.offset.borrow().clone();
            let zoom = drawer.zoom.borrow().clone();

            let alloc = drawer.area.get_allocation();
            let scaled_width = surface.get_width() as f64 * zoom;
            let scaled_height = surface.get_height() as f64 * zoom;

            let page_size_x = alloc.width as f64 / scaled_width;
            hadj.set_lower(0.0);
            hadj.set_page_size(page_size_x);
            hadj.set_upper(1.0);
            hadj.set_step_increment(0.1);
            hadj.set_page_increment(0.5);

            let page_size_y = alloc.height as f64 / scaled_height;
            vadj.set_lower(0.0);
            vadj.set_page_size(page_size_y);
            vadj.set_upper(1.0);
            vadj.set_step_increment(0.1);
            vadj.set_page_increment(0.5);

            ctx.scale(zoom, zoom);
            ctx.translate(offset.0, offset.1);
            ctx.set_source_surface(&surface, 0.0, 0.0);
            ctx.paint();
            gtk::Inhibit(false)
        });

        // Selection
        let drawer = s.clone();
        self.area.connect_button_press_event(move |_, event| {
            let position = event.get_position();
            drawer.select_position.replace(position);
            gtk::Inhibit(false)
        });

        let drawer = s.clone();
        self.area.connect_button_release_event(move |_, event| {
            let start_position = drawer.select_position.borrow().clone();
            let end_position = event.get_position();

            drawer.apply_filter(start_position, end_position);
            drawer.select_position.replace((0.0, 0.0));

            gtk::Inhibit(false)
        });
        self.area.connect_enter_notify_event(move |widget, _| {
            let display = gdk::Display::get_default().unwrap();
            if let Some(cursor) = gdk::Cursor::new_from_name(&display, "cell") {
                let top_level = widget.get_window().unwrap();
                top_level.set_cursor(Some(&cursor));
            }
            gtk::Inhibit(false)
        });

        self.area.connect_leave_notify_event(move |widget, _| {
            let display = gdk::Display::get_default().unwrap();
            if let Some(cursor) = gdk::Cursor::new_from_name(&display, "default") {
                let top_level = widget.get_window().unwrap();
                top_level.set_cursor(Some(&cursor));
            }
            gtk::Inhibit(false)
        });
        let drawer = s.clone();
        self.area.connect_motion_notify_event(move |widget, event| {
            if drawer.select_position.borrow().clone() != (0.0, 0.0) {
                let pixbuf = drawer.get_pixbuf();
                let surface = pixbuf.create_surface(drawer.get_zoom() as i32, None).unwrap();
                let position = event.get_position();

                let start_position = drawer.select_position.borrow().clone();
                let ctx = cairo::Context::new(&surface);
                let scale = drawer.get_zoom();
                ctx.set_operator(cairo::Operator::Over);
                ctx.set_line_width(0.3);
                ctx.rectangle(
                    start_position.0 / scale,
                    start_position.1 / scale,
                    (position.0 - start_position.0) / scale,
                    (position.1 - start_position.1) / scale,
                );

                ctx.set_source_rgb(36.0 / 255.0, 31.0 / 255.0, 49.0 / 255.0);
                ctx.stroke();
                widget.queue_draw();
                drawer.surface.replace(surface);
            }
            gtk::Inhibit(false)
        });
    }

    fn apply_filter(&self, start_position: (f64, f64), end_position: (f64, f64)) {
        let surface = self.surface.borrow().clone();
        let scale = self.get_zoom();
        match self.get_filter() {
            Filter::Filled => {
                let ctx = cairo::Context::new(&surface);
                ctx.set_operator(cairo::Operator::Over);
                ctx.set_source_rgb(0.0, 0.0, 0.0);
                ctx.rectangle(
                    start_position.0 / scale,
                    start_position.1 / scale,
                    (end_position.0 - start_position.0) / scale,
                    (end_position.1 - start_position.1) / scale,
                );

                ctx.fill();
                self.area.queue_draw();
            }
            Filter::Blur => {
                let ctx = cairo::Context::new(&surface);
                ctx.set_operator(cairo::Operator::Saturate);
                self.area.queue_draw();
            }
        }
        // Save state
        let pixbuf = gdk::pixbuf_get_from_surface(&surface, 0, 0, surface.get_width(), surface.get_height()).unwrap();
        self.history.borrow_mut().push(pixbuf);
    }

    pub fn draw_from_file(&self, filepath: &Path) {
        let pixbuf = gdk_pixbuf::Pixbuf::new_from_file(filepath).unwrap();
        self.load_pixbuf(pixbuf);
    }

    fn load_pixbuf(&self, pixbuf: gdk_pixbuf::Pixbuf) {
        self.history.replace(Vec::new()); // clear the history
        self.future.replace(Vec::new()); // clear the future

        let new_surface = pixbuf.create_surface(self.get_zoom() as i32, None).unwrap();
        self.surface.replace(new_surface);
        let ctx = cairo::Context::new(&self.surface.borrow());

        ctx.set_source_pixbuf(&pixbuf, 0.0, 0.0);
        self.history.borrow_mut().push(pixbuf);
        ctx.paint();
        ctx.fill();
    }
}
