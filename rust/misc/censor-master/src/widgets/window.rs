use crate::application::Action;

use crate::config::{APP_ID, PROFILE};
use crate::widgets::drawer::{Drawer, Filter};
use crate::window_state;
use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use std::cell::RefCell;
use std::path::Path;
use std::rc::Rc;

#[derive(PartialEq, Debug)]
pub enum View {
    Empty,
    Photo,
}
const TARGET_URI: u32 = 1;

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    sender: Sender<Action>,
    builder: gtk::Builder,
    drawer: RefCell<Rc<Drawer>>,
    settings: RefCell<gio::Settings>,
}

impl Window {
    pub fn new(sender: Sender<Action>) -> Self {
        let settings = gio::Settings::new(APP_ID);
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Censor/window.ui");
        let widget: gtk::ApplicationWindow = builder.get_object("window").unwrap();
        let drawer = RefCell::new(Drawer::new(sender.clone()));

        if PROFILE == "Devel" {
            widget.get_style_context().add_class("devel");
        }
        let window = Window {
            widget,
            sender,
            builder,
            settings: RefCell::new(settings),
            drawer,
        };

        window.init();
        window.setup_actions();
        window.set_view(View::Empty);
        window
    }

    pub fn set_view(&self, view: View) {
        let main_stack: gtk::Stack = self.builder.get_object("main_stack").unwrap();
        let headerbar_stack: gtk::Stack = self.builder.get_object("headerbar_stack").unwrap();
        match view {
            View::Empty => {
                headerbar_stack.set_visible_child_name("empty");
                main_stack.set_visible_child_name("empty");
            }
            View::Photo => {
                headerbar_stack.set_visible_child_name("photo");
                main_stack.set_visible_child_name("photo");
            }
        }

        let editor_actions = vec!["zoom", "effect", "save"];
        let actions_group = self.widget.get_action_group("window").unwrap();
        let actions_group = actions_group.downcast::<gio::SimpleActionGroup>().unwrap();
        for action_name in editor_actions.iter() {
            if let Some(action) = actions_group.lookup_action(action_name) {
                let action = action.downcast::<gio::SimpleAction>().unwrap();
                action.set_enabled(view == View::Photo);
                if action_name == &"zoom" {
                    action.activate(Some(&glib::Variant::from("")));
                }
            }
        }
    }

    pub fn set_open_file(&self, open_file: String) {
        let subtitle: gtk::Label = self.builder.get_object("subtitle").unwrap();
        let path = Path::new(&open_file);

        subtitle.set_text(path.file_name().and_then(|s| s.to_str()).unwrap());
        self.drawer.borrow_mut().draw_from_file(&path);
    }

    fn init(&self) {
        // load latest window state
        let settings = self.settings.borrow().clone();
        window_state::load(&self.widget, &settings);
        // save window state on delete event
        self.widget.connect_delete_event(move |window, _| {
            window_state::save(&window, &settings);
            Inhibit(false)
        });

        let drawing_container: gtk::Box = self.builder.get_object("drawing_container").unwrap();
        drawing_container.pack_start(&self.drawer.borrow_mut().widget, true, true, 0);

        let main_stack: gtk::Stack = self.builder.get_object("main_stack").unwrap();

        let uri_target = gtk::TargetEntry::new("text/uri-list", gtk::TargetFlags::OTHER_APP, TARGET_URI);
        main_stack.drag_dest_set(gtk::DestDefaults::ALL, &[uri_target], gdk::DragAction::COPY);

        let sender = self.sender.clone();
        main_stack.connect_drag_data_received(move |_, _, _, _, data, target_id, _| {
            if target_id == TARGET_URI {
                let uris = data.get_uris();
                if let Some(dropped_uri) = uris.get(0) {
                    let file = gio::File::new_for_uri(dropped_uri.as_str());
                    let filepath = file.get_path().unwrap().to_str().unwrap().to_string();
                    sender.send(Action::LoadFile(filepath)).unwrap();
                }
            }
        });
    }

    fn setup_actions(&self) {
        let actions = gio::SimpleActionGroup::new();
        let open = gio::SimpleAction::new("open", None);
        let sender = self.sender.clone();
        open.connect_activate(move |_, _| {
            sender.send(Action::OpenFile).unwrap();
        });
        actions.add_action(&open);

        // Undo
        let undo = gio::SimpleAction::new("undo", None);
        let drawer = self.drawer.clone();
        undo.connect_activate(move |_, _| {
            drawer.borrow().undo();
        });
        actions.add_action(&undo);
        // Redo
        let redo = gio::SimpleAction::new("redo", None);
        let drawer = self.drawer.clone();
        redo.connect_activate(move |_, _| {
            drawer.borrow().redo();
        });
        actions.add_action(&redo);

        // Copy
        let copy = gio::SimpleAction::new("copy", None);
        let drawer = self.drawer.clone();
        copy.connect_activate(move |_, _| {
            drawer.borrow().copy();
        });
        actions.add_action(&copy);
        // Paste
        let paste = gio::SimpleAction::new("paste", None);
        let drawer = self.drawer.clone();
        paste.connect_activate(move |_, _| {
            drawer.borrow().paste(drawer.borrow().clone());
        });
        actions.add_action(&paste);
        // save
        let save = gio::SimpleAction::new("save", None);
        let drawer = self.drawer.clone();
        let parent = self.widget.clone();
        save.connect_activate(move |_, _| {
            let dialog = gtk::FileChooserNative::new(Some("Save File"), Some(&parent), gtk::FileChooserAction::Save, Some("Save"), Some("Cancel"));

            if gtk::ResponseType::from(dialog.run()) == gtk::ResponseType::Accept {
                let file = dialog.get_filename().unwrap();
                drawer.borrow().save_to(file);
            };
            dialog.destroy();
        });
        actions.add_action(&save);

        let blur_btn: gtk::ToggleButton = self.builder.get_object("blur_btn").unwrap();
        let filled_btn: gtk::ToggleButton = self.builder.get_object("filled_btn").unwrap();

        blur_btn.bind_property("active", &filled_btn, "active").flags(glib::BindingFlags::INVERT_BOOLEAN).build();
        filled_btn.bind_property("active", &blur_btn, "active").flags(glib::BindingFlags::INVERT_BOOLEAN).build();

        blur_btn.set_action_target_value(Some(&glib::Variant::from("blur")));
        filled_btn.set_action_target_value(Some(&glib::Variant::from("filled")));

        let drawer = self.drawer.clone();
        let effect = gio::SimpleAction::new("effect", Some(glib::VariantTy::new("s").unwrap()));
        effect.connect_activate(move |_, data| match data.unwrap().get_str() {
            Some(data) => {
                let filter = Filter::from_str(data);
                drawer.borrow_mut().set_filter(filter);
            }
            _ => {
                blur_btn.set_active(true);
                drawer.borrow_mut().set_filter(Filter::Blur);
            }
        });
        actions.add_action(&effect);

        let zoom_in_btn: gtk::Button = self.builder.get_object("zoom_in_btn").unwrap();
        let zoom_out_btn: gtk::Button = self.builder.get_object("zoom_out_btn").unwrap();
        let zoom_reset_btn: gtk::Button = self.builder.get_object("zoom_reset_btn").unwrap();
        let zoom_level: gtk::Label = self.builder.get_object("zoom_level").unwrap();
        let drawer = self.drawer.clone();

        zoom_in_btn.set_action_target_value(Some(&glib::Variant::from("in")));
        zoom_out_btn.set_action_target_value(Some(&glib::Variant::from("out")));
        zoom_reset_btn.set_action_target_value(Some(&glib::Variant::from("")));

        let zoom = gio::SimpleAction::new("zoom", Some(glib::VariantTy::new("s").unwrap()));
        zoom.connect_activate(move |_, data| match data.unwrap().get_str() {
            Some(zoom) => {
                let mut new_zoom = drawer.borrow().get_zoom();
                match zoom {
                    "in" => new_zoom = new_zoom + 0.1,
                    "out" => new_zoom = new_zoom - 0.1,
                    _ => new_zoom = 1.0,
                }
                zoom_out_btn.set_sensitive(new_zoom >= 0.1);
                zoom_in_btn.set_sensitive(new_zoom <= 2.0);
                zoom_reset_btn.set_sensitive(new_zoom != 1.0);
                zoom_level.set_text(&format!("{:.1}%", new_zoom * 100.0));
                if 0.1 <= new_zoom && new_zoom <= 2.0 {
                    drawer.borrow_mut().set_zoom(new_zoom);
                }
            }
            _ => {
                drawer.borrow_mut().set_zoom(1.0);
            }
        });
        actions.add_action(&zoom);

        self.widget.insert_action_group("window", Some(&actions));
    }
}
