<a href="https://flathub.org/apps/details/com.belmoussaoui.Censor">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Censor

<img src="https://gitlab.gnome.org/belmoussaoui/censor/raw/master/data/icons/com.belmoussaoui.Censor.svg" width="128" height="128" />

Censor personal and private information in images.

## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Censor
To build the development version of Censor and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](/code-of-conduct.md) when participating in project
spaces.
