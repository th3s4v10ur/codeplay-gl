fn main() {

    let x: i32 = 1;

    let y = 3; // i32

    let mut z = 10; // mut i32

    println!("Before reassignment: x is {}, y is {}, and z is {}", x, y, z);

    z = 12; // This is valid

    //x = 7; This is invalid and won't compile

    println!("After reassignment: x is {}, y is {}, and z is {}", x, y, z);

    let mut greeting = "World";

    println!("Hello {}!", greeting);

    greeting = "lil_firelord";

    println!("Hello {}!", greeting);

}
