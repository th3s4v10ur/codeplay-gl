## Rust learning files

### rust-book-web-examples folder

Examples while learning the rust language from The Rust Programming Language Book and web.

### code-wars-kata folder

code for the katas

### exercism-track folder

code for tasks
