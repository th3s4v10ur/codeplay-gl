use std::env;
use std::fs;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    // parse the arguments and copy into config struct
    let config = Config::new(&args).unwrap_or_else(|err| {
        // closure to output message and terminate program
        println!("Problem parsing arguments: {}", err);
        process::exit(1);
    });
    // print arguments out
    println!("In file {}", config.filename);
    println!("Searching for {}", config.query);
    run(config);
}

fn run(config: Config) {
    let contents =
        fs::read_to_string(config.filename).expect("Something went wrong reading the file");
    println!("With text:\n{}", contents);
}

// hold arguments in structure
struct Config {
    filename: String,
    query: String,
}
// implement new config structure
impl Config {
    fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 3 {
            // 0 path and command 1 filename 2 query < 3
            return Err("not enough arguments passed in");
        }
        // create copies of the strings so the struct owns the strings
        let filename = args[1].clone();
        let query = args[2].clone();

        Ok(Config { filename, query })
    }
}
