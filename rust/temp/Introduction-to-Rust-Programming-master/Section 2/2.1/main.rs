mod robot1{
//private function (by default)
fn say_hello(){
	println!("Hello");
}

//public function
pub fn say_hi(){
	println!("Hi");
}
}

fn main () {

//robot1::say_hello();
//robot1::say_hi();

//use robot1::say_hi;
//say_hi();

use robot1::say_hi as hi;
hi();

} 