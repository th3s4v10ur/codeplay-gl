extern crate websocket;
extern crate hyper;

use std::thread;
use std::io::Write;
use websocket::{Message, OwnedMessage};
use websocket::sync::Server;
use hyper::Server as HttpServer;
use hyper::net::Fresh;
use hyper::server::request::Request;
use hyper::server::response::Response;

// The HTTP server handler
fn http_handler(_: Request, response: Response<Fresh>) {
	let mut response = response.start().unwrap();
	// Send a client webpage
	response.write_all(b"
<!DOCTYPE html>
<html>
<head>
<meta charset=\"utf-8\">
<title>WebSocket Test</title>
</head>
<body>
<script>var socket=new WebSocket(\"ws://127.0.0.1:2794\", \"rust-websocket\");socket.onmessage=function (event){var received=document.getElementById(\"received\");var br=document.createElement(\"BR\");var text=document.createTextNode(event.data);received.appendChild(br);received.appendChild(text);};function send(element){var input=document.getElementById(element);socket.send(input.value);input.value=\"\";}
</script>
<p id=\"received\"><strong>Received Messages:</strong>
</p>
<form onsubmit=\"send('message'); return false\">
<input type=\"text\" id=\"message\">
<input type=\"submit\"value=\"Send\">
</form>
</body>
</html>
").unwrap();
response.end().unwrap();
}


fn main() {
	// Start listening for http connections
	thread::spawn(move || {
		              let http_server = HttpServer::http("127.0.0.1:8080").unwrap();
		              http_server.handle(http_handler).unwrap();
		             });

	// Start listening for WebSocket connections
	let ws_server = Server::bind("127.0.0.1:2794").unwrap();

	for connection in ws_server.filter_map(Result::ok) {
		// Spawn a new thread for each connection.
		thread::spawn(move || {
			if !connection.protocols().contains(&"rust-websocket".to_string()) {
				connection.reject().unwrap();
				return;
			}

			let mut client = connection.use_protocol("rust-websocket").accept().unwrap();

			let ip = client.peer_addr().unwrap();

			println!("Connection from {}", ip);

			let message = Message::text("Hello");
			client.send_message(&message).unwrap();

			let (mut receiver, mut sender) = client.split().unwrap();

			for message in receiver.incoming_messages() {
				let message = message.unwrap();

				match message {
					OwnedMessage::Close(_) => {
						let message = Message::close();
						sender.send_message(&message).unwrap();
						println!("Client {} disconnected", ip);
						return;
					}
					OwnedMessage::Ping(data) => {
						let message = Message::pong(data);
						sender.send_message(&message).unwrap();
					}
					_ => sender.send_message(&message).unwrap(),
				}
			}
		});
	}
}