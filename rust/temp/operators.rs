fn main() {
    let a = 20;
    let b = a + 1;
    let c = a - 2;
    let d = a * 3;
    let e = a / 4;
    let f = a % 3;
    println!("a: {}, b: {0}+1={}, c: {0}-2={}, d: {0}*3={}, e: {0}/4={}, f: {0}%3={}", a, b, c, d, e, f);
}
