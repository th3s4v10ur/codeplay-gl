/* Program to demonstrate variable decleration*/
void main() {
  
  // Declaring Strings
  String name = "John";
  String location = "World";
  
  // Delcaring Numbers - integers or doubles
  int age = 30;
  double balance = 1394.5;
  
  // Boolean values - true or false values
  bool isAlive = true;
  bool isDead = false;
  
  // Outputting declared varibles 
  print(name);
  print(location);
  print("\n"); // Printing out a new line
  print(age);
  print(balance);
  print("\n");
  print(isAlive);
  print(isDead);
   
}

/* Expected Output:

John
World

30
1394.5

true
false

*/
