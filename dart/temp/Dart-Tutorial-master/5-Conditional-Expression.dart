/* Program which shows use of cobditional expressions in context of finding the cheaper ingredient*/
void main() {
   
  // Declaring variables
  double salt = 1.50;
  double sugar = 2.25; 
  String cheaper; // String to hold the name of the cheaper item
    
  // Method 1 -> If-Else Statement
  if (salt > sugar) {
      cheaper = "sugar";
  }else {
      cheaper = "salt";
  }
  
  print("Cheaper ingredient is $cheaper");
  
  
  // Method 2 -> use conditional expressions in form -> variable = condition ? expression1 : expression2
  cheaper = salt > sugar ? cheaper = "sugar" : cheaper = "salt";
  print("Cheaper ingredient is $cheaper");
  
}

/* Expected Output:
  
Cheaper ingredient is salt
Cheaper ingredient is salt

*/