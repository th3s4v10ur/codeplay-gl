/* Demonstrating how to make custom exception class */ 
void main() {
  int money = -1;
  
  // Using exception handling to safely withdraw money
  try { 
      withdrawMoney(money);
  } catch(e){
      print(e.errorMessage());
  }
}

// Create class for the what to do if exception is caught
class WithdrawException implements Exception{
  String errorMessage(){
    return("Error: You cannot withdraw a negative amount!");
  }
}

// Function which withdraws money
void withdrawMoney(int money) {
  if (money < 0) {
    throw new WithdrawException(); 
  }  
}

/* Expected Output: 

Error: You cannot withdraw a negative amount!

*/