/* Program to demonstrate Exception Handling in Dart - used to handle when app crashes etc */

void main() {
  
  // Case 1: Catch clause with Exception -> When you do not know the Exception
    try {
      int result = 20 ~/0; // Dividing zero
      print("Result is $result");
  } catch(e){
      print("Exception Caught: $e"); // Outputting the exception
  }
  
  print("\n");
  
  // Case 2: Catch clause with exception and stack -> use stack trace to see which      events occurred before exception thrown
    try {
      int result = 20 ~/0; // Dividing zero
      print("Result is $result");
  } catch(e, s){
      print("Exception Caught: $e"); // Outputting the exception
      print("Stack Trace:\n $s"); // Outputting the stack trace
  }
  
  print("\n");
  
  // Case 3: Finally is always used regardless of if there is an exception or not
    try {
      int result = 20 ~/4; // Dividing zero
      print("Result is $result");
  } catch(e){
      print("Exception Caught: $e"); // Outputting the exception
  } finally {
      print("Finally statement executed");   
  }
  
}

/* Expected Output (depends on IDE - I used Dartpad.dartlang.org)
  
Exception Caught: Unsupported operation: Result of truncating division is Infinity: 20 ~/ 0


Exception Caught: Unsupported operation: Result of truncating division is Infinity: 20 ~/ 0
Stack Trace:
 Unsupported operation: Result of truncating division is Infinity: 20 ~/ 0
    at Object.wrapException (<anonymous>:355:17)
    at JSNumber._tdivSlow$1 (<anonymous>:1555:15)
    at JSNumber.$tdiv (<anonymous>:1544:19)
    at main (<anonymous>:1449:35)
    at dartMainRunner (<anonymous>:18:5)
    at <anonymous>:1936:7
    at <anonymous>:1922:7
    at dartProgram (<anonymous>:1933:5)
    at <anonymous>:1940:3
    at replaceJavaScript (https://dartpad.dartlang.org/scripts/frame.html:35:27)


Result is 5
Finally statement executed

*/