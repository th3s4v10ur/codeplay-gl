/* Demonstrating use of Classes in Dart */

 void main() {
   
   // Creating objects (instances of classes)
   Customer customer1 = new Customer("Adam", 30.42);
   Customer customer2 = new Customer("Isaac", 40.75);
   
   // Customer 1
   print("CUSTOMER DETAILS");
   customer1.buyItem(); // Customer 1 buys an item 
   print("${customer1.name} spent £${customer1.spent}"); // Outputting details
   customer1.returnItem(); // Customer 1 returns an item 
   
   // Customer 2
   print("\nCustomer Details");
   customer2.buyItem(); // Customer 2 buys an item
   print("${customer2.name} spent £${customer2.spent}");
   customer2.returnItem(); // Customer 2 returns an item
   
 }

// Class = blueprint for object creation
class Customer{
  
  // Declaring attributes 
  String name;
  double spent;
  
  // Constructor function to give the customer a name and assign the total they spent
  Customer(String name, double spent){
    this.name = getName(name);
    this.spent = setSpent(spent);
  }
  
  // Get Method to get the name of the Customer
  String getName(String name) => name;
  
  // Set Method - set spent total to include an extra store cost 
  double setSpent(double spent) => spent + 2.50;

  // Function when a customer buys an item (can use big arrow)
  void buyItem() => print("Customer has bought an item");
  
  // Function when a customer returns an item (can use big arrow)
  void returnItem() => print("Customer has returned an item");

   
}

/* Expected Output:


CUSTOMER DETAILS
Customer has bought an item
Adam spent £32.92
Customer has returned an item

Customer Details
Customer has bought an item
Isaac spent £43.25
Customer has returned an item

*/