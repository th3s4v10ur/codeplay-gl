/* Program which demonstrates how to write functions in Dart */
void main() {
 
 int length = 4;
 int width = 5;
  
 // Calling integer function and outputting area of rectangle 
 int area = findArea(length, width); 
 int area2 = findArea2(length, width); 
 print("Area of Rectangle is $area");
 print("Area of Rectangle is $area2");
  
 String name = "Adam";
  
 // Printing out value returned by String method 
 print(firstLetter("$name")); 
 print(firstLetter2("$name"));
  
 // Printing out the value of the boolean value
 print(isTrue("$name"));

 // Calling the void functions  
 outputName(name); 
 outputName2(name);  
}


// Integer returning function which passes in values of length and width to find area of rectangle
int findArea(int l,int w) {
  return l * w;
}
// Above function can be written in a more efficient way as below -> use big fat arrow to replace 'return' line. Use this only if the function has a single return line as above
int findArea2(int l, int w) => l * w;


// String funtion which returns the first letter of one's name
String firstLetter(String name) {
    return name.substring(0, 1);
}
// More efficient way of returning the first letter of one's name as there is only one expression (i.e. only one return line in the method)
String firstLetter2(String name) => name.substring(0, 1);


// Boolean function which verifies whether one has a name under 5 characters
bool isTrue(String name){
  if (name.length < 5) {
     return true;
  }
  else{
     return false;
  }
}
/* Note as there is more than one expression in this boolean method, there is no need to use the big fat arrow*/


// Void function which outputs Hello (name)
void outputName(String name){
  print("Hello $name");
}
// More efficient implementation of above
void outputName2(String name) => print("Hello $name");

/* Expected Result:
 
Area of Rectangle is 20
Area of Rectangle is 20
A
A
true
Hello Adam
Hello Adam
 
 */