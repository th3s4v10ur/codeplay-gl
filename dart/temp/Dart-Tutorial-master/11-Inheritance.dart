/* Demonstrating use of Inheritance in Dart */

 void main() {
   
   Dog dog = new Dog();
   dog.breed = "Labrador";
   dog.bark();
   // Accessed from superclass
   dog.color = "Black";
   dog.eat();
   
   Cat cat = new Cat();
   cat.age = 4;
   cat.meow();
   // Accessed from superclass
   cat.color = "White"; 
   cat.eat(); 
 }

// Superclass where attributes and functions can be accessed by sub-classes
class Animal {
   String color; 
  
   void eat() {
     print("Animal is eating");
   }
  
}

// Subclass which extends Supercass
class Dog extends Animal{
    String breed;
    
    void bark() {
      print("Dog is barking");
    }
      
}

// Subclass which extends Supercass
class Cat extends Animal{
    int age;
    
    void meow() {
      print("Cat meow");
    }
      
}

   


/* Expected Output:


Dog is barking
Animal is eating
Cat meow
Animal is eating

*/
