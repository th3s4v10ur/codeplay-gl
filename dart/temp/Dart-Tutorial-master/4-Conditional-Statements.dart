/* Program which shows off Conditional Statements in context of a football striker*/
void main() {
  
  int goalsScored = 13;
  
  // Statements used to perform an action based on fulfillment of a condition
  if (goalsScored <= 5) {
      print("You had a poor season");
  } else if(goalsScored > 5 && goalsScored <= 10){
      print ("You had a mediocre season");
  } else if(goalsScored > 10 && goalsScored <=20){
      print("You had a good season");
  } else{
      print("You had an amazing season!");
  }
 
}

/* Expected Output:
 
You had a good season 

*/