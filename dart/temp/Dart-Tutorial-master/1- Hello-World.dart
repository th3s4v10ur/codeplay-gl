/* Using print function for Hello World */
void main() {
    print("Hello World!");
}

/* Expected Output: 
Hello World
*/
