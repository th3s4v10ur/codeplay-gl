/* Program which shows use of different types of loops */
void main() {
  
  // Declaring variables
  int sum = 0;
  
  /* Using a for loop to add 5 to a counting sum for 4 iterations */
  for (int i = 0; i < 4; i++){
      sum += 5;
      print("Sum value: $sum");
  }
  print("Sum value after Loop: $sum");
  print("\n");
  
  /* Using a while loop to add 10 to a counting sum for 4 iterations */
  int j = 0; // Initializing counter
  sum = 0; // Can use same variable as before if old value isn't needed
  while (j < 4){
      sum += 10;
      print("Sum value: $sum");
      j++; // Incrementing counter
  }
  print("Sum value after Loop: $sum");
  print("\n");
  
  /* Using a do-while loop to multiply numbers 0 to 5 by 9 and adding the result to a    counting sum variable. */
  int k = 0; // Initializing counter
  sum = 0;
  do{
      sum += k * 9;
      print("Sum value: $sum");
      k++; // Incrementing counter
  } while (k < 6);
  print("Sum value after Loop: $sum");
  print("\n");
  
  /* More syntax (optional): using break or continue 
   * break = used to break out of a loop
   * continue = used to tell loop to iterate to next step
   */
   
  // Example of break statement - loop breaks if iteration counter reaches 1
  for (int i = 0; i < 3; i++) {
    if (i == 1) {
      print("Loop has broken");
      break;
    }
    print("Value of counter $i");
  }
  print("\n");
  
   // Example of continue statement - loop continues to next step if counter reaches 3
  for (int i = 0; i < 5; i++) {
    if (i == 3) {
      continue;
    }
     print("Value of counter $i");
  }
  
}

/* Expected Result:

Sum value: 5
Sum value: 10
Sum value: 15
Sum value: 20
Sum value after Loop: 20


Sum value: 10
Sum value: 20
Sum value: 30
Sum value: 40
Sum value after Loop: 40


Sum value: 0
Sum value: 9
Sum value: 27
Sum value: 54
Sum value: 90
Sum value: 135
Sum value after Loop: 135


Value of counter 0
Loop has broken


Value of counter 0
Value of counter 1
Value of counter 2
Value of counter 4

*/