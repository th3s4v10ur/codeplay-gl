/* Program to show different String types, how to combine Strings with other data types and how to use some String functions*/
void main() {
  /* Different String types - using single or double quotation marks */
  String singleQuote = 'It\'s hot';
  String doubleQuote = "It's hot";
  
  print(singleQuote);
  print(doubleQuote);
  
  /* Combinining Strings with other Strings/variables - 2 Methods */
  
  // Declaring Variables
  String event = "Rain";
  double chance = 0.75;
  
  // Method 1: Use plus symbol to combine variables
  print("The chance of " + event + " today is " + chance.toString() + "%");
  /* NOTE: To print out a String with another variable such as a int, double or a boolean   value, you need to use the .toString() 
  method on that variable as shown above*/
  
  // Method 2 (Easier): Use '$' symbol before variable as shown below */ 
  print("The chance of $event today is $chance%");
  
  
  /* Other functions for Strings */
  String word = "Impossible";
  print(word.length); // Prints the length of the String
  print(word.toLowerCase()); //Prints String in all lower case letters
  print(word.toUpperCase()); // Prints String in all upper case letters  
}

/* Expected Output:

It's hot
It's hot
The chance of Rain today is 0.75%
The chance of Rain today is 0.75%
10
impossible
IMPOSSIBLE

*/
