import 'dart:io';

// Simple logic of a BMI (body mass index) calculator
// Without using static typing

bmi() {
  stdout.write('Type the weight (kg): ');
  var textWeight = stdin.readLineSync();
  stdout.write('Type the height (cm): ');
  var textHeight = stdin.readLineSync();

  var weight = double.parse(textWeight), height = double.parse(textHeight);
  var bmi = _bmiCalculation(weight, height);

  _printResult(bmi);
}

_bmiCalculation(double weight, double height) {
  return weight / (height * height);
}

_printResult(bmi) {
  print('========================================');

  if (bmi < 18.5) {
    print('Underweight...');
  } else if (bmi > 18.5 && bmi < 24.9) {
    print('Normal weight, great!');
  } else if (bmi > 24.5 && bmi < 29.9) {
    print('Warning, overweight!!!');
  } else if (bmi > 30.0 && bmi < 34.9) {
    print('Obesity grade one...');
  } else if (bmi > 35.0 && bmi < 39.9) {
    print('Obesity grade two...');
  } else {
    print('Obesity grade three!!!');
  }
}
