
/* 
 * main is the entry point of all Dart programs
 */
void main() {
  // Print hello 5 times
  for (int i = 0; i < 5; i++) {
    print('hello ${i + 1}'); // print statement
  }
}