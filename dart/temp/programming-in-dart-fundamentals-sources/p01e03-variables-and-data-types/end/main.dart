void main() {
  var myAge = 35;
  print(myAge);

  int yourAge = 42;
  print(yourAge);

  var pi = 3.14;
  double c = 299792458;
  print(pi);
  print(c);

  dynamic numberOfKittens;
  numberOfKittens = 'There are no kittens!';
  print(numberOfKittens);
  numberOfKittens = 8.0;
  print(numberOfKittens);

  numberOfKittens = 0;
  bool areThereKittens = false;
  print(areThereKittens);
  numberOfKittens = 8;
  areThereKittens = true;
  print(areThereKittens);

  int numberOfPuppies;
  print(numberOfPuppies);

  const speedOfLight = 299792458;
  print(speedOfLight);

  final planet = 'Jupiter';
  final String moon = 'Europa';

  print('$planet has a moon $moon');

  const avogadro = 6.023e23;
  print(avogadro);

  var numbers = [1, 2, 3];
  numbers.add(4);
  print(numbers);

  final moreNumbers = const [7, 8, 9];
//   moreNumbers = [4, 5, 6]; // error since moreNumbers is final
//   moreNumbers.add(10); // exception since list is const
  print(moreNumbers);
}
