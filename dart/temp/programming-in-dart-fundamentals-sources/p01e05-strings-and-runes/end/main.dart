
void main() {
  var firstName = 'Albert';
  String lastName = "Einstein";

  var physicist = "$firstName $lastName";
  print(physicist);

  var quote = 'If you can\'t'  + ' explain it simply\n'
    "you don't understand it well enough.";
  print(quote);

  var rawString = r"If you can't explain it simply\nyou don't understand it well enough.";
  print(rawString);

  const String myNameIs = "My name is ";
  print(myNameIs);
//   myNameIs = 'Joe'; // error compile-time constant

  var apples = 3;
  var oranges = 5;
  var totalFruit = "Total amount of fruit is ${apples + oranges} pieces.";
  print(totalFruit);
  print('${totalFruit.length}');

  apples = int.parse('2');
  double bananas = double.parse('2.5');
  print('$apples and $bananas');

  var smiley = '\u263a';
  print(smiley);

  var womanScientist = '\u{1f469}';
  print(womanScientist);
  print(womanScientist.codeUnits);
  print(womanScientist.runes);

  Runes ou812 = Runes('\u{1f62f}\u{1f447}\u{1f963}\u{1f550}\u{0032}');
  print(ou812);
  print(String.fromCharCodes(ou812));
}
