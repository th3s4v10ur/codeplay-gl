
void main() {
  bool test;
  test = 12 > 5;
  print(test);

var str = 'abc';
  if(str) {
    print('String is not empty');

//    Error: A value of type 'dart.core::String' can't be assigned to a variable of type 'dart.core::bool'.
//  Try changing the type of the left hand side, or casting the right hand side to 'dart.core::bool'.
  } else {
    print('Empty String');
  }

}
