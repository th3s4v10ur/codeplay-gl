void main(List<String> args) {
  // break and continue keywords
  String search = "ogasule1@gmail.com";
  bool found = false;
  List<String> emails = ["empireandz@gmail.com","nobledaniels2002@gmail.com","tobajoshua37@yahoo.com","segunejisola@gmail.com",
  "ogasule001@gmail.com","p.o.adebayo218@gmail.com","praiseadebayo218@yahoo.com"];
  for (String email in emails) {
    if (email == search){
    print("$email Found!");
    found = true;
    break; // at this point, stop searching the list
    }
  }
  if(found == false){
    print("$search not Found!");
  }
  print("\n");
  List<int> numbers = [23,43,10,19,92,54,39,40];
  for(int number in numbers){
    if(number>40){
      continue; // if the number is greater than 40 skip it
    }
      print(number);
  }
}