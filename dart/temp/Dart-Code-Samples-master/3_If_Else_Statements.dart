main(List<String> args) {
  // if else statements
  bool isTeenager;
  int age = 19;
  if(age<13){
    isTeenager = false;
    print("Teenage Status: $isTeenager");
  }
  else{
    isTeenager = true;
    print("Teenage Status: $isTeenager");
  }

  // if else if ladder statements
  String colour = "blue";
  if(colour == "blue"){
    print("The sky is blue");
  }
  else if(colour == "red"){
    print("Your blood is red");
  }
  else if(colour == "green"){
    print("Vegetables are green");
  }
  else if(colour == "orange"){
    print('Let\'s get the pumpkin reasy for haloween.');
  }
  else if(colour == "yellow"){
    print("The sun is yellow");
  }
  else if(colour == "black"){
    print("My hair is black");
  }
  else{
    print("Thank you.");
  }
}