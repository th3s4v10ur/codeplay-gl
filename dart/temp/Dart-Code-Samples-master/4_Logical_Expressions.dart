void main(){
  int num1 = 67,num2 = 89;
  int biggerNum = num1 > num2 ? num1 : num2;
  print(biggerNum);

  String name;
  String printName = name ?? "Student";
  print(printName);

  /**Note to self
   * condition ? exp1 : exp2
	 * If condition is true, evaluates expr1 (and returns its value);
	 * otherwise, evaluates and returns the value of expr2.
   */

  // 2.   exp1 ?? exp2
	// If expr1 is non-null, returns its value; otherwise, evaluates and
	// returns the value of expr2.

}