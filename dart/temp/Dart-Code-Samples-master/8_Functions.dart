// Functions in dart
/* Syntax returnType functionName(optional arguements i.e type argName){
  Function code goes Here
}
*/
bool isTeenager(int age){ // User defined function
  bool status;
  if(age<13){
    status = false;
  }
  else{
    status = true;
  }
  return status;
}
// => is a shorthand way of saying return operation
// Short hand syntax for writing a function: returnType functionName(optional arguements) => function code;
int add(int a, int b) => a + b;
int sub(int a, int b) => a - b;
int mul(int a, int b) => a * b;
double div(int a, int b) => a / b;
void main(){
  List<String> childNames = ["Obed","Vanesa","Elizabeth","Timothy","Scot","Valery","Stan"];
  List<int> childAges = [19,14,12,9,15,11,13];
  for(int i = 0;i<childNames.length;i++ ){
    print("${childNames[i]}'s status as a teenager is ${isTeenager(childAges[i])}"); // Made our function call here
  }
  int num1=19,num2= 43;
  print("\n");
  print(add(num1,num2));
  print(sub(num1,num2));
  print(mul(num1,num2));
  print(div(num1,num2));
}