main(List<String> args) {
    /**Dart data types
     * Numbers: int, double
     * var: is a way to declare a variable without specifying its type
     * String: anything enclosed within single or double quotes
     * bool: data type for boolean value true and false
     * final/const: either can be used to declare variables that you don't want its variables to be altered 
     * All data types in dart are objects, initial value is set to null
     */  
     final double pi = 3.147; // These value can't be altered
     const int planets = 9; // same goes when const is used.
     var hex = "ABC5643";
     int age = 19;
     bool isTeenager = true;
     String name = "Emmanuel Sule";
     double weightKG = 77.65;
     print("$name is $age years old\nHe weighs $weightKG kg it is $isTeenager that He is a teenager.");
     print(hex);
     print("There are $planets planets and pi = $pi");
}