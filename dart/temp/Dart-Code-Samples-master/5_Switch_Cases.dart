void main(List<String> args) {
  // Switch cases can only be used on Strings and integer type variables
  // 1 - add, 2 - subtract, 3 - multiply, 4 - divide
  var n1 = 34,n2 = 95;
  int operationType = 1;
  switch(operationType){
    case 1:
      print("$n1 + $n2 = ${n1+n2}");
      break;
    case 2:
      print("$n1 - $n2 = ${n1-n2}");
      break;
    case 3:
      print("$n1 * $n2 = ${n1*n2}");
      break;
    case 4:
      print("$n1 / $n2 = ${n1/n2}");
      break;
    default: // optional when using switch cases
      print("Invalid operation");
  } 
}