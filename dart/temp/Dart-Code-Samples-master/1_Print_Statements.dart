void main(){ // this is the entry point of our program

    print("Hello world!");

    print("Hello, my name is Emmanuel Sule.\nI am confused right now on what to do with my life.");

    print("Current age: ${2019 - 2000}");

    print("Name\tAge\nEmma\t19\nTiti\t18\nShedy\t15\nAgbenu\t14\nOjilima\t12\nEhi\t5");

    /**Note to self!!!
     * All statements in dart end with a semicolon
     * print("statements"); this is the syntax for printing out text to console in dart
     * in cases where you need to print out multiple statements of various types, use ${expression/statement} within the statement
     */
  

}