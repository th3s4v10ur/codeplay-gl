void main(List<String> args) {
  // Dart supports two types of for loops
  // 1. The traditional for(datatype var;expression;operation) loop
  // 2. for... in loop
  print("The Natural Numbers");
  for(int i = 0;i<=9;i++){
    print(i);
  }
  print("\n");

  List<String> roomies = ["Ebube","Samuel","Michael","Tumishe"];

  for (String roomMate in roomies) {
    if(roomMate == "Michael"){
      print("$roomMate is a fresher");
    }
    print("$roomMate is in room 9");
  }
  print("\n");

  // While Loops
  int two = 2;
  while(two<50){
    print(two);
    two*=2;
  }
  print("\n");

  // do... while() loops
  int k = 10;
  print("Countdown to launch rocket");
  do {
    print(k--);
  } while (k>0);
}