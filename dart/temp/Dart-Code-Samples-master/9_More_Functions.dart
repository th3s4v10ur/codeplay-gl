void main(){

  print(add(3,4)); // performing 2 input addition sequence on same function
  print(add(3, 4,2)); // performing 3 input addition sequence on same function
  force(mass: 5.6,acceleration: 9.01); // function call requires the input values to be mapped to a name

  defaultVal(19);
  defaultVal(20,val2: 60,val3: 70); // overiding the default values

}
/**
 * The optional positional arguement is enclosed in square brackets
 * e.g int add(int a, int b, [int c]) 
 */
int add(int a, int b, [int c]){
  if (c == null){ // if value of c is null
    return (a + b);
  }
  else{ // else if the value of c is given return the output
    return (a + b + c);
  }
}

/**
 * To name a parameter optionally, enclose all the arguements of the function in curly braces i.e {}
 * e.g  void force({double mass, double acceleration})
 * Note: the sequence does not matter in named parameters
 */
void force({double mass, double acceleration}){
  print("Force = mass(kg) * acceleration(m/s2)");
  print("Force = ${mass * acceleration} Newton");
}
/**
 * We can assign our optional parameters default values from the function definition
 * It can later be overidden at the function call
 */
void defaultVal(int val1, {val2 = 50,val3 = 12}){
  print(val1);
  print(val2);
  print(val3);
  print('\n');
}