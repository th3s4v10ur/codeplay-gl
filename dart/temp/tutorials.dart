import 'dart:core'; //already imported
import 'dart:math';

void main() {
  print("welcome to dart");
  //DataTypes
  print("------Datatypes-------");
  String dar = "merna";
  int x = 6;
  bool s = true;
  print(s);
  List myinfo = [1, 2, 3, 4];
  print(myinfo);
  var u = "mernaadel";

  var y = 21;
  print(u.isEmpty);
  var t = u.substring(0, 2);
  print(t);
  var e = myinfo.first;
  print(e);
  print(myinfo.reversed);
  var i = myinfo.reversed
      .toList(); //shoud use tolist again to reused function of list
  print(i[0]);
  dynamic d = 2; //dynamic or var
  print(d);
  final val1 = 12; //cant modifiy it lw fadya mesh hta5od memeory
  print(val1);
  const pi = 3.14; //use const or final
  const area = pi * 12 * 12;
  print("The output is ${area}");
  //string methods
  print("----string-------");
  String h = " merna".toString();
  print(h[0]);
  var k = h.toLowerCase();
  print(k);
  print(h.trim());
  //Operators
  print("------Operators-------");
  //conditional expression

  var ab = 10;
  var res =
      ab > 12 ? "value greater than 10" : "value lesser than or equal to 10";
  print(res);
  var a = null;
  var b = 12;
  var resl = a ?? b; //if a not null return a else return b
  print(resl);
  print("------Methods-------");
  int xyz() {
    var x = 2 * 2;
    return x;
  }

  print(xyz());
  print("------Math Library-------");
  print(max(1, 2));
  double age = 21.2;
  print(age.round());
  print("------lists-------");
  var l = [5, 4, 6, 4];
  print(l.runtimeType); //type elii gwa l list
  var li = [5, 4, 6, 4, "merna"];
  print(li.runtimeType); //type elii gwa l list no3 object
  //listof two values
  List lo = [
    {"merna": 1, "k": 2}
  ];
  print(lo[0]["k"]);
  print("------forloop-------");
  int o = 0;
  for (o = 0; o < li.length; o++) {
    print(li[o]);
  }
  li.forEach((val) => print(val));
  print("------if condition------");
}
