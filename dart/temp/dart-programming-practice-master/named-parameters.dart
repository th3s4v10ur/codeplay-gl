void main() {
  
  new Car(make: 'Ford', model: "Mondeo");
  new Car(model: "NSX 90", make: "Nisson");
  new Car();
  
}

class Car {
  
  String _make;
  String _model;
  
  Car({make, model}) {
    
    _make = make;
    _model = model;
    
    print("Make: ${getOptional(_make)} \nModel: ${getOptional(_model)}\n\n");
    
  }
  
  String getOptional(String str) {
    return str == null ? "" : str;
  }
  
}