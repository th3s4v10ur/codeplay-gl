//dart offers arrow functions, which enable the developer to shorten signle-line functions that
//calculate & return someting.

//You can use: x => xxx instead of: { return xxx; }

//arrow functions are mostly used by the event handlers

num divideNonLamda(num a, num b) {
  return a / b;
}

num divideLamda(num a, num b) => a / b;

void main() {
  print("Non-Lambda ${divideNonLamda(6, 2)}");
  print("Non-Lambda ${divideNonLamda(9, 2)}");
  print("Non-Lambda ${divideNonLamda(9, 2.5)}");

  print("Lambda ${divideLamda(6, 2)}");
  print("Lambda ${divideLamda(9, 2)}");
  print("Lambda ${divideLamda(9, 2.5)}");
}
