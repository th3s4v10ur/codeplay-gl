void main() {
  new App();
  //dynamic x = "Thangappa thakkam";
  dynamic x = 12.89;
  if(x is int) print("x ${x} is an integer");
  else print("x (${x}) => ${x.runtimeType} Type");
  
  var clapping = ['\u{1f44f}', '\u{1F344}', '\u{1F319}'];
  print(clapping);
  
  var car = new Car('Ford', 'Fortuner', '2018');
  
  print("Make: ${car.make}, Model: ${car.model}, Year: ${car.getYear()}");
}


class App {
  App() {
    callHelloWorld();
    print("Welcome to dart programming....");    
  }
  
  void callHelloWorld() {
    print("Hello World!!!");
  }
}

class Car {
  
  String make;
  String model;
  String _year;
  
  Car(this.make, this.model, this._year);
  
  String getYear() {
    return _year;
  }
  
  
}
