void main() {
  var log = new Logger();
  log.log("Program Started.");
  log.log("Running the program.");
  log.log("Program Ended.");
  
  //.. is used for cascading for methods
  new Logger()
    ..log("Program Started.")
    ..log("Running the program.")
    ..log("Program Ended.")
    ..logme();
}

class Logger {
  void log(dynamic v) {
    print("${DateTime.now()} ${v}");
  }

  void logme() {
    print("${DateTime.now()} ends now.");
  }
}
