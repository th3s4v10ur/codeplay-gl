//caution: @required is part of Flutter so it won't work on dart

void main() {
  
  new Car(make: 'Ford', model: "Mondeo");
  new Car(model: "NSX 90", make: "Nisson");
  new Car();
  
}

class Car {
  
  String make;
  String model;
  
  Car({@required this.make, this.model}) {
    
    print("Make: ${getOptional(make)} \nModel: ${getOptional(model)}\n\n");
    
  }
  
  String getOptional(String str) {
    return str == null ? "" : str;
  }
  
}