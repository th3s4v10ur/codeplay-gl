void main() {
  
  var lion = new Lion();
  lion.useMouth();
  lion.usehands();
  
  var indians = new Human();
  indians.useMouth();
  indians.usehands();
   
}

//if a class going to implement this class then it had to use all the methods defined in the
//abstract class otherwise it will give you an error.
abstract class Animals {
  void useMouth();
  void usehands();
}

class Lion implements Animals {
  void useMouth() {
    print("Lions use mouth to eat raw fleshes");
  }
  void usehands() {
    print("Lions have no hands");
  }
}

class Human implements Animals {
  void useMouth() {
    print("Human use mouth to eat & to speak");
  }
  void usehands() {
    print("Without hand there is no human civilization");
  }  
}