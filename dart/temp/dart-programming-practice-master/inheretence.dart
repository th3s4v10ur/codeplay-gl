void main() {
  
  new Car('Ford', "Mondeo");
  new Car("Nisson");
  
}

class Car {
  
  String _make;
  String _model;

  //[] is used for optional parameter. 
  Car(this._make, [this._model]) {
    
    print("${_make} ${_model}");
    
  }
  
}