void main() {
  new App();
  //dynamic x = "Thangappa thakkam";
  dynamic x = 12.89;
  if(x is int) print("x ${x} is an integer");
  else print("x (${x}) => ${x.runtimeType} Type");
  
  var clapping = ['\u{1f44f}', '\u{1F344}', '\u{1F319}'];
  print(clapping);
}


class App {
  App() {
    callHelloWorld();
    print("Welcome to dart programming....");    
  }
  
  void callHelloWorld() {
    print("Hello World!!!");
  }
}
