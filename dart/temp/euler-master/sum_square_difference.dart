//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

//Generate a list of hundres numbers
List hundredNumbers(){
  List l =Iterable<int>.generate(100).toList();
  return l;
}

//sum of the squares
int sumOfSquares(){
  int m = 0;
  for(int i = 1; i<=100; i++){
    m = m + i*i;
  }
  return m;
}

// square of the sum
int squareOfSum(){
  int m = 0;
  for(int i = 1; i<=100; i++){
    m = m + i;
  }
  return m*m;
}

void main(List<String> args) {
  print(sumOfSquares());
  print(squareOfSum());
  print(squareOfSum() - sumOfSquares());
}

