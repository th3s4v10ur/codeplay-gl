// Find the largest palindrome made from the product of two 3-digit numbers.

int lowest = 100;
int highest = 999;

void main(){
  List resulting_products = products();
  List palindrome_products = [];
  for(int i = 0; i<resulting_products.length;i++){
    if (resulting_products[i] == reversedCopy(resulting_products[i])) {
      palindrome_products.add(resulting_products[i]);            
    }
  }
  print(largestNumber(palindrome_products));
}

List products(){
  List product = [];
  for(int i=lowest; i<=highest; i++){
    for (int j =lowest; j<= highest; j++){
      product.add(i*j);
    }
  }
  return product;
}


int reversedCopy(x){
  String num = x.toString();
  List temp = [];
  List r = [];
  temp = num.split("");
  for(int i=temp.length-1; i>=0;i--){
    r.add(temp[i]);
  }  
  return int.parse(r.join(""));
}



int largestNumber(myList){
  int temp;
  for(int i=0; i<myList.length;i++){
    if(myList[0]<myList[i]){
      myList[0] =myList[i];
    }
  }
  return myList[0];
}