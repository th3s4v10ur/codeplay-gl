/*
* 编程中非常常用的集合是一个数组。Dart以List对象的形式表示数组。一个列表仅仅是对象的有序组。：核心库提供的列表类，使创建和列表的操作。
*
* */

void main() {
//  固定列表
  var lst = new List(3);
  lst[0] = 12;
  lst[1] = 13;
  lst[2] = 11;
  print(lst);
//  可增长列表的长度可以在运行时更改。声明和初始化可增长列表的语法如下所示 -
  var num_list = [1,2,3];
  print(num_list);
  num_list.add(12);
  num_list.add(13);
  print(num_list);
}
