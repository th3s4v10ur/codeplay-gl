void main() {
	/*Working with strings in dart programming language*/

	String msg = 'Hello world';
	String msg2   = "The quick brown fox jumps over the lazy dog!";
	String msg3 = """
     A journey of a thousand light years :-) 
     This is thy kingdom come...
     This is the end of the beginning;
     Better days ahead!
     A silver lining in the yonder horizons of the midnight sky
     This is thy time to shine -:)
	""";
	var obj = [msg,msg2,msg3];
	for (var x in obj){
		print(x);
	}

	print("${msg.toUpperCase()}");
	print("${msg.toLowerCase()}");
	print("${msg.length}");
}
