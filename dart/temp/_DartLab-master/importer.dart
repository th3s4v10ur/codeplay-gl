import 'employee.dart';

@Employee("Admin","ADM") //Metadata from the employee package
void sayHello(){
	print("Hello world, welcome to Freetown");
}

/*Creating a callable class*/
class Panda{
	String call(String name,int age){
		return "${name} is ${age} years old";
	}
}
void main() {
	//sayHello();

	Panda teddy = new Panda();
	String detail =  teddy("Teddy Cruz",30);
	print(detail);
}

