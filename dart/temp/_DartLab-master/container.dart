void main() {
	
	List ls = new  List(1000);//fixed length
	int i = ls.length;
	for(int k =0; k<ls.length;k++){
		ls[k] =  (k + i)/((k*i)+1);
	}

	print(ls);


	List ls = new List(); //growable list 
	for(int k=0;k<=1000;k++){
		ls.add((k+1)^k);
	}
	print(ls);

	var obj = {"usr":"John","pwd":"doe"};

	var obj =  new Map();
	obj['usr'] = "admin";
	obj['pwd'] = "1234";
	obj.forEach((k,v)=>print("key ${k} val ${v}"));
}
  
  var usrMap = new Map();
  usrMap['uID'] = "abeaab134";
  usrMap['usr'] = "admin";
  usrMap['email'] = "admin@xyz.com";
  usrMap.forEach((k,v)=>print("${k} ${v}"));

}