void main() {
	ConsolePrinter cp  =  new ConsolePrinter("0xff02","HP Pavillion",1200.0);
	// cp.print_data();
	// cp.priority_print("12aeaffx");
	 cp.printer_info();
	 cp.printer_cost =  24000.0;
	 cp.printer_info();
}
class Printer{

	void print_data(){
		//print("_________________printing some data_____________");
	}
}

class Printer_Concept{
	void checkQueue(p_id){

	}
	void priority_print(queue_id){

	}
}

class ConsolePrinter implements Printer,Printer_Concept {
	String printer_id =  "";
	String printer_make = "";
	double cost = 0.0;
	bool printer_is_on =  true;
	ConsolePrinter(printer_id,printer_make,cost,{printer_is_on:true}){
		this.printer_id =  printer_id;
		this.printer_make =  printer_make;
		this.cost = cost;
		this.printer_is_on = printer_is_on;
	}
	void print_data(){
		print("_________________printing some data from the using console printer_____________");
	}
	void printer_info(){
		print("Printer ID ${this.printer_id}");
		print("Printer Make ${this.printer_make}");
		print("Printer Cost \$ ${this.cost}");
		String flag = (this.printer_is_on)?"ON":"OFF";
		print("Printer Status ${flag}");
		print("Thank you!");
	}
	double get printer_cost{
		return cost;
	}
	bool get check_printer_is_on{
		return printer_is_on;
	}
	double set printer_cost(cost){
		this.cost =  cost;
	}
	void checkQueue(p_id){
		print("${p_id} is currently in the queue...");
	}
	void priority_print(queue_id){
		print("${queue_id} is being sent to print....");
	}
}
