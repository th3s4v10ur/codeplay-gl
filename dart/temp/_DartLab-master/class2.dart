void main() {
	Employee yp =  new Employee("Yusuf",26,300.0);
	yp.showInfo();

}


class Person{

	int age = 0;
	String name = "";
	Person(String p_name,int p_age){
		this.name =  p_name;
		this.age =  p_age;
	}

	showPerInfo(){
		print("Person name ${this.name}");
		print("Person age ${this.age}");
	}
}

class Employee extends Person{

	double empSalary =  0.0;

	Employee(String e_name,int e_age,double e_Salary):super(e_name,e_age){
		this.empSalary =  e_Salary;
	}

	showInfo(){
		print("Employee name ${this.name}");
		print("Employee age ${this.age}");
		print("Employee Salary \$ ${this.empSalary}");
	}
}