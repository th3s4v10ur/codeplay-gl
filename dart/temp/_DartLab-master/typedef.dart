/*Working with typedef*/

typedef ManyOperations(int a, int b); 

Add(int a,int b){
	return a +b;
}
Subtract(int a,int b){
	return a -b;
}
Multiply(int a,int b){
	return a *b;
}
void main() {
	 
  ManyOperations ops =  Add;
  print("Add() ${Add(2,1)}");
  ops =  Subtract;
  print("Subtract() ${Subtract(2,1)}");
}
