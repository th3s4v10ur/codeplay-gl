void main() {
	 // test(12);
	 // test(21,30);
	 // demo2(121);
   
	 //print(factorial(5));
	 // fib(100);
	 print(sum(-1,20));
}
 /*Working with Lamda functions*/
int test()=>  121;
int sum(a,b)=> a+b;

void fib(n){
	int x=1,y=1;
	int z  = 0;
	print("${x}\n${y}");
	for(int i=1;i<=n;i++){
		z =  x + y;
		print("${z}");
		x = y;
		y =  z;
	}
}
int lamdaFactorial(n)=> if(n<=0){return 1;}else { return lamdaFactorial(n-1);};
int factorial(n){
	if(n<=0){
		return 1;
	}else{
		return (n *factorial(n-1));
	}
}

// test(x,[k]){
// 	print(x);
// 	print(k);
// }

// demo(x,{y,z}){
// 	print(x);
// 	print(y);
// }


// demo2(x,{y:12}){
// 	print(x);
// 	print(y);
// }