void main() {
  // Employee yb = new Employee("Yusuf Brima","Research and Development",6500.0);
  // yb.showEmpolyeeDetails();	
  // Employee cb = new Employee("Isatu C Brima","Systems Engineering",7000.0);
  // cb._employeeSalaray = 10000.0 ;
  // cb.showEmpolyeeDetails();
  // print(Employee.departmentID); // Printing a static variable departmentID in the Employee class 
  // Employee.msg(); // A call to a static method msg() in the Employee class 

  // subClass s = new subClass();
  //    s.printNum();//
  //    s.printer();

  childClass cc  =  new childClass();
  cc.display();
}
class superClass{
   int num =100;
   void display(){
   	  print("We're printing from the superclass");
   }
}
class subClass extends superClass{
	int num =  150;
	void printNum(){
		print("${super.num}"); //prints the variable num from the parent superclass 
	}
	void display(){
		print("We're printing from the subclass");
	}

	void printer(){
		display(); //will call the display method in the subclass 

		super.display(); //will call the display method in the superclass 
	}
}

class Employee {
	String employeeName = "";
	String employeeDepartment = "";
	double employeeSalaray = 0.0;
	static  String departmentID = "RD0013"; 
	Employee(employeeName,employeeDepartment,employeeSalaray){
		this.employeeName =  employeeName;
		this.employeeDepartment =  employeeDepartment;
		this.employeeSalaray =  employeeSalaray;
	}
	static void msg(){
		print("_______Hallo!... Welcome to your Company >)(< ::___________");
	}
	void showEmpolyeeDetails(){
		print("_______________Employee Details____________");
		print("Name ${this.employeeName}");
		print("Department ${this.employeeDepartment}");
		print("Salary \$ ${this.employeeSalaray}");
	}

	double set _employeeSalaray(salary){
		this.employeeSalaray =  salary;
	}
	String set _employeeDepartment(employeeDepartment){
		this.employeeDepartment =  employeeDepartment;
	}
	String set _employeeName(employeeName){
		this.employeeName =  employeeName;
	}
	String get _employeeName{
		return this.employeeName;
	}
	double get _employeeSalaray{
		return this.employeeSalaray;
	}
	String get _employeeDepartment{
		return this.employeeDepartment;
	}
}


class parentClass{
	parentClass(String str){
		print("We're printing from parent class");
		print(str);
	}
}

class childClass extends parentClass{
	childClass():super("Hey there!"){
		print("We're printing from child class");
	}
	void display(){
		print("Hello world!");
	}
}