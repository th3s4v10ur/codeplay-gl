void main() {
	/*Dart Double Data type*/
	double a =  2.0;
	double pi =  22/7;
	double result =  a * pi;
	print(a * pi);


	/*Dart int Data type*/
	int x =  2;
	int y = 1;
	print(++x + ++y);


	/*Working with string*/
	String name = "Yusuf Brima";
	String msg = 'The quick brown fox jumps over the lazy dog';
	print(name);
	print(msg.length);


	/*Working with boolean data types*/

	bool switchIsOff =  true;
	bool traffiLightGreen =  false;
	if(switchIsOff){
		print("Turn on the switch please");
	}else{
		print("Voila! Switch is On...");
	}

	if(traffiLightGreen){
		print("Time to cross the road");
	}else{
		print("Please stay back and wait for green light");
	}


	/*Working with dynamic variables*/


	dynamic foo = 1000;
	print(foo);
	foo = "bar";
	print(foo);


	/*Working with constants*/

	const int a =  1;
	print(a);

	const String _dayOfTheWeek = "Monday";
	if(_dayOfTheWeek == "Monday"){
      print("Hello Monday");
	}else if(_dayOfTheWeek == "Teusday"){
       print("Hello Teusday");
	}else if(_dayOfTheWeek == "Wednesday"){
       print("Hello Wednesday");
	}else if(_dayOfTheWeek == "Thursday"){
       print("Hello Thursday");
	}else if(_dayOfTheWeek == "Friday"){
       print("Hello Friday");
	}else if(_dayOfTheWeek == "Saturday"){
       print("Hello Saturday");
	}else if(_dayOfTheWeek == "Sunday"){
       print("Hello Sunday");
	}else{
    	print("Unknown selected day");
    }

  
  const double r =  2;
  print("The area of the circle is ${area(r)}");

  int a = 10;
  print(a.hashCode); //num.parse() parses string literal that is numeric to a number data type

}
final double pi = 3.142;

double area(double r){
	return pi*r*r;
}