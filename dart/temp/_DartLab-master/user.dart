import 'dart:convert';
import 'package:crypto/crypto.dart';

const String signed_u_name = "admin";
const String pwd = "admin";
void main() {
	User u = new User("admin","admin");
	if(u.login()){
		print("Logged in successfully");
	}else{
		print("Login attempt failed :-:)");
	}

	Admin yb = new Admin("admin","admin1",0);
	Moderator m = new Moderator("nanah","admin",1);

	print(yb.login()); //call to the login method in the User class from the Admin subclass 
	print(m.login()); //Call to the same login method from the User class in the Moderator childclass 


}


class User{
	String u_name = "";
	String u_pwd = "";
	User(String username,String password){
		this.u_name = username;
		this.u_pwd = password;
	}
	bool login(){
		if((this.u_name  == signed_u_name) && (Security.hashToMD5(this.u_pwd) == Security.hashToMD5(pwd))){
			return true;
		}else{
			return false;
		}
	}
    String get userName{
   	  return this.u_name;
   }

   void showProfileInfo(){
   	  print("______User Account Info__________");
   	  print("Username: ${this.u_name}");
   	  print("Password: ${Security.hashToMD5(this.u_pwd)}");
   	  print("Account created on: 06/07/1990");
   }
}

class Admin extends User{
   int access_level = 0;
   Admin(String username,String password,int a_level):super(username,password){
   	 this.access_level = a_level;
   }
   int get accessLevel{
   	 return this.access_level;
   }
}

class Moderator extends User{
	int access_level = 1;
   Moderator(String username,String password,int a_level):super(username,password){
   	 this.access_level = a_level;
   }

   int get accessLevel{
   	 return this.access_level;
   }
}

class Security{
	static String hashToMD5(String input) {
		return md5.convert(utf8.encode(input)).toString();
	}
}


class Panda{
	String call(String name,int age){
		return "${name} is ${age} years old";
	}
}