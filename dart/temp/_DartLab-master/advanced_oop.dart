void main() {
	primate chimp =  new primate("Godzilla",14.7,450.3,"Male");
	 chimp.showPrimateInfo();
	 human h =  new human("Daniel",80.0,6.2,"Male","Sierra Leone");
	 h.showHumanInfo();
}

// abstract class animal{
//    // animal(name,height,weight,gender){
//    // 	  this.height =  height;
//    // 	  this.weight =  weight;
//    // 	  this.gender =  gender;
//    // 	  this.name =  name;
//    // }
//    void show_details();
// }
class primate{
	double height = 0.0;
   double weight = 0.0;
   String gender  = "";
   String name = "";
   primate(name,height,weight,gender){
   	  this.height =  height;
   	  this.weight =  weight;
   	  this.gender =  gender;
   	  this.name  =  name;
   }
   void showPrimateInfo(){
   	 print("Name ${this.name}");
   	 print("Height ${this.height} feets ");
   	 print("Weight ${this.weight} kg");
   	 print("Gender  ${this.gender}");
   }
}

class human extends primate{
   String countryOfOrigin = "";
   human(name,height,weight,gender,countryOfOrigin):super(name,height,weight,gender){
   	 this.countryOfOrigin =  countryOfOrigin;  
   }
  void showHumanInfo(){
   	 print("Name ${this.name}");
   	 print("Height ${this.height} feets ");
   	 print("Weight ${this.weight} kg");
   	 print("Gender  ${this.gender}");
   	 print("Country of Origin  ${this.countryOfOrigin}");
   }

}
