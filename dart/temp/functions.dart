/*
* Functions are building blocks that are readable, maintainable, and reusable. A function is a set of statements that are used to perform a specific task. The function organizes the program into logical code blocks. Once defined, you can call a function to access the code. This allows the code to be reused. In addition, functions can easily read and maintain program code.
* */

/*
* The function declaration tells the name of the compiler function, return type and parameters. The function definition provides the actual body of the function.
Sr.No function and description
1 Define a function
      Function definitions specify how and how a particular task is performed.
2 call function
       The function must be called to execute it.
3 return function
      The function can also return the value to the caller along with the control.
4 parameterization function
      A parameter is a mechanism for passing a value to a function.
*
*
* Optional parameters
Optional parameters can be used when the function does not need to be forced to pass parameters. You can mark a parameter as optional by appending a question mark to its name. The optional parameter should be set to the last parameter in the function.
We have three optional parameters in Dart -
Sr.No parameters and description
1 optional positional parameters
      To specify an optional positional parameter, use square brackets [] brackets.
2 optional named parameters
      Unlike positional parameters, you must specify the name of the parameter when you pass the value. Curly brace {} can be used to specify optional named parameters.
3 optional parameters with default values
      By default, you can also specify values ​​for function parameters. However, these parameters can also pass values ​​explicitly.
* */

/*
* Dart recursive function
Recursion is a technique that iterates through repeated calls to the function itself until it reaches the result. It is best to apply recursion when you need to call the same function repeatedly with different parameters within the loop.
example
*
* */

/*
* Optional positional parameters
* Syntax
Void function_name(param1, [optional_param_1, optional_param_2]) { }
If the optional parameter value is not passed, it is set to NULL.
* */
Test_param (n1, [s1]) {
  Print (n1);
  Print (s1);
}

/*
* Optional named parameters
* void function_name(a, {optional_param1, optional_param2}) { }
Syntax - call function
Function_name(optional_param:value,...);
Unlike positional parameters, you must specify a parameter name when you pass a value. Curly brace {} can be used to specify optional named parameters.
* */
Test_param2 (n1, {s1, s2}) {
  Print (n1);
  Print (s1);
  Print (s2);
}

/*
* By default, you can also specify values ​​for function parameters. However, these parameters can also pass values ​​explicitly.
* */
Void  test_param3 (n1, {s1 :  'test_param3 default' }) {
  Print (n1);
  Print (s1);
}

Void  main () {
  Test_param ( 123 );
  Test_param2 ( 123 , s1 :  'hello' , s2 :  'world' );
  Test_param3 ( 'test3' );
  Print ( factorial ( 6 ));
  printMsg ();
  Print ( test ());
}

// factorial recursive function
Factorial (number) {
  If (number <=  0 ) {
    // termination case
    Return  1 ;
  } else {
    Return (number *  factorial (number -  1 ));
    // function invokes itself
  }
}

/*
* Lambda function
Lambda functions are a compact mechanism for representing functions. These functions are also known as arrow functions.
[return_type]function_name(parameters)=>expression;
* */
printMsg () = >  print ( "hello" );

Int  test () = >  123 ;
// returning function
