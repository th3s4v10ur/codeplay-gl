class Vehicle{
  String brand;
  String model;
  String colour;
  //Vehicle(this.brand,this.model,this.colour);
  //Vehicle([this.brand,this.model,this.colour]);
  Vehicle({this.brand,this.model,this.colour});
  toShow(){
    print('$brand $model $colour');
  }
}

main(){
  //var car = Vehicle('Toyota','X18','Red');
  //var car = Vehicle('Mazda','Z10','Blue');
  var car = Vehicle(colour: 'Green',brand: 'Toyota',model: 'w200');
  car.toShow();
}
