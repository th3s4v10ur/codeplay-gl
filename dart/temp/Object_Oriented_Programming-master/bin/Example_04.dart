class Vehicle{
  String brand;
  String model;
  String colour;
  toShowVehicle(){
    print('Brand: $brand, Model: $model, Colour: $colour');
  }
  whatVehicle(){
    print('Not available');
  }
}

class Truck extends Vehicle{
  @override
  whatVehicle() {
    print('Available');
  }
}

class Car extends Vehicle{
  @override
  whatVehicle() {
    print('Available');
  }
}

main(){
  var truck = Truck();
  truck.brand = 'Volvo';
  truck.model = 'V200';
  truck.colour = 'Red';
  truck.toShowVehicle();
  truck.whatVehicle();

  var car = Car();
  car.brand = 'Toyota';
  car.model = 't200';
  car.colour = 'Black';
  car.toShowVehicle();
  car.whatVehicle();
}