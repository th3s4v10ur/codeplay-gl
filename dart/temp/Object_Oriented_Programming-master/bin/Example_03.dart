class Vehicle{
  String brand;
  String model;
  String colour;
  toShowVehicle(){
    print('Brand: $brand, Model: $model, Colour: $colour');
  }
}

class Truck extends Vehicle{

}

class Car extends Vehicle{

}

main(){
  var truck = Truck();
  truck.brand = 'Volvo';
  truck.model = 'V200';
  truck.colour = 'Red';
  truck.toShowVehicle();

  var car = Car();
  car.brand = 'Toyota';
  car.model = 't200';
  car.colour = 'Black';
  car.toShowVehicle();
}