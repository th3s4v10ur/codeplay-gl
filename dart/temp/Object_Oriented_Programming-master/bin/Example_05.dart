class Vehicle{
  String brand;
  String model;
  String colour;
  Vehicle(this.brand,this.model);
  toShowVehicle(){
    print('Brand: $brand, Model: $model');
  }
  whatVehicle(){
    print('Not available');
  }
}

class Truck extends Vehicle{
  Truck(String brand, String model) : super(brand, model);
  @override
  whatVehicle() {
    print('Available');
  }
}

class Car extends Vehicle{
  Car(String brand, String model) : super(brand, model);
  @override
  whatVehicle() {
    print('Available');
  }
}

main(){
  var truck = Truck('Volvo','V200');
  truck.toShowVehicle();
  truck.whatVehicle();

  var car = Car('Toyota','t200');
  car.toShowVehicle();
  car.whatVehicle();
}