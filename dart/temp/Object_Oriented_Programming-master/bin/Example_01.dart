class Vehicle{
  String brand;
  String model;
  String colour;
  toShow(){
    print('$brand $model $colour');
  }
}

main(){
  var car = Vehicle();
  car.brand = 'Toyota';
  car.model = 'X18';
  car.colour = 'Red';
  car.toShow();
}
