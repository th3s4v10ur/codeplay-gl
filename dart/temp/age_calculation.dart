import 'dart:io';

// Without using static typing

ageCalculation() {
  print("=== Enter a age ===");
  var input = stdin.readLineSync();
  var age = int.parse(input);

  if (age >= 50) {
    print("Uôu, best age, haha!");
  } else if (age >= 18) {
    print("Adult.");
  } else if (age >= 12) {
    print("Teenager, man!");
  } else {
    print("OMG, little cat, haha. Child!");
  }
}
