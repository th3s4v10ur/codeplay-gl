void main() {
    var person = new Person("Krishna");
    
    //Initializing Person class property
    //person.firstName = "Krishna";

    //Calling Person class method.
    person.printName();
    
}

class Person{
    String firstName;

    Person(this.firstName);
    
    printName() {
        print(firstName);
    }
}