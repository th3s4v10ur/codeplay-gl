//operator

/*
Operators available in Dart.
Arithmetic operator
Equality and relational operators
Type test operator
Bitwise operator
Distribution operator
Logical Operators */

/*
Arithmetic operator
The following table shows the arithmetic operators supported by Dart.
Display example
    Operator and meaning
    +
plus
    -
minus
    -expr
One dollar minus sign, also known as negation (the sign of the inverted expression)
    *
Multiply
    /
except
    ~/
Divide by, return an integer result
    %
Get the remainder of the integer division (modulo)
    ++
Increment
    -
Decrement */


/* The relational operator tests or defines the type of relationship between two entities.
* The relational operator returns a boolean value, which is true/false.
Suppose A has a value of 10 and B has a value of 20.
Display example
Operator Description
> Greater than (A> B) is false
< less than (A <B) is true
>= greater than or equal to (A> = B) is false
<= less than or equal to (A <= B) is true
== Equality (A == B) is true
! = not equal (A!= B) is true */

// type test operator

/*
* These operators make it easy to check types at runtime.
E.g
Operator meaning
Is True if the object has the specified type
Is! false if the object has the specified type
* */

/*
* bitwise operator
The following table lists the bitwise operators available in Dart and their effects -
Display example
Operator Description
Bitwise AND a&b returns the position of each bit, with the corresponding bit of the two operands being 1.
Bitwise OR a | b returns a bit at each bit position where the corresponding bit of one or both operands is 1.
Bitwise XOR a ^ b returns 1 for each bit position, and the corresponding bit of either but not both operands is 1.
Bits NOT ~a invert the bits of its operands.
Shift left a ≪ b Shifts the binary representation b (<32) bit to the left and zero from the right.
Signing right shift a»b Shifts the binary to the right to indicate the b(<32) bit, discarding the shifted bits.
*
* */

/*
* Assignment operator
*
*
1 = (simple allocation)
Assign values ​​from the right operand to the left operand
For example: C = A + B assigns the value of A + B to C
2 '=
Assign values ​​only if the variable is null
3 += (add and assign)
It adds the right operand to the left operand and the result to the left operand.
Example: C += A is equal to C = C + A.
4 ─=(minus and assign)
It subtracts the right operand from the left operand and assigns the result to the left operand.
Example: C -= A is equivalent to C = C - A.
5 *= (multiplication and allocation)
It multiplies the right operand with the left operand and assigns the result to the left operand.
Example: C *= A is equal to C = C * A.
6 /= (division and allocation)
It separates the left and right operands and assigns the result to the left operand.
*
* */


/*
*
Logical Operators
Logical operators are used to combine two or more conditions. The logical operator returns a boolean value. Suppose the value of variable A is 10 and B is 20.
Display example
Operator Description
&& and - The operator returns trueA> 10 && B> 10) only if all the specified expressions return true.
||
OR - If at least one of the specified expressions returns true, the operator returns true (A> 10 || B> 10) is true.
! The NOT - operator returns the inverse of the result of the expression. E.g:! (7> 5) returns false
*
* */


/*
* Conditional expression
Dart has two operators that let you evaluate expressions that may require an ifelse statement -
condition? Expr1:expr2
If condition is true, the expression evaluates expr1 (and returns its value); otherwise, it evaluates and returns the value of expr2.
Expr1 ?? expression 2
If expr1 is non-null, return its value; otherwise, calculate and return the value of expr2
example
*
* */

void  main (){
// var a = 10;
// var res = a > 12 ? "value greater than 10":"value lesser than or equal to 10";
// print(res);
  var a =  null ;
  var b =  12 ;
  var res = a ?? b;
  print (res);
}
