//
//  Generated file. Do not edit.
//

#import "GeneratedPluginRegistrant.h"

#if __has_include(<hands_on_platform_views/HandsOnPlatformViewsPlugin.h>)
#import <hands_on_platform_views/HandsOnPlatformViewsPlugin.h>
#else
@import hands_on_platform_views;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [HandsOnPlatformViewsPlugin registerWithRegistrar:[registry registrarForPlugin:@"HandsOnPlatformViewsPlugin"]];
}

@end
