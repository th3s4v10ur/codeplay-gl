/*
* The String data type represents a series of characters. A Dart string is a series of UTF 16 code units.
String values ​​in Dart can be expressed in single or double or triple quotes. Single-line strings are represented by single or double quotes. The triple quotes are used to represent multi-line strings.
*
* */
Void  main () {
  String str1 =  'this is a single line string' ;
  String str2 =  "this is a single line string" ;
  String str3 =  '''this is a multiline line string''' ;
  String str4 =  """this is a multiline line string""" ;

  Print (str1);
  Print (str2);
  Print (str3);
  Print (str4);

  /*
  * String interpolation
The process of creating a new string by appending a value to a static string is called join or interpolation. In other words, it is the process of adding a string to another string.
The operator plus (+) is a common mechanism for concatenating/inserting strings.
  *
  * */
  String string1 =  "hello" ;
  String string2 =  "world" ;
  String res = string1 + string2;

  Print ( "The concatenated string : ${ res }" );
/*
* Interpolation
*
* */

  Int n =  1  +  1 ;

  String stringinterpolation1 =  "The sum of 1 and 1 is ${ n }" ;
  Print (stringinterpolation1);

  String stringinterpolation2 =  "The sum of 2 and 2 is ${ 2 + 2 }" ;
  Print (stringinterpolation2);

  /*
  * The dart: String class in the core library also provides methods for manipulating strings. Some of these methods are as follows -
Sr.No method and description
1 toLowerCase()
Convert all characters in this string to lowercase.
2 toUpperCase()
Convert all characters in this string to uppercase.
3 trim()
Returns a string without any leading and trailing spaces.
4 compareTo()
Compare this object to another object.
5. replaceAll()
Replaces all substrings that match the specified pattern with the given value.
6 split()
Splits the string at the match of the specified separator and returns a list of substrings.
7 toString()
Returns a substring of this string that extends from startIndex (included) to endIndex, exclusive.
8 toString()
Returns a string representation of this object.
9 codeUnitAt()
Returns the 16-bit UTF-16 code unit at the given index.
  *
  * */
}
