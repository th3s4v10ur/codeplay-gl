# gtkDcoding README
GtkD is GTK+ for the D programming language. And what is GTK+? It's a GUI building toolkit for desktop applications and works on a number of POSIX operating systems (UNIX, UNIX-like, and OSX) as well as Windows. Together with D, GtkD can be used to build cross-platform desktop applications in a write-once-compile-for-many process.

This repository is where I keep all the example code I've written for GtkD. Each example is written to demonstrate just one technique. For instance, in the buttons section, there are examples for each of the button types as well as how each of the available signals are handled. Each is written with the least amount of clutter and with comments intended to maximize clarity.

I've only been working with D and GtkD for a couple of months, so the Dlang and GtkD forums have been invaluable to furthering my understanding. Thanks are very much in order.

In the spirit of the community, and with a tip of the hat to Richard Stallman, all code contained in this blog is public domain. If it helps you better understand GtkD or helps you remember how a particular technique is done, then it's served its purpose.

-Ron Tarrant

(gtkDcoding.com)
