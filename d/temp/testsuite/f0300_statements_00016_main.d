module test;
// Context begin
#line 789 "0300-statements.tex"
import std.stdio;
void main() {
   print([5, 2, 8]);
}
// Context end
#line 795 "0300-statements.tex"
void print(int[] array) {
   foreach (i, e; array) {
      writefln("array[%s] = %s;", i, e);
   }
}
