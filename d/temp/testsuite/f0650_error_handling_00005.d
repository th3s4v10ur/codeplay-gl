module test;
#line 305 "0650-error-handling.tex"
import std.stdio;

string fun(int x) {
   string result;
   try {
      if (x == 1) {
         throw new Exception("some exception");
      }
      result = "didn't throw";
      return result;
   } catch (Exception e) {
      if (x == 2) throw e;
      result = "thrown and caught: " ~ e.toString;
      return result;
   } finally {
      writeln("Exiting fun");
   }
}


void main(){}
