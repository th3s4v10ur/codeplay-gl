module test;
unittest {
#line 644 "0350-arrays.tex"
auto a = [ 0.5, -0.5, 1.5, 2 ];
auto b = [ 3.5, 5.5, 4.5, -1 ];
auto c = new double[4];              // Must be already allocated
c[] = (a[] + b[]) / 2;               // Take the average of a and b
assert(c == [ 2.0, 2.5, 3.0, 0.5 ]);
}


void main(){}
