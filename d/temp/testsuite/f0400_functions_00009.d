module test;
#line 434 "0400-functions.tex"
void fun(double x) {
   static double minInput;
   static bool minInputInitialized;
   if (!minInputInitialized) {
      minInput = x;
      minInputInitialized = true;
   } else {
      if (x < minInput) minInput = x;
   }
   
}


void main(){}
