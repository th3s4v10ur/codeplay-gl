module test;
// Context begin
#line 36 "0500-classes.tex"
class Widget {
   // A constant
   enum fudgeFactor = 0.2;
   // A shared immutable value
   static immutable defaultName = "A Widget";
   // Some state allocated for each Widget object
   string name = defaultName;
   uint width, height;
   // A static method
   static double howFudgy() {
      return fudgeFactor;
   }
   // A method
   void changeName(string another) {
      name = another;
   }
   // A non-overridable method
   final void quadrupleSize() {
      width *= 2;
      height *= 2;
   }
}
// Context end
#line 69 "0500-classes.tex"
unittest {
   // Access a static method of Widget
   assert(Widget.howFudgy() == 0.2);
   // Create a Widget
   auto w = new Widget;
   // Play with the Widget
   assert(w.name == w.defaultName); // Or Widget.defaultName
   w.changeName("My Widget");
   assert(w.name == "My Widget");
}


void main(){}
