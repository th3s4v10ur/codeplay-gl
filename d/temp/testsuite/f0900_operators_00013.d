module test;
// Context begin
#line 1013 "0900-operators.tex"
struct SimpleList(T) {
private:
   struct Node {
      T _payload;
      Node * _next;
   }
   Node * _root;
public:
   @property bool empty() { return !_root; }
   @property ref T front() { return _root._payload; }
   void popFront() { _root = _root._next; }
   
}
// Context end
#line 1031 "0900-operators.tex"
void process(SimpleList!int lst) {
   foreach (value; lst) {
      // Use value of type int
   }
}


void main(){}
