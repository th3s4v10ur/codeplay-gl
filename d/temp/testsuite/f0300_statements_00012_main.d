module test;
#line 688 "0300-statements.tex"
import std.math, std.stdio;

void main() {
   foreach (float elem; 1.0 .. 100.0) {
      writeln(log(elem)); // Logarithms in single precision
   }
   foreach (double elem; 1.0 .. 100.0) {
      writeln(log(elem)); // Double precision
   }
   foreach (elem; 1.0 .. 100.0) {
      writeln(log(elem)); // Same
   }
}
