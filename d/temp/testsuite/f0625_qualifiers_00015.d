module test;
#line 549 "0625-qualifiers.tex"
import std.conv;

struct S {
   private int[] a;
   // Conversion from immutable to mutable
   this(immutable(S) source) {
      // Duplicate the array into a non-immutable array
      a = to!(int[])(source.a);
   }
   // Conversion from mutable to immutable
   this(S source) immutable {
      // Duplicate the array into an immutable array
      a = to!(immutable(int[]))(source.a);
   }
   
}

unittest {
   S a;
   auto b = immutable(S)(a);
   auto c = S(b);
}


void main(){}
