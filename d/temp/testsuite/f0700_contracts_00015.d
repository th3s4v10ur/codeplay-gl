module test;
#line 1275 "0700-contracts.tex"
interface Stack(T) {
   @property bool empty();
   @property ref T top();
   void push(T value);
   void pop();
}


void main(){}
