module test;
unittest {
#line 1758 "0200-expressions.tex"
auto a1 = new(10) int[20];
assert(a1.length == 20);
auto a2 = new int[](30);
assert(a2.length == 30);
}


void main(){}
