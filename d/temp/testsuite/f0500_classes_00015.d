module test;
#line 1037 "0500-classes.tex"
class Contact { }
class Friend : Contact { }
void fun(Contact c) {  }
unittest {
   auto c = new Contact;   // c has type Contact
   fun(c);
   auto f = new Friend;    // f has type Friend
   fun(f);
}


void main(){}
