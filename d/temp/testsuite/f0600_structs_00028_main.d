module test;
#line 1718 "0600-structs.tex"
import std.stdio;/*[\nopagebreak]*/

struct A {
   char a;
   int b;
   char c;
}

void main() {
   A x;
   writefln("%s %s %s", x.a.offsetof, x.b.offsetof, x.c.offsetof);
}
