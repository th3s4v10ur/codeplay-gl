module test;
#line 2178 "0600-structs.tex"
string toString(E)(E value) if (is(E == enum)) {
   foreach (s; __traits(allMembers, E)) {
      if (value == mixin("E." ~ s)) return s;
   }
   return null;
}

enum OddWord { acini, alembicated, prolegomena, aprosexia }

void main() {
   auto w = OddWord.alembicated;
   assert(toString(w) == "alembicated");
}
