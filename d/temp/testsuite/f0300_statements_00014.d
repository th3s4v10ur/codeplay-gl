module test;
#line 749 "0300-statements.tex"
void scale(float[] array, float s) {
   foreach (ref e; array) {
      e *= s; // Updates array in place
   }
}


void main(){}
