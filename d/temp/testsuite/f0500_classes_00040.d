module test;
#line 3045 "0500-classes.tex"
class Widget {
   abstract uint width();
   abstract uint height();
}

Widget makeWidget(uint w, uint h) {
   return new class Widget {
      override uint width() { return w; }
      override uint height() { return h; }
      
   };
}


void main(){}
