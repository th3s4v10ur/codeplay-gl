module test;
// Context begin
#line 1232 "0500-classes.tex"
class Widget {
   
   this(Widget source) {
      // Copy state
   }
   Widget duplicate() {
      return new Widget(this); // Allocates memory
                               //    and calls this(Widget)
   }
}
// Context end
#line 1248 "0500-classes.tex"
class TextWidget : Widget {
   
   this(TextWidget source) {
      super(source);
      // Copy state
   }
   override Widget duplicate() {
      return new TextWidget(this);
   }
}


void main(){}
