module test;
unittest {
#line 346 "0400-functions.tex"
void fun(in int[][] data) {
   assert(!__traits(compiles, {data[5] = null;}));
   assert(!__traits(compiles, {data[5][0] = data[0][5];}));
}
}


void main(){}
