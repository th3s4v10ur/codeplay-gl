module test;
#line 247 "0400-functions.tex"
void bump(ref int x) { ++x; }
unittest {
   int x = 1;
   bump(x);
   assert(x == 2);
}


void main(){}
