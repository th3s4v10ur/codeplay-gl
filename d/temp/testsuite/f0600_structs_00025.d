module test;
// Context begin
#line 1574 "0600-structs.tex"
struct Final(T) {
   private T payload;
   this(T bindTo) {
      payload = bindTo;
   }
   // Disable assignment by leaving opAssign undefined
   private void opAssign(Final);
   // Subclass T, but do not allow rebinding
   @property T get() { return payload; }
   alias get this;
}
// Context end
#line 1607 "0600-structs.tex"
class Widget {
   private int x;
   @property int get() { return x; }
}
unittest {
   auto w = Final!Widget(new Widget);
   auto x = w.get;  // Gets the Widget in Final,
                    // Not the int in Widget
}


void main(){}
