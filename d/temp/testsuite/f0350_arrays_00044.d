module test;
unittest {
#line 1361 "0350-arrays.tex"
int[4] fixed = [1, 2, 3, 4];
auto anotherFixed = fixed;
assert(anotherFixed !is fixed); // Not the same (value semantics)
assert(anotherFixed == fixed);  // Same data
auto dynamic = fixed[];         // Fetches the limits of fixed
assert(dynamic is fixed);
assert(dynamic == fixed);       // Obviously
dynamic = dynamic.dup;          // Creates a copy
assert(dynamic !is fixed);
assert(dynamic == fixed);
}


void main(){}
