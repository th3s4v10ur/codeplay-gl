module test;
// Context begin
#line 3100 "0500-classes.tex"
interface Observer {
   void notify(Object data);
   
}
interface VisualElement {
   void draw();
   
}
interface Actor {
   void nudge();
   
}
interface VisualActor : Actor, VisualElement {
   void animate();
   
}
class Sprite : VisualActor, Observer {
   void draw() { }
   void animate() { }
   void nudge() { }
   void notify(Object data) { }
   
}
// Context end
// Context begin
#line 3195 "0500-classes.tex"
interface ObservantActor : Observer, Actor {
   void setActive(bool active);
}
interface HyperObservantActor : ObservantActor {
   void setHyperActive(bool hyperActive);
}
// Context end
#line 3207 "0500-classes.tex"
class Sprite3 : HyperObservantActor, VisualActor {
   override void notify(Object) { }
   override void setActive(bool) { }
   override void setHyperActive(bool) { }
   override void nudge() { }
   override void animate() { }
   override void draw() { }
   
}


void main(){}
