module test;
#line 58 "0350-arrays.tex"
import std.random;

void main() {
   // Anywhere between 1 and 127 elements
   auto array = new double[uniform(1, 128)];
   foreach (i;  0 .. array.length) {
      array[i] = uniform(0.0, 1.0);
   }
   
}
