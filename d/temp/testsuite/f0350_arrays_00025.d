module test;
unittest {
#line 736 "0350-arrays.tex"
auto array = [0, 2, 4, 6, 8, 10];
array = array[0 .. $ - 2];         // Right-shrink by two elements
assert(array == [0, 2, 4, 6]);
array = array[1 .. $];             // Left-shrink by one element
assert(array == [2, 4, 6]);
array = array[1 .. $ - 1];         // Shrink from both sides
assert(array == [4]);
}


void main(){}
