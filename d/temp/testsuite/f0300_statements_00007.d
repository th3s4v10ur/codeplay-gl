module test;
#line 356 "0300-statements.tex"
enum size_t
   g_maxDataSize = 100_000_000,
   g_maxMemory = 1_000_000_000;


// The declaration of Numeric will be seen at module scope
static if (g_maxMemory / 4 > g_maxDataSize) {
   alias double Numeric;
} else {
   alias float Numeric;
}

double transmogrify(double x) {
   Numeric[] y;
   // Complicated computation
   return y[0];
}


void main(){}
