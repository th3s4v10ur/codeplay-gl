module test;
#line 804 "0600-structs.tex"
import std.stdio;

struct S {
   int x = 42;
   ~this() {
      writeln("An S with payload ", x, " is going away. Bye!");
   }
}

void main() {
   writeln("Creating an object of type S.");
   {
      S object;
      writeln("Inside object's scope");
   }
   writeln("After object's scope.");
}
