module test;
// Context begin
#line 1822 "0350-arrays.tex"
import std.stdio;
// Context end
unittest {
#line 1825 "0350-arrays.tex"
auto gammaFunc = [-1.5:2.363, -0.5:-3.545, 0.5:1.772];
// Write all keys
foreach (k; gammaFunc.byKey()) {
   writeln(k);
}
}


void main(){}
