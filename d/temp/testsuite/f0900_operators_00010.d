module test;
#line 725 "0900-operators.tex"
struct CheckedInt(N) if (isIntegral!N) {
   private N value;
   ref CheckedInt opOpAssign(string op)(CheckedInt rhs)
         if (op == "+") {
      auto result = value + rhs.value;
      enforce(rhs.value >= 0 ? result >= value : result <= value);
      value = result;
      return this;
   }
   
}


void main(){}
