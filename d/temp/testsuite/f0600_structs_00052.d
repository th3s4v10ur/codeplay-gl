module test;
#line 2391 "0600-structs.tex"
template Select(bool cond, T1, T2) {
   static if (cond) {
      alias T1 Type;
   } else {
      alias T2 Type;
   }
}

unittest {
   alias Select!(false, int, string).Type MyType;
   static assert(is(MyType == string));
}


void main(){}
