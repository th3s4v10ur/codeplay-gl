module test;
// Context begin
#line 2403 "0400-functions.tex"
import std.stdio;
class Widget { void mayThrow() {} }
nothrow void tryLog(string msg) {}
// Context end
#line 2408 "0400-functions.tex"
nothrow void sensitive(Widget w) {
   tryLog("Starting sensitive operation");
   try {
      w.mayThrow();
      tryLog("Sensitive operation succeeded");
   } catch (Exception) {
      tryLog("Sensitive operation failed");
   }
}


void main(){}
