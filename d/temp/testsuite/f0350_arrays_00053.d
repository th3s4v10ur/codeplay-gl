module test;
// Context begin
#line 1649 "0350-arrays.tex"
string[string] aa;
unittest {
   aa["hello"] = "ciao";
}
// Context end
unittest {
#line 1655 "0350-arrays.tex"
assert(aa["hello"] == "ciao");
// Key "hello" exists, therefore ignore the second argument
assert(aa.get("hello", "salute") == "ciao");
// Key "yo" doesn't exist, return the second argument
assert(aa.get("yo", "buongiorno") == "buongiorno");
}


void main(){}
