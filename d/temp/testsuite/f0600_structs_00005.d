module test;
#line 466 "0600-structs.tex"
struct Widget {
   private int[] array;
   this(uint length) {
      array = new int[length];
   }
   int get(size_t offset) {
      return array[offset];
   }
   void set(size_t offset, int value) {
      array[offset] = value;
   }
}


void main(){}
