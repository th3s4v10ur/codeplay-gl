module test;
#line 1087 "0600-structs.tex"
struct Widget {
   private int[] array;
   // this(uint), this(this), etc.
   ref Widget opAssign(ref Widget rhs) {
      array = rhs.array.dup;
      return this;
   }
}


void main(){}
