module test;
unittest {
#line 590 "0350-arrays.tex"
auto a = ["hello", "world"];
auto b = a;
assert(a is b);              // Pass, a and b have the same bounds
assert(a == b);              // Pass, of course
b = a.dup;
assert(a == b);              // Pass, a and b are equal although
                             //    they have different locations
assert(a !is b);             // Pass, a and b are different although
                             //    they have equal contents
}


void main(){}
