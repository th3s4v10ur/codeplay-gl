module test;
#line 2410 "0600-structs.tex"
struct /* or class */ Select2(bool cond, T1, T2) { // Or class
   static if (cond) {
      alias T1 Type;
   } else {
      alias T2 Type;
   }
}

unittest {
   alias Select2!(false, int, string).Type MyType;
   static assert(is(MyType == string));
}


void main(){}
