module test;
unittest {
#line 44 "0350-arrays.tex"
auto array = new int[20];
auto x = array[5];         // Valid indices are 0 through 19
assert(x == 0);            // Initial element values are int.init = 0
array[7] = 42;             // Elements are assignable
assert(array[7] == 42);
}


void main(){}
