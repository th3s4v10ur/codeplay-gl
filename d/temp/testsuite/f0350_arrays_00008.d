module test;
unittest {
#line 119 "0350-arrays.tex"
string[] a;         // Same as string[] a = null
assert(a is null);
assert(a == null);  // Same as above
a = new string[2];
assert(a !is null);
a = a[0 .. 0];
assert(a !is null);
}


void main(){}
