module test;
#line 2390 "0500-classes.tex"
interface Transmogrifier {
   // Client interface
   final void thereAndBack() {
      transmogrify();
      untransmogrify();
   }
   // Implementation interface
private:
   void transmogrify() { }
   void untransmogrify() { }
}


void main(){}
