module test;
// Context begin
#line 2821 "0500-classes.tex"
class Outer {
   int x;
   void fun(int a) { }
   // Define an inner class
   class Inner {
      int y;
      void gun() {
         fun(x + y);
      }
   }
}
// Context end
#line 2865 "0500-classes.tex"
unittest {
   Outer obj1 = new Outer;
   auto obj = obj1.new Inner; // Aha!
}


void main(){}
