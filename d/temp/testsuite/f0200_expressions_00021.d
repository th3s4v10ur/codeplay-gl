module test;
unittest {
#line 953 "0200-expressions.tex"
int c = 2;
auto f = delegate double(int x) { return c * x / 10.; };
auto a = f(5);
assert(a == 1);
c = 3;
auto b = f(5);
assert(b == 1.5);
}


void main(){}
