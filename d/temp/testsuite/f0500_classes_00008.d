module test;
#line 501 "0500-classes.tex"
class Widget {
   this(uint height) {
      this(1, height); // Defer to the other constructor
   }
   this(uint width, uint height) {
      this.width = width;
      this.height = height;
   }
   uint width, height;
   
}


void main(){}
