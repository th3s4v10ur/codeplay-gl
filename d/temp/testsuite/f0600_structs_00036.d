module test;
#line 1921 "0600-structs.tex"
import std.exception;

struct TaggedUnion {
    enum Tag  { _tvoid, _tint, _tdouble, _tstring, _tarray }
    private Tag _tag;
    private union {
        int _int;
        double _double;
        string _string;
        TaggedUnion[] _array;
    }

public:
    void opAssign(int v) {
        _int = v;
        _tag = Tag._tint;
    }
    int getInt() {
        enforce(_tag == Tag._tint);
        return _int;
    }
    
}

unittest {
    TaggedUnion a;
    a = 4;
    assert(a.getInt() == 4);
}


void main(){}
