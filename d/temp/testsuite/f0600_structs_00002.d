module test;
// Context begin
#line 272 "0600-structs.tex"
import std.math;

struct Test {
   double a = 0.4;
   double b;
}

unittest {
   // Use the struct name as a function to create an object
   auto t = Test();
   assert(t.a == 0.4 && isnan(t.b));
}
// Context end
#line 295 "0600-structs.tex"
unittest {
   auto t1 = Test(1);
   assert(t1.a == 1 && isnan(t1.b));
   auto t2 = Test(1.5, 2.5);
   assert(t2.a == 1.5 && t2.b == 2.5);
}


void main(){}
