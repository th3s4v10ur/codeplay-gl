module test;
#line 2183 "0350-arrays.tex"
import std.stdio;

void main() {
   string a = "No matter how you put it, a \u03bb costs \u20AC20.";
   wstring b = "No matter how you put it, a \u03bb costs \u20AC20.";
   dstring c = "No matter how you put it, a \u03bb costs \u20AC20.";
   writeln(a, '\n', b, '\n', c);
}
