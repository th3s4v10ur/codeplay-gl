module test;
#line 1825 "0400-functions.tex"
import std.algorithm, std.array;

// Computes the average of a set of numbers,
//    passable directly or via an array.
double average(double[] values...) {
   if (values.empty) {
      throw new Exception("Average of zero elements is undefined");
   }
   return reduce!((a, b) { return a + b; })(0.0, values)
      / values.length;
}

unittest {
   assert(average(0) == 0);
   assert(average(1, 2) == 1.5);
   assert(average(1, 2, 3) == 2);
   // Passing arrays and slices works as well
   double[] v = [1, 2, 3];
   assert(average(v) == 2);
}


void main(){}
