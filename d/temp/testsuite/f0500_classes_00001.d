module test;
// Context begin
#line 98 "0500-classes.tex"
import std.stdio;

class A {
   int x = 42;
}

unittest {
   auto a1 = new A;
   assert(a1.x == 42);
   auto a2 = a1;
   a2.x = 100;
   assert(a1.x == 100);
}
// Context end
// Context begin
#line 158 "0500-classes.tex"
unittest {
   auto a1 = new A;
   auto a2 = new A;
   a1.x = 100;
   a2.x = 200;
   // Let's swap a1 and a2
   auto t = a1;
   a1 = a2;
   a2 = t;
   assert(a1.x == 200);
   assert(a2.x == 100);
}
// Context end
#line 222 "0500-classes.tex"
unittest {
   A a;
   assert(a is null);
   a = new A;
   assert(a !is null);
   a = null;
   assert(a is null);
   a = A.init;
   assert(a is null);
}


void main(){}
