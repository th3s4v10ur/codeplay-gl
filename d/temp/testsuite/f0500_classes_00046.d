module test;
#line 3650 "0500-classes.tex"
class StackImpl(T, Backend) : Stack!T {
   private Backend _store;
   @property bool empty() {
      return _store.empty;
   }
   @property ref T top() {
      assert(!empty);
      return _store.back;
   }
   void push(T value) {
      _store ~= value;
   }
   void pop() {
      assert(!empty);
      _store.popBack();
   }
}


void main(){}
