module test;
#line 2907 "0500-classes.tex"
class Outer {
   class Inner { }
   Inner _member;
   this() {
      _member = new Inner;           // Same as this.new Inner
      assert(_member.outer is this); // Check the link
   }
}


void main(){}
