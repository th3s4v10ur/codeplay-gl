module test;
// Context begin
#line 1547 "0200-expressions.tex"
unittest {
   assert(a && !b && c && d);
}
// Context end
#line 1552 "0200-expressions.tex"
bool
   a = is(int[5] : int[]),  // True, int[5] convertible to int[]
   b = is(int[5] == int[]), // False; they are distinct types
   c = is(uint : long),     // True
   d = is(ulong : long);    // True


void main(){}
