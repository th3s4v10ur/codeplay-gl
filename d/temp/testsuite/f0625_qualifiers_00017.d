module test;
#line 627 "0625-qualifiers.tex"
void print(const(int[]) data) { }
unittest {
   immutable(int[]) myData = [ 10, 20 ];
   print(myData); // Fine
   int[] myMutableData = [ 32, 42 ];
   print(myMutableData); // Fine
}


void main(){}
