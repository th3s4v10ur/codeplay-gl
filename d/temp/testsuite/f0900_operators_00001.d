module test;
#line 175 "0900-operators.tex"
struct CheckedInt(N) if (isIntegral!N) {
   private N value;
   ref CheckedInt opUnary(string op)() if (op == "++") {
      enforce(value != value.max);
      ++value;
      return this;
   }
   ref CheckedInt opUnary(string op)() if (op == "--") {
      enforce(value != value.min);
      --value;
      return this;
   }
   
}


void main(){}
