module test;
// Context begin
#line 1845 "0600-structs.tex"
union IntOrFloat {
   int _int;
   float _float;
}

unittest {
   IntOrFloat iof;
   iof._int = 5;
   // Read only iof._int, but not iof._float
   assert(iof._int == 5);
   iof._float = 5.5;
   // Read only iof._float, not iof._int
   assert(iof._float == 5.5);
}
// Context end
// Context begin
#line 1882 "0600-structs.tex"
unittest {
   IntOrFloat iof = { 5 };
   assert(iof._int == 5);
}
// Context end
// Context begin
#line 1892 "0600-structs.tex"
unittest {
   static IntOrFloat iof = { _float : 5 };
   assert(iof._float == 5);
}
// Context end
#line 1906 "0600-structs.tex"
unittest {
   IntOrFloat iof;
   iof._float = 1;
   assert(iof._int == 0x3F80_0000);
}


void main(){}
