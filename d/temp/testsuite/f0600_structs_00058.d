module test;
// Context begin
#line 2580 "0600-structs.tex"
mixin template InjectX() {
   private int x;
   int getX() { return x; }
   void setX(int y) {
      // Checks
      x = y;
   }
}
// Context end
#line 2594 "0600-structs.tex"
// Inject at module scope
mixin InjectX;

class A {
   // Inject into a class
   mixin InjectX;
   
}

void fun() {
   // Inject into a function
   mixin InjectX;
   setX(10);
   assert(getX() == 10);
}


void main(){}
