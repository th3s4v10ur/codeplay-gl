module test;
unittest {
#line 2356 "0350-arrays.tex"
int x = 42;
int* p = &x;     // Take the address of x
*p = 10;         // Using *p is the same as using x
++*p;            // Regular operators also apply
assert(x == 11); // x was modified through p
}


void main(){}
