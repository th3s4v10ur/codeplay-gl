module test;
unittest {
#line 1216 "0200-expressions.tex"
void fun(int val) {
   ubyte c;
   static assert(!__traits(compiles, c = val & 256));
   static assert(!__traits(compiles, c = val >>> 23));
}
}


void main(){}
