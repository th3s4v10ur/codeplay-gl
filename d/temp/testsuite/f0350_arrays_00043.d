module test;
unittest {
#line 1294 "0350-arrays.tex"
int[3] a = [1, 2, 3];
int[3] b = a;
a[1] = 42;
assert(b[1] == 2); // b is an independent copy of a
int[3] fun(int[3] x, int[3] y) {
   // x and y are copies of the arguments
   x[0] = y[0] = 100;
   return x;
}
auto c = fun(a, b);         // c has type int[3]
assert(c == [100, 42, 3]);
assert(b == [1, 2, 3]);     // b is unaffected by fun
}


void main(){}
