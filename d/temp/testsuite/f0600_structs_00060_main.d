module test;
#line 2688 "0600-structs.tex"
import std.stdio;

string lookMeUp = "Found at module level";

template TestT() {
   string get() { return lookMeUp; }
}

mixin template TestM() {
   string get() { return lookMeUp; }
}

void main() {
   string lookMeUp = "Found at function level";
   alias TestT!() asTemplate;
   mixin TestM!() asMixin;
   writeln(asTemplate.get());
   writeln(asMixin.get());
}
