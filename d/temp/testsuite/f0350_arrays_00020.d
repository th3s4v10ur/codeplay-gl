module test;
unittest {
#line 623 "0350-arrays.tex"
int[] a = [0, 10, 20];
int[] b = a ~ 42;
assert(b == [0, 10, 20, 42]);
a = b ~ a ~ 15;
assert(a.length == 8);
}


void main(){}
