module test;
#line 875 "0600-structs.tex"
import std.conv, std.stdio;

struct S {
   private string name;
   this(string name) {
      writeln(name, " checking in.");
      this.name = name;
   }
   ~this() {
      writeln(name, " checking out.");
   }
}

void main() {
   auto obj1 = S("first object");
   foreach (i; 0 .. 3) {
      auto obj = S(text("object ", i));
   }
   auto obj2 = S("last object");
}
