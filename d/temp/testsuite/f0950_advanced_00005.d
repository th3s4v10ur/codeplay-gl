module test;
#line 485 "0950-advanced.tex"
void swap1(T)(ref T a, ref T b) {
   auto t = a;
   a = b;
   b = t;
}


void main(){}
