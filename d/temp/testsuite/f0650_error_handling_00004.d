module test;
#line 217 "0650-error-handling.tex"
import std.stdio;

class MyException : Exception {
   this(string s) { super(s); }
}

void fun(int x) {
   if (x == 1) {
      throw new MyException("");
   } else if (x == 2) {
      throw new StdioException("");
  } else {
      throw new Exception("");
   }
}

void funDriver(int x) {
   try {
      fun(x);
   }
   catch (MyException e) {
      writeln("MyException");
   }
}

unittest {
   foreach (i; 1 .. 4) {
      try {
         funDriver(i);
      } catch (StdioException e) {
         writeln("StdioException");
      } catch (Exception e) {
         writeln("Just an Exception");
      }
   }
}


void main(){}
