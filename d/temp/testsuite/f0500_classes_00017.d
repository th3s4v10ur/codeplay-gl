module test;
#line 1157 "0500-classes.tex"
class Clickable {
   void onClick() { }
}
class Button : Clickable {
   void drawClicked() { }
   override void onClick() {
      drawClicked();    // Introduce graphical effect
      super.onClick();  // Dispatch click to listeners
   }
}


void main(){}
