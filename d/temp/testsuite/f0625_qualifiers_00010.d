module test;
#line 384 "0625-qualifiers.tex"
class C {
   void fun() {}
   void gun() immutable {}
}

unittest {
   auto c1 = new C;
   auto c2 = new immutable(C);
   c1.fun(); // Fine
   c2.gun(); // Fine
             // No other calls would work
}


void main(){}
