module test;
// Context begin
#line 1032 "0700-contracts.tex"
import std.conv;

class BasicDate {
   private uint day, month, year;
   string format(string spec)
   in {
      // Require str to be equal to "%Y/%m/%d"
      assert(spec == "%Y/%m/%d");
   }
   body {
      // Simplistic implementation
      return text(year, '/', month, '/', day);
   }
   
}
// Context end
#line 1065 "0700-contracts.tex"
import std.regex;

class Date : BasicDate {
   override string format(string spec)
   in {
      auto pattern = regex("(%[mdY%]|[^%])*");
      assert(!match(spec, pattern).empty);
   }
   body {
      string result;
      
      return result;
   }
   
}


void main(){}
