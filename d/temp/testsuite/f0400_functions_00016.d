module test;
#line 941 "0400-functions.tex"
// Two ordered functions: write(double) is less specialized than
//     write(int) because the former can be called with an int, but
//     the latter cannot be called with a double
void write(double);
void write(int);


void main(){}
