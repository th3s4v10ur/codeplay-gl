module test;
// Context begin
#line 821 "0300-statements.tex"
import std.stdio;
void main() {
   print(["Moon":  1.283,
   "Sun":   499.307,
   "Proxima Centauri": 133814298.759]);
}
// Context end
#line 829 "0300-statements.tex"
void print(double[string] map) {
   foreach (i, e; map) {
      writefln("array[`%s`] = %s;", i, e);
   }
}
