module test;
#line 1013 "0600-structs.tex"
void fun(int x) {
   assert(x != 0);
}

// Illustrating name lookup rules
struct S {
   int x = 1;
   static int y = 324;

   void fun(int x) {
      assert(x == 0);       // Fetch parameter x
      assert(this.x == 1);  // Fetch member x
   }

   void gun() {
      fun(0);               // Call method fun
      .fun(1);              // Call module-level fun
   }

   // unittests may be struct members
   unittest {
      S obj;
      obj.gun();
      assert(y == 324); // "Member" unittests see static data
   }
}


void main(){}
