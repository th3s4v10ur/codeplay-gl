module test;
unittest {
#line 1256 "0200-expressions.tex"
void fun(uint val) {
   ushort c;
   static assert(__traits(compiles, c = val / 100_000));
   static assert(!__traits(compiles, c = val / 10_000));
}
}


void main(){}
