module test;
#line 2927 "0500-classes.tex"
void fun(int x) {
   string y = "Hello";
   class Nested {
      double z;
      this() {
         // Access to parameter
         x = 42;
         // Access to local variable
         y = "world";
         // Access to own members
         z = 0.5;
      }
   }
   auto n = new Nested;
   assert(x == 42);
   assert(y == "world");
   assert(n.z == 0.5);
   
}


void main(){}
