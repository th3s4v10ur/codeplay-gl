module test;
#line 895 "0700-contracts.tex"
import std.file, std.utf;

string readText(in char[] filename)
out(result) {
   std.utf.validate(result);
}
body {
    return cast(string) read(filename);
}


void main(){}
