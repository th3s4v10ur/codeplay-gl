module test;
#line 753 "0900-operators.tex"
struct CheckedInt(N) if (isIntegral!N) {
   // As before
   // Define all assignment operators
   ref CheckedInt opOpAssign(string op)(CheckedInt rhs) {
      value = opBinary!op(rhs).value;
      return this;
   }
}


void main(){}
