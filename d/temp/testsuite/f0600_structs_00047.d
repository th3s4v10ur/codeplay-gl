module test;
#line 2257 "0600-structs.tex"
import std.stdio;

void fun(int) {}
void fun(string) {}
int var;
enum E { e }
struct S { int x; }
S s;

unittest {
   alias object.Object Root;         // Root of all classes
   alias std           phobos;       // Package name
   alias std.stdio     io;           // Module name
   alias var           sameAsVar;    // Variable
   alias E             MyEnum;       // Enumerated type
   alias E.e           myEnumValue;  // Value of that type
   alias fun           gun;          // Overloaded function
   alias S.x           field;        // Field of a struct
   alias s.x           sfield;       // Field of an object
}


void main(){}
