module test;
unittest {
#line 564 "0350-arrays.tex"
int[] array = [0, 1, 2];
int[] subarray = array[1 .. $];
assert(subarray.length == 2);
subarray[1] = 33;
assert(array[2] == 33); // Writing to subarray affected array
}


void main(){}
