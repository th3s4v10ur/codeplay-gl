module test;
#line 2498 "0600-structs.tex"
template isNumeric(T) {
   enum bool isNumeric = is(T : long) || is(T : real);
}

unittest {
  static assert(isNumeric!(int));
  static assert(!isNumeric!(char[]));
}


void main(){}
