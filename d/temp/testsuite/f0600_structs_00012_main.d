module test;
#line 945 "0600-structs.tex"
import std.stdio;

struct A {
   static ~this() {
      writeln("First static destructor");
   }
   
   static this() {
      writeln("First static constructor");
   }
   
   static this() {
      writeln("Second static constructor");
   }
   
   static ~this() {
      writeln("Second static destructor");
   }
}

void main() {
   writeln("This is main speaking");
}
