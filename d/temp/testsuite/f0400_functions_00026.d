module test;
// Context begin
#line 1324 "0400-functions.tex"
import std.algorithm;
int[] a = [ 1, 2, 3, 4, -5, 3, -4 ];
// Context end
#line 1328 "0400-functions.tex"
void transmogrify(int[] input, int z) {
   // Nested function
   bool isTransmogrifiable(int x) {
      if (x == 42) {
         throw new Exception("42 cannot be transmogrified");
      }
      return x < z;
   }
   // Find the first transmogrifiable element in the input
   input = find!(isTransmogrifiable)(input);
   
   // and again
   input = find!(isTransmogrifiable)(input);
   
}


void main(){}
