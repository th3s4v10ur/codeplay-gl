module test;
unittest {
#line 1032 "0350-arrays.tex"
int[] a = [0, 10, 20, 30, 40, 50, 60, 70];
auto b = a[4 .. $];
a = a[0 .. 4];
// At this point a and b are adjacent
a ~= [0, 0, 0, 0];
assert(b == [40, 50, 60, 70]); // Pass; a got reallocated
}


void main(){}
