module test;
// Context begin
#line 1251 "0400-functions.tex"
import std.algorithm;
// Context end
#line 1254 "0400-functions.tex"
unittest {
   int[] a = [ 1, 2, 3, 4, -5, 3, -4 ];
   int z = -2;
   // Find the first number less than z
   auto b = find!((x) { return x < z; })(a);
   assert(b == a[4 .. $]);
}


void main(){}
