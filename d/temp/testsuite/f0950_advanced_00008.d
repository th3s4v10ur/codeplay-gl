module test;
#line 558 "0950-advanced.tex"
import core.stdc.stdlib, std.exception;

void swap(T)(ref T a, ref T b) {
   static if (is(T == struct)){
      // For structs, move memory directly
      // First check for undue aliasing
      assert(!pointsTo(a, b) && !pointsTo(b, a) 
         && !pointsTo(a, a) && !pointsTo(b, b));
      // Swap bits
      ubyte[T.sizeof] t = void;
      memcpy(&t, &a, T.sizeof);
      memcpy(&a, &b, T.sizeof);
      memcpy(&b, &t, T.sizeof);
   } else {
      // For non-struct types, suffice to do the classic swap
      auto t = a;
      a = b;
      b = t;
   }
}


void main(){}
