module test;
unittest {
#line 884 "0200-expressions.tex"
auto f = function double(int x) { return x / 10.; };
auto a = f(5);
assert(a == 0.5);
}


void main(){}
