module test;
#line 595 "0625-qualifiers.tex"
void print(immutable(int[]) data) { }
unittest {
   immutable(int[]) myData = [ 10, 20 ];
   print(myData); // Fine
}


void main(){}
