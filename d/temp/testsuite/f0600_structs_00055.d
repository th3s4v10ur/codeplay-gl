module test;
#line 2460 "0600-structs.tex"
template factorial(uint n) {
   static if (n <= 1)
      enum ulong value = 1;
   else
      enum ulong value = factorial!(n - 1).value * n;
}


void main(){}
