module test;
#line 1760 "0400-functions.tex"
V reduce(alias fun, V, R)(V x, R range)
   if (is(typeof(x = fun(x, range.front)))
      && is(typeof(range.empty) == bool)
      && is(typeof(range.popFront())))
{
   for (; !range.empty; range.popFront()) {
      x = fun(x, range.front);
   }
   return x;
}


void main(){}
