module test;
#line 316 "0950-advanced.tex"
auto max(T...)(T a) if (T.length >= 2) {
   static if (a.length == 2) {
      return a[1] > a[0] ? a[1] : a[0];
   } else {
      return max(max(a[0], a[1]), a[2 .. $]);
   }
}


void main(){}
