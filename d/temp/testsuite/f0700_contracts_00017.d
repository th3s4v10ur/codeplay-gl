module test;
#line 1328 "0700-contracts.tex"
interface NVIStack(T) {
protected:
   ref T topImpl();
   void pushImpl(T value);
   void popImpl();

public:
   @property bool empty();

   final @property ref T top() {
      enforce(!empty);
      return topImpl();
   }

   final void push(T value) {
      enforce(!empty);
      pushImpl(value);
      enforce(value == topImpl());
   }

   final void pop() {
      assert(!empty);
      popImpl();
   }
}


void main(){}
