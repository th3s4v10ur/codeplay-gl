module test;
#line 1499 "0200-expressions.tex"
bool/*[\nopagebreak]*/
   a = is(int[]),    // True, int[] is a valid type
   b = is(int[5]),   // True, int[5] is also valid
   c = is(int[-3]),  // False, array size is invalid
   d = is(Blah);     // False (if Blah wasn't defined)


void main(){}
