module test;
// Context begin
#line 1799 "0350-arrays.tex"
import std.stdio;
// Context end
unittest {
#line 1802 "0350-arrays.tex"
auto gammaFunc = [-1.5:2.363, -0.5:-3.545, 0.5:1.772];
double[] keys = gammaFunc.keys;
assert(keys == [ -1.5, 0.5, -0.5 ]);
}


void main(){}
