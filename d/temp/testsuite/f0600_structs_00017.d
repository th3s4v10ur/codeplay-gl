module test;
#line 1116 "0600-structs.tex"
import std.algorithm;

struct Widget {
   private int[] array;
   // this(uint), this(this), etc.
   ref Widget opAssign(ref Widget rhs) {
      array = rhs.array.dup;
      return this;
   }
   ref Widget opAssign(Widget rhs) {
      swap(array, rhs.array);
      return this;
   }
}


void main(){}
