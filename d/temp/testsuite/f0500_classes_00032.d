module test;
#line 2710 "0500-classes.tex"
class Abstract {
   abstract void fun();
}
class Concrete : Abstract {
   override void fun() { }
}
class BornAgainAbstract : Concrete {
   abstract override void fun();
}


void main(){}
