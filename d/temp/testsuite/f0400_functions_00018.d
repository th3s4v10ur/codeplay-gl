module test;
#line 991 "0400-functions.tex"
// As above, plus 
void transmogrify(T)(T value) {}

unittest {
    transmogrify(42);      // Still calls transmogrify(uint)
    transmogrify("hello"); // Calls transmogrify(T), T=string
    transmogrify(1.1);     // Calls transmogrify(T), T=double
}


void main(){}
