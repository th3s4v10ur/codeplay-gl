module test;
// Context begin
#line 98 "0950-advanced.tex"
T max(T)(T a, T b) {
   return b > a ? b : a;
}
// Context end
#line 108 "0950-advanced.tex"
unittest {
   assert(max(4, 5) == 5);
   assert(max(2.2, 4.5) == 4.5);
   assert(max("Little", "Big") == "Little");
}


void main(){}
