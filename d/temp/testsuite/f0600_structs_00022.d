module test;
#line 1331 "0600-structs.tex"
struct S {
    private int a;  // Accessible within this file and S's methods
    package int b;  // Accessible within this file's directory
    public int c;   // Accessible from within the application
    export int d;   // Accessible outside the application
                    //    (where applicable)
}


void main(){}
