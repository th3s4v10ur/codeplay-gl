module test;
unittest {
#line 825 "0350-arrays.tex"
auto a = [87, 40, 10];
a ~= 42;
assert(a == [87, 40, 10, 42]);
a ~= [5, 17];
assert(a == [87, 40, 10, 42, 5, 17]);
}


void main(){}
