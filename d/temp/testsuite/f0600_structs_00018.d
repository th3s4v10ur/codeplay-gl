module test;
#line 1173 "0600-structs.tex"
struct Point {
   int x, y;
}
unittest {
   Point a, b;
   assert(a == b);
   a.x = 1;
   assert(a != b);
}


void main(){}
