module test;
#line 679 "0625-qualifiers.tex"
struct A {
   const(int[]) c;
   immutable(int[]) i;
}

unittest {
   const(A) ca;
   immutable(A) ia;
}


void main(){}
