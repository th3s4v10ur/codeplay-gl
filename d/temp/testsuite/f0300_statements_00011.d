module test;
#line 667 "0300-statements.tex"
struct A
{
    const bool opEquals(ref const A) { return true; }
    ref A opUnary(string op)() if (op == "++")
    {
        return this;
    }
}
unittest {
    A a, b;
    foreach (e; a .. b) {
    }
}


void main(){}
