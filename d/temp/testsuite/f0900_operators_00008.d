module test;
#line 570 "0900-operators.tex"
struct CheckedInt(N) if (isIntegral!N) {
   // As before
   // Implement right-hand operators
   CheckedInt opBinaryRight(string op)(N lhs) {
      return CheckedInt(lhs).opBinary!op(this);
   }
}


void main(){}
