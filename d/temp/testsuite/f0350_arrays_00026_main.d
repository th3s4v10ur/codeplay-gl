module test;
#line 772 "0350-arrays.tex"
import std.conv, std.stdio;

int main(string[] args) {
   // Get rid of the program name
   args = args[1 .. $];
   while (args.length >= 2) {
      if (to!int(args[0]) != to!int(args[$ - 1])) {
         writeln("not palindrome");
         return 1;
      }
      args = args[1 .. $ - 1];
   }
   writeln("palindrome");
   return 0;
}
