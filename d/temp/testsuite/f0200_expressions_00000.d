module test;
#line 229 "0200-expressions.tex"
auto
   a = 42,          // a has type int
   b = 42u,         // b has type uint
   c = 42UL,        // c has type ulong
   d = 4000000000,  // long; wouldn't fit in an int
   e = 4000000000u, // uint; it does fit in a uint
   f = 5000000000u; // ulong; wouldn't fit in a uint


void main(){}
