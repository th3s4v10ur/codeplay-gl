module test;
// Context begin
#line 121 "0600-structs.tex"
struct Widget {
   // A constant
   enum fudgeFactor = 0.2;
   // A shared immutable value
   static immutable defaultName = "A Widget";
   // Some state allocated for each Widget object
   string name = defaultName;
   uint width, height;
   // A static method
   static double howFudgy() {
      return fudgeFactor;
   }
   // A method
   void changeName(string another) {
      name = another;
   }
}
// Context end
#line 150 "0600-structs.tex"
class C {
   int x = 42;
   double y = 3.14;
}

struct S {
   int x = 42;
   double y = 3.14;
}

unittest {
   C c1 = new C;
   S s1;                 // No new for S: stack allocation
   auto c2 = c1;
   auto s2 = s1;
   c2.x = 100;
   s2.x = 100;
   assert(c1.x == 100);  // c1 and c2 refer to the same object
   assert(s1.x == 42);   //but s2 is a true copy of s1
}


void main(){}
