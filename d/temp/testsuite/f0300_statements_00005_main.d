module test;
#line 329 "0300-statements.tex"
import std.stdio;

void main() {
   static if (real.sizeof > double.sizeof) {{
      auto maximorum = real.max;
      writefln("Really big numbers - up to %s!", maximorum);
   }}
   /* Maximorum is invisible here */ 
}
