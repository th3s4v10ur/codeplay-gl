module test;
#line 395 "0300-statements.tex"
import std.stdio;

void classify(char c) {
   write("You passed ");
   switch (c) {
      case '#':
         writeln("a hash sign.");
         break;
      case '0': .. case '9':
         writeln("a digit.");
         break;
      case 'A': .. case 'Z': case 'a': .. case 'z':
         writeln("an ASCII character.");
         break;
      case '.', ',', ':', ';', '!', '?':
         writeln("a punctuation mark.");
         break;
      default:
         writeln("quite a character!");
         break;
   }
}


void main(){}
