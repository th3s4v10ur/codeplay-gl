module test;
// Context begin
#line 2609 "0400-functions.tex"
ulong primeFactorsOnly(ulong n) {
   ulong accum = 1;
   ulong iter = 2;
   for (; n >= iter * iter; iter += 2 - (iter == 2)) {
      if (n % iter) continue;
      accum *= iter;
      do n /= iter; while (n % iter == 0);
   }
   return accum * n;
}
// Context end
// Context begin
#line 2641 "0400-functions.tex"
unittest {
   assert(primeFactorsOnly(100) == 10);
   assert(primeFactorsOnly(11) == 11);
   assert(primeFactorsOnly(7 * 7 * 11 * 11 * 15) == 7 * 11 * 15);
   assert(primeFactorsOnly(129 * 2) == 129 * 2);
}
// Context end
// Context begin
#line 2654 "0400-functions.tex"
import std.numeric;
// Context end
// Context begin
#line 2657 "0400-functions.tex"
bool properLinearCongruentialParameters(ulong m, ulong a, ulong c) {
   // Bounds checking
   if (m == 0 || a == 0 || a >= m || c == 0 || c >= m) return false;
   // c and m are relatively prime
   if (gcd(c, m) != 1) return false;
   // a - 1 is divisible by all prime factors of m
   if ((a - 1) % primeFactorsOnly(m)) return false;
   // If a - 1 is multiple of 4, then m is a multiple of 4, too.
   if ((a - 1) % 4 == 0 && m % 4) return false;
   // Passed all tests
   return true;
}
// Context end
// Context begin
#line 2674 "0400-functions.tex"
unittest {
   // Our broken example
   assert(!properLinearCongruentialParameters(
      1UL << 32, 210, 123));
   // Numerical Recipes book /*[\cite{press2007numerical}]*/
   assert(properLinearCongruentialParameters(
      1UL << 32, 1664525, 1013904223));
   // Borland C/C++ compiler
   assert(properLinearCongruentialParameters(
      1UL << 32, 22695477, 1));
   // glibc
   assert(properLinearCongruentialParameters(
      1UL << 32, 1103515245, 12345));
   // ANSI C
   assert(properLinearCongruentialParameters(
      1UL << 32, 134775813, 1));
   // Microsoft Visual C/C++
   assert(properLinearCongruentialParameters(
      1UL << 32, 214013, 2531011));
}
// Context end
#line 2718 "0400-functions.tex"
unittest {
   enum ulong m = 1UL << 32, a = 1664525, c = 1013904223;
   // Method 1: use static assert
   static assert(properLinearCongruentialParameters(m, a, c));
   // Method 2: assign the result to an enum
   enum proper1 = properLinearCongruentialParameters(m, a, c);
   // Method 3: assign the result to a static value
   static proper2 = properLinearCongruentialParameters(m, a, c);
}


void main(){}
