module test;
unittest {
#line 1738 "0350-arrays.tex"
auto aa = [ "hello":1, "goodbye":2 ];
aa.remove("hello");
assert("hello" !in aa);
aa.remove("yowza");        // Has no effect: "yowza" was not in aa
}


void main(){}
