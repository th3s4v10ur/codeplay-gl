module test;
// Context begin
#line 1732 "0400-functions.tex"
import std.algorithm;
// Context end
#line 1735 "0400-functions.tex"
unittest {
   int[] r = [ 10, 14, 3, 5, 23 ];
   // Compute the sum of all elements
   int sum = reduce!((a, b) { return a + b; })(0, r);
   assert(sum == 55);
   // Compute minimum
   int min = reduce!((a, b) { return a < b ? a : b; })(r[0], r);
   assert(min == 3);
}


void main(){}
