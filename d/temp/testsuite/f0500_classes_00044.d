module test;
#line 3501 "0500-classes.tex"
interface Stack(T) {
   @property bool empty();
   @property ref T top();
   void push(T value);
   void pop();
}


void main(){}
