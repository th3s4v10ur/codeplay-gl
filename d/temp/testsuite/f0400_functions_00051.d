module test;
#line 2298 "0400-functions.tex"
ulong fib(uint n) {
   ulong iter(uint i, ulong fib_1, ulong fib_2) {
      return i == n
         ? fib_2
         : iter(i + 1, fib_1 + fib_2, fib_1);
   }
   return iter(0, 1, 0);
}


void main(){}
