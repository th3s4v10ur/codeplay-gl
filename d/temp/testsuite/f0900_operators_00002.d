module test;
#line 230 "0900-operators.tex"
struct CheckedInt(N) if (isIntegral!N) {
   private N value;
   this(N value) {
      this.value = value;
   }
   CheckedInt opUnary(string op)()
         if (op == "+" || op == "-" || op == "~") {
      return CheckedInt(mixin(op ~ "value"));
   }
   bool opUnary(string op)() if (op == "!") {
      return !value;
   }
   ref CheckedInt opUnary(string op)() if (op == "++" || op == "--") {
      enum limit = op == "++" ? N.max : N.min;
      enforce(value != limit);
      mixin(op ~ "value");
      return this;
   }
   
}


void main(){}
