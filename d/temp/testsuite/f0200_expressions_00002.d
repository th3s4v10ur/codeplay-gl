module test;
#line 361 "0200-expressions.tex"
auto
   a = 1.0,     // a has type double
   b = .345E2f, // b = 34.5 has type float
   c = 10f,     // c has type float due to suffix
   d = 10.,     // d has type double
   e = 0x1.fffffffffffffp1023, // e is the largest double
   f = 0XFp1F;  // f = 30.0 has type float


void main(){}
