module test;
unittest {
#line 762 "0300-statements.tex"
float[] arr = [ 1.0, 2.5, 4.0 ];
foreach (ref float elem; arr) {
   elem *= 2; // Fine
}
foreach (ref double elem; arr) { // /*[\codeError]*/
   elem /= 2;
}
}


void main(){}
