module test;
#line 2007 "0400-functions.tex"
import std.conv;

void write(T...)(T args) {
   foreach (arg; args) {
      stdout.rawWrite(to!string(arg));
   }
}

void writeln(T...)(T args) {
   write(args, '\n');
   stdout.flush();
}


void main(){}
