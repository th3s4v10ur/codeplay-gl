module test;
#line 1889 "0400-functions.tex"
import std.stdio;

void average(double[]) { writeln("non-variadic"); }
void average(double[]...) { writeln("variadic"); }
void main() {
   average(1, 2, 3);   // Writes "variadic"
   average([1, 2, 3]); // Writes "non-variadic"
}
