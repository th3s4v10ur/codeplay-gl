module test;
#line 1969 "0400-functions.tex"
import std.stdio;

void testing(T...)(T values) {
   writeln("Called with ", values.length, " arguments.");
   // Access each index and each value
   foreach (i, value; values) {
      writeln(i, ": ", typeid(T[i]), " ", value);
   }
}

void main() {
   testing(5, "hello", 4.2);
}
