module test;
#line 1744 "0600-structs.tex"
import std.stdio;

struct A {
   char a;
   int b;
   char c;
}

void main() {
   // Access field offsets without an object
   writefln("%s %s %s", A.init.a.offsetof,
      A.init.b.offsetof, A.init.c.offsetof);
}
