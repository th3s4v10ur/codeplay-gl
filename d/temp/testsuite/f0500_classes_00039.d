module test;
#line 2953 "0500-classes.tex"
class Calculation {
   double result() {
      double n;
      
      return n;
   }
}

Calculation truncate(double limit) {
   assert(limit >= 0);
   class TruncatedCalculation : Calculation {
      override double result() {
         auto r = super.result();
         if (r < -limit) r = -limit;
         else if (r > limit) r = limit;
         return r;
      }
   }
   return new TruncatedCalculation;
}


void main(){}
