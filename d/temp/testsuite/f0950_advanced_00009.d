module test;
// Context begin
#line 677 "0950-advanced.tex"
import std.range;

R linearSearch(R, E)(R haystack, E needle) {
   for (; !haystack.empty; haystack.popFront) {
      if (needle == haystack.front) {
         break;
      }
   }
   return haystack;
}
// Context end
#line 702 "0950-advanced.tex"
unittest {
   // test on arrays of integers
   int[] a1 = [ 1, 2, 5, 9, 3 ];
   int[] a2 = linearSearch(a1, 5);
   assert(a2 == a1[2 .. $]);
   // test on strings
   string b1 = "Mary has a little lamb.";
   string b2 = linearSearch(b1, ' ');
   assert(b2 == " has a little lamb.");
}


void main(){}
