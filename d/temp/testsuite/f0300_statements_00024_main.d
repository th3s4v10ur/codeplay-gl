module test;
#line 1208 "0300-statements.tex"
import std.math, std.stdio;

struct Point {
   double x, y;
   double norm() { return sqrt(x * x + y * y); }
}

void main() {
   Point p;
   int z;
   with (p) {
      x = 3;   // Assigns p.x
      p.y = 4; // It's fine to still use p explicitly
      writeln(norm()); // Writes p.norm, which is 5
      z = 1;           // z is still visible
   }
}
