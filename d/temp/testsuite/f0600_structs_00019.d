module test;
// Context begin
#line 1188 "0600-structs.tex"
import std.math, std.stdio;

struct Point {
   float x = 0, y = 0;
   /*[\textit{\textbf{// Added}}]*/
   bool opEquals(ref const Point rhs) const {
      // Perform an approximate comparison
      return approxEqual(x, rhs.x) && approxEqual(y, rhs.y);
   }
}

unittest {
   Point a, b;
   assert(a == b);
   a.x = 1e-8;
   assert(a == b);
   a.y = 1e-1;
   assert(a != b);
}
// Context end
#line 1227 "0600-structs.tex"
struct Rectangle {
   Point leftBottom, rightTop;
}

unittest {
   Rectangle a, b;
   assert(a == b);
   a.leftBottom.x = 1e-8;
   assert(a == b);
   a.rightTop.y = 5;
   assert(a != b);
}


void main(){}
