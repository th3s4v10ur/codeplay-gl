module test;
#line 1517 "0300-statements.tex"
bool g_verbose;

void silentFunction() {
   auto oldVerbose = g_verbose;
   scope(exit) g_verbose = oldVerbose;
   g_verbose = false;
   
}


void main(){}
