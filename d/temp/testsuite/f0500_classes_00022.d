module test;
#line 1557 "0500-classes.tex"
class C {
   // x is accessible only inside this file
   private int x;
   // This file plus all classes inheriting C directly or
   //    indirectly may call setX()
   protected void setX(int x) { this.x = x; }
   // Everybody may call getX()
   public int getX() { return x; }
}


void main(){}
