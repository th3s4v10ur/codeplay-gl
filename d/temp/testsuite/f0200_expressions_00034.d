module test;
#line 1648 "0200-expressions.tex"
unittest {
   /* Testing order of evaluation */
   string s;
   void delegate(string, string) fun(string) {
      s ~= "b";
      return delegate void(string, string) { s ~= "e"; };
   }
   fun(s ~= "a")(s ~= "c", s ~= "d");
   assert(s == "abcde", s);
}


void main(){}
