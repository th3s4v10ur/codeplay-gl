module test;
unittest {
#line 105 "0350-arrays.tex"
auto array = new int[100];

auto copy = array.dup;
assert(array !is copy);          // The arrays are distinct
assert(array == copy);           //     but have equal contents
}


void main(){}
