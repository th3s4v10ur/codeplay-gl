module test;
unittest {
#line 1087 "0350-arrays.tex"
auto array = new int[10];
array.length += 1000;            // Grow
assert(array.length == 1010);
array.length /= 10;
assert(array.length == 101);     // Shrink
}


void main(){}
