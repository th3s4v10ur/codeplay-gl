module test;
// Context begin
#line 1281 "0900-operators.tex"
import std.ctype;

string underscoresToCamelCase(string sym) {
   string result;
   bool makeUpper;
   foreach (c; sym) {
      if (c == '_') {
         makeUpper = true;
      } else {
         if (makeUpper) {
            result ~= toupper(c);
            makeUpper = false;
         } else {
            result ~= c;
         }
      }
   }
   return result;
}

unittest {
   assert(underscoresToCamelCase("hello_world") == "helloWorld");
   assert(underscoresToCamelCase("_a") == "A");
   assert(underscoresToCamelCase("abc") == "abc");
   assert(underscoresToCamelCase("a_bc_d_") == "aBcD");
}
// Context end
#line 1318 "0900-operators.tex"
class A {
   auto opDispatch(string m, Args...)(Args args) {
      return mixin("this."~underscoresToCamelCase(m)~"(args)");
   }
   int doSomethingCool(int x, int y) {
      
      return 0;
   }
}

unittest {
   auto a = new A;
   a.doSomethingCool(5, 6);   // Straight call
   a.do_something_cool(5, 6); // Same call going via opDispatch
}


void main(){}
