module test;
#line 2067 "0600-structs.tex"
struct Color {
   ubyte r, g, b;
}

enum
   red = Color(255, 0, 0),
   green = Color(0, 255, 0),
   blue = Color(0, 0, 255);


void main(){}
