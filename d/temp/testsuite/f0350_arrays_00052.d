module test;
unittest {
#line 1620 "0350-arrays.tex"
// Create a string-to-string associative array
auto aa = [ "hello":"salve", "world":"mundi" ];
// Overwrite values
aa["hello"] = "ciao";
aa["world"] = "mondo";
// Create some new key/value pairs
aa["cabbage"] = "cavolo";
aa["mozzarella"] = "mozzarella";
}


void main(){}
