module test;
unittest {
#line 1690 "0350-arrays.tex"
auto a1 = [ "Jane":10.0, "Jack":20, "Bob":15 ];
auto a2 = a1;                    // a1 and a2 refer to the same data
a1["Bob"] = 100;                 // Changing a1
assert(a2["Bob"] == 100);        //is the same as changing a2
a2["Sam"] = 3.5;                 //and vice
assert(a2["Sam"] == 3.5);        //    versa
}


void main(){}
