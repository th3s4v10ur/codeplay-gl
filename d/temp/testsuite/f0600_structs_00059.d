module test;
// Context begin
#line 2623 "0600-structs.tex"
mixin template InjectX(T) {
   private T x;
   T getX() { return x; }
   void setX(T y) {
      // Checks
      x = y;
   }
}
// Context end
// Context begin
#line 2636 "0600-structs.tex"
mixin InjectX!int;
mixin InjectX!double;
// Context end
// Context begin
#line 2647 "0600-structs.tex"
mixin InjectX!int MyInt;
mixin InjectX!double MyDouble;
// Context end
unittest {
#line 2656 "0600-structs.tex"
MyInt.setX(5);
assert(MyInt.getX() == 5);
MyDouble.setX(5.5);
assert(MyDouble.getX() == 5.5);
}


void main(){}
