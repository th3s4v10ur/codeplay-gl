module test;
#line 366 "0500-classes.tex"
import std.math;

class Test {
   double a = 0.4;
   double b;
}

unittest {
   // Use a new expression to create an object
   auto t = new Test;
   assert(t.a == 0.4 && isnan(t.b));
}


void main(){}
