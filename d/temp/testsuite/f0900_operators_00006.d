module test;
#line 457 "0900-operators.tex"
struct CheckedInt(N) if (isIntegral!N) {
   private N value;
   // Addition
   CheckedInt opBinary(string op)(CheckedInt rhs) if (op == "+") {
      auto result = value + rhs.value;
      enforce(rhs.value >= 0 ? result >= value : result < value);
      return result;
   }
   // Subtraction
   CheckedInt opBinary(string op)(CheckedInt rhs) if (op == "-") {
      auto result = value - rhs.value;
      enforce(rhs.value >= 0 ? result <= value : result > value);
      return result;
   }
   // Multiplication
   CheckedInt opBinary(string op)(CheckedInt rhs) if (op == "*") {
      auto result = value * rhs.value;
      enforce(value && result / value == rhs.value ||
         rhs.value && result / rhs.value == value ||
         result == 0);
      return result;
   }
   // Division and remainder
   CheckedInt opBinary(string op)(CheckedInt rhs)
         if (op == "/" || op == "%") {
      enforce(rhs.value != 0);
      return CheckedInt(mixin("value" ~ op ~ "rhs.value"));
   }
   // Shift
   CheckedInt opBinary(string op)(CheckedInt rhs)
         if (op == "<<" || op == ">>" || op == ">>>") {
      enforce(rhs.value >= 0 && rhs.value <= N.sizeof * 8);
      return CheckedInt(mixin("value" ~ op ~ "rhs.value"));
   }
   // Bitwise (unchecked, can't overflow)
   CheckedInt opBinary(string op)(CheckedInt rhs)
         if (op == "&" || op == "|" || op == "^") {
      return CheckedInt(mixin("value" ~ op ~ "rhs.value"));
   }
   
}


void main(){}
