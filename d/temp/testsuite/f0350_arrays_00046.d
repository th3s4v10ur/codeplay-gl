module test;
unittest {
#line 1418 "0350-arrays.tex"
auto array = new double[][5];  // Array of five arrays of double,
                               //    each initially null
// Make a triangular matrix
foreach (i, ref e; array) {
   e = new double[array.length - i];
}
}


void main(){}
