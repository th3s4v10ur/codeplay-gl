module test;
unittest {
#line 1060 "0350-arrays.tex"
int[] array;
assert(array.length == 0);
array.length = 1000;            // Grow
assert(array.length == 1000);
array.length = 500;
assert(array.length == 500);    // Shrink
}


void main(){}
