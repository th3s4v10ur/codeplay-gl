module test;
#line 2106 "0200-expressions.tex"
unittest {
   int  a = 50;
   uint b = 35;
   assert(!__traits(compiles, a << 33));
   auto c = a << b;      // Implementation-defined result
   auto d = a >> b;      // Implementation-defined result
}


void main(){}
