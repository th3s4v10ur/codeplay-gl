module test;
#line 1084 "0900-operators.tex"
import std.stdio;

class SimpleTree(T) {
private:
   T _payload;
   SimpleTree _left, _right;

public:
   this(T payload) {
      _payload = payload;
   }

   // inorder traversal of the tree
   int opApply(int delegate(ref T) dg) {
      auto result = dg(_payload);
      if (result) return result;
      if (_left) {
         result = _left.opApply(dg);
         if (result) return result;
      }
      if (_right) {
         result = _right.opApply(dg);
         if (result) return result;
      }
      return 0;
   }
}

void main() {
   auto obj = new SimpleTree!int(1);
   obj._left = new SimpleTree!int(5);
   obj._right = new SimpleTree!int(42);
   obj._right._left = new SimpleTree!int(50);
   obj._right._right = new SimpleTree!int(100);
   foreach (i; obj) {
      writeln(i);
   }
}
