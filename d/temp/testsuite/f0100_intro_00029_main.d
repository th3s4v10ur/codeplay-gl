module test;
// Context begin
#line 1566 "0100-intro.tex"
interface Stat {
   void accumulate(double x);
   void postprocess();
   double result();
}
// Context end
// Context begin
#line 1581 "0100-intro.tex"
class Min : Stat {
   private double min = double.max;
   void accumulate(double x) {
      if (x < min) {
         min = x;
      }
   }
   void postprocess() {} // Nothing to do
   double result() {
      return min;
   }
}
// Context end
#line 1623 "0100-intro.tex"
import std.exception, std.stdio;

void main(string[] args) {
   Stat[] stats;
   foreach (arg; args[1 .. $]) {
      auto newStat = cast(Stat) Object.factory("stats." ~ arg);
      enforce(newStat, "Invalid statistics function: " ~ arg);
      stats ~= newStat;
   }
   for (double x; readf(" %s ", &x) == 1; ) {
      foreach (s; stats) {
         s.accumulate(x);
      }
   }
   foreach (s; stats) {
      s.postprocess();
      writeln(s.result());
   }
}
