module test;
#line 3535 "0500-classes.tex"
class StackImpl(T) : Stack!T {
   private T[] _store;
   @property bool empty() {
      return _store.empty;
   }
   @property ref T top() {
      assert(!empty);
      return _store.back;
   }
   void push(T value) {
      _store ~= value;
   }
   void pop() {
      assert(!empty);
      _store.popBack();
   }
}


void main(){}
