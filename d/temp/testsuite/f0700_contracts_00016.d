module test;
#line 1288 "0700-contracts.tex"
interface Stack(T) {
   @property bool empty();

   @property ref T top()
   in {
      assert(!empty);
   }

   void push(T value)
   in {
      assert(!empty);
   }
   out {
      assert(value == top);
   }

   void pop()
   in {
      assert(!empty);
   }
}


void main(){}
