#line 1193 "0900-operators.tex"
class A {
   // Non-overridable method
   A opBinary(string op)(A rhs) {
      // Forward to an overridable function
      return opBinary(op, rhs);
   }
   // Overridable method, dispatch string at runtime
   A opBinary(string op, A rhs) {
      switch (op) {
         case "+":
            // Implement addition
            break;
         case "-":
            // Implement subtraction
            break;
         
      }
   }
}
