#line 2533 "0600-structs.tex"
template isNumeric(T) {
   enum bool test1 = is(T : long);
   enum bool test2 = is(T : real);
   enum bool isNumeric = test1 || test2;
}

unittest {
  static assert(!isNumeric!(int).test1);
}
