#line 1124 "0300-statements.tex"
unittest {
   assert(!__traits(compiles, {
      goto target;
      int x = 10;
      target: {}
   }));
}
