// Context begin
#line 376 "0650-error-handling.tex"
import std.conv, std.stdio;

class MyException : Exception {
   this(string s) { super(s); }
}

void fun() {
   try {
      throw new Exception("thrown from fun");
   } finally {
      gun(100);
   }
}

void gun(int x) {
   try {
      throw new MyException(text("thrown from gun #", x));
   } finally {
      if (x > 1) {
         gun(x - 1);
      }
   }
}
// Context end
#line 460 "0650-error-handling.tex"
unittest {
   try {
      fun();
   } catch (Exception e) {
      writeln("Primary exception: ", typeid(e), " ", e);
      for (Throwable c = e; (c = c.next) !is null; ) {
         writeln("Collateral exception: ", typeid(c), " ", c);
      }
   }
}
