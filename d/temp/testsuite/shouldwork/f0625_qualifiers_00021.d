#line 756 "0625-qualifiers.tex"
class X { }

class Y {
   private Y _another;
   inout(Y) another() inout {
      enforce(_another !is null);
      return _another;
   }
}
