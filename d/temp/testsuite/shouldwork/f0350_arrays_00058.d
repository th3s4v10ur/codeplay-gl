#line 1748 "0350-arrays.tex"
unittest {
   int[int] a = [ 1:2 ];
   assert(a.remove(1));
   assert(!a.remove(1));
}
