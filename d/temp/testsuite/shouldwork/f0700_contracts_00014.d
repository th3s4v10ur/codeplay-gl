// Context begin
#line 1161 "0700-contracts.tex"
import std.range, std.string;

class BasicDate {
   private uint day, month, year;
   string format(string spec)
   out(result) {
      assert(!result.empty || spec.empty);
   }
   body {
      return std.string.format("%04s/%02s/%02s", year, month, day);
   }
   
}
// Context end
#line 1181 "0700-contracts.tex"
import std.algorithm, std.regex;

class Date : BasicDate {
   override string format(string spec)
   out(result) {
      bool escaping;
      size_t expectedLength;
      foreach (c; spec) {
         switch (c) {
            case '%':
               if (escaping) {
                  ++expectedLength;
                  escaping = false;
               } else {
                  escaping = true;
               }
               break;
            case 'Y':
               if (escaping) {
                  expectedLength += 4;
                  escaping = false;
               }
               break;
            case 'm': case 'd':
               if (escaping) {
                  expectedLength += 2;
                  escaping = false;
               }
               break;
            default:
               assert(!escaping);
               ++expectedLength;
               break;
         }
      }
      assert(walkLength(result) == expectedLength);
   }
   body {
      string result;
      
      return result;
   }
   
}
