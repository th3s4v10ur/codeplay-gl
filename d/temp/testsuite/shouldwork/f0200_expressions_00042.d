#line 1975 "0200-expressions.tex"
unittest {
   {
      uint base;
      uint exp;
      static assert(is(typeof(base ^^ exp) == uint));
   }
   {
      int base;
      uint exp;
      static assert(is(typeof(base ^^ exp) == int));
   }
   {
      ulong base;
      uint exp;
      static assert(is(typeof(base ^^ exp) == ulong));
   }
   {
      long base;
      uint exp;
      static assert(is(typeof(base ^^ exp) == long));
   }
   {
      long base;
      int exp;
      static assert(is(typeof(base ^^ exp) == double));
   }
}
