#line 1108 "1000-concurrency.tex"
import std.algorithm, std.concurrency, std.exception, std.stdio;

void main() {
   enum bufferSize = 1024 * 100;
   auto tid = spawn(&fileWriter);
   // Read loop
   foreach (buffer; stdin.byChunk(bufferSize)) {
      send(tid, buffer.idup);
   }
}

void fileWriter() {
  // Write loop
   for (;;) {
      auto buffer = receiveOnly!(immutable(ubyte[]))();
      stdout.write(buffer);
   }
}
