// Context begin
#line 1369 "0900-operators.tex"
import std.variant;

alias Variant delegate(Dynamic self, Variant[] args...) DynMethod;
// Context end
// Context begin
#line 1383 "0900-operators.tex"
class Dynamic {
   private DynMethod[string] methods;
   void addMethod(string name, DynMethod m) {
      methods[name] = m;
   }
   void removeMethod(string name) {
      methods.remove(name);
   }
   // Dispatch dynamically on method
   Variant call(string methodName, Variant[] args...) {
      return methods[methodName](this, args);
   }
   // Provide syntactic sugar with opDispatch
   Variant opDispatch(string m, Args)(Args args...) {
      Variant[] packedArgs = new Variant[args.length];
      foreach (i, arg; args) {
         packedArgs[i] = Variant(arg);
      }
      return call(m, args);
   }
}
// Context end
#line 1409 "0900-operators.tex"
unittest {
  auto obj = new Dynamic;
  obj.addMethod("sayHello",
     Variant(Dynamic, Variant[]) {
        writeln("Hello, world!");
        return Variant();
     });
  obj.sayHello();  // Prints "Hello, world!"
}
