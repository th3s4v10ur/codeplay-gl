module test;
unittest {
#line 1707 "0200-expressions.tex"
int[] a = new int[5]; // Create an array of five integers
int[] b = a[3 .. 5];  // b refers to the last two elements of a
b[0] = 1;
b[1] = 3;
assert(a == [ 0, 0, 0, 1, 3 ]); // a was modified
}


void main(){}
