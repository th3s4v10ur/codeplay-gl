module test;
// Context begin
#line 1734 "0100-intro.tex"
interface Stat {
   void accumulate(double x);
   void postprocess();
   double result();
}
unittest {
   assert(!__traits(compiles, new IncrementalStat));
}
// Context end
#line 1744 "0100-intro.tex"
class IncrementalStat : Stat {
   protected double _result;
   abstract void accumulate(double x);
   void postprocess() {}
   double result() {
      return _result;
   }
}


void main(){}
