module test;
unittest {
#line 1316 "0200-expressions.tex"
short a;
assert(is(typeof(+a) == short));
assert(is(typeof(-a) == short));
assert(is(typeof(~a) == short));
assert(is(typeof(!a) == bool));
assert(is(typeof(++a) == short));
assert(is(typeof(a++) == short));
assert(is(typeof(--a) == short));
assert(is(typeof(a--) == short));
}


void main(){}
