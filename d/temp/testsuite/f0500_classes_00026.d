module test;
#line 2270 "0500-classes.tex"
interface Transmogrifier {
   void transmogrify();
   void untransmogrify();
   final void thereAndBack() {
      transmogrify();
      untransmogrify();
   }
}


void main(){}
