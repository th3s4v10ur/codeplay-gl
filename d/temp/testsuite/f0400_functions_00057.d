module test;
#line 2557 "0400-functions.tex"
// Implementation of Euclid's algorithm
ulong gcd(ulong a, ulong b) {
   while (b) {
      auto t = b;
      b = a % b;
      a = t;
   }
   return a;
}


void main(){}
