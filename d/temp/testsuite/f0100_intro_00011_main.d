module test;
#line 599 "0100-intro.tex"
import std.stdio, std.string;

void main() {
   uint[string] dictionary;
   foreach (line; stdin.byLine()) {
      // Break sentence into words
      // Add each word in the sentence to the vocabulary
      foreach (word; splitter(strip(line))) {
         if (word in dictionary) continue; // Nothing to do
         auto newID = dictionary.length;
         dictionary[word] = newID;
         writeln(newID, '\t', word);
      }
   }
}
