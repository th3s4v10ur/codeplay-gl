module test;
#line 424 "0625-qualifiers.tex"
class A {
   int a;
   int[] b;
   this() immutable {
      a = 5;
      b = [ 1, 2, 3 ];
      // Calling fun() wouldn't be allowed
   }
   void fun() immutable {
      
   }
}


void main(){}
