module test;
#line 2322 "0400-functions.tex"
ulong fib(uint n) {
   ulong fib_1 = 1, fib_2 = 0;
   foreach (i; 0 .. n) {
      auto t = fib_1;
      fib_1 += fib_2;
      fib_2 = t;
   }
   return fib_2;
}


void main(){}
