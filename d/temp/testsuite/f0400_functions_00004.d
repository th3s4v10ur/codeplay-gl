module test;
#line 274 "0400-functions.tex"
ref int bump(ref int x) { return ++x; }
unittest {
   int x = 1;
   bump(bump(x)); // Two increments
   assert(x == 3);
}


void main(){}
