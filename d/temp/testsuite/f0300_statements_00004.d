module test;
#line 273 "0300-statements.tex"
enum size_t
   g_maxDataSize = 100_000_000,
   g_maxMemory = 1_000_000_000;

double transmogrify(double x) {
   static if (g_maxMemory / 4 > g_maxDataSize) {
      alias double Numeric;
   } else {
      alias float Numeric;
   }
   Numeric[] y;
   // Complicated computation
   return y[0];
}


void main(){}
