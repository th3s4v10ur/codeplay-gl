module test;
unittest {
#line 1520 "0350-arrays.tex"
enum size_t rows = 64, columns = 128;
// Allocate a matrix with 64 rows and 128 columns
double[columns][rows] matrix;
// No need to allocate the array at all - it's a value
foreach (ref row; matrix) {
   // Use row of type double[columns]
}
}


void main(){}
