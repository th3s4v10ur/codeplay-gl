module test;
#line 160 "0350-arrays.tex"
void main() {
    auto a = new int[2];
    // The name "length" should not pop up in an index expression
    static assert(!is(typeof(a[length - 1])));
}
