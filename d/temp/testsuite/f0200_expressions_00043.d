module test;
unittest {
#line 2084 "0200-expressions.tex"
int a = -1;               // That's 0xFFFF_FFFF
int b = a << 1;
assert(b == -2);          // 0xFFFF_FFFE
int c = a >> 1;
assert(c == -1);          // 0xFFFF_FFFF
int d = a >>> 1;
assert(d == +2147483647); // 0x7FFF_FFFF
}


void main(){}
