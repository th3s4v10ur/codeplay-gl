module test;
#line 2478 "0400-functions.tex"
import std.stdio;

void main() {
   enum uint a = 210, c = 123, x0 = 1_780_588_661;
   auto x = x0;
   foreach (i; 0 .. 100) {
      x = a * x + c;
      writeln(x);
   }
}
