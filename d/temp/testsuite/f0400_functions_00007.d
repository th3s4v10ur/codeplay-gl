module test;
#line 369 "0400-functions.tex"
// Computes divisor and remainder of a and b
// Returns divisor by value, remainder in the `rem' parameter
int divrem(int a, int b, out int rem) {
   assert(b != 0);
   rem = a % b;
   return a / b;
}
unittest {
   int r;
   int d = divrem(5, 2, r);
   assert(d == 2 && r == 1);
}


void main(){}
