module test;
// Context begin
#line 1057 "0500-classes.tex"
class Contact { string bgColor() { return ""; } }
class Friend : Contact {
   override string bgColor() { return "LightGreen"; }
}
// Context end
// Context begin
#line 1063 "0500-classes.tex"
unittest {
   Contact c = new Friend; // c has type Contact
                           //    but really refers to a Friend
   assert(c.bgColor() == "LightGreen");
                           // It's a friend indeed!
}
// Context end
#line 1097 "0500-classes.tex"
unittest {
   auto c = new Contact;   // c has static and dynamic type Contact
   auto f = cast(Friend) c;
   assert(f is null);      // f has static type Friend and is unbound
   c = new Friend;         // Static: Contact, dynamic: Friend
   f = cast(Friend) c;     // Static: Friend, dynamic: Friend
   assert(f !is null);     // Passes!
}


void main(){}
