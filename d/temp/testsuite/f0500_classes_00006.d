module test;
#line 462 "0500-classes.tex"
class NoGo {
   void fun() {
      // Let's just rebind this to a different object
      this = new NoGo; // /*[\codeError]*/ Cannot modify `this'!
   }
}


void main(){}
