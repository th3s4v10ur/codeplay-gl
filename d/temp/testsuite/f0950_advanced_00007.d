module test;
#line 544 "0950-advanced.tex"
/**
Returns true if source's representation embeds a pointer
that points to target's representation or somewhere inside
it. Note that evaluating pointsTo(x, x) checks whether x has
internal pointers.
*/
bool pointsTo(S, T)(ref S source, ref T target);


void main(){}
