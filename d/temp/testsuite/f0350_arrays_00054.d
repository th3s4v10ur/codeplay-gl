module test;
// Context begin
#line 1668 "0350-arrays.tex"
import std.stdio;
string[string] aa;
unittest {
   aa["hello"] = "ciao";
}
// Context end
unittest {
#line 1675 "0350-arrays.tex"
assert("hello" in aa);
assert("yowza" !in aa);
// Trying to read aa["yowza"] would throw
}


void main(){}
