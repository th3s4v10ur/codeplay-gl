module test;
#line 170 "0650-error-handling.tex"
import std.stdio;

class MyException : Exception {
   this(string s) { super(s); }
}

void fun(int x) {
   if (x == 1) {
      throw new MyException("");
   } else {
      throw new StdioException("");
   }
}

void main() {
   foreach (i; 1 .. 3) {
      try {
         fun(i);
      } catch (StdioException e) {
         writeln("StdioException");
      } catch (Exception e) {
         writeln("Exception");
      }
   }
}
