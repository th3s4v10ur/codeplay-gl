module test;
#line 449 "0625-qualifiers.tex"
import std.exception;
class List {
   private int payload;
   private List next;
   this(int[] data) immutable {
      enforce(data.length);
      payload = data[0];
      if (data.length == 1) return;
      next = new immutable(List)(data[1 .. $]);
   }
}


void main(){}
