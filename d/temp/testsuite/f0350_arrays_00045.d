module test;
unittest {
#line 1384 "0350-arrays.tex"
double[2] a;
double[] b = a ~ 0.5;   // Concat double[2] with value, get double[]
auto c = a ~ 0.5;       // Same as above
double[3] d = a ~ 1.5;  // Fine, explicitly ask for fixed-size array
double[5] e = a ~ d;    // Fine, explicitly ask for fixed-size array
}


void main(){}
