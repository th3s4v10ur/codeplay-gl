module test;
// Context begin
#line 679 "0500-classes.tex"
import core.stdc.stdlib;

class Buffer {
   private void* data;
   // Constructor
   this() {
      data = malloc(1024);
   }
   // Destructor
   ~this() {
      free(data);
   }
   
}
// Context end
#line 751 "0500-classes.tex"
unittest {
   auto b = new Buffer;
   
   clear(b); // Get rid of b's extra state
   // b is still usable here
}


void main(){}
