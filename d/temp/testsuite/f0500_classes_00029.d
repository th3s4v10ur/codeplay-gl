module test;
#line 2577 "0500-classes.tex"
interface Timer {
   final void run() { }
   
}
interface Application {
   final void run() { }
   
}
class TimedApp : Timer, Application {
   // Cannot define run()
}


void main(){}
