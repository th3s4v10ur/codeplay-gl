module test;
#line 2332 "0600-structs.tex"
// This is object.di
// Define the type of the difference between two pointers
static if (size_t.sizeof == 4) {
   alias int ptrdiff_t;
} else {
   alias long ptrdiff_t;
}
// Use ptrdiff_t 


void main(){}
