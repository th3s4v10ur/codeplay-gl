module test;
unittest {
#line 2120 "0350-arrays.tex"
string a = "hello";
string b = a;          // b is also "hello"
string c = b[0 .. 4];  // c is "hell"
// If this were allowed, it would change a, b, and c
// a[0] = 'H';
// The concatenation below leaves b and c unmodified
a = 'H' ~ a[1 .. $];
assert(a == "Hello" && b == "hello" && c == "hell");
}


void main(){}
