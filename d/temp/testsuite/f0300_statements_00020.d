module test;
#line 1070 "0300-statements.tex"
void fun(string[] strings) {
   loop: foreach (s; strings) {
      switch (s) {
      default:; break;   // Break the switch
      case "ls":; break; // Break the switch
      case "rm":; break; // Break the switch
      
      case "#": break loop;  // Ignore rest of strings (break foreach)
      }
   }
   
}


void main(){}
