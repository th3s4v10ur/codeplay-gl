module test;
// Context begin
#line 1181 "0400-functions.tex"
import std.algorithm;
// Context end
#line 1184 "0400-functions.tex"
unittest {
   int[] a = [ 1, 2, 3, 4, -5, 3, -4 ];
   // Find the first negative number
   auto b = find!(function bool(int x) { return x < 0; })(a);
}


void main(){}
