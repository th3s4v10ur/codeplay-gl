module test;
#line 2227 "0200-expressions.tex"
import std.stdio;

void main() {
   auto a = "some thing";
   auto b = a; // a and b refer to the same array
   a is b && writeln("Yep, they're the same thing really");
   auto c = "some (other) thing";
   a is c || writefln("Indeed... not the same");
}
