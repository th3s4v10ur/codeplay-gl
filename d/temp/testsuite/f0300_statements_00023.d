module test;
unittest {
#line 1164 "0300-statements.tex"
enum Pref { superFast, veryFast, fast, accurate,
   regular, slow, slower };
Pref preference;
double coarseness = 1;

switch (preference) {
  case Pref.fast:; break;
  case Pref.veryFast: coarseness = 1.5; goto case Pref.fast;
  case Pref.superFast: coarseness = 3; goto case Pref.fast;
  case Pref.accurate:; break;
  case Pref.regular: goto default;
  default: 
  
}
}


void main(){}
