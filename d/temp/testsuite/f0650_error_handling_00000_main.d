module test;
#line 39 "0650-error-handling.tex"
import std.stdio;

void main() {
   try {
      auto x = fun();
   } catch (Exception e) {
      writeln(e);
   }
}

int fun() {
   return gun() * 2;
}

int gun() {
   throw new Exception("Going straight back to main");
}
