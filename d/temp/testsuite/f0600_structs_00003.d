module test;
#line 361 "0600-structs.tex"
struct Test {
   double a = 0.4;
   double b;
   this(double b) {
      this.b = b;
   }
}
unittest {
   auto t = Test(5);
}


void main(){}
