module test;
#line 1427 "0600-structs.tex"
void fun(int a) {
   int b;
   struct Local {
      int c;
      int sum() {
         // Access parameter, variable, and Local's own member
         return a + b + c;
      }
   }
   Local obj;
   int x = obj.sum();
   assert(Local.sizeof == 2 * size_t.sizeof);
}
unittest {
   fun(5);
}


void main(){}
