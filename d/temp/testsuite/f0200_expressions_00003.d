module test;
unittest {
#line 453 "0200-expressions.tex"
auto c1 = '\xee';
static assert(is(typeof(c1) == char));
auto c2 = '\u1110';
static assert(is(typeof(c2) == wchar));
auto c3 = '\U00010010';
static assert(is(typeof(c3) == dchar));
auto c4 = '\&copy;';
static assert(is(typeof(c4) == dchar));
auto c5 = '\377';
static assert(is(typeof(c5) == char));
// auto c6 = '\&copy;'w;
// static assert(is(typeof(c6) == dchar));
}


void main(){}
