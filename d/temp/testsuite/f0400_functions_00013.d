module test;
// Context begin
#line 770 "0400-functions.tex"
T1[] find(T1, T2)(T1[] longer, T2[] shorter)
   if (is(typeof(longer[0 .. 1] == shorter) : bool))
{
   while (longer.length >= shorter.length) {
      if (longer[0 .. shorter.length] == shorter) {
         return longer;
      }
      longer = longer[1 .. $];
   }
   return null;
}
// Context end
#line 837 "0400-functions.tex"
import std.conv;
unittest {
   // Test the newly introduced overload
   double[] d1 = [ 6.0, 1.0, 2.5, 3 ];
   float[] d2 = [ 1.0, 2.5 ];
   assert(find(d1, d2) == d1[1 .. $], text(find(d1, d2)));
}


void main(){}
