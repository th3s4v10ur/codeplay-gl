module test;
#line 1723 "0300-statements.tex"
import std.exception, std.stdio;

void transactionalCreate(string filename) {
   string tempFilename = filename ~ ".fragment";
   scope(success) {
      std.file.rename(tempFilename, filename);
   }
   auto f = File(tempFilename, "w");
   // Write to f at your leisure
}


void main(){}
