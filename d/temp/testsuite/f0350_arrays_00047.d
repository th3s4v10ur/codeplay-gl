module test;
unittest {
#line 1499 "0350-arrays.tex"
enum size_t columns = 128;
// Define a matrix with 64 rows and 128 columns
auto matrix = new double[columns][64];
// No need to allocate each row - they already exist in situ
foreach (ref row; matrix) {
   // Use row of type double[columns]
}
}


void main(){}
