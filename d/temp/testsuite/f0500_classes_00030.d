module test;
// Context begin
#line 2628 "0500-classes.tex"
class Rectangle {
   uint left, right, top, bottom;
}
// Context end
#line 2633 "0500-classes.tex"
class Shape {
   protected Rectangle _bounds;
   abstract void draw();
   bool overlaps(Shape that) {
      return _bounds.left <= that._bounds.right &&
         _bounds.right >= that._bounds.left &&
         _bounds.top <= that._bounds.bottom &&
         _bounds.bottom >= that._bounds.top;
   }
}


void main(){}
