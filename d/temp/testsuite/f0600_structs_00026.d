module test;
#line 1623 "0600-structs.tex"
struct Final(T) {
   private T Final_payload;
   this(T bindTo) {
      Final_payload = bindTo;
   }
   // Disable assignment
   @disable void opAssign(Final);
   // Subclass T, but do not allow rebinding
   @property T Final_get() { return Final_payload; }
   alias Final_get this;
}


void main(){}
