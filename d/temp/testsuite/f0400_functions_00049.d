module test;
// Context begin
#line 2254 "0400-functions.tex"
pure bool leapYear(uint y) {
    return (y % 4) == 0 && (y % 100 || (y % 400) == 0);
}
// Context end
#line 2259 "0400-functions.tex"
// Certified pure
pure uint daysInYear(uint y) {
   return 365 + leapYear(y);
}


void main(){}
