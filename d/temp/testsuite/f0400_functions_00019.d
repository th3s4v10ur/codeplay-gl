module test;
#line 1163 "0400-functions.tex"
T[] find(alias pred, T)(T[] input)
   if (is(typeof(pred(input[0])) == bool))
{
   for (; input.length > 0; input = input[1 .. $]) {
      if (pred(input[0])) break;
   }
   return input;
}


void main(){}
