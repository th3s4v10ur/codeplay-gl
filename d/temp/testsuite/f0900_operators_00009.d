module test;
#line 604 "0900-operators.tex"
struct Complex(N) if (isFloatingPoint!N) {
   N re, im;
   // Implement commutative operators
   Complex opBinaryRight(string op)(N lhs)
      if (op == "+" || op == "*")
   {
      // Assumes the left-hand-side operator is implemented
      return opBinary!op(lhs);
   }
   // Implement non-commutative operators by hand
   Complex opBinaryRight(string op)(N lhs) if (op == "-") {
     return Complex(lhs - re, -im);
   }
   Complex opBinaryRight(string op)(N lhs) if (op == "/") {
      auto norm2 = re * re + im * im;
      enforce(norm2 != 0);
      auto t = lhs / norm2;
      return Complex(re * t, -im * t);
   }
}


void main(){}
