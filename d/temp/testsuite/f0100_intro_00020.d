module test;
#line 945 "0100-intro.tex"
import std.array;

bool binarySearch(T)(T[] input, T value) {
   if (input.empty) return false;
   auto i = input.length / 2;
   auto mid = input[i];
   if (mid > value) return binarySearch(input[0 .. i]);
   if (mid < value) return binarySearch(input[i + 1 .. $]);
   return true;
}


void main(){}
