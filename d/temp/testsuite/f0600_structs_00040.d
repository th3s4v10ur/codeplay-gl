module test;
// Context begin
#line 2085 "0600-structs.tex"
enum OddWord { acini, alembicated, prolegomena, aprosexia }
// Context end
unittest {
#line 2093 "0600-structs.tex"
OddWord w;
assert(w == OddWord.acini);  // Default initializer is
                             //    the first value in the set: acini
w = OddWord.aprosexia;       // Always use type name to qualify the
                             //    value name
                             //    (it's not what you might think btw)
int x = w;                   // OddWord is convertible to int
                             //    but not vice versa
assert(x == 3);              // Values are numbered 0, 1, 2, 
}


void main(){}
