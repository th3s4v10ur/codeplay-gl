module test;
#line 350 "0900-operators.tex"
struct CheckedInt(N) if (isIntegral!N) {
   private N value;
   // Conversions to all integrals
   N1 opCast(N1)() if (isIntegral!N1) {
      static if (N.min < N1.min) {
         enforce(N1.min <= value);
      }
      static if (N.max > N1.max) {
         enforce(N1.max >= value);
      }
      // It is now safe to do a raw cast
      return cast(N1) value;
   }
   
}


void main(){}
