module test;
// Context begin
#line 511 "0600-structs.tex"
struct Widget {
   private int[] array;
   this(uint length) {
      array = new int[length];
   }
   /*[\textit{\textbf{// Postblit constructor}}]*/
   this(this) {
      array = array.dup;
   }
   // As before
   int get(size_t offset) { return array[offset]; }
   void set(size_t offset, int value) { array[offset] = value; }
}
// Context end
// Context begin
#line 551 "0600-structs.tex"
unittest {
   auto w1 = Widget(10);
   auto w2 = w1;              // this(this) invoked here against w2
   w1.set(5, 100);
   w2.set(5, 42);
   assert(w1.get(5) == 100);  // Pass
}
// Context end
// Context begin
#line 567 "0600-structs.tex"
void fun(Widget w) {          // Pass by value
   w.set(2, 42);
}

void gun(ref Widget w) {      // Pass by reference
   w.set(2, 42);
}

unittest {
   auto w1 = Widget(10);
   w1.set(2, 100);
   fun(w1);                   // A copy is created here
   assert(w1.get(2) == 100);  // Pass
   gun(w1);                   // No copy
   assert(w1.get(2) == 42);   // Pass
}
// Context end
// Context begin
#line 594 "0600-structs.tex"
struct Widget2 {
   Widget w1;
   int x;
}

struct Widget3 {
   Widget2 w2;
   string name;
   this(this) {
      name = name ~ " (copy)";
   }
}
// Context end
// Context begin
#line 617 "0600-structs.tex"
unittest {
   Widget2 a;
   a.w1 = Widget(10);                  // Allocate some data
   auto b = a;                         // this(this) called for b.w
   assert(a.w1.array !is b.w1.array);  // Pass

   Widget3 c;
   c.w2.w1 = Widget(20);
   auto d = c;                         // this(this) for d.w2.w
   assert(c.w2.w1.array !is d.w2.w1.array);  // Pass
}
// Context end
// Context begin
#line 695 "0600-structs.tex"
Widget hun(uint x) {
   return Widget(x * 2);
}

unittest {
   auto w = hun(1000);
   
}
// Context end
// Context begin
#line 716 "0600-structs.tex"
Widget iun(uint x) {
   auto result = Widget(x * 2);
   
   return result;
}

unittest {
   auto w = iun(1000);
   
}
// Context end
// Context begin
#line 733 "0600-structs.tex"
void jun(Widget w) {
   
}

unittest {
   auto w = Widget(1000);
   // /*[\metai{code}]*/
   jun(w);
   // /*[\metaii{code}]*/
}
// Context end
#line 772 "0600-structs.tex"
import std.algorithm;

void kun(Widget w) {
   
}

unittest {
   auto w = Widget(1000);
   // /*[\metai{code}]*/
   /*[\textit{\textbf{// Call to move inserted}}]*/
   kun(move(w));
   assert(w == Widget.init); // Passes
   // /*[\metaii{code}]*/
}


void main(){}
