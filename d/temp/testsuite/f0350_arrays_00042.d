module test;
unittest {
#line 1279 "0350-arrays.tex"
int[10] a;
int[] b = a[1 .. 7];   // Fine
auto c = a[1 .. 7];    // Fine, c also has type int[]
int[6] d = a[1 .. 7];  // Fine, a[1 .. 7] copied into d
}


void main(){}
