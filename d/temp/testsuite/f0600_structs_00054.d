module test;
#line 2437 "0600-structs.tex"
template isSomeString(T) {
   enum bool value = is(T : const(char[]))
      || is(T : const(wchar[])) || is(T : const(dchar[]));
}

unittest {
   // Non-strings
   static assert(!isSomeString!(int).value);
   static assert(!isSomeString!(byte[]).value);
   // Strings
   static assert(isSomeString!(char[]).value);
   static assert(isSomeString!(dchar[]).value);
   static assert(isSomeString!(string).value);
   static assert(isSomeString!(wstring).value);
   static assert(isSomeString!(dstring).value);
   static assert(isSomeString!(char[4]).value);
}


void main(){}
