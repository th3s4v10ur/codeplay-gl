module test;
#line 2308 "0600-structs.tex"
// Define a container class
class Container(T) {
   alias T ElementType;
   
}

unittest {
   Container!int container;
   Container!int.ElementType element;
   
}


void main(){}
