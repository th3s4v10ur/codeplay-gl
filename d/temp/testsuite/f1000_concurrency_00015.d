module test;
#line 2529 "1000-concurrency.tex"
shared struct SharedList(T) {
   shared struct Node {
      private T _payload;
      private Node * _next;

      @property shared(Node)* next() {
         return clearlsb(_next);
      }

      bool removeAfter() {
         shared(Node)* thisNext, afterNext;
         // Step 1: set the lsb of _next for the node to delete
         do {
            thisNext = next;
            if (!thisNext) return false;
            afterNext = thisNext.next;
        } while (!cas(&thisNext._next, afterNext, setlsb(afterNext)));
        // Step 2: excise the node to delete
        if (!cas(&_next, thisNext, afterNext)) {
           afterNext = thisNext._next;
           while (!haslsb(afterNext)) {
              thisNext._next = thisNext._next.next;
           }
           _next = afterNext;
        }
      }

      void insertAfter(T value) {
         auto newNode = new Node(value);
         for (;;) {
            // Attempt to find an insertion point
            auto n = _next;
            while (n && haslsb(n)) {
               n = n._next;
            }
            // Found a possible insertion point, attempt insert
            auto afterN = n._next;
            newNode._next = afterN;
            if (cas(&n._next, afterN, newNode)) {
               break;
            }
         }
      }
   }

   private Node * _root;

   void pushFront(T value) {
      // Same as for Stack.push
   }

   shared(T)* popFront() {
      // Same as for Stack.pop
   }
}


void main(){}
