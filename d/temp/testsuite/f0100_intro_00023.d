module test;
// Context begin
#line 1043 "0100-intro.tex"
import std.algorithm, std.array, std.stdio;
uint[string] freqs;
// Context end
unittest {
#line 1047 "0100-intro.tex"
// Print counts
string[] words = freqs.keys;
sort!((a, b) { return freqs[a] > freqs[b]; })(words);
foreach (word; words) {
   writefln("%6u\t%s", freqs[word], word);
}
}


void main(){}
