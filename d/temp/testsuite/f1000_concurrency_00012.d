module test;
// Context begin
#line 1865 "1000-concurrency.tex"
import std.exception;
// Context end
#line 1868 "1000-concurrency.tex"
// D interlocked bank account using a synchronized class
synchronized class BankAccount {
   private double _balance;
   void deposit(double amount) {
      _balance += amount;
   }
   void withdraw(double amount) {
      enforce(_balance >= amount);
      _balance -= amount;
   }
   double balance() {
      return _balance;
   }
}


void main(){}
