module test;
#line 2681 "0500-classes.tex"
class Shape {
   // As above
   abstract void draw();
   
}
class RectangularShape : Shape {
   // Inherits one abstract method from Shape
   //    and introduces one more
   abstract void drawFrame();
}
class Rectangle : RectangularShape {
   override void draw() { }
   // Rectangle is still an abstract class
}
class SolidRectangle : Rectangle {
   override void drawFrame() { }
   // SolidRectangle is concrete:
   //    no more abstract functions to implement
}


void main(){}
