module test;
#line 1680 "1000-concurrency.tex"
import std.exception;

// Single-threaded bank account
class BankAccount {
   private double _balance;
   void deposit(double amount) {
      _balance += amount;
   }
   void withdraw(double amount) {
      enforce(_balance >= amount);
      _balance -= amount;
   }
   @property double balance() {
      return _balance;
   }
}


void main(){}
