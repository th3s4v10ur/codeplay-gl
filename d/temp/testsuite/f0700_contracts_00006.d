module test;
#line 338 "0700-contracts.tex"
import std.conv, std.exception;

class CustomException : Exception {
   private string origin;
   private double val;
   this(string msg, string origin, double val) {
      super(msg);
      this.origin = origin;
      this.val = val;
   }
   override string toString() {
      return text(origin, ": ", super.toString(), val);
   }
}

double fun(double x)
in {
    if (x !>= 0) {
       throw new CustomException("hey!", "fun", x);
    }
}
body {
   double y;
   // Implementation of fun
   
   return y;
}


void main(){}
