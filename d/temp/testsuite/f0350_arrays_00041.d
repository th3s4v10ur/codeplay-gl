module test;
unittest {
#line 1261 "0350-arrays.tex"
int[5] array = [40, 30, 20, 10, 0];
auto slice1 = array[2 .. $];         // slice1 has type int[]
assert(slice1 == [20, 10, 0]);
auto slice2 = array[];               // Same as array[0 .. $]
assert(slice2 == array);
}


void main(){}
