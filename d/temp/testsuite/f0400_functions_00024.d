module test;
// Context begin
#line 1287 "0400-functions.tex"
import std.algorithm;
int[] a = [ 1, 2, 3, 4, -5, 3, -4 ];
// Context end
#line 1291 "0400-functions.tex"
unittest {
   int z = 3;
   auto b = find!(delegate(x) { return x < z; })(a); // OK
}


void main(){}
