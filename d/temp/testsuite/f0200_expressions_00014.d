module test;
unittest {
#line 808 "0200-expressions.tex"
auto constants = [ 2.71, 3.14, 6.023e22 ];
constants[0] = 2.21953167;  // The "moving sofa" constant
auto salutations = [ "hi", "hello", "yo" ];
salutations[2] = "Ave Caesar";
}


void main(){}
