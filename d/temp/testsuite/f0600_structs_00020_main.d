module test;
#line 1273 "0600-structs.tex"
import std.stdio;

struct Point {
   private int x, y;

   private static string formatSpec = "(%s %s)\n";

   static void setFormatSpec(string newSpec) {
      // Check the format spec for correctness
      formatSpec = newSpec;
   }

   void print() {
      writef(formatSpec, x, y);
   }
}

void main() {
   auto pt1 = Point(1, 2);
   pt1.print();
   // Call static member by prefixing it with either Point or pt1
   Point.setFormatSpec("[%s, %s]\n");
   auto pt2 = Point(5, 3);
   // The new spec affects all Point objects
   pt1.print();
   pt2.print();
}
