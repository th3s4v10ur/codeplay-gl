module test;
// Context begin
#line 1402 "0300-statements.tex"
uint bitsSet(uint value) {
    uint result;
    for (; value; ++result) {
        value &= value - 1;
    }
    return result;
}
unittest {
   assert(bitsSet(10) == 2);
   assert(bitsSet(0) == 0);
   assert(bitsSet(255) == 8);
}
// Context end
// Context begin
#line 1429 "0300-statements.tex"
import std.conv;

string makeHammingWeightsTable(string name, uint max = 255) {
    string result = "immutable ubyte["~to!string(max + 1)~"] "
       ~name~" = [ ";
    foreach (b; 0 .. max + 1) {
        result ~= to!string(bitsSet(b)) ~ ", ";
    }
    return result ~ "];";
}
// Context end
#line 1455 "0300-statements.tex"
mixin(makeHammingWeightsTable("hwTable"));
unittest {
   assert(hwTable[10] == 2);
   assert(hwTable[0] == 0);
   assert(hwTable[255] == 8);
}


void main(){}
