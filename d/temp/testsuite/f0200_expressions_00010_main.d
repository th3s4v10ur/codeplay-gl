module test;
#line 747 "0200-expressions.tex"
import std.stdio;

void main() {
   immutable(char)[3] a = "Hi!";
   immutable(char)[] b = a;
   writeln(a.length, " ", b.length); // Prints "3 3"
}
