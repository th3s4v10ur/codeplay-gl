module test;
#line 1782 "0400-functions.tex"
import std.range;

V reduce(alias fun, V, R)(V x, R range)
   if (isInputRange!R && is(typeof(x = fun(x, range.front))))
{
   for (; !range.empty; range.popFront()) {
      x = fun(x, range.front);
   }
   return x;
}


void main(){}
