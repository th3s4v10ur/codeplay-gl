module test;
unittest {
#line 812 "0100-intro.tex"
int[] a = new int[100];
int[] b = a;
// ++x increments value x
++b[10];    // b[10] is now 1, as is a[10]
b = a.dup;  // Copy a entirely into b
++b[10];    // b[10] is now 2, a[10] stays 1
}


void main(){}
