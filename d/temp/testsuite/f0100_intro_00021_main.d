module test;
#line 981 "0100-intro.tex"
import std.stdio, std.string;

void main() {
  // Compute counts
  uint[string] freqs;
  foreach (line; stdin.byLine()) {
    foreach (word; split(strip(line))) {
      ++freqs[word.idup];
    }
  }
  // Print counts
  foreach (key, value; freqs) {
    writefln("%6u\t%s", value, key);
  }/*[\nopagebreak]*/
}
