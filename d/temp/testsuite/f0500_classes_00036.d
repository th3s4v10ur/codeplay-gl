module test;
#line 2884 "0500-classes.tex"
class Outer {
   int x;
   class Inner {
      int y;
      this() {
         x = 42;
         // x or this.outer.x are the same thing
         assert(this.outer.x == 42);
      }
   }
}
unittest {
   auto outer = new Outer;
   auto inner = outer.new Inner;
   assert(outer.x == 42); // inner changed outer
}


void main(){}
