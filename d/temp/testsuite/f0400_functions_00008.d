module test;
#line 415 "0400-functions.tex"
static int zeros; // Practically the same as private int zeros;

void fun(int x) {
   static int calls;
   ++calls;
   if (!x) ++zeros;
   
}


void main(){}
