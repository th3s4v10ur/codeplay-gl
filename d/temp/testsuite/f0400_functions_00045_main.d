module test;
#line 2129 "0400-functions.tex"
import std.stdio, std.typecons;

void fun(T...)(T args) {
   // Create a Tuple to pack all arguments together
   gun(tuple(args));
}

void gun(T)(T value) {
   // Expand the tuple back
   writeln(value.expand);
}

void main() {
   fun(1);      // Fine
   fun(1, 2.2); // Fine
}
