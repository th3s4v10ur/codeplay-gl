module test;
#line 1913 "0400-functions.tex"
import std.conv;

void writeln(T...)(T args) {
   foreach (arg; args) {
      stdout.rawWrite(to!string(arg));
   }
   stdout.rawWrite('\n');
   stdout.flush();
}


void main(){}
