module test;
#line 324 "0350-arrays.tex"
import std.stdio;

void main() {
   auto array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
   // Print only the last half
   writeln(array[$ / 2 .. $]);
}
