module test;
// Context begin
#line 924 "0500-classes.tex"
class Contact {
   string bgColor() {
      return "Gray";
   }
}
unittest {
   auto c = new Contact;
   assert(c.bgColor() == "Gray");
}
// Context end
// Context begin
#line 941 "0500-classes.tex"
class Friend : Contact {
   string currentBgColor = "LightGreen";
   string currentReminder;
   override string bgColor() {
      return currentBgColor;
   }
   string reminder() {
      
      return currentReminder;
   }
}

// Context end
#line 966 "0500-classes.tex"
unittest {
   Friend f = new Friend;
   Contact c = f;            // Substitute a Friend for a Contact
   auto color = c.bgColor(); // Call a Contact method
}


void main(){}
