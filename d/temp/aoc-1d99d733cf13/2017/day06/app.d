import std.algorithm: map;
import std.conv: to;
import std.array: array;
import std.range: enumerate;
import std.stdio;
import std.algorithm: maxElement;

auto redistribute(numType)(numType[] membank) {
  auto max = membank.enumerate.maxElement!"a.value";
  membank[max.index] = 0;
  for(auto idx = 0; idx < max.value; idx++) {
    auto circ_idx = (max.index + idx + 1) % membank.length;
    membank[circ_idx]++;
  }
  return membank;
}

auto waitForDupe(numType)(numType[] membank) {
  numType steps = 1;
  auto seen = [membank.idup: steps];
  writeln(membank);
  while( membank.redistribute !in seen) {
    seen[membank.idup] = steps;
    steps++;
  }

  return [steps, steps - seen[membank]];
}

void main(string[] args) {
  auto membank = args[1..$].map!(to!int).array;
  writeln(waitForDupe(membank));
}
