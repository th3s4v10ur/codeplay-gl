import std.getopt;
import std.stdio;
import std.conv: to;
import std.traits: isNumeric;

numType day1(numType)(string input, size_t stride = 0)
  if(isNumeric!numType)
{
  auto n = input.length;
  if(stride == 0) {
    stride = n/2;
  }
  int sum = 0;
  foreach(idx, c; input) {
    auto prev = input[(idx + stride)%n];
    if (c == prev) {
      sum += to!(numType)(c - '0');
    }
  }
  return sum;
}

void main(string[] args) {
  string input;
  auto opts = getopt(
    args,
    "input|i", "Input captcha to process", &input
  );

  if (opts.helpWanted) {
    defaultGetoptPrinter("Day 1 of AoC", opts.options);
  }

  auto result1 = day1!(int)(input, 1);
  auto result2 = day1!(int)(input);
  writeln(result1);
  writeln(result2);
}
