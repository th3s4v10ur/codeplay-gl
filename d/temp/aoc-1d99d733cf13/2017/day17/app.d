import std.stdio;
import std.algorithm: find;
import std.conv: to;

auto getBuffer(ulong skip) {
  ulong[] circ = [0];
  ulong pos = 0;
  foreach(y; 1..2018) {
    pos = (pos + skip) % circ.length + 1;
    circ = circ[0..pos] ~ y ~ circ[pos..$];
  }
  return circ;
}

auto getPastZero(ulong skip) {
  ulong pos = 0;
  ulong pastzero = 0;
  foreach(length; 1..50_000_000) {
    pos = (pos + skip) % length + 1;
    if (pos == 1) {
      pastzero = length;
    }
  }
  return pastzero;
}

void main(string[] args) {
  auto circ = getBuffer(args[1].to!ulong);
  writeln(find(circ, 2017)[1]);
  writeln(getPastZero(args[1].to!ulong));
}
