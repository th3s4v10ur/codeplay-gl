import std.algorithm: map;
import std.array: array;
import std.stdio;
import std.conv: to;

auto walkmaze(numType)(numType[] instructions) {
  auto steps = 0;
  auto idx = 0;
  
  while(idx >= 0 && idx < instructions.length) {
    auto motion = instructions[idx];
    if (motion > 2) {
      instructions[idx]--;
    }
    else {
      instructions[idx]++;
    }
    idx += motion;
    steps++;
  }

  return steps;
}

void main(string[] args) {
  auto instructions = 
    File(args[1])
    .byLine
    .map!(x => to!int(x))
    .array;
  
  writeln(walkmaze(instructions));
}
