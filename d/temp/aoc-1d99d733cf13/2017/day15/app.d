import std.stdio;
import std.conv: to;
import std.range: generate, zip, take;
import std.algorithm: filter;
import std.array: array;

auto generator(ulong val, ulong mult) {
  return generate!(delegate(){
      val = (val * mult) % 2147483647; 
      return val;
    });
}

auto countMatches(
  bool delegate(ulong) filterA, 
  bool delegate(ulong) filterB
)(ulong valA, ulong valB, int patience) 
{
  auto genA = generator(valA, 16807);
  auto genB = generator(valB, 48271);
  return zip(
    genA.filter!(filterA), 
    genB.filter!(filterB)
  )
  .take(patience)
  .filter!( 
    x => (x[0] & 0xFFFF) == (x[1] & 0xFFFF)
  ).array.length;
}

void main(string[] args) {
  auto valA = to!ulong(args[1]), valB = to!ulong(args[2]);
  writeln(countMatches!(x => true, x => true)(valA, valB, 40_000_000));
  writeln(countMatches!(x => x%4 == 0, x => x%8 == 0)(valA, valB, 5_000_000));
}
