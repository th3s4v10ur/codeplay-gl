import std.stdio;
import std.algorithm: filter, map, sort;
import std.array: array;
import std.string: split;
import std.conv: to;

auto allUnique(string[] arr) {
  bool[string] counter;
  foreach(elt; arr) {
    auto key = to!string(sort(elt.array));
    counter[key] = true;
  }
  return counter.length == arr.length;
}

auto countUnique(string[][] passphrases) {
  return passphrases.filter!(allUnique).array.length;
}

void main(string[] args) {
  auto passphrases =
    File(args[1])
    .byLineCopy
    .map!(split)
    .array;
  writeln(countUnique(passphrases));
}
