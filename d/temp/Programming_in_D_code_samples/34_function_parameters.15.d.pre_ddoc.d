Ddoc
---
/* NOTE: This program is expected to fail compilation. */

module function_parameters_15;

int[] globalSlice;

$(HILITE @safe) int[] foo($(HILITE scope) int[] parameter) {
    globalSlice = parameter;    $(DERLEME_HATASI)
    return parameter;           $(DERLEME_HATASI)
}

void main() {
    int[] slice = [ 10, 20 ];
    int[] result = foo(slice);
}
---
Macros:
DDOC=$(BODY)
DDOC_COMMENT=
HILITE=$0
DERLEME_HATASI=// ← compilation ERROR
D_CODE=$0
D_KEYWORD=$0
D_STRING=$0
D_COMMENT=$0
RED=$0
BLUE=$0
GREEN=$0
YELLOW=$0
BLACK=$0
WHITE=$0
CODE_NOTE=// $0
ESCAPES=/&/&/ /</</ />/>/
