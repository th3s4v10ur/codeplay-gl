Ddoc
---
/* NOTE: This program is expected to fail compilation. */

module const_and_immutable_5;

import std.stdio;

void main() {
    immutable int[] slice = [ 10, 20, 30, 40 ];
    print(slice);    $(DERLEME_HATASI)
}

void print(int[] slice) {
    writefln("%s elements: ", slice.length);

    foreach (i, element; slice) {
        writefln("%s: %s", i, element);
    }
}
---
Macros:
DDOC=$(BODY)
DDOC_COMMENT=
HILITE=$0
DERLEME_HATASI=// ← compilation ERROR
D_CODE=$0
D_KEYWORD=$0
D_STRING=$0
D_COMMENT=$0
RED=$0
BLUE=$0
GREEN=$0
YELLOW=$0
BLACK=$0
WHITE=$0
CODE_NOTE=// $0
ESCAPES=/&/&/ /</</ />/>/
