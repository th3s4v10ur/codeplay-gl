module templates_more_4;

struct S {
    int i;
}

// Value template parameter of struct S
void foo(S s)() {
    // ...
}

void main() {
    foo!(S(42))();    // Instantiating with literal S(42)
}
