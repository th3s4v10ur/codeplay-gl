Ddoc
---
/* NOTE: This program is expected to fail compilation. */

module name_space_1;

void main() {
    int outer;

    if (aCondition) $(HILITE {)  // ← curly bracket starts a new scope
        int inner = 1;
        outer = 2;     // ← 'outer' is available here

    $(HILITE }) // ← 'inner' is not available beyond this point

    inner = 3;  $(DERLEME_HATASI)
                //   'inner' is not available in the outer scope
}
---
Macros:
DDOC=$(BODY)
DDOC_COMMENT=
HILITE=$0
DERLEME_HATASI=// ← compilation ERROR
D_CODE=$0
D_KEYWORD=$0
D_STRING=$0
D_COMMENT=$0
RED=$0
BLUE=$0
GREEN=$0
YELLOW=$0
BLACK=$0
WHITE=$0
CODE_NOTE=// $0
ESCAPES=/&/&/ /</</ />/>/
