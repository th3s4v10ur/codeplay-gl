Ddoc
---
/* NOTE: This program is expected to fail compilation. */

module ranges_4;

import std.array;

void main() {
    char[] s = "hello".dup;
    s.front = 'H';                   $(DERLEME_HATASI)
}
---
Macros:
DDOC=$(BODY)
DDOC_COMMENT=
HILITE=$0
DERLEME_HATASI=// ← compilation ERROR
D_CODE=$0
D_KEYWORD=$0
D_STRING=$0
D_COMMENT=$0
RED=$0
BLUE=$0
GREEN=$0
YELLOW=$0
BLACK=$0
WHITE=$0
CODE_NOTE=// $0
ESCAPES=/&/&/ /</</ />/>/
