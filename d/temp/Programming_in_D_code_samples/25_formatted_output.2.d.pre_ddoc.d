Ddoc
---
/* NOTE: This program is expected to fail compilation. */

module formatted_output_2;

import std.stdio;

void main() {
    writefln!"%s %s"(1);       $(DERLEME_HATASI) (extra %s)
    writefln!"%s"(1, 2);       $(DERLEME_HATASI) (extra 2)
    writefln!"%s %d"(1, 2.5);  $(DERLEME_HATASI) (mismatched %d and 2.5)
}
---
Macros:
DDOC=$(BODY)
DDOC_COMMENT=
HILITE=$0
DERLEME_HATASI=// ← compilation ERROR
D_CODE=$0
D_KEYWORD=$0
D_STRING=$0
D_COMMENT=$0
RED=$0
BLUE=$0
GREEN=$0
YELLOW=$0
BLACK=$0
WHITE=$0
CODE_NOTE=// $0
ESCAPES=/&/&/ /</</ />/>/
