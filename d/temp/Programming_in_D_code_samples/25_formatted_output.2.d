/* NOTE: This program is expected to fail compilation. */

module formatted_output_2;

import std.stdio;

void main() {
    writefln!"%s %s"(1);       // ← compilation ERROR (extra %s)
    writefln!"%s"(1, 2);       // ← compilation ERROR (extra 2)
    writefln!"%s %d"(1, 2.5);  // ← compilation ERROR (mismatched %d and 2.5)
}
