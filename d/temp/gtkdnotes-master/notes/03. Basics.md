# Basics

This section will introduce some of the most important aspects of GTK+.

## Main loop and Signals

Like most GUI toolkits, GTK+ uses an event-driven programming model. When the user is doing nothing, GTK+ sits in the main loop and waits for input. If the user performs some action - say, a mouse click - then the main loop “wakes up” and delivers an event to GTK+.

When widgets receive an event, they frequently emit one or more signals. Signals notify your program that “something interesting happened” by invoking functions you’ve connected to the signal. Such functions are commonly known as callbacks. When your callbacks are invoked, you would typically take some action - for example, when an Open button is clicked you might display a file chooser dialog. After a callback finishes, GTK+ will return to the main loop and await more user input.

## Properties

Properties describe the configuration and state of widgets. As for signals, each widget has its own particular set of properties. For example, a button has the property “label” which contains the text of the label widget inside the button. You can specify the name and value of any number of properties as keyword arguments when creating an instance of a widget. 

```d
auto btn = new Button("Click Me", delegate void(Button _){
    writeln("Button clicked.");
});
```

is equivalent to

```d
auto btn = new Button();
btn.setLabel("Click Me");
btn.addOnClicked(delegate void(Button _) {
    writeln("Button Clicked");
});
```


