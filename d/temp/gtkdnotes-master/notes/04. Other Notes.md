## A few pointers:

* You can find the GtkD documentation in https://api.gtkd.org/gtkd/gtk/AboutDialog.html

* You can discuss GtkD in the forum: https://forum.gtkd.org/groups/GtkD/

* GtkD automatically manages the memory and resources for you, there's no need for manual memory management

* Many gtk macros are converted into functions with the prefix `addOn`

* You can find almost all of the gtk enums in the gtkc module

