# Timeout

`glib.Timeout.Timeout` is a way to call a function (or a delegate) periodically over time until the function returns `false`. When you create a timeout you don't need to call anything to activate it. 

### Constructors:

* `this(uint interval, bool delegate() dlg, bool fireNow = false);`
    Creates a new timeout cycle with the default priority, `GPriority.DEFAULT`.

    **Note that timeout functions may be delayed**, due to the processing of other event sources. Thus they **should not be relied on for precise timing.** After each call to the timeout function, the time of the next timeout is recalculated based on the current time and the given interval (it does not try to 'catch up' time lost in delays). 

    * Params:

        `uint interval`: 	the timeout **in milieconds** delegate() = the delegate to be executed

        `bool fireNow`: 	When true the delegate will be executed emmidiatly


* `this(uint interval, bool delegate() dlg, GPriority priority, bool fireNow = false);`

    Creates a new timeout cycle.

    * Params:

        `uint interval`: 	the timeout in milieconds delegate() = the delegate to be executed

        `GPriority priority`: 	Priority for the timeout function

        `bool fireNow`: 	When true the delegate will be executed emmidiatly

* `this(bool delegate() dlg, uint seconds, bool fireNow = false);`

    Creates a new timeout cycle with the default priority, `GPriority.DEFAULT`.

    * Params:

        `uint seconds`: 	interval in seconds.

        `bool fireNow`: 	When true the delegate will be executed emmidiatly

* `this(bool delegate() dlg, uint seconds, GPriority priority, bool fireNow = false);`

    Creates a new timeout cycle.

   * Params:

       `uint seconds`: 	interval **in seconds.**

       `GPriority priority`: 	Priority for the timeout function

       `bool fireNow`: 	When true the delegate will be executed emmidiatly

### Example:

Let's create an app that let's users input a specified time in seconds and display a Gtk Spinner for the given time. The app should disable the input and any other buttons while the spinner is running and should display an ETA in the trigger button. The accuracy of the timing is not important for the sake of demonstration.

```d
int main(string[] args) {
    import gio.Application : GioApp = Application;
    import gtk.Application : Application;
    import gtkc.giotypes : GApplicationFlags;

    auto app = new Application("org.gitlab.radagast.spinner", GApplicationFlags.FLAGS_NONE);
    app.addOnActivate(delegate void(GioApp _) {
        import gtk.ApplicationWindow : ApplicationWindow;

        auto aw = new ApplicationWindow(app);
        scope (success)
            aw.showAll();

        aw.setDefaultSize(320, 200);
        aw.setBorderWidth(40);
        aw.setTitle("Spinner Demo");

        import gtk.Box : Box;
        import gtkc.gtktypes : GtkOrientation;

        auto vbox = new Box(GtkOrientation.VERTICAL, 10);
        scope (success)
            aw.add(vbox);

        import gtk.Spinner : Spinner;

        auto spinner = new Spinner();
        vbox.packStart(spinner, true, true, 0);

        auto hbox = new Box(GtkOrientation.HORIZONTAL, 10);
        scope (success)
            vbox.packStart(hbox, false, true, 0);

        import gtk.SpinButton : SpinButton;
        import gtkc.gtktypes : SpinButtonUpdatePolicy;

        auto spinButton = new SpinButton(0, 50, 1);
        spinButton.setNumeric(true);
        spinButton.setUpdatePolicy(SpinButtonUpdatePolicy.IF_VALID);
        hbox.packStart(spinButton, true, true, 0);

        import gtk.Button : Button;

        auto trigger = new Button("Start");
        trigger.addOnClicked(delegate void(Button _) {
            // retrieve the time
            auto time = spinButton.getValueAsInt();

            // set the button and the spinbutton insensitive
            spinButton.setSensitive(false);
            trigger.setSensitive(false);

            // start the spinner
            spinner.start();

            // stop the spinner after the given time
            import glib.Timeout : Timeout;  // read the foot-note
            import gtkc.gtktypes : GPriority;

            auto timeout = new Timeout(1000, delegate bool() {
                static int s;
                s += 1;
                if (s >= time) {
                    s = 0;
                    // stop the spinner
                    spinner.stop();
                    // make the button and the spinbutton sensitive again
                    spinButton.setSensitive(true);
                    trigger.setLabel("Start");
                    trigger.setSensitive(true);
                    return false;
                }
                else {
                    import std.format : format;

                    trigger.setLabel(format("Start (%s)", time - s));
                    return true;
                }
            }, GPriority.HIGH);
        });
        hbox.packStart(trigger, true, true, 0);
    });
    return app.run(args);
}
```