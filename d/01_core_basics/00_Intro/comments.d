
/+
     // A single line of comment

     /*
       A comment that spans
       multiple lines
      */

      /+
        It can even include nested /+ comments +/
      +/

      A comment block that includes other comments
+/

import std.stdio;
import std.string;
import std.conv;

int main(){
 //this is an example of comment in d
 //you can only see a comment in a program's source code
 return 0;
}