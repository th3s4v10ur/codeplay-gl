package main

import (
	"fmt"

	"github.com/imdario/mergo"
	"gopkg.in/yaml.v2"
)

type Spec struct {
	// Name name of the function
	Name string `yaml:"name"`

	// Image docker image name of the function
	Image string `yaml:"image"`

	Environment map[string]string `yaml:"environment,omitempty"`

	//Limits for the function
	Limits *FunctionResources `yaml:"limits,omitempty"`
}

// FunctionResources Memory and CPU
type FunctionResources struct {
	Memory string `yaml:"memory"`
	CPU    string `yaml:"cpu"`
}

// func main() {
// 	bytesOut, err := ioutil.ReadFile("config.yaml")
// 	if err != nil {
// 		panic(err)
// 	}

// 	spec := Spec{}
// 	if err := yaml.Unmarshal(bytesOut, &spec); err != nil {
// 		panic(err)
// 	}

// 	fmt.Printf("Function name: %s\tImage: %s\tEnvs: %d\n", spec.Name, spec.Image, len(spec.Environment))
// }

// func main() {
// 	spec := Spec{
// 		Image: "docker.io/functions/figlet:latest",
// 		Name:  "figlet",
// 	}

// 	bytesOut, err := yaml.Marshal(spec)
// 	if err != nil {
// 		panic(err)
// 	}

// 	err = ioutil.WriteFile("figlet.yaml", bytesOut, os.ModePerm)
// 	if err != nil {
// 		panic(err)
// 	}
// 	fmt.Printf("Wrote: figlet.yaml.. OK.\n")
// }

func main() {
	base := Spec{
		Image: "docker.io/functions/figlet:latest",
		Name:  "figlet",
	}

	production := Spec{
		Environment: map[string]string{"environment": "production"},
		Limits:      &FunctionResources{Memory: "1Gi", CPU: "100Mi"},
	}

	overrides := []Spec{
		base,
		production,
	}

	merged := Spec{}
	for _, override := range overrides {
		err := mergo.Merge(&merged, override, mergo.WithOverride)
		if err != nil {
			panic(err)
		}
	}

	bytesOut, err := yaml.Marshal(merged)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Merged content:\n\n%s\n", string(bytesOut))
}
