package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// computeSum generates a SHA256 hash of the body
func computeSum(body []byte) []byte {
	h := sha256.New()
	h.Write(body)
	hashed := hex.EncodeToString(h.Sum(nil))
	return []byte(hashed)
}

func main() {
	port := 8080
	readTimeout := time.Second * 10
	writeTimeout := time.Second * 10

	mux := http.NewServeMux()

	hashesInflight := prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "http",
		Name:      "hashes_inflight",
		Help:      "total hashes inflight",
	})
	prometheus.MustRegister(hashesInflight)

	customHash := func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if r.Body != nil {
			defer r.Body.Close()

			hashesInflight.Inc()
			defer hashesInflight.Dec()

			time.Sleep(time.Second * 5)
			body, _ := ioutil.ReadAll(r.Body)
			fmt.Fprintf(w, "%s", computeSum(body))
		}
	}

	mux.HandleFunc("/hash", customHash)
	mux.Handle("/metrics", promhttp.Handler())

	s := &http.Server{
		Addr:           fmt.Sprintf(":%d", port),
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: 1 << 20, // Max header of 1MB
		Handler:        mux,
	}
	log.Printf("Listening on: %d", port)
	s.ListenAndServe()
}
