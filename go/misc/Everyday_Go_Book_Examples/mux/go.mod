module mux

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/openfaas/faas/gateway v0.0.0-20210524150144-833be2947e73
)
