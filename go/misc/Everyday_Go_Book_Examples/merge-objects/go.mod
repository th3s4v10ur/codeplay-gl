module github.com/alexellis/everyday-go/samples/merge-objects

go 1.15

require (
	github.com/imdario/mergo v0.3.12
	gopkg.in/yaml.v2 v2.4.0
)
