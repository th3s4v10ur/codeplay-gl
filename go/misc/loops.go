package main

import "fmt"

func main() {
	target := 1
	//while loop
	for target > 0 {
		fmt.Printf("\nNumber of loops?\n")
		fmt.Scanf("%d", &target)
		//for loop
		for i := 0; i < target; i++ {
			fmt.Printf("%d", i)
		}
	}
	//infinite loop
	for {
		//explicitly terminated
		break
	}
}

// 0123456789
