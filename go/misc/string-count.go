package main

import (
	"fmt"
	"strings"
)

func main() {
	words := "hello world"
	fmt.Printf("Number of l's in %s is: ", words)
	fmt.Printf("%d\n", strings.Count(words, "l"))
}
