package main

import (
	"fmt"
	"time"
)

func mainLoop() {
	fmt.Println("starting main loop, this will die after dividing by 0")
	i := 0
	fmt.Println("%d", 1/i)

}

func main() {
	defer func() {
		if err := recover(); err != nil {
			time.Sleep(2 * time.Second)
			main()
		}
	}()
	mainLoop()
}
