package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Printf("fields are: %q\n", strings.Fields(" foo bar baz "))
}

/*%q
//Integers - %q a single-quoted character literal safely escaped with Go syntax.
Strings - %q a double-quoted string safely escaped with Go syntax
source - http://golang.org/pkg/fmt/
package main


...will give you:
fields are: ["foo" "bar" "baz"]
*/
