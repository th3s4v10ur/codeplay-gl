package main

import "fmt"

func main() {
	longString :=
		`this is
    a very, very
    very long string
    notice the back ticks`
	fmt.Println(longString)
}
