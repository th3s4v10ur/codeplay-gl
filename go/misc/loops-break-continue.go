package main

import (
	"fmt"
	"time"
)

func main() {
	test := 0
	for {
		test += 1
		if test > 10 {
			break
		}
		if test == 5 {
			continue
		}

		fmt.Println(test)
		time.Sleep(300 * time.Millisecond)
	}
}
