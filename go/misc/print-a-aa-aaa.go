package main

import (
	"fmt"
	"time"
)

func main() {
	s := "a"
	for s != "aaaaa" {
		fmt.Println(s)
		s = s + "a"
		time.Sleep(1000 * time.Millisecond)
	}
}
