package main

import "fmt"

func main() {
	var numbers = [4]int{1, 2, 3, 4}
	var name = []string{"j", "i", "m"}

	fmt.Println(numbers[0], numbers[1], numbers[2], numbers[3])
	fmt.Println(name[0], name[1], name[2])
	fmt.Printf("name: %s\n", name[0]+name[1]+name[2])
}

/*
1 2 3 4
j i m
name: jim
*/
