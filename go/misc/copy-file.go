package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	// open files r and w
	r, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer r.Close()

	w, err := os.Create("output.txt")
	if err != nil {
		panic(err)
	}
	defer w.Close()

	// do the actual work
	n, err := io.Copy(w, r)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Copied %v bytes\n", n)
}

/*
With the file input.txt in the same directory,

execute >go run copyfile.go and output will something similar to this

Copied 561 bytes
*/
