package main

import (
	"fmt"
	"math"
)

func main() {
	pvCalc()
}

func pvCalc() {
	var (
		interest,
		futureValue,
		period,
		presentValue,
		rate,
		pvFactor,
		ratex float64
	)
	fmt.Println("Enter interest amount: ")
	fmt.Scanf("%g", &interest)
	rate = interest / 100
	fmt.Println("Enter future value: ")
	fmt.Scanf("%g", &futureValue)
	fmt.Println("Enter time period: ")
	fmt.Scanf("%g", &period)
	fmt.Printf("period is %g\n", period)
	ratex = (1 + rate)
	pvFactor = math.Pow(ratex, period)
	presentValue = futureValue * (1 / pvFactor)
	fmt.Printf("Present value is = %g\n", presentValue)
}
