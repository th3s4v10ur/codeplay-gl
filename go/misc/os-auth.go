package main

import (
	"log"
	"os/exec"
	"strconv"
)

func main() {
	cmd := exec.Command("id", "-u")
	output, _ := cmd.Output()

	runAsRoot, _ := strconv.Atoi(string(output[:len(output)-1]))

	if runAsRoot == 0 {
		log.Println("You have root permission")
	} else {
		log.Fatal("You dont have root permission")
	}
}
