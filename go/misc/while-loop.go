package main

import "fmt"

func main() {
	var choice int
	for choice != 1 && choice != 2 {
		fmt.Println("Pick a number between 1 or 2")
		fmt.Scanf("%d", &choice)
	}
	fmt.Printf("You chose %d\n", choice)
}
