package main

import "fmt"

func main() {
	m := make(map[string]string)
	m["answer"] = "first"
	fmt.Println("the value:", m["answer"])
	m["answer"] = "second"
	fmt.Println("The value:", m["answer"])
}

/*
the value: first
The value: second
*/
