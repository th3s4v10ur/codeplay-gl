package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("What is your favorite color?")
	var favoriteColor string
	fmt.Scanf("%s", &favoriteColor)
	fmt.Println("Fave color is", favoriteColor)
	fmt.Println("What is your favorite food?")
	var myfood string
	fmt.Scanf("%s", &myfood)
	fmt.Printf("I like %s too!\n", myfood)
	fmt.Printf("Wait two seconds please...\n")
	time.Sleep(2000 * time.Millisecond)
	fmt.Printf("Your favorite color is %s, and the food you like best is %q\n", favoriteColor, myfood)
}
