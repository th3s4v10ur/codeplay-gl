package main

import (
	"fmt"
	"math"
)

var (
	interest,
	futureValue,
	period,
	presentValue,
	rate, //converts interest into decimal... interest / 100
	pvFactor,
	ratex float64 //used to compute pvfactor
)

func main() {
	fmt.Println("Enter 1 to calculate present value")
	fmt.Println("Enter 2 to calculate future value")
	var whichCalc int
	fmt.Scanf("%d", &whichCalc)
	if whichCalc == 1 {
		pvCalc()
	} else if whichCalc == 2 {
		fvCalc()
	} else {
		fmt.Println("You did not make a choice. Now closing.\n")
	}
}
func fvCalc() {
	//fv = pv * (1 + i)**t
	// http://www.accountingcoach.com/online-accounting-course/84Xpg02.html
	fmt.Println("Enter interest amount: ")
	fmt.Scanf("%g", &interest)
	rate = interest / 100
	fmt.Println("Enter present value: ")
	fmt.Scanf("%g", &presentValue)
	fmt.Println("Enter time period: ")
	fmt.Scanf("%g", &period)
	ratex = (1 + rate)
	pvFactor = math.Pow(ratex, period)
	futureValue = presentValue * pvFactor
	fmt.Println("Future value is = %g\n", futureValue)
}
func pvCalc() {
	//pv = fv * (1 /(1 +i)**n)
	// http://www.accountingcoach.com/online-accounting-course/80Xpg03.html
	fmt.Println("Enter interest amount: ")
	fmt.Scanf("%g", &interest)
	rate = interest / 100
	fmt.Println("Enter future value: ")
	fmt.Scanf("%g", &futureValue)
	fmt.Println("Enter time period: ")
	fmt.Scanf("%g", &period)
	fmt.Printf("period is %g\n", period)
	ratex = (1 + rate)
	pvFactor = math.Pow(ratex, period)
	presentValue = futureValue * (1 / pvFactor)
	fmt.Printf("Present value is = %g\n", presentValue)
}
