package main

import "fmt"

func main() {

	// Formating booleans
	fmt.Printf("%v\n", false)
	fmt.Printf("%t\n", true)

	// Formatting integers
	fmt.Printf("%v\n", 987)
	fmt.Printf("%d\n", 345)

	// Formatting floats
	fmt.Printf("%v\n", 987123456.987345123432)
	fmt.Printf("%f\n", 987123456.987345123432)

	// Use %g for large exponents
	fmt.Printf("%g\n", 987123456.987345123432)

}
