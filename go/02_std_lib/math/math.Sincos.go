package main

import (
    "fmt"
    "math"
)

//Sincos(x) function returns both Sin(x) and cos(x) values at same instance  

func main() {
    z:= func (x float64) (sin, cos float64){
        return math.Sincos(x)
    }
        
    fmt.Println("Sin","Cos")
    fmt.Println(z(90))
}

/*

Output
-------

Sin Cos
0.8939966636005579 -0.4480736161291701

*/