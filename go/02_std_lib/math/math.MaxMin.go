package main

import (
	"fmt"
	"math"
)

/*
In this program we are implementing Max and Min functions
*/

func main() {

	z := func(x, y float64) float64 {
		return math.Max(x, y) //Max returns the larger of x or y
	}
	y := func(x, y float64) float64 {
		return math.Min(x, y) //Min returns the smaller  of x or y
	}

	fmt.Println("Max of 5 and 6 is ", z(5, 6))
	fmt.Println("Min of 5 and 6 is ", y(5, 6))

}

/*

Output
-------
Max of 5 and 6 is  6
Min of 5 and 6 is  5

*/
