package main

import (
	"fmt"
	"math"
)

//Here were are using  Sin(x) and Sinh(x) functions

func main() {
	z := func(x float64) float64 {
		return math.Sin(x)
	}
	y := func(x float64) float64 {
		return math.Sinh(x)
	}

	fmt.Println(z(90))
	fmt.Print(y(90))
}

/*

Output
-------
sin(90) =0.893996663600558
sinh(90)=6.102016471589204e+38
*/
