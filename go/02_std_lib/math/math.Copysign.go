package main

import (
	"fmt"
	"math"
)

/*

Copysign returns a value with the magnitude of x and the sign of y

*/

func main() {
	z := func(x, y float64) float64 {
		return math.Copysign(x, y)
	}
	fmt.Println(z(-8, 9))
	fmt.Println(z(81, -9))
	fmt.Println(z(-21, -2))
	fmt.Prin
}

/*

Output
-------
8
-81
-21

*/
