package main

import (
	"fmt"
	"math"
)

/* func Pow10

func Pow10(e int) float64
Pow10 returns 10**e, the base-10 exponential of e.
*/

func main() {
	a := 2
	z := func(e int) float64 {
		return math.Pow10(e)
	}
	fmt.Println("Pow10 of ", a, " is ", z(a))

}

/*

Output
-------
 Pow10 of  2  is  100

*/
