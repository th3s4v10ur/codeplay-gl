package main

import (
    "fmt"
    "math"
)

/*

func Mod

func Mod(x, y float64) float64
Mod returns the floating-point remainder of x/y.
The magnitude of the result is less than y and its sign agrees with that of x.

*/

func main() {
    z:= func (x,y float64)  float64 {
        return math.Mod(x,y)
    }
    fmt.Println(z(8,3))

    }


/*

Output
-------
 2
*/
