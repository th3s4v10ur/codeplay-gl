package main

import (
    "fmt"
    "math"
)

func main() {
    z := func(x, y float64) float64 {
        return math.Sqrt(x*x + y*y)
    }

    fmt.Println(z(3, 4))
}

/*
Output
-------

5

3*3+4*4 =25
sqrt(25) = 5
*/