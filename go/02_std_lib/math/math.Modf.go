package main

import (
    "fmt"
    "math"
)

/*

func Modf
func Modf(f float64) (int float64, frac float64)
Modf returns integer and fractional floating-point numbers that sum to f
 of x.

*/

func main() {
    z:= func (f float64) (int float64, frac float64) {
        return math.Modf(f)
    }
    fmt.Println(z(825.264))
    
    }


/*

Output
-------
825 0.26400000000001
*/