package main

import (
	"fmt"
	"math"
)

/*

func Trunc

format func Trunc(x float64) float64
Trunc returns the integer value of x.

*/

func main() {
	z := func(x float64) float64 {
		return math.Trunc(x)
	}
	fmt.Println(z(5))

}

/*

Output
-------
 5
*/
