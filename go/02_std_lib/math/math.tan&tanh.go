/* Here we used both tan and tanh function */

package main

import (
	"fmt"
	"math"
)

func main() {

	z := func(x float64) float64 {

		return math.Tan(x)
	}

	y := func(x float64) float64 {

		return math.Tanh(x)
	}

	fmt.Println(z(90))

	fmt.Println(y(45))
}

/*
Output
------

-1.995200412208242
1

*/
