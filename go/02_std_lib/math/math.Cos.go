package main

import (
    "fmt"
    "math"
)

// we are using cos and cosh functions

func main() {
    z:= func (x float64)  float64 {
        return math.Cos(x)
    }
    y:= func (x float64) float64{
    	return math.Cosh(x)
    }

    fmt.Println(z(90))
    fmt.Print(y(90))
    }

/*
output
--------
-0.4480736161291701
6.102016471589204e+38

/*
