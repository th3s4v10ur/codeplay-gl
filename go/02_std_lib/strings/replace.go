/*

This function takes four parameters as input

culprit string
old string
new string
number of times new string will replace old string in culprit string
if n < 0 there is no limit on a number of replacements.

*/

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Replace("no-banana no-banana no-banana", "no", "yes", 2))
	fmt.Println(strings.Replace("no-banana no-banana no-banana", "no", "yes", -1))
}

/*

output:
yes-banana yes-banana no-banana
yes-banana yes-banana yes-banana

*/
