package main

import (
	"fmt"
	"strings"
)

//This function takes two strings as input and checks number of times
//second string argument appears in first string argument and returns
//count as an integer.

func main() {
	fmt.Println(strings.Count("gopher-gopher-goher", "g"))
	fmt.Println(strings.Count("gopher", "g"))
}

/*
output:
3
1
*/
