//This function takes two strings as input and returns a slice of the
//substrings based on the second argument string.

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Printf("%q\n", strings.Split("gopher,19,gopher", ","))
}

//output:
//["gopher" "19" "gopher"]
