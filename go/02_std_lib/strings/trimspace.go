//One of the most used feature to remove white spaces,
//this function takes a string as input and returns a
// string with trailing and leading spaces removed.

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.TrimSpace(" \t\n Covid-19's vaccine will be invented and out soon to rescue us all from this insane. \n\t\r\n"))
}

//output:
//Covid-19's vaccine will be invented and out soon to rescue us all from this insane.
