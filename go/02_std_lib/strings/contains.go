//contains function returns an appropriate
//Boolean value if given substring present in the string being searched.

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Contains("gopher-20", "20"))
	fmt.Println(strings.Contains("gopher-20", "30"))
	fmt.Println(strings.Contains("gopher-20", ""))
	fmt.Println(strings.Contains("", ""))
}

/*output:
true
false
true
true
*/
