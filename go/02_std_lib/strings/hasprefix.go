//This function takes two strings as input and checks if the second
// argument string is present in starting characters of the first
//argument string.

//.HasPrefix has his brother called .HasSuffix does exactly same
//but this time it checks at the ending part of the string.

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.HasPrefix("gopher", "go"))
	fmt.Println(strings.HasPrefix("gopher", "java"))
	fmt.Println(strings.HasPrefix("gopher", ""))
}

/*
output:
true
false
true
*/
