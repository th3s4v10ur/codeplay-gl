//compares two string lexicographically Outcome of this function is an
// integer and could be either 0, -1 or 1

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Compare("gopher19", "gopher20"))
	fmt.Println(strings.Compare("gopher19", "gopher19"))
	fmt.Println(strings.Compare("gopher20", "gopher19"))
}

/*Output :
-1
0
1
*/
