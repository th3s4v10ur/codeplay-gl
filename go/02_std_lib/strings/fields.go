//This function takes a string as input and splits into an array by
//white space characters in the string and returns a string array.

package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Printf("Fields are: %q", strings.Fields("gopher is sleeping."))
}

//If you ever wanted to split a string into string array by white space characters
//output:
//Fields are: ["gopher" "is" "sleeping."]
