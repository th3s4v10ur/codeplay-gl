package main

import "fmt"

func main() {
	// tag::main[]
	dwarfs := [5]string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	// end::main[]
	fmt.Println(dwarfs)
}
