package main

import "fmt"

func main() {
	// tag::main[]
	superpowers := &[3]string{"flight", "invisibility", "super strength"}

	fmt.Println(superpowers[0])   //<1>
	fmt.Println(superpowers[1:2]) //<2>
	// end::main[]
}
