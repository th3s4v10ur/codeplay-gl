package main

import "fmt"

// tag::birthday[]
type person struct {
	name, superpower string
	age              int
}

func birthday(p *person) {
	p.age++
}

// end::birthday[]

func main() {
	// tag::rebecca[]
	rebecca := person{
		name:       "Rebecca",
		age:        14,
		superpower: "imagination",
	}

	birthday(&rebecca)

	fmt.Printf("%+v\n", rebecca) //<1>
	// end::rebecca[]
}
