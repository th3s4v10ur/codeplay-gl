package main

import "fmt"

func main() {
	// tag::main[]
	answer := 42
	address := &answer

	fmt.Printf("address is a %T\n", address) //<1>
	// end::main[]
}
