package main

import "fmt"

func main() {
	// tag::main[]
	type person struct {
		name, superpower string
		age              int
	}

	timmy := &person{
		name: "Timothy",
		age:  10,
	}
	// end::main[]

	// tag::dereference[]
	timmy.superpower = "flying"

	fmt.Printf("%+v\n", timmy) //<1>
	// end::dereference[]
}
