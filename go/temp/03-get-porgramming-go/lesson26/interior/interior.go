package main

import "fmt"

// tag::levelup[]
type stats struct {
	level             int
	endurance, health int
}

func levelUp(s *stats) {
	s.level++
	s.endurance = 42 + (14 * s.level)
	s.health = 5 * s.endurance
}

// end::levelup[]

func main() {
	// tag::main[]
	type character struct {
		name  string
		stats stats
	}

	player := character{name: "Matthias"}
	levelUp(&player.stats)

	fmt.Printf("%+v\n", player.stats) //<1>
	// end::main[]
}
