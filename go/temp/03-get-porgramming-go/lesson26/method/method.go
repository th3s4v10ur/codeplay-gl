package main

import "fmt"

// tag::birthday[]
type person struct {
	name string
	age  int
}

func (p *person) birthday() {
	p.age++
}

// end::birthday[]

func main() {
	// tag::terry[]
	terry := &person{
		name: "Terry",
		age:  15,
	}
	terry.birthday()
	fmt.Printf("%+v\n", terry) //<1>
	// end::terry[]

	// tag::nathan[]
	nathan := person{
		name: "Nathan",
		age:  17,
	}
	nathan.birthday()
	fmt.Printf("%+v\n", nathan) //<1>
	// end::nathan[]
}
