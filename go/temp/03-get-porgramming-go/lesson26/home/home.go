package main

import "fmt"

func main() {
	// tag::main[]
	canada := "Canada"

	var home *string
	fmt.Printf("home is a %T\n", home) //<1>

	home = &canada
	fmt.Println(*home) // <2>
	// end::main[]
}
