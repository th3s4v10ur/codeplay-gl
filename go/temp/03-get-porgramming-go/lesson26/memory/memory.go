package main

import "fmt"

func main() {
	// tag::dereference[]
	// tag::address[]
	answer := 42
	fmt.Println(&answer) //<1>
	// end::address[]

	address := &answer
	fmt.Println(*address) //<2>
	// end::dereference[]
}
