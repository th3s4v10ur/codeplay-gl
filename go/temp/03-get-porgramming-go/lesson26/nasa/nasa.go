package main

import "fmt"

func main() {
	// tag::point[]
	var administrator *string

	scolese := "Christopher J. Scolese"
	administrator = &scolese
	fmt.Println(*administrator) //<1>

	bolden := "Charles F. Bolden"
	administrator = &bolden
	fmt.Println(*administrator) //<2>
	// end::point[]

	// tag::modify[]
	bolden = "Charles Frank Bolden, Jr."
	fmt.Println(*administrator) //<1>
	// end::modify[]

	// tag::indirect[]
	*administrator = "Maj. Gen. Charles Frank Bolden, Jr."
	fmt.Println(bolden) //<1>
	// end::indirect[]

	// tag::major[]
	major := administrator
	*major = "Major General Charles Frank Bolden, Jr."
	fmt.Println(bolden) //<1>
	// end::major[]

	// tag::equal[]
	fmt.Println(administrator == major) // <1>
	// end::equal[]

	// tag::lightfoot[]
	lightfoot := "Robert M. Lightfoot Jr."
	administrator = &lightfoot
	fmt.Println(administrator == major) //<1>
	// end::lightfoot[]

	// tag::copy[]
	charles := *major
	*major = "Charles Bolden"
	fmt.Println(charles) //<1>
	fmt.Println(bolden)  //<2>
	// end::copy[]

	// tag::compare[]
	charles = "Charles Bolden"
	fmt.Println(charles == bolden)   //<1>
	fmt.Println(&charles == &bolden) //<2>
	// end::compare[]
}
