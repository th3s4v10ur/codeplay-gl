package main

import (
	"fmt"
	"time"
)

func main() {
	// tag::main[]
	const layout = "Mon, Jan 2, 2006"

	day := time.Now()
	tomorrow := day.Add(24 * time.Hour)

	fmt.Println(day.Format(layout))      //<1>
	fmt.Println(tomorrow.Format(layout)) //<2>
	// end::main[]
}
