package main

import (
	"fmt"
	"strings"
)

// tag::all[]
type talker interface {
	talk() string
}

func shout(t talker) {
	louder := strings.ToUpper(t.talk())
	fmt.Println(louder)
}

type martian struct{}

func (m martian) talk() string {
	return "nack nack"
}

func main() {
	shout(martian{})  // <1>
	shout(&martian{}) // <1>
}

// end::all[]
