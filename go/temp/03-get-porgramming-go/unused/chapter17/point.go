package main

import (
	"fmt"
	"math"
)

// point with a latitude, longitude.
type point struct {
	lat  float64
	long float64
}

func main() {
	// curiosity (bradburyLanding in Gale Crater) Aug 6, 2012 UTC
	curiosity := point{-4.5895, 137.4417}
	// spirit := point{-14.5684, 175.472636}
	// opportunity := point{-1.9462, 354.4734}

	// "traverse"
	fmt.Println(curiosity)

	// traverse ~624 meters east to Yellowknife Bay (125 days, Dec 12)
	curiosity = destination(curiosity, .983, 90)
	fmt.Println(curiosity)

	curiosity = destination(curiosity, .005*687, 225) // SW
	fmt.Println(curiosity)

	// fmt.Println(distance(a, b))
	// fmt.Println(bearing(a, b))
	// fmt.Println(math.Mod(bearing(b, a)+180, 360)) // final bearing
	// fmt.Println(destination(a, distance(a, b), bearing(a, b)))
}

// volumetric mean radius of Mars in kilometers.
const radius = 3389.5

// const radius = 6371 // Earth

// distance calculation using the Spherical Law of Cosines
func distance(p1, p2 point) float64 {
	s1, c1 := math.Sincos(rad(p1.lat))
	s2, c2 := math.Sincos(rad(p2.lat))
	clong := math.Cos(rad(p1.long - p2.long))
	return radius * math.Acos(s1*s2+c1*c2*clong)
}

// Haversine
func distance2(p1, p2 point) float64 {
	dlat := rad(p2.lat - p1.lat)
	dlong := rad(p2.long - p1.long)
	a := math.Pow(math.Sin(dlat/2), 2) + math.Cos(rad(p1.lat))*math.Cos(rad(p2.lat))*math.Pow(math.Sin(dlong/2), 2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return radius * c
}

// Equirectangular approximation
func distance3(p1, p2 point) float64 {
	x := rad(p2.long-p1.long) * math.Cos(rad((p1.lat+p2.lat)/2))
	y := rad(p2.lat - p1.lat)
	return radius * math.Sqrt(x*x+y*y)
}

// initial bearing (forward azimuth)
func bearing(p1, p2 point) float64 {
	slat1, clat1 := math.Sincos(rad(p1.lat))
	slat2, clat2 := math.Sincos(rad(p2.lat))
	slong, clong := math.Sincos(rad(p2.long - p1.long))

	y := slong * clat2
	x := clat1*slat2 - slat1*clat2*clong
	b := math.Atan2(y, x)

	return math.Mod(deg(b)+360, 360)
}

// destination
func destination(p1 point, distance, bearing float64) point {
	slat1, clat1 := math.Sincos(rad(p1.lat))
	sb, cb := math.Sincos(rad(bearing))
	sd, cd := math.Sincos(distance / radius)

	lat2 := math.Asin(slat1*cd + clat1*sd*cb)
	long2 := rad(p1.long) + math.Atan2(sb*sd*clat1, cd-slat1*math.Sin(lat2))
	return point{deg(lat2), deg(long2)}
}

// rad converts degrees to radians.
func rad(deg float64) float64 {
	return deg * math.Pi / 180
}

// deg converts radians to degrees.
func deg(rad float64) float64 {
	return rad * 180 / math.Pi
}

// String formats a point with a hemisphere.
func (p point) String() string {
	latH := 'N'
	if p.lat < 0 {
		latH = 'S'
	}
	longH := 'E'
	if p.long < 0 {
		longH = 'W'
	}
	return fmt.Sprintf("%v %c, %v %c", dms(p.lat), latH, dms(p.long), longH)
}

// dms formats a coordinate in degrees, minutes, and seconds.
func dms(c float64) string {
	dd := math.Abs(c)
	d := math.Floor(dd)
	m := math.Floor(60 * (dd - d))
	s := 3600 * (dd - d - m/60)
	return fmt.Sprintf("%vº %v' %.1f\"", d, m, s)
}
