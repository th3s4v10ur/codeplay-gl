package main

import "fmt"

func main() {
	celsius := -273.15                         //21.0
	fahrenheit := CelsiusToFahrenheit(celsius) //<1>
	fmt.Print(celsius, "ºC is ", fahrenheit, "ºF")
}

// CelsiusToFahrenheit convert ºC to ºF
func CelsiusToFahrenheit(c float64) float64 { //<2>
	return (c * 9.0 / 5.0) + 32.0
}

// type Fahrenheit float64
