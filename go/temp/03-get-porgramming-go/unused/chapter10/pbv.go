package main

import "fmt"

func main() {
	kelvin := 294.0
	k := kelvin //<1>

	kelvin = 293.0

	fmt.Print("kelvin is ", kelvin, "ºK\n") //<2>
	fmt.Print("k is still ", k, "ºK")       //<3>
}

// <2> kelvin is 293ºK
// <3> k is 294ºK
