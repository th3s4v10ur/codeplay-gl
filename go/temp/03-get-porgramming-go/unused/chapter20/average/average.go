package main

import "fmt"

type temperature struct {
	high, low celsius
}

type celsius float64

func (t temperature) average() celsius {
	return (t.high + t.low) / 2
}

type averager interface {
	average() celsius
}

type temperatures []temperature

func (ts temperatures) average() celsius {
	var sum celsius
	for _, t := range ts {
		sum += t.average()
	}
	return sum / celsius(len(ts))
}

func averages(ts []averager) celsius {
	var sum celsius
	for _, t := range ts {
		sum += t.average()
	}
	return sum / celsius(len(ts))
}

type report struct {
	sol int
	temperature
}

type reports []report

func main() {
	ts := temperatures{
		{high: -1.0, low: -78.0},
	}
	fmt.Println(averages(ts))

	reports := reports{
		{sol: 15, temperature: temperature{high: -1.0, low: -78.0}},
	}
	fmt.Println(reports[0].average())
}
