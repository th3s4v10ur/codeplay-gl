package main

import "fmt"

func main() {
	// tag::main[]
	var p *int
	a := 42
	p = &a
	b := *p
	fmt.Println(b)

	a = 7
	fmt.Println(*p)

	c := a
	a = 10
	fmt.Println(a, c)
	// end::main[]
}
