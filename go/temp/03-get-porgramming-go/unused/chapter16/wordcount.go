package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"strings"
)

// The War of the Worlds by H. G. Wells
const book = "http://www.gutenberg.org/ebooks/36.txt.utf-8"

func main() {
	count := make(map[string]int, 1000)

	// (playground) dial tcp: Protocol not available
	res, err := http.Get(book)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	scanner := bufio.NewScanner(res.Body)
	// Set the split function for the scanning operation.
	scanner.Split(bufio.ScanWords)
	// TODO: also split on / --

	// Count the words.
	for scanner.Scan() {
		word := strings.ToLower(strings.Trim(scanner.Text(), `.,!?"'()-_;*`))
		count[word]++
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	for k, v := range count {
		fmt.Printf("%4d: %v\n", v, k)
	}
}
