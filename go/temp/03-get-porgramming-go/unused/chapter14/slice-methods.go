package main

import "fmt"

type stringSlice []string

func (slice stringSlice) dump(label string) {
	fmt.Printf("%v: length %v, capacity %v %v\n", label, len(slice), cap(slice), slice)
}

func main() {
	dwarfs := stringSlice{"Ceres", "Pluto", "Haumea"}
	dwarfs.dump("dwarfs") //<1>
}
