package main

import "fmt"

func main() {
	// tag::main[]
	phases := []string{
		"New Moon",
		"Waxing Crescent",
		"First Quarter",
		"Waxing Gibbous",
		"Full Moon",
		"Waning Gibbous",
		"Third Quarter",
		"Waning Crescent",
	}

	fmt.Println(phases)
	// end::main[]
}
