package main

import "fmt"

func message() func() {
	message := "Functions anonymous"
	f := func() {
		fmt.Println(message)
	}
	return f
}

func main() {
	func() { //<1>
		fmt.Println("Functions anonymous")
	}() //<2>

	f := func(message string) { //<1>
		fmt.Println(message)
	}
	f("Functions anonymous") //<2>

	m := message()
	m()
}

//<1> 294
//<2> 295
