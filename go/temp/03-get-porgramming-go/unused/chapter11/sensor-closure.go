package main

import "fmt"

type Kelvin float64

func main() {
	var k Kelvin = 294.0

	sensor := func() Kelvin {
		return k
	}
	fmt.Println(sensor()) //<1>

	k++
	fmt.Println(sensor()) //<2>
}

//<1> 294
//<2> 295
