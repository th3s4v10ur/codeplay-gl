package main

import (
	"fmt"
	"os"
)

type Celsius float64

func main() {
	logTemperature(21.0)
}

func (c Celsius) String() string {
	return fmt.Sprintf("%.2fºC", c) //<1>
}

func logTemperature(c Celsius) {
	file, _ := os.Create("log.txt") //<1>
	file.WriteString("Temperature " + c.String())
	file.Close()
}
