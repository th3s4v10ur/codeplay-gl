package main

import (
	"fmt"
	"os"
)

type Celsius float64
type Fahrenheit float64

func main() {
	logTemperature(21.0)
}

func (c Celsius) String() string {
	return fmt.Sprintf("%.2fºC", c)
}

func logTemperature(c Celsius) error {
	file, err := os.Create("log.txt")
	if err != nil {
		return err
	}
	defer file.Close() //<1>

	_, err = file.WriteString("Temperature " + c.String())
	return err
}
