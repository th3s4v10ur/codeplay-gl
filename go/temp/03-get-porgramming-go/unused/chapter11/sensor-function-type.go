package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Kelvin float64
type Sensor func() Kelvin //<1>

func measureTemperature(samples int, sensor Sensor) { //<2>
	// the same as before
	for i := 0; i < samples; i++ {
		k := sensor()
		fmt.Printf("%vºK\n", k)
		time.Sleep(time.Second)
	}
}

func fakeSensor() Kelvin {
	return Kelvin(rand.Intn(151) + 150)
}

func main() {
	measureTemperature(3, fakeSensor)
}
