package main

import (
	"fmt"
	"math/rand"
	"os"
)

type Celsius float64
type Kelvin float64

func fakeSensor() Kelvin {
	return Kelvin(rand.Intn(151) + 150) //<1>
}

func (k Kelvin) Celsius() Celsius {
	return Celsius(k - 273.15)
}

func (c Celsius) String() string {
	return fmt.Sprintf("%.2fºC", c)
}

func measureTemperature(sensor func() Kelvin) Celsius {
	return sensor().Celsius()
}

func main() {
	c := measureTemperature(fakeSensor)
	logLine(c.String())
}

func logLine(line string) error {
	file, err := os.Create("log.txt")
	if err != nil {
		return err
	}
	defer file.Close() //<1>

	_, err = file.WriteString(line)
	return err
}
