package main

import "fmt"

const (
	Picometer  = Nanometer / 1000
	Nanometer  = Micrometer / 1000
	Micrometer = Millimeter / 1000
	Millimeter = Meter / 1000
	Centimeter = Meter / 100
	Decimeter  = Meter / 10
	Meter      = 1
	Decameter  = 10 * Meter
	Kilometer  = 1000 * Meter
	Megameter  = 1000 * Kilometer
	Gigameter  = 1000 * Megameter
	Terameter  = 1000 * Gigameter
	Petameter  = 1000 * Terameter
	Exameter   = 1000 * Petameter
	Zettameter = 1000 * Exameter
	Yottameter = 1000 * Zettameter
)

const (
	Inch = 1
	Foot = 12 * Inch
	Yard = 3 * Foot
	Mile = 1760 * Yard
)

const (
	A = 1
	B
	C
)

func main() {
	fmt.Println(56000000 * Kilometer)
}
