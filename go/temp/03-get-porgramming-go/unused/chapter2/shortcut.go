package main

func main() {
	var count = 0
	var price = 20.0

	var (
		count = 0
		price = 20.0
	)

	var count, price = 0, 20.0

	price = price * 2
	price *= 2

	count = count + 1
	count += 1
	count++
}
