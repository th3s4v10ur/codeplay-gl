package main

type location struct {
	lat, long float64
}

type world struct {
	radius float64
}

var mars = world{radius: 3389.5}

// tag::rover[]
type rover struct {
	sol      int
	location location
	world    world
}

var red = rover{
	location: location{-4.5895, 137.4417},
	world:    mars,
}

// end::rover[]

func main() {
	// tag::main[]
	red.East(0.624)
	red.South(4)
	// end::main[]
}

func (r *rover) East(distance float64)  {}
func (r *rover) South(distance float64) {}
