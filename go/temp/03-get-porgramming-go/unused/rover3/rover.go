package main

import (
	"image"
	"log"
	"time"
)

// tag::main[]
func main() {
	r := NewRoverDriver()
	time.Sleep(1 * time.Second)
	r.Left()
	time.Sleep(1 * time.Second)
	r.Right()
	time.Sleep(1 * time.Second)
	log.Printf("final position: %v", r.Position())
}

// end::main[]

type RoverDriver struct {
	commandc chan command
}

func NewRoverDriver() *RoverDriver {
	r := &RoverDriver{
		commandc: make(chan command),
	}
	go r.drive()
	return r
}

// tag::command[]

type commandKind int

const (
	right         = commandKind(0)
	left          = commandKind(1)
	queryPosition = commandKind(2)
)

type command struct {
	kind          commandKind
	positionReply chan image.Point
}

// end::command[]

// drive is responsible for driving the rover. It
// is expected to be started in a goroutine.
func (r *RoverDriver) drive() {
	pos := image.Point{X: 0, Y: 0}           //<1>
	direction := image.Point{X: 1, Y: 0}     //<2>
	updateInterval := 250 * time.Millisecond //<3>
	nextMove := time.After(updateInterval)   //<4>
	for {
		select {
		// tag::select[]
		case c := <-r.commandc:
			switch c.kind {
			case right:
				direction = image.Point{
					X: -direction.Y,
					Y: direction.X,
				}
			case left:
				direction = image.Point{
					X: direction.Y,
					Y: -direction.X,
				}
			case queryPosition:
				c.positionReply <- pos
			}
			log.Printf("new direction %v", direction)
		// end::select[]
		case <-nextMove:
			pos = pos.Add(direction)
			nextMove = time.After(updateInterval)
			log.Printf("moved to %v", pos)
		}
	}
}

// Left turns the rover left (90° anticlockwise).
func (r *RoverDriver) Left() {
	r.commandc <- command{kind: left}
}

// Right turns the rover right (90° clockwise).
func (r *RoverDriver) Right() {
	r.commandc <- command{kind: right}
}

// tag::positionmethod[]
// Position returns the rover's current position.
func (r *RoverDriver) Position() image.Point {
	reply := make(chan image.Point)
	r.commandc <- command{
		kind:          queryPosition,
		positionReply: reply,
	}
	return <-reply
}

// end::positionmethod[]
