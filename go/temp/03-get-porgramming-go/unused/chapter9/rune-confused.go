package main

import (
	"fmt"
	"strconv"
)

func main() {
	score := 128533

	str := "Score " + string(score)
	fmt.Println(str) //<1>

	str = "Score " + strconv.Itoa(score) //<1>
	fmt.Println(str)                     //<2>

	str = fmt.Sprintf("Score %v", score)
	fmt.Println(str) //<1>
}

// <1> Score 😕

// <1> Itoa is short for integer to ASCII
// <2> Score 128533
