package main

import "fmt"

func main() {
	age := 40
	newAge := float64(age)

	marsDays := 687.0
	earthDays := 365.2425

	fmt.Println("I am", newAge*earthDays/marsDays, "years old on Mars.") //<1>

	fmt.Println(int(earthDays))
	fmt.Println("I am", age*int(earthDays)/int(marsDays), "years old on Mars.") //<2>

	var v uint8 = 255
	fmt.Println(int8(v))
}

// <1> I am 21.265938864628822 years old on Mars.
// <2> I am 21 years old on Mars.
