package main

import "fmt"

func main() {
	type Celsius float64

	const degrees = 20
	var temperature Celsius = degrees

	temperature += 10

	var warming float64 = 10
	temperature += Celsius(warming)

	fmt.Println(temperature)
}
