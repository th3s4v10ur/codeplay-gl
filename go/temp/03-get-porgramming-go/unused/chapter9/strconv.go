package main

import (
	"fmt"
	"strconv"
)

func main() {
	score, err := strconv.Atoi("128533") //<1>
	if err != nil {
		// oh oh, something went wrong
	}
	fmt.Println(score) //<2>
}

// <1> Atoi is short for ASCII to integer
// <2> 128533
