package main

import "fmt"

func main() {
	type Celsius float64
	type Fahrenheit float64

	var t1 Celsius = 21.0
	var t2 Fahrenheit = 68.0

	t1 += 8
	fmt.Println(t1, t2)
}
