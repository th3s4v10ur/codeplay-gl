package main

import "fmt"

func main() {
	var greeting interface{} = "Hello Universe"
	greeting = 42

	var num interface{} = 42
	num = 3.14

	fmt.Println(greeting, num)
}
