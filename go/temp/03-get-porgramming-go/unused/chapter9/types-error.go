package main

import "fmt"

func main() {
	// countdown := "Launch in T minus " + 10 + " seconds."

	age := 38                                                         //<1>
	marsDays := 687                                                   //<1>
	earthDays := 365.2425                                             //<2>
	fmt.Println("I am", age*earthDays/marsDays, "years old on Mars.") //<3>
}

// <1> Age and marsDays are integers
// <2> earthDays is floating point
// <3> The Go compiler gives a mismatched types error
