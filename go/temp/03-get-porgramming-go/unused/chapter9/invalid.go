package main

func main() {
	var greeting = "Hello Universe"
	greeting = 42 //<1>

	var num = 42
	num = 3.14 //<2>
}
