package main

import (
	"fmt"
	"math"
)

func main() {
	var word uint64 = 'o'*256 + 'G'
	fmt.Printf("%016b\n", word)
	fmt.Println(word)
	fmt.Println(math.Float64frombits(word))
}
