package main

func main() {
	temperature := 21.0          //<1>
	var temperature = 21.0       //<1>
	var temperature float64 = 21 //<2>
}

// <1> The decimal point is necessary for Go to infer a float64
// <2> A whole number is fine when the type is explicitly specified
