package main

import "fmt"

func main() {
	celsius := 21.0
	fahrenheit := (9 / 5 * celsius) + 32 //<1>
	fmt.Print(fahrenheit, "ºF")
}

// <1> 53ºF rather than 69.8ºF
