package main

import "fmt"

func main() {
	marsDays := 687.0
	earthDays := 365.2425
	fmt.Println("I am", 38*earthDays/marsDays, "years old on Mars.") //<1>
}

// <1> I am 21.265938864628822 years old on Mars.
