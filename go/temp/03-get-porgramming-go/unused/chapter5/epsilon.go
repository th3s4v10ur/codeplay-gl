package main

import (
	"fmt"
	"math"
)

// https://en.wikipedia.org/wiki/Machine_epsilon
var (
	Epsilon64 = math.Pow(2, -52)
	Epsilon32 = math.Pow(2, -23)
)

func main() {
	piggyBank := 0.10
	piggyBank += 0.20
	fmt.Println(piggyBank) //<1>

	fmt.Println(piggyBank == 0.30)                 //<2>
	fmt.Println(math.Abs(piggyBank-0.30) < 0.0001) //<3>

	Epsilon64 := math.Pow(2, -52)
	fmt.Println(math.Abs(piggyBank-0.30) < Epsilon64) //<3>

	fmt.Println(0.10+0.20 == 0.30) // true in Go 1.5+

	piggyBank = 0
	cents := 0
	for {
		piggyBank += 0.10
		cents += 10
		fmt.Println(piggyBank, cents)
		if math.Abs(piggyBank-(float64(cents)/100)) >= Epsilon64 {
			break
		}
	}
}

// <1> 0.30000000000000004
// <2> false
// <3> true
