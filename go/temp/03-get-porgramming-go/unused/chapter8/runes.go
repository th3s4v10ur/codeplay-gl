package main

import "fmt"

func main() {
	str := "Estación Espacial Internacional"

	for i, c := range str { //<1>
		_ = i //<2>
		fmt.Printf("%c ", c)
	}
}
