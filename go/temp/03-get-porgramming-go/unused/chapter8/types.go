package main

import "fmt"

func main() {
	fmt.Println("I am", 38*365/687, "years old on Mars.")       //<1>
	fmt.Println("I am", 38.0*365.0/687.0, "years old on Mars.") //<2>
}
