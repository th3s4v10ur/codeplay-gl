package main

import "fmt"

func main() {
	grade := 'A'
	var grade = 'A'
	var grade rune = 'A'

	fmt.Println(grade)
	fmt.Printf("%c\n", grade)

	var star byte = '*'
	fmt.Printf("%c %d\n", star, star)
}
