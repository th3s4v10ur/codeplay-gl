package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var num = rand.Intn(3)

	switch {
	case num == 0:
		fmt.Println("Space Adventures")
	case num == 1:
		fmt.Println("SpaceX")
	case num == 2:
		fmt.Println("Virgin Galactic")
	default:
		fmt.Println("This was not foreseen")
	}
}
