package main

import "fmt"

func main() {
	// Andromeda Galaxy is 24 quintillion kilometers away
	var distance uint64 = 24e18 // constant 24000000000000000000 overflows uint64
	fmt.Println("Andromeda Galaxy is", distance, "km away.")

	// Andromeda Galaxy is 24 quintillion kilometers away
	const distance uint64 = 24000000000000000000 // constant 24000000000000000000 overflows uint64
}
