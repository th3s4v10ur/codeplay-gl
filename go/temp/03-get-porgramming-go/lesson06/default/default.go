package main

import "fmt"

func main() {
	// tag::main[]
	var price float64
	fmt.Println(price) // <1>
	// end::main[]
}
