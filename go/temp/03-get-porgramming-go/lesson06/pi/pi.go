package main

import (
	"fmt"
	"math"
)

func main() {
	// tag::main[]
	var pi64 = math.Pi
	var pi32 float32 = math.Pi

	fmt.Println(pi64) //<1>
	fmt.Println(pi32) //<2>
	// end::main[]
}
