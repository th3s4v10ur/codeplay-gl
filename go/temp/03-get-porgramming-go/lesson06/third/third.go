package main

import "fmt"

func main() {
	// tag::main[]
	third := 1.0 / 3
	fmt.Println(third)           //<1>
	fmt.Printf("%v\n", third)    //<1>
	fmt.Printf("%f\n", third)    //<2>
	fmt.Printf("%.3f\n", third)  //<3>
	fmt.Printf("%4.2f\n", third) //<4>
	// end::main[]
	// tag::zero[]
	fmt.Printf("%05.2f\n", third) //<1>
	// end::zero[]
}
