package main

import "fmt"

func main() {
	// tag::main[]
	third := 1.0 / 3
	fmt.Printf("%f\n", third)     // <1>
	fmt.Printf("%7.4f\n", third)  // <2>
	fmt.Printf("%06.2f\n", third) // <3>
	// end::main[]
}
