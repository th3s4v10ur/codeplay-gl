package main

import "fmt"

func main() {
	// tag::main[]

	var haveTorch = true
	var litTorch = false

	if !haveTorch || !litTorch {
		fmt.Println("Nothing to see here.") //<1>
	}
	// end::main[]
}
