package main

import "fmt"

func main() {
	var command = "go east"

	if command == "go east" { //<1>
		fmt.Println("You head further up the mountain.")
	} else if command == "go inside" { //<2>
		fmt.Println("You enter the cave where you live out the rest of your life.")
	} else { //<3>
		fmt.Println("Didn't quite get that.")
	}
}
