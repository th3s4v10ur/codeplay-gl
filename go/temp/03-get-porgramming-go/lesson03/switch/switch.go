package main

import "fmt"

func main() {
	// tag::main[]
	var room = "lake"

	switch { //<1>
	case room == "cave":
		fmt.Println("You find yourself in a dimly lit cavern.")
	case room == "lake":
		fmt.Println("The ice seems solid enough.")
		fallthrough //<2>
	case room == "underwater":
		fmt.Println("The water is freezing cold.")
	}
	// end::main[]
}
