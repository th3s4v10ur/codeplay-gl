package main

import (
	"fmt"
	"time"
)

func main() {
	var count = 10 //<1>

	for count > 0 { //<2>
		fmt.Println(count)
		time.Sleep(time.Second)
		count-- //<3>
	}
	fmt.Println("Liftoff!")
}
