package main

import "fmt"

func main() {
	// tag::main[]
	const distance = 24000000000000000000
	const lightSpeed = 299792
	const secondsPerDay = 86400

	const days = distance / lightSpeed / secondsPerDay

	fmt.Println("Andromeda Galaxy is", days, "light days away.") //<1>
	// end::main[]

	// tag::literal[]
	fmt.Println("Andromeda Galaxy is", 24000000000000000000/299792/86400, "light days away.") //<1>
	// end::literal[]
}
