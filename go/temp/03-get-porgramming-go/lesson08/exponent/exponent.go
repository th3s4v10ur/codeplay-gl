package main

import "fmt"

func main() {
	// tag::main[]
	var distance int = 56e6
	distance = 401e6
	// end::main[]

	fmt.Println(distance) //<1>
}
