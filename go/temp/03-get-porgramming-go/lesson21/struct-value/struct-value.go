package main

import "fmt"

func main() {
	type location struct {
		lat  float64
		long float64
	}

	// tag::main[]
	bradbury := location{-4.5895, 137.4417}
	curiosity := bradbury

	curiosity.long += 0.0106 //<1>

	fmt.Println(bradbury, curiosity) //<2>
	// end::main[]
}
