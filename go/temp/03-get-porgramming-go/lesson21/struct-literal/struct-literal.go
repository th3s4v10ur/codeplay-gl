package main

import "fmt"

func main() {
	// tag::opportunity[]
	type location struct {
		lat, long float64
	}

	opportunity := location{lat: -1.9462, long: 354.4734}
	fmt.Println(opportunity) //<1>

	insight := location{lat: 4.5, long: 135.9}
	fmt.Println(insight) //<2>
	// end::opportunity[]

	// tag::spirit[]
	spirit := location{-14.5684, 175.472636}
	fmt.Println(spirit) //<1>
	// end::spirit[]

	// tag::fmtv[]
	curiosity := location{-4.5895, 137.4417}

	fmt.Printf("%v\n", curiosity)  //<1>
	fmt.Printf("%+v\n", curiosity) //<2>
	// end::fmtv[]
}
