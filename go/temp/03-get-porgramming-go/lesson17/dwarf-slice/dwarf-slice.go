package main

import "fmt"

func main() {
	// tag::main[]
	dwarfArray := [...]string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	dwarfSlice := dwarfArray[:]
	// end::main[]

	// tag::shortcut[]
	dwarfs := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	// end::shortcut[]

	fmt.Println(dwarfSlice, dwarfs)

	// tag::type[]
	fmt.Printf("array %T\n", dwarfArray) // <1>
	fmt.Printf("slice %T\n", dwarfs)     // <2>
	// end::type[]
}
