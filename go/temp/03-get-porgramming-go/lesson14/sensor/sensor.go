package main

import (
	"fmt"
	"math/rand"
)

type kelvin float64

func fakeSensor() kelvin {
	return kelvin(rand.Intn(151) + 150)
}

func realSensor() kelvin {
	return 0 //<1>
}

func main() {
	sensor := fakeSensor //<2>
	fmt.Println(sensor())

	sensor = realSensor
	fmt.Println(sensor())
}
