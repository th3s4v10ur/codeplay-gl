package main

import "fmt"

func main() {
	f := func(message string) { //<1>
		fmt.Println(message)
	}
	f("Go to the party.") //<2>
}
