package main

import "fmt"

var f = func() { //<1>
	fmt.Println("Dress up for the masquerade.")
}

func main() {
	f() //<2>
}
