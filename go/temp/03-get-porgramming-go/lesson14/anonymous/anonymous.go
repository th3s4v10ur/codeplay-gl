package main

import "fmt"

func main() {
	func() { //<1>
		fmt.Println("Functions anonymous")
	}() //<2>
}
