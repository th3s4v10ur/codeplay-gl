package main

import (
	"fmt"
	"time"
)

func main() {
	go worker()
	time.Sleep(5 * time.Second)
}

// tag::main[]
func worker() {
	n := 0
	next := time.After(time.Second) // <1>
	for {
		select {
		case <-next: // <2>
			n++
			fmt.Println(n)                 // <3>
			next = time.After(time.Second) // <4>
		}
	}
}

// end::main[]
