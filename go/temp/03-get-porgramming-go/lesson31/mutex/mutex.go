package main

import "sync" // <1>

var mu sync.Mutex // <2>

func main() {
	mu.Lock()         // <3>
	defer mu.Unlock() // <4>
	// The lock is held until we return from the function.
}
