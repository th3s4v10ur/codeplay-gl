package main

import (
	"image"
	"log"
	"time"
)

// tag::main[]
func main() {
	r := NewRoverDriver()
	time.Sleep(3 * time.Second)
	r.Left()
	time.Sleep(3 * time.Second)
	r.Right()
	time.Sleep(3 * time.Second)
}

// end::main[]

// tag::type[]

// RoverDriver drives a rover around the surface of Mars.
type RoverDriver struct {
	commandc chan command
}

// end::type[]

// NewRoverDriver starts a new RoverDriver and returns it.
// tag::create[]
func NewRoverDriver() *RoverDriver {
	r := &RoverDriver{
		commandc: make(chan command),
	}
	go r.drive()
	return r
}

// end::create[]

// tag::command[]
type command int

const (
	right = command(0)
	left  = command(1)
)

// end::command[]

// tag::drive[]
// drive is responsible for driving the rover. It
// is expected to be started in a goroutine.
func (r *RoverDriver) drive() {
	pos := image.Point{X: 0, Y: 0}
	direction := image.Point{X: 1, Y: 0}
	updateInterval := 250 * time.Millisecond
	nextMove := time.After(updateInterval)
	for {
		select {
		case c := <-r.commandc: // <1>
			switch c {
			case right: // <2>
				direction = image.Point{
					X: -direction.Y,
					Y: direction.X,
				}
			case left: // <3>
				direction = image.Point{
					X: direction.Y,
					Y: -direction.X,
				}
			}
			log.Printf("new direction %v", direction)
		case <-nextMove:
			pos = pos.Add(direction)
			log.Printf("moved to %v", pos)
			nextMove = time.After(updateInterval)
		}
	}
}

// end::drive[]

// tag::methods[]

// Left turns the rover left (90° anticlockwise).
func (r *RoverDriver) Left() {
	r.commandc <- left
}

// Right turns the rover right (90° clockwise).
func (r *RoverDriver) Right() {
	r.commandc <- right
}

// end::methods[]
