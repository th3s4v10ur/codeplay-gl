package main

import "sync"

// tag::type[]

// Visited records whether web pages have been visited.
// Its methods may be used concurrently with one another.
type Visited struct {
	// mu guards the visited map.
	mu      sync.Mutex     // <1>
	visited map[string]int // <2>
}

// end::type[]

// tag::methods[]

// VisitLink records that the page with the given URL has
// been visited, and returns the updated link count.
func (v *Visited) VisitLink(url string) int {
	v.mu.Lock()         //<1>
	defer v.mu.Unlock() //<2>
	count := v.visited[url]
	count++
	v.visited[url] = count // <3>
	return count
}

// end::methods[]

func main() {
}
