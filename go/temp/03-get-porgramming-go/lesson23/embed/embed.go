package main

import "fmt"

// tag::report[]
type report struct {
	sol         int
	temperature //<1>
	location
}

// end::report[]

type temperature struct {
	high, low celsius
}

type location struct {
	lat, long float64
}

type celsius float64

func (t temperature) average() celsius {
	return (t.high + t.low) / 2
}

func main() {
	// tag::method[]
	report := report{
		sol:         15,
		location:    location{-4.5895, 137.4417},
		temperature: temperature{high: -1.0, low: -78.0},
	}

	fmt.Printf("average %vºC\n", report.average()) //<1>
	// end::method[]

	// tag::name[]
	fmt.Printf("average %vºC\n", report.temperature.average()) //<1>
	// end::name[]

	// tag::field[]
	fmt.Printf("%vºC\n", report.high) //<1>
	report.high = 32
	fmt.Printf("%vºC\n", report.temperature.high) //<2>
	// end::field[]
}
