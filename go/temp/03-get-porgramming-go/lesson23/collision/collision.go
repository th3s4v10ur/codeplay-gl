package main

import "fmt"

type sol int

type report struct {
	sol
	location
	temperature
}

type temperature struct {
	high, low celsius
}

type location struct {
	lat, long float64
}

type celsius float64

func (s sol) days(s2 sol) int {
	days := int(s2 - s)
	if days < 0 {
		days = -days
	}
	return days
}

// tag::location[]
func (l location) days(l2 location) int {
	// TODO: complicated distance calculation //<1>
	return 5
}

// end::location[]

// tag::override[]
func (r report) days(s2 sol) int {
	return r.sol.days(s2)
}

// end::override[]

func main() {
	// tag::main[]
	report := report{sol: 15}
	d := report.days(1446) // <1>
	// end::main[]

	fmt.Println(d)
}
