package main

import "fmt"

type report struct {
	sol         int
	location    location
	temperature temperature
}

type temperature struct {
	high, low celsius
}

type location struct {
	lat, long float64
}

type celsius float64

// tag::average[]
func (t temperature) average() celsius {
	return (t.high + t.low) / 2
}

// end::average[]

// tag::forward[]
func (r report) average() celsius {
	return r.temperature.average()
}

// end::forward[]

func main() {
	// tag::main[]
	t := temperature{high: -1.0, low: -78.0}
	fmt.Printf("average %vºC\n", t.average()) //<1>
	// end::main[]

	// tag::chain[]
	report := report{sol: 15, temperature: t}
	fmt.Printf("average %vºC\n", report.temperature.average()) //<1>
	// end::chain[]

	fmt.Printf("average %vºC\n", report.average()) //<1>
}
