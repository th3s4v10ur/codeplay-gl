package main

import "fmt"

// tag::types[]
type report struct {
	sol         int
	temperature temperature //<1>
	location    location
}

type temperature struct {
	high, low celsius
}

type location struct {
	lat, long float64
}

type celsius float64

// end::types[]

func main() {
	// tag::main[]
	bradbury := location{-4.5895, 137.4417}
	t := temperature{high: -1.0, low: -78.0}
	report := report{sol: 15, temperature: t, location: bradbury}

	fmt.Printf("%+v\n", report) //<1>

	fmt.Printf("a balmy %vºC\n", report.temperature.high) //<2>
	// end::main[]
}
