package main

import "fmt"

// tag::report[]
type sol int

type report struct {
	sol
	location
	temperature
}

// end::report[]

type temperature struct {
	high, low celsius
}

type location struct {
	lat, long float64
}

type celsius float64

// tag::days[]

func (s sol) days(s2 sol) int {
	days := int(s2 - s)
	if days < 0 {
		days = -days
	}
	return days
}

func main() {
	report := report{sol: 15}

	fmt.Println(report.sol.days(1446)) // <1>
	fmt.Println(report.days(1446))     // <1>
}

// end::days[]
