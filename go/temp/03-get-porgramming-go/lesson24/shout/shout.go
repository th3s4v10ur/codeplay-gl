package main

import (
	"fmt"
	"strings"
)

type martian struct{}

func (m martian) talk() string {
	return "nack nack"
}

type laser int

func (l laser) talk() string {
	return strings.Repeat("pew ", int(l))
}

// tag::type[]
type talker interface {
	talk() string
}

// end::type[]

// tag::shout[]
func shout(t talker) {
	louder := strings.ToUpper(t.talk())
	fmt.Println(louder)
}

// end::shout[]

func main() {
	// tag::main[]
	shout(martian{}) // <1>
	shout(laser(2))  // <2>
	// end::main[]

	type crater struct{}
	// crater does not implement talker (missing talk method)
	// shout(crater{})
}
