package main

import (
	"fmt"
	"time"
)

// tag::interface[]
type stardater interface {
	YearDay() int
	Hour() int
}

// stardate returns a fictional measure of time.
func stardate(t stardater) float64 {
	doy := float64(t.YearDay())
	h := float64(t.Hour()) / 24.0
	return 1000 + doy + h
}

// end::interface[]

// tag::sol[]
type sol int

func (s sol) YearDay() int {
	return int(s % 668) //<1>
}

func (s sol) Hour() int {
	return 0 //<2>
}

// end::sol[]

func main() {
	// tag::main[]
	day := time.Date(2012, 8, 6, 5, 17, 0, 0, time.UTC)
	fmt.Printf("%.1f Curiosity has landed\n", stardate(day)) //<1>

	s := sol(1422)
	fmt.Printf("%.1f Happy birthday\n", stardate(s)) //<2>
	// end::main[]
}
