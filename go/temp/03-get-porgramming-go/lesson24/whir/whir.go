package main

import (
	"fmt"
	"strings"
)

type talker interface {
	talk() string
}

func shout(t talker) {
	louder := strings.ToUpper(t.talk())
	fmt.Println(louder)
}

type laser int

// tag::laser[]
func (l laser) talk() string {
	return strings.Repeat("toot ", int(l))
}

// end::laser[]

// tag::rover[]
type rover string

func (r rover) talk() string {
	return string(r)
}

func main() {
	r := rover("whir whir")
	shout(r) //<1>
}

// end::rover[]
