package main

import (
	"fmt"
	"strings"
)

// tag::talk[]
type martian struct{}

func (m martian) talk() string {
	return "nack nack"
}

type laser int

func (l laser) talk() string {
	return strings.Repeat("pew ", int(l))
}

// end::talk[]

func main() {
	// tag::main[]
	// tag::interface[]
	var t interface {
		talk() string
	}
	// end::interface[]

	t = martian{}
	fmt.Println(t.talk()) // <1>

	t = laser(3)
	fmt.Println(t.talk()) // <2>
	// end::main[]
}
