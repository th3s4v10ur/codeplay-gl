package main

import (
	"fmt"
	"time"
)

// tag::whole[]
func main() {
	c := make(chan int) //<1>
	for i := 0; i < 5; i++ {
		go sleepyGopher(i, c)
	}
	for i := 0; i < 5; i++ {
		gopherID := <-c //<2>
		fmt.Println("gopher ", gopherID, " has finished sleeping")
	}
}

func sleepyGopher(id int, c chan int) { //<3>
	time.Sleep(3 * time.Second)
	fmt.Println("... ", id, " snore ...")
	c <- id //<4>
}

// end::whole[]
