package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	c := make(chan int)
	for i := 0; i < 5; i++ {
		go sleepyGopher(i, c)
	}
	// tag::select[]
	timeout := time.After(2 * time.Second)
	for i := 0; i < 5; i++ {
		select { //<1>
		case gopherID := <-c: //<2>
			fmt.Println("gopher ", gopherID, " has finished sleeping")
		case <-timeout: //<3>
			fmt.Println("my patience ran out")
			return //<4>
		}
	}
	// end::select[]
}

// tag::sleepy[]
func sleepyGopher(id int, c chan int) { //<3>
	time.Sleep(time.Duration(rand.Intn(4000)) * time.Millisecond)
	c <- id //<4>
}

// end::sleepy[]
