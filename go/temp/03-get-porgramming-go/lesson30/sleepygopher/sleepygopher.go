package main

import (
	"fmt"
	"time"
)

func main() {
	go sleepyGopher()           //<1>
	time.Sleep(4 * time.Second) //<2>
} //<3>

func sleepyGopher() {
	time.Sleep(3 * time.Second) //<4>
	fmt.Println("... snore ...")
}
