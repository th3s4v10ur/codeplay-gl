package main

import "fmt"

func main() {
	// tag::main[]
	fmt.Println("My weight on the surface of Mars is", 149.0*0.3783, "lbs, and I would be", 41*365.2425/687, "years old.") //<1>
	// end::main[]
}
