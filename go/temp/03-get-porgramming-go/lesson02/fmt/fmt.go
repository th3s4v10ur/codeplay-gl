package main

import "fmt"

func main() {
	// tag::simple[]
	fmt.Printf("My weight on the surface of Mars is %v lbs,", 149.0*0.3783) //<1>
	fmt.Printf(" and I would be %v years old.\n", 41*365/687)               //<2>
	// end::simple[]

	// tag::multiple[]
	fmt.Printf("My weight on the surface of %v is %v lbs.\n", "Earth", 149.0) //<1>
	// end::multiple[]

	// tag::width[]
	fmt.Printf("%-15v $%4v\n", "SpaceX", 94)
	fmt.Printf("%-15v $%4v\n", "Virgin Galactic", 100)
	// end::width[]
}
