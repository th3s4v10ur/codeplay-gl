// My weight loss program. //<1>
package main

import "fmt"

// main is the function where it all begins. //<1>
func main() {
	fmt.Print("My weight on the surface of Mars is ")
	fmt.Print(149.0 * 0.3783) //<2>
	fmt.Print(" lbs, and I would be ")
	fmt.Print(41 * 365 / 687) //<3>
	fmt.Print(" years old.")
}
