package main

import "fmt"

func main() {
	// tag::main[]
	var count = 0

	for count = 10; count > 0; count-- {
		fmt.Println(count)
	}

	fmt.Println(count) // <1>
	// end::main[]
}
