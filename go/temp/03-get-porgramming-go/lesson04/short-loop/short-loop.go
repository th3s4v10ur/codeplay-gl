package main

import "fmt"

func main() {
	// tag::main[]
	for count := 10; count > 0; count-- {
		fmt.Println(count)
	} // <1>
	// end::main[]
}
