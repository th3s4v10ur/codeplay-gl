package main

import "fmt"

func main() {
	// tag::main[]
	a := "text"
	fmt.Printf("Type %T for %[1]v.\n", a) // <1>

	b := 42
	fmt.Printf("Type %T for %[1]v.\n", b) // <2>

	c := 3.14
	fmt.Printf("Type %T for %[1]v.\n", c) // <3>

	d := true
	fmt.Printf("Type %T for %[1]v.\n", d) // <4>
	// end::main[]
}
