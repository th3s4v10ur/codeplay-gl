package main

import "fmt"

func main() {
	// tag::main[]
	year := 2018
	fmt.Printf("Type %T for %v.\n", year, year) //<1>
	// end::main[]

	// tag::indexes[]
	days := 365.2425
	fmt.Printf("Type %T for %[1]v.\n", days) //<1>
	// end::indexes[]
}
