package main

func main() {
	// tag::main[]
	type celsius float64

	const degrees = 20
	var temperature celsius = degrees

	temperature += 10
	// end::main[]
}
