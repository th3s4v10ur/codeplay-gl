package main

import "fmt"

func main() {
	// tag::main[]
	type celsius float64 //<1>

	var temperature celsius = 20

	fmt.Println(temperature)
	// end::main[]
}
