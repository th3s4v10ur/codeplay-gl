package main

import "fmt"

type celsius float64
type kelvin float64

// tag::func[]
func celsiusToKelvin(c celsius) kelvin {
	return kelvin(c + 273.15)
}

func main() {
	var c celsius = 127.0
	k := celsiusToKelvin(c)
	fmt.Print(c, "ºC is ", k, "ºK") // <1>
}

// end::func[]
