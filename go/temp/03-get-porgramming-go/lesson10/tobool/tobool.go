package main

import "fmt"

func main() {
	// tag::main[]
	yesNo := "no"

	launch := (yesNo == "yes")
	fmt.Println("Ready for launch:", launch) //<1>
	// end::main[]
}
