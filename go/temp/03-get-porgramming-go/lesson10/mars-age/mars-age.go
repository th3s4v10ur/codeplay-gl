package main

import "fmt"

func main() {
	// tag::main[]
	// tag::convert[]
	age := 41
	marsAge := float64(age)
	// end::convert[]

	marsDays := 687.0
	earthDays := 365.2425
	marsAge = marsAge * earthDays / marsDays
	fmt.Println("I am", marsAge, "years old on Mars.") //<1>
	// end::main[]
}
