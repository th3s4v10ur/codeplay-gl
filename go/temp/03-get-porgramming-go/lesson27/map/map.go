package main

import "fmt"

func main() {
	// tag::main[]
	var soup map[string]int
	fmt.Println(soup == nil) //<1>

	measurement, ok := soup["onion"]
	if ok {
		fmt.Println(measurement)
	}

	for ingredient, measurement := range soup {
		fmt.Println(ingredient, measurement)
	}
	// end::main[]
}
