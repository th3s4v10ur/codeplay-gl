package main

import "fmt"

// tag::all[]
type number struct {
	value int
	valid bool
}

func newNumber(v int) number {
	return number{value: v, valid: true}
}

func (n number) String() string {
	if !n.valid {
		return "not set"
	}
	return fmt.Sprintf("%d", n.value)
}

func main() {
	n := newNumber(42)
	fmt.Println(n) //<1>

	e := number{}
	fmt.Println(e) //<2>
}

// end::all[]
