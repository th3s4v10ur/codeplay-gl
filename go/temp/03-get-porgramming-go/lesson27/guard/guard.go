package main

type person struct {
	age int
}

// tag::birthday[]
func (p *person) birthday() {
	if p == nil {
		return
	}
	p.age++
}

// end::birthday[]

func main() {
	var nobody *person
	nobody.birthday()
}
