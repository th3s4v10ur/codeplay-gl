package main

import "fmt"

func main() {
	// tag::main[]
	var soup []string
	fmt.Println(soup == nil) //<1>

	for _, ingredient := range soup {
		fmt.Println(ingredient)
	}

	fmt.Println(len(soup)) //<2>

	soup = append(soup, "onion", "carrot", "celery")
	fmt.Println(soup) //<3>
	// end::main[]

	t := make([]string, 0, 0)
	fmt.Println(t == nil)
}
