package main

import "fmt"

func main() {
	// tag::nil[]
	var v interface{}
	fmt.Printf("%T %v %v\n", v, v, v == nil) // <1>
	// end::nil[]

	// tag::wat[]
	var p *int
	v = p
	fmt.Printf("%T %v %v\n", v, v, v == nil) // <1>
	// end::wat[]

	// tag::inspect[]
	fmt.Printf("%#v\n", v) // <1>
	// end::inspect[]
}
