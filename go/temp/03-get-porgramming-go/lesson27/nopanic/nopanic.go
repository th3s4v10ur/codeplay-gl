package main

import "fmt"

func main() {
	// tag::main[]
	var nowhere *int

	if nowhere != nil {
		fmt.Println(*nowhere)
	}
	// end::main[]
}
