package main

import "fmt"

func main() {
	// tag::main[]
	var fn func(a, b int) int
	fmt.Println(fn == nil) // <1>
	// end::main[]

	// fn(1, 2)
}
