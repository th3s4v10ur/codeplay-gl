package main

import "fmt"

func main() {
	// tag::main[]
	var nowhere *int
	fmt.Println(nowhere) //<1>
	// end::main[]

	// fmt.Println(*nowhere)
}
