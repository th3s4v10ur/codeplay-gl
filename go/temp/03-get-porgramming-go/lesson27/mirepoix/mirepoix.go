package main

import "fmt"

// tag::all[]
func main() {
	soup := mirepoix(nil)
	fmt.Println(soup) //<1>
}

func mirepoix(ingredients []string) []string {
	return append(ingredients, "onion", "carrot", "celery")
}

// end::all[]
