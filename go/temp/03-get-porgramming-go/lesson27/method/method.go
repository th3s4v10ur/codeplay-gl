package main

import "fmt"

// tag::all[]
type person struct {
	age int
}

func (p *person) birthday() {
	p.age++ // <1>
}

func main() {
	var nobody *person
	fmt.Println(nobody) // <2>

	nobody.birthday()
}

// end::all[]
