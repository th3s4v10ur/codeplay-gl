package main

import "fmt"

func main() {
	// tag::main[]
	c := 'g'
	c = c - 'a' + 'A'
	fmt.Printf("%c", c) //<1>
	// end::main[]
}
