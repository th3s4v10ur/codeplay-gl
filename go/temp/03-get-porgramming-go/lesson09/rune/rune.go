package main

import "fmt"

func main() {
	// tag::main[]
	var pi rune = 960
	var alpha rune = 940
	var omega rune = 969
	var bang byte = 33

	fmt.Printf("%v %v %v %v\n", pi, alpha, omega, bang) //<1>
	// end::main[]

	// tag::verb[]
	fmt.Printf("%c%c%c%c\n", pi, alpha, omega, bang) //<1>
	// end::verb[]
}
