package main

import "fmt"

func main() {
	// tag::main[]
	fmt.Println(`
		peace be upon you
		upon you be peace`)
	// end::main[]
}
