package main

import "fmt"

func main() {
	// tag::main[]
	message := "shalom"
	c := message[5]
	fmt.Printf("%c\n", c) //<1>
	// end::main[]
}
