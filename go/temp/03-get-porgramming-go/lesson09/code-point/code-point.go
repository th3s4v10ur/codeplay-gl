package main

import "fmt"

func main() {
	// tag::main[]
	var star byte = '*'
	fmt.Printf("%c %[1]v\n", star) // <1>

	smile := '😃'
	fmt.Printf("%c %[1]v\n", smile) // <2>

	acute := 'é'
	fmt.Printf("%c %[1]v\n", acute) // <3>
	// end::main[]
}
