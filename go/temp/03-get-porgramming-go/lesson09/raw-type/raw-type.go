package main

import "fmt"

func main() {
	// tag::main[]
	fmt.Printf("%v is a %[1]T\n", "literal string")     // <1>
	fmt.Printf("%v is a %[1]T\n", `raw literal string`) // <2>
	// end::main[]
}
