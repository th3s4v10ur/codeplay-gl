package main

import (
	"fmt"
	"net/url"
	"os"
)

func main() {
	// tag::main[]
	u, err := url.Parse("http://a b.com/")

	if err != nil {
		fmt.Println(err)         // <1>
		fmt.Printf("%#v\n", err) // <2>

		if e, ok := err.(*url.Error); ok {
			fmt.Println("Op:", e.Op)   // <3>
			fmt.Println("URL:", e.URL) // <4>
			fmt.Println("Err:", e.Err) // <5>
		}
		os.Exit(1)
	}

	fmt.Println(u)
	// end::main[]
}
