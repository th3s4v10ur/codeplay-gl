package main

import "fmt"

func main() {
	// tag::main[]
	temperature := map[string]int{
		"Earth": 15, // <1>
		"Mars":  -65,
	}

	temp := temperature["Earth"]
	fmt.Printf("On average the Earth is %vºC.\n", temp) //<2>

	temperature["Earth"] = 16 //<3>
	temperature["Venus"] = 464

	fmt.Println(temperature) //<4>
	// end::main[]

	// tag::moon[]
	moon := temperature["Moon"]
	fmt.Println(moon) //<1>
	// end::moon[]

	// tag::moonok[]
	if moon, ok := temperature["Moon"]; ok { // <1>
		fmt.Printf("On average the moon is %vºC.\n", moon)
	} else {
		fmt.Println("Where is the moon?") //<2>
	}
	// end::moonok[]

	// tag::delete[]
	_, ok := temperature["Earth"]
	fmt.Println(ok) //<1>

	delete(temperature, "Earth") //<2>
	// end::delete[]
}
