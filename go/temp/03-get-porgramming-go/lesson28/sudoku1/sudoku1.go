package main

import (
	"errors"
	"fmt"
	"os"
)

// tag::grid[]
const rows, columns = 9, 9

// Grid is a Sudoku grid
type Grid [rows][columns]int8

// end::grid[]

// Set a digit on a Sudoku grid
// tag::set[]
func (g *Grid) Set(row, column int, digit int8) error {
	if !inBounds(row, column) {
		return errors.New("out of bounds")
	}

	g[row][column] = digit
	return nil
}

// end::set[]

// tag::inbounds[]
func inBounds(row, column int) bool {
	if row < 0 || row >= rows {
		return false
	}
	if column < 0 || column >= columns {
		return false
	}
	return true
}

// end::inbounds[]

// tag::main[]
func main() {
	var g Grid
	err := g.Set(10, 0, 5)
	if err != nil {
		fmt.Printf("An error occurred: %v.\n", err)
		os.Exit(1)
	}
}

// end::main[]
