package main

import (
	"fmt"
	"os"
)

// tag::wisdom[]
func wisdom(name string) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}

	_, err = fmt.Fprintln(f, "The road to wisdom? - Well, it's plain")
	if err != nil {
		f.Close()
		return err
	}

	_, err = fmt.Fprintln(f, "and simple to express:")
	f.Close()
	return err
}

// end::wisdom[]

func main() {
	// tag::main[]
	err := wisdom("wisdom.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// end::main[]
}
