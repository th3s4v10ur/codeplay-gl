package main

import "fmt"

func main() {
	// tag::main[]
	defer func() {
		if e := recover(); e != nil { // <1>
			fmt.Println(e) // <2>
		}
	}()

	panic("I forgot my towel") // <3>
	// end::main[]
}
