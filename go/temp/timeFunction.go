package main

import "fmt"
import "time"
import "strconv"

func getUnixTime() int {
    return int(time.Now().Unix())
}

func getUnixNanoTime() int64 {
    ntime := time.Now().UnixNano()
    return ntime % 1e6 / 1e3
}

func setSleep(sleepTime int) {
    fmt.Println("Sleep start ...")
    time.Sleep(time.Duration(sleepTime) * time.Second)
    fmt.Println("Sleep finish ...")
}

func setUnix2Fomat(unixTime int64) string {
    dt := time.Unix(unixTime, 0)
    y := dt.Year()
    m := int(dt.Month())
    d := dt.Day()
    h := dt.Hour()
    i := dt.Minute()
    s := dt.Second()
    return strconv.Itoa(y) + "/" + strconv.Itoa(m) + "/" + strconv.Itoa(d) + " " + strconv.Itoa(h) + ":" + strconv.Itoa(i) + ":" + strconv.Itoa(s)
}

func getYmdhis2Unixtime(y int, m int, d int, h int, i int, s int) int64 {
    t := time.Date(y, time.Month(m), d, h, i, s, 0, time.Local)
    return t.Unix()
}

func main() {

    // 1
    tmStart := getUnixTime()
    fmt.Println(tmStart)

    // 2
    setSleep(3)

    // 3
    dateString := setUnix2Fomat(int64(tmStart))
    fmt.Println(dateString)

    // 4
    tmFormat := getYmdhis2Unixtime(1970, 1, 1, 9, 0, 1)
    fmt.Println(tmFormat)

    // 5
    fmt.Println(getUnixNanoTime())

    // 6
    tmEnd := getUnixTime()
    fmt.Println(tmEnd-tmStart, "s")
}

/*
Commentary
1. sleep-time
Since an error occurs with an int value, it must be set to time.Duration (**)
here

Default

time.Duration(sleepTime)
1
time.Duration(sleepTime)


2. time.Sleep () is a nanosecond processing
It is better to multiply time.Second

Default

time.Sleep(time.Duration(sleepTime) * time.Second)
1
time.Sleep(time.Duration(sleepTime) * time.Second)
3. Month () somehow returns as a string
Originally written as follows, I want you to return the numerical value of that month ...

Default

time.Now().Month()
> May
1
2
time.Now().Month()
> May
What is this ...
who is pleased with this value very subtle! !
By converting the int to say that it returns to the number ...

Default

int(time.Now().Month())
> 5
1
2
int(time.Now().Month())
> 5
This is difficult to understand, but for the time being, it seems to be a theory in the Go language acquisition date.

4. How to make time type from numerical values ​​such as year, month, day etc.
Syntax
Year, month, day, hour, minute, second, nanosecond, TimeZone

Default

t := time.Date( y , time.Month(m) , d , h , i , s , 0 , time.Local)
1
t := time.Date( y , time.Month(m) , d , h , i , s , 0 , time.Local)
There are some discomfort, but since nanoseconds can not imagine much other than putting 0, is it necessary to specify honestly? I think that this is specification and divisions,
timezone is impressed with well thought specs,
again the moon's place is like a new comedy punchy style.
Let's divide this into specifications as well.
※So I will not forget! ! !

5. How to calculate microseconds
Default

time.Now().UnixNano() % 1e6 / 1e3
1
time.Now().UnixNano() % 1e6 / 1e3
You can get microsecond by truncating the last 6 digits and obtaining the last 3 digits.
I honestly have 9 digits to be honest, so this is enough


*/
