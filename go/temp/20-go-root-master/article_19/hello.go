// Seriál "Programovací jazyk Go"
//
// Devatenáctá část
//     Využití WebAssembly z programovacího jazyka Go
//     https://www.root.cz/clanky/vyuziti-webassembly-z-programovaciho-jazyka-go/
//
// Demonstrační příklad:
//     Typický program typu "Hello world"

package main

import "fmt"

func main() {
	fmt.Println("Hello World!")
}
