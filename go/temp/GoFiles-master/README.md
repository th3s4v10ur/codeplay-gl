# knowGo
了解go语言
go语言是一个静态语言，特点就是好学好用，一般像好学好用的语言大多数人都愿意去学习😄
通常来讲写第一个语言的时候就是要用hello world来写。我觉得go和js还是有很多相似之处的

## 详情可以看[具体介绍](https://github.com/ThomasHuke/knowGo/blob/master/doc/basic.md)

1. 简单好学
2. 函数式编程写法。

## 举个例子

```go

package main

import (
	"errors"   
	"net/http"

	"github.com/go-chi/chi"
)

// Handler 是一个func类型。
type Handler func(w http.ResponseWriter, r *http.Request) error

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
    //这里是为了设定“版权"
	w.Header().Set("Author", "ThomasHuke[github.com/ThomasHuke]")
	w.Header().Set("Powred-by", "jago[github.com/go-jago]")
	if err := h(w, r); err != nil {
		w.WriteHeader(503)
		w.Write([]byte("网站遭遇服务器错误，http状态码503，coastroad给您带来了不便，在此我表示道歉。"))
	}
}

func main() {
	r := chi.NewRouter()
	r.Method("GET", "/", Handler(f))
	http.ListenAndServe(":8080", r)
}
func f(w http.ResponseWriter, r *http.Request) error {
	if e := r.URL.Query().Get("err"); e != "" {
		return errors.New(e)
	}
	w.Write([]byte("<h1>1212</h1>"))
	return nil

}

```

```go
package main

import "fmt"

func sum(c, d, fault chan int) {
	x, y := 1, 2
	for {// 设置for的意思是要不断的执行这段程序 select 并没有这个功能。
	// select 监听管道里是否有数据产生，你可以把channel类似为Unix中的双向管道，或者你可以类似为一个可以往两边同行的管道。
		select {//监听中
		case c <- x:// 将x 这个数据传递给c
			x, y = y, x+y
		case d <- y:
			x, y = y, x*y
		case <-fault:
			return // 记得要关闭这个for循环。
		}
	}
}

func main() {
	c := make(chan int)// 必须通过make来进行生成chan
	d := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 3; i++ {
			// 将c 传递给 某这里 没有接受者 直接传递给 prinln即可
			fmt.Println(<-c, <-d)

		}
		quit <- 0
	}() // 记得go 开一个新的栈 ，所以函数先这行的是sum再执行的是 go 后面 的匿名函数

	sum(c, d, quit)
}
```



` 这是一个关于 context和go的并行并发和channel通信的一个案例。`

```go

package main
import (
	"context"
	"log"
	"os"
	"time"
)

var logg *log.Logger

func someHandler() {
	ctx, cancel := context.WithCancel(context.Background())
	go doStuff(ctx)
	go doStuff(ctx)
	go doStuff(ctx)
	go doStuff(ctx)
	//10秒后取消doStuff
	time.Sleep(10 * time.Second)
	cancel()

}

//每1秒work一下，同时会判断ctx是否被取消了，如果是就退出
func doStuff(ctx context.Context) {
	for {
		time.Sleep(1 * time.Second)
		select {
		case <-ctx.Done():
			logg.Printf("done")
			return
		default:
			logg.Printf("work")
		}
	}
}

func main() {
	logg = log.New(os.Stdout, "", log.Ltime)
	someHandler()
	logg.Printf("down")
}

```

### 这个案例就是如何解决form问题
```go

package main

import (
	"net/http"
)

var a = `
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>CoastRoad</title>
</head>
<body>
<form method="post" action="/log">
<input type="text" name="username">
<input type="submit" value="提交">
</form>
</body>
</html>
`

func main() {
	http.HandleFunc("/", func(arg1 http.ResponseWriter, arg2 *http.Request) {
		arg1.Header().Add("Content-Type", "text/html")
		arg1.Header().Add("charset", "utf-8")
		arg1.Write([]byte(a))
	})
	http.HandleFunc("/log", func(arg1 http.ResponseWriter, arg2 *http.Request) {
		arg1.Header().Add("Content-Type", "text/html")
		arg1.Header().Add("charset", "utf-8")
		arg2.ParseForm()
		if arg2.Method == "method" {
			arg1.Write([]byte("看这是我给你显示的数据"))

		} else {
			arg1.Write([]byte("wait something~!~!~!"))
			arg1.Write([]byte(`<head><meta charset="utf-8"/></head><body><h1>您的网页会在5秒钟后返回主页面。</h1></body>`))
			arg1.Write([]byte(`<head><meta charset="utf-8"/></head><body><h1>这有些内容提供给您，请您查收： 或者您可以点击<a href="/">home</a>来快速返回主页面</h1></body>`))

			arg1.Write([]byte(`<html><head><script>
				(()=>{
					setTimeout(()=>{
						location.href="/"
					},5000)
				})()
				</script></head><body></body></html>`))
		}
	})

	http.ListenAndServe(":8080", nil)
}

```
2. 函数式编程写法。

## 关于我

[ME](https://github.com/ThomasHuke/me)

## 捐赠


[donate](https://github.com/thomashuke/donate)
