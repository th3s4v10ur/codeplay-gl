module github.com/pathbox/learning-go

require (
	github.com/OneOfOne/xxhash v1.2.2
	github.com/arriqaaq/qalam v0.0.0-20180728065844-48c478013079
	github.com/arriqaaq/rbt v0.0.0-20180905140721-59445effa8a7
	github.com/arriqaaq/xring v0.0.0-20180905140838-c1b1ea24864d
	github.com/coreos/etcd v3.3.9+incompatible
	github.com/coreos/go-semver v0.2.0 // indirect
	github.com/d4l3k/go-pry v0.0.0-20180929012351-0d2718e73ed8
	github.com/go-redis/redis v6.14.1+incompatible
	github.com/gongo/9t v0.0.0-20180922145652-fc98dd15970b // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2 // indirect
	github.com/gorilla/pat v0.0.0-20180118222023-199c85a7f6d1
	github.com/hpcloud/tail v1.0.0 // indirect
	github.com/jasonlvhit/gocron v0.0.0-20180312192515-54194c9749d4
	github.com/jinzhu/gorm v1.9.1 // indirect
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/lestrrat-go/strftime v0.0.0-20180821113735-8b31f9c59b0f
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mattn/go-runewidth v0.0.3 // indirect
	github.com/mattn/go-tty v0.0.0-20180907095812-13ff1204f104 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/panjf2000/ants v0.0.0-20181003125539-711dbdb7a222
	github.com/pkg/errors v0.8.0 // indirect
	github.com/shirou/gopsutil v2.17.12+incompatible
	github.com/ugorji/go/codec v0.0.0-20180927125128-99ea80c8b19a // indirect
	golang.org/x/crypto v0.0.0-20181001203147-e3636079e1a4
	golang.org/x/net v0.0.0-20180926154720-4dfa2610cdf3
	golang.org/x/sys v0.0.0-20180928133829-e4b3c5e90611 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
