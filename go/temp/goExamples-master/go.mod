module github/masterZSH/goExamples

go 1.13

require (
	github.com/Shopify/sarama v1.26.4
	github.com/gorilla/websocket v1.4.2
	github.com/micro/examples v0.2.0
	github.com/micro/go-micro/v2 v2.9.0
	github.com/sirupsen/logrus v1.6.0
	github.com/streadway/amqp v1.0.0
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	google.golang.org/genproto v0.0.0-20200709005830-7a2ca40e9dc3 // indirect
	google.golang.org/grpc v1.30.0
	google.golang.org/grpc/examples v0.0.0-20200709232328-d8193ee9cc3e // indirect
	google.golang.org/grpc/security/advancedtls v0.0.0-20200709232328-d8193ee9cc3e // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
