# Golang
## Golang basic example codes.
- Types
- Variables
- Control Structures
- If-Else Conditional Statements
- Switch Case
  * fallthrough
- Nested Conditional Statements
- For, While Loop
  * Break
  * Pass
  * Continue
- Arrays
- Slice
- Map
- Basic Functions
  * Variadic Function
  * Make Function
  * Panic Function
  * Defer Function
  * Recover Function
  * Recursion Function
  * Closure Function
- Pointers(*-->asterisk,&-->ampersand)
- Structs and Interfaces
  * Structs
  * Methods
  * Interfaces
- Concurrency
  * Goroutines
  * Channels
- Packages
- Core Packages
  * Strings
  * Input / Output
  * Files & Folders
  * Errors
## GO & OOP
 * Methods
 * Package Oriented design
 * Type embedding
 * Interfaces
- Encapsulation
- Message passing
- Inheritance
- Composition
- Polymorphism
