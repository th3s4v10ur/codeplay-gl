package globals

// Samples used in a small go tutorial
//
// Copyright (C) 2017,2018 framp at linux-tips-and-tricks dot de
//
// Samples for go - simple trafficlight simulation using go channels and go routines
//
// See github.com/framps/golang_tutorial for latest code

var (
	// Debug - enable debug message
	Debug bool
	// Monitor - write preudo LEDs on monitor
	Monitor bool
	// EnableLEDs - Trigger real LEDs connected
	EnableLEDs bool
)
