package main

import (
	"bytes"
	"fmt"
)

func main() {

	var InputString string
	var RepetitionFactor int

	fmt.Println("Enter input string")
	_, err := fmt.Scanln(&InputString)
	if err != nil {
		fmt.Println("oops there was an error!")
		return
	}

	fmt.Println("Enter number of times you want to repeat that string")
	_, err = fmt.Scanf("%d", &RepetitionFactor)

	if err != nil {
		fmt.Println("oops there was an error! May be entered value is not an integer")
	}

	var buffer bytes.Buffer

	for i := 0; i < RepetitionFactor; i++ {
		buffer.WriteString(InputString)
	}

	fmt.Println(buffer.String())
}
