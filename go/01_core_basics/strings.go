package main

import "fmt"

func main() {
	// Object Formatting
	//Sample struct that we will use

	type Student struct {
		ID           int64
		Name         string
		Grade        int
		CurrentScore float32
	}

	student := Student{
		ID:           50,
		Name:         "John Smith",
		Grade:        5,
		CurrentScore: 3.8,
	}

	//Print the Struct

	result := fmt.Sprintf("%v", student)

	fmt.Printf(result)
	// Output: {50 John Smith 5 3.8}

	//Print the Struct With Field Names

	result1 := fmt.Sprintf("%+v", student)
	fmt.Printf(result1)
	// Output:{Id:50 Name:John Smith Grade:5 CurrentScore:3.8}

	//Print the Type of the Value
	result2 := fmt.Sprintf("%T", student)
	fmt.Printf(result2)
	//Output: main.Student

	// Print the Type and the Actual Value
	result3 := fmt.Sprintf("%#v", student)
	fmt.Printf(result3)
	// Output: main.Student{Id:50, Name:"John Smith", Grade:5, CurrentScore:3.8}

	//String Formatting

	sample := "I love Golang"
	sampleresult := fmt.Sprintf("Sample is : %s", sample)
	fmt.Printf(sampleresult)
	// Output : Sample is : I love Golang!

}
