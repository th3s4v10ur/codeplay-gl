/*
Every package should have a package comment, a block comment immediately preceding the
package statement, introducing the package and provide information relevant to the package and
its functionality as a whole. A package can be spread over many files, but the comment needs to
be in only one of them. This comment is shown when a developer demands info of the package
with godoc . Subsequent sentences and/or paragraphs can give more details. Sentences should be
properly punctuated.
*/

// Package superman implements methods for saving the world.
//
// Experience has shown that a small number of procedures can prove
// helpful when attempting to save the world.
package comment
// enterOrbit causes Comment to fly into low Earth orbit, a position
// that presents several possibilities for planet salvation.
func enterOrbit() error {
...
}
