package main

import "fmt" //Single line comments - fmt package

/*
A multi-line or block-comment starts with
and ends with
nesting is not allowed; this is used
for making package documentation and commenting out code.
*/

func main() {

	fmt.Printf("Comments")
	fmt.Printf("test")
}
