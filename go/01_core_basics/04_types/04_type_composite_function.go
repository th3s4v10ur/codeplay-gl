// go run go-function.go
package main

import "fmt"

type newFunc func(int, int) int

func main() {
	var a = 1
	var b = 2
	fs := []newFunc{
		func(a int, b int) int { return a + b },
		func(a int, b int) int { return a - b },
		func(a int, b int) int { return a * b },
		func(a int, b int) int { return a / b },
	}
	var fn = fs[0]
	fmt.Println("a + b = ", fn(a, b))
	fn = fs[1]
	fmt.Println("a - b = ", fn(a, b))
	fn = fs[2]
	fmt.Println("a * b = ", fn(a, b))
	fn = fs[3]
	fmt.Println("a / b = ", fn(a, b))
}
