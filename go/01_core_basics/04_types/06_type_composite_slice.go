// go run go-slice.go
package main

import "fmt"

func main() {
	slice1 := make([]int, 3)
	slice1[0] = 2
	slice2 := slice1[0:2]
	fmt.Println("slice1[0]", slice1[0])
	fmt.Println("slice2[0]", slice2[0])
	slice1[0] = 3
	fmt.Println("After modification\nslice1[0]", slice1[0])
	fmt.Println("slice2[0]", slice2[0])
}
