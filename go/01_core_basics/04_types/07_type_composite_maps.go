// go run go-map.go
package main

import "fmt"

func main() {
	m := make(map[int]string)
	m[1] = "hello"
	m[42] = "world"
	fmt.Println(m[1], m[42])
}
