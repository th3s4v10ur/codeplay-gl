// go run go-pointer.go
package main

import "fmt"

func print_int_pointer(p *int) {
	fmt.Println("The value of the pointer is", *p)
}

func main() {
	i := 42
	print_int_pointer(&i)
}
