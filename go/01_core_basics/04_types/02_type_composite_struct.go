// go run go-struct.go
package main

import "fmt"

type banana struct {
	foo int
	bar string
}

func main() {
	ba := banana{foo: 1, bar: "hello"}
	fmt.Println("ba's foo is", ba.foo)
	fmt.Println("ba's bar is", ba.bar)
}
