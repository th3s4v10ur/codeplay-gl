// go run go-array.go
package main

import "fmt"

func main() {
	var a [10]int
	fmt.Println("array: ", a)

	fmt.Println("a[0] is ", a[0])
	a[0] = 10 // set the a[0] to 10
	fmt.Println("a[0] is ", a[0])
}
