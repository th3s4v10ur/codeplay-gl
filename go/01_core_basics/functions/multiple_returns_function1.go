package main

import (
	"fmt"
)

func Names() (string, string) { //#1
	return "Foo", "Bar" //#2
}
func main() {
	n1, n2 := Names()   //#3
	fmt.Println(n1, n2) //#3
	n3, _ := Names()    //#4
	fmt.Println(n3)
}
