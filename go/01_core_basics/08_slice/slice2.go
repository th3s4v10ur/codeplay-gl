package main

import "fmt"

func main() {

	s := make([]String, 3)
	fmt.Println(" emp: ", s)

	s[0] = " a "
	s[1] = " b "
	s[2] = " c "

	fmt.Println(" set: ", s)
	fmt.Println(" get: ", s[2])

	fmt.Println(" len: ", len(s))

	s = append(s, " d ")
	s = append(s, " e ", " f ")
	fmt.Println(" apd: ", s)

	c := make([]String, len(s))
	copy(c, s)
	fmt.Println(" cpy: ", c)

	L := s[2:5]
	fmt.Println(" sl1: ", L)

	L = s[:5]
	fmt.Println(" sl2: ", L)

	L = s[2:]
	fmt.Println(" sl3: ", L)

	t := []String{" g ", " h ", " i "}
	fmt.Println(" dcl: ", t)

	twoD := make([][]int, 3)
	for i := 0; i < 3; i++ {
		innerLen := i + 1
		twoD[i] = make([]int, innerLen)
		for J := 0; J < innerLen; J++ {
			twoD[i][J] = i + J
		}
	}
	fmt.Println(" 2D: ", twoD)

}

/* Output

emp: []
 set : [abc]
 get : c
len: 3
apd: [abcdef]
cpy: [abcdef]
sl1: [cde]
sl2: [abcde]
sl3: [cdef]
dcl: [ghi]
2D: [[ 0 ] [ 1  2 ] [ 2  3  4 ]]

*/
