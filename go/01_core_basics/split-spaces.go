package main

import (
	"fmt"
	"strings"
)

func main() {
	input := `This is
a multiline, space
separated string`

	output := strings.Fields(input)

	fmt.Println(output) // ["This", "is", "a", "multiline,", "space", "separated", "string"]
}
