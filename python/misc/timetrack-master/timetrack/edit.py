from gettext import gettext as _
from gi.repository import Gtk, Gdk

from datetime import datetime, timedelta
from timetrack.activity import Activity


class EditDialog(Gtk.Dialog):
    activity_name = NotImplemented
    start_date = NotImplemented
    start_hour = NotImplemented
    start_minute = NotImplemented

    working_hour = NotImplemented
    working_minute = NotImplemented

    db = None
    activity_id = None
    main_window = None

    def __init__(self, db, main_window, *args, **kwargs):
        self.db = db
        self.main_window = main_window
        super().__init__(*args, use_header_bar=True, **kwargs)

        builder = Gtk.Builder()
        builder.add_from_resource("/org/gnome/timetrack/edit_dialog.ui")

        self.activity_name = builder.get_object("edit-activity")
        self.start_date = builder.get_object("edit-calendar")
        self.start_hour = builder.get_object("edit-hour")
        self.start_minute = builder.get_object("edit-minute")

        self.working_hour = builder.get_object("edit-working-hour")
        self.working_minute = builder.get_object("edit-working-minute")
        box = builder.get_object("edit-box")

        box.show_all()
        self.get_content_area().add(box)
        self.show_all()

        header = self.get_header_bar()
        close = Gtk.Button(label=_("Close"))
        close.connect("clicked", lambda x: self.response(0))
        header.pack_start(close)
        save = Gtk.Button(label=_("Save"))
        save.connect("clicked", lambda x: self.response(1))
        header.pack_end(save)
        header.show_all()

        self.set_deletable(False)
        self.set_transient_for(main_window)
        self.set_attached_to(main_window)
        self.set_destroy_with_parent(True)
        self.set_type_hint(Gdk.WindowTypeHint.DIALOG)
        self.set_modal(True)

    def open(self, aid=None):
        self.activity_id = aid
        if self.activity_id:
            act = self.db.get(self.activity_id)

            self.activity_name.set_text(act.name)
            self.start_date.set_property("day", act.start.day)
            self.start_date.set_property("month", act.start.month - 1)
            self.start_date.set_property("year", act.start.year)

            self.start_hour.set_value(act.start.hour)
            self.start_minute.set_value(act.start.minute)

            h, m, _s = act.full_time
            self.working_hour.set_value(h)
            self.working_minute.set_value(m)

        else:
            now = datetime.now()
            self.activity_name.set_text("")
            self.start_date.set_property("day", now.day)
            self.start_date.set_property("month", now.month - 1)
            self.start_date.set_property("year", now.year)

            self.start_hour.set_value(now.hour)
            self.start_minute.set_value(now.minute)

            self.working_hour.set_value(1)
            self.working_minute.set_value(0)

        resp = self.run()
        if resp != 1:
            self.close()
            return

        return self.save()

    def save(self):
        name = self.activity_name.get_text()
        date = self.start_date.get_date()

        start_h = self.start_hour.get_value()
        start_m = self.start_minute.get_value()

        start_date = datetime(date.year, date.month + 1, date.day,
                              int(start_h), int(start_m))

        working_h = self.working_hour.get_value()
        working_m = self.working_minute.get_value()
        working_s = (int(working_h) * 60 + int(working_m)) * 60

        stop_date = start_date + timedelta(seconds=working_s)
        activity = Activity(id=self.activity_id,
                            name=name,
                            start=start_date,
                            stop=stop_date)

        aid = self.db.update(activity)
        activity.id = aid

        self.close()

        return activity
        
