from typing import Optional
from gi.repository import Gtk
from datetime import datetime
from dataclasses import dataclass


@dataclass
class Activity:
    name: str
    start: datetime
    stop: datetime
    id: Optional[int]

    @property
    def seconds(self):
        if not self.stop:
            return 0
        return (self.stop - self.start).seconds

    @property
    def full_time(self):
        if not self.stop:
            return (0, 0, 0)

        m = self.seconds // 60
        s = self.seconds % 60
        h = m // 60
        m = m % 60

        return (h, m, s)
        

class ActivityWidget(Gtk.Box):
    label = NotImplemented
    time_label = NotImplemented
    start_label = NotImplemented
    activity = NotImplemented

    activity_id = None

    def __init__(self, act):
        super().__init__(orientation=Gtk.Orientation.VERTICAL)
        self.activity = act
        if act.id:
            self.activity_id = act.id

        box1 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        box2 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        self.label = Gtk.Label(label=act.name)
        self.label.set_xalign(0.0)

        self.time_label = Gtk.Label(label=self.time())
        self.time_label.set_xalign(1.0)

        box1.pack_start(self.label, True, True, 6)
        box1.pack_start(self.time_label, False, False, 6)

        self.start_label = Gtk.Label(label=self.activity.start.strftime("%c"))
        self.start_label.set_xalign(0.0)
        context = self.start_label.get_style_context()
        context.add_class("dim-label")

        box2.pack_start(self.start_label, True, True, 6)

        self.add(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL))
        self.pack_start(box2, False, False, 6)
        self.add(box1)

        self.show_all()

        self.set_size_request(-1, 80)

    def update(self, act):
        self.activity = act
        if act.id:
            self.activity_id = act.id
        self.label.set_text(act.name)
        self.time_label.set_text(self.time())
        self.start_label.set_text(self.activity.start.strftime("%c"))

    @property
    def seconds(self):
        return self.activity.seconds

    def time(self):
        return "{:02d}:{:02d}:{:02d}".format(*self.activity.full_time)
