## Update pot files from source files:
cd po
intltool-update --maintain
cd ..
find timetrack -iname "*.py" | xargs xgettext --from-code=UTF-8 --output=po/timetrack-python.pot
find data -iname "*.ui" | xargs xgettext --from-code=UTF-8 --output=po/timetrack-glade.pot -L Glade
msgcat --use-first po/timetrack-glade.pot po/timetrack-python.pot > po/timetrack.pot
rm po/timetrack-glade.pot po/timetrack-python.pot

## Generate po file for language
cd po
msginit --locale=xx --input=timetrack.pot

