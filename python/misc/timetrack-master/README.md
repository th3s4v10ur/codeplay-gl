# Timetrack

Simple timetrack app for GNOME

![Screenshot1](https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack6.png)
![Screenshot2](https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack4.png)
![Screenshot3](https://gitlab.gnome.org/danigm/timetrack/raw/master/screenshots/timetrack1.png)
