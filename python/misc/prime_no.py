import threading
import sys
import time


def prime(num):
    primeNums = []
for n in range(2, num):
    #threading.Thread(target=print, args=('\rCurrent number: %d' % n), kwargs={'end': ''}).start()
print('\rCurrent number: %d' % n, end='')
for x in range(2, n):
    if n % x == 0:
        break
else:
    primeNums.append(n)
print()
return primeNums
start_time = time.time()
prime(int(sys.argv[1]))
print("--- Done. Took %s seconds. -" % (time.time() - start_time))
