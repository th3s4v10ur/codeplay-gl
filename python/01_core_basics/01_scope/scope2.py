# Local versus Global


def local():
    # m doesn't belong to the scope defined by the local function
    # so Python will keep looking into next enclosing scope
    print(m, "Printing from local scope")


m = 5

print(m, "printing from global scope")

local()
